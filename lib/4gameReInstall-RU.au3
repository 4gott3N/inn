;Script for install or reinstall 4game application
;arg1 - install file location - absolute path
;arg2 - uninstall file location - absolute path

If WinExists(@ScriptName) Then Exit
AutoItWinSetTitle(@ScriptName)
AutoItSetOption("TrayIconDebug", 1)

global $title1="�������� 4game", $title2="��������� ���������� ������" , $title3="���������� ���������� ������" 
ConsoleWrite("[AutoIt]4game application install script started" & @CRLF)

global $var = RegRead("HKEY_LOCAL_MACHINE\SOFTWARE\Innova Systems\4game", "Version")
 ConsoleWrite("Version of application is "&$var & @CRLF)
  If isVarIsNull($var) Then
   Run ($CmdLine[1],"", @SW_MAXIMIZE)
   ;Run ("D:\Downloads\4game_setup-ru.exe","", @SW_MAXIMIZE)
   ConsoleWrite("[AutoIt]Starting 4game installer" & @CRLF)
   AdlibRegister("Activate1", 50)
   WinWait($title2, "��������� ����������")
   controlclick($title2, "", "����������")
   ConsoleWrite("[AutoIt]Button INSTALL was clicked" & @CRLF)
   WinWait($title2, "���������� �����������")
   ConsoleWrite("[AutoIt]Installation complete" & @CRLF)
   WinClose($title2)
   ConsoleWrite("[AutoIt]Installer script execution finished" & @CRLF)
  
Else
   ;If FileExists("C:\Program Files\4game\"&$var&"\uninstall.exe") Then ;checking - if 4game application already installed
   Run ($CmdLine[1],"", @SW_MAXIMIZE)
   ConsoleWrite("[AutoIt]Old 4game installation detected, starting update" & @CRLF)
   ;Run ("C:\Program Files\4game\"&$var&"\uninstall.exe","", @SW_MAXIMIZE)
   ;Run ("C:\Program Files (x86)\4game\uninstall.exe","", @SW_MAXIMIZE)
   ;ConsoleWrite("[AutoIt]Starting 4game uninstall exe file" & @CRLF)
   ;AdlibRegister("Activate", 50)
   ;WinWait($title1, "�������")
   ;ConsoleWrite("[AutoIt]Button Delete was clicked" & @CRLF)
   ;Send("{Enter}")
   ;controlclick($title1, "", "����� >")
   ;ConsoleWrite("[AutoIt]Button Forward was clicked" & @CRLF)
   ;WinWaitClose($title1)
   ;ConsoleWrite("[AutoIt]Uninstall complete" & @CRLF)

   AdlibRegister("Activate3", 50)
   WinWait($title3, "��������")
   controlclick($title3, "", "��������")
   ConsoleWrite("[AutoIt]Button Update was clicked" & @CRLF)
   WinWait($title3, "���������� ������ �������")
   ConsoleWrite("[AutoIt]Update complete" & @CRLF)
   WinClose($title3)
   ConsoleWrite("[AutoIt]Updater script execution finished" & @CRLF)
EndIf

Func Activate()
    If Not WinActive($title1) Then
        WinActivate($title1)
    EndIf
 EndFunc 
 
 Func Activate1()
    If Not WinActive($title2) Then
        WinActivate($title2)
    EndIf
 EndFunc 
  Func Activate3()
    If Not WinActive($title3) Then
        WinActivate($title3)
    EndIf
 EndFunc 
 
 Func isVarIsNull ( $_Var )
    If Not $_Var Then 
        Return True 
    Else
        Return False
    EndIf
EndFunc 