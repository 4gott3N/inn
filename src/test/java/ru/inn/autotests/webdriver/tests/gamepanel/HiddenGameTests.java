package ru.inn.autotests.webdriver.tests.gamepanel;

import org.junit.AfterClass;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import ru.inn.autotests.webdriver.common.PrepareUser;
import ru.inn.autotests.webdriver.common.entity.Message;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.composite.pages.GamePanelPage;
import ru.inn.autotests.webdriver.composite.pages.LoginPage;
import ru.inn.autotests.webdriver.composite.pages.SpecialPages;
import ru.inn.autotests.webdriver.composite.pages.UserBarPage;
import ru.inn.autotests.webdriver.composite.popups.PopupFinalFormalities;
import ru.inn.autotests.webdriver.games.Aion;
import ru.inn.autotests.webdriver.games.Game;
import ru.inn.autotests.webdriver.games.Lineage2;
import ru.inn.autotests.webdriver.games.Lineage2eu;
import ru.inn.autotests.webdriver.games.PlanetSide2;
import ru.inn.autotests.webdriver.games.XXX;
import ru.inn.autotests.webdriver.tests.AbstractTest;
import ru.inn.autotests.webdriver.tests.AbstractTest.TypeUser;
import ru.inn.autotests.webdriver.toolkit.GameData;
import ru.inn.autotests.webdriver.toolkit.MailProvider;
import ru.inn.autotests.webdriver.toolkit.PlatformUtils;
import ru.inn.autotests.webdriver.toolkit.UserData;



public class HiddenGameTests extends AbstractTest {

    PrepareUser prepareUser = new PrepareUser();
    Game gameRu = new Aion();
	
    @Test(description = "test to check that user can launch hidden game", enabled = true, groups = { "hidden" })
    public void testUserCantSeeHiddenGame() {
        Game game = new XXX();
        log4j.info("test to check that user can launch hidden game " + game.getGameName());
        User user = switcherLocation(TypeUser.USER);
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openUrlIn4game("aion/play");
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        Assert.assertTrue(gpp.validateElementIsNotVisible(UserBarPage.getGameNavBarLink(game)), 
        		"The user USER can see an icon for hidden game, but mustn't");
        
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        Assert.assertTrue(gpp.validateElementVisible(SpecialPages.CLASS_BODY_OF_404), 
        		"The user USER can see a hidden game page, but mustn't");
        
    }
    
    @Test(description = "test to check that user can launch hidden game", enabled = true, groups = { "hidden" })
    public void testPlayHiddenGame() {
        Game game = new XXX();
        log4j.info("test to check that user can launch hidden game " + game.getGameName());
        User user = switcherLocation(TypeUser.HARDCORE_USER);
        prepareUser.setLastAuthTime(user, game.getServiceId(), OperationsHelper.getChangedDate(-1));
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openUrlIn4game("");
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.getGameNavBarLink(game)), 
        		"The superuser HARDCORE_USER can't see an icon for hidden game.");
        gpp.waitToPlayHiddenGame(false);
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_PLAY),
                "Button 'Play' is not Visible " + game.getAppName());
        gpp.click(GamePanelPage.BUTTON_PLAY);
        gpp.clickPopupIfItPresent(game);
        Assert.assertTrue(PlatformUtils.detectGameProcess(game, 30),
                "Game " + game.getGameName() + " process wasn't launched");
        PlatformUtils.killGame(game);
    }


	@Test(dependsOnGroups={ "hidden" })
    public void testChangeEnv() {
    	log4j.info("test to check that user can switch game env " + randomGame.getGameName());
    	GamePanelPage gpp = new GamePanelPage(game, language);
    	gpp.openUrlIn4game(randomGame.getURL() + "play");
    	gpp.changeGameEnv("live");
    	
    }
    
    @Test(description = "test to check that user can switch game env")
    public void testZ() {
    	log4j.info("test to check that user can switch game env " + gameRu.getGameName());
    	GamePanelPage gpp = new GamePanelPage(gameRu, language);
    	gpp.openUrlIn4game(gameRu.getURL() + "play");
    	gpp.changeGameEnv("live");
    	
    }

}
