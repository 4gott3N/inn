package ru.inn.autotests.webdriver.tests.playinstall;


import org.testng.Assert;
import org.testng.annotations.Test;
import ru.inn.autotests.webdriver.composite.pages.MainPage;
import ru.inn.autotests.webdriver.tests.AbstractTest;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Sergey.Kashapov
 * Date: 29.04.13
 * Time: 15:52
 * To change this template use File | Settings | File Templates.
 */
public class VideoTests extends AbstractTest {

    @Test
    public void testVideoMainPage() throws InterruptedException, IOException {
       MainPage mainPage=new MainPage();
        mainPage.openPage(mainPage);
        mainPage.click(MainPage.VIDEO_SPAN);
        mainPage.validateElementPresent(MainPage.IFRAME_VIDEO);
        Assert.assertTrue(mainPage.checkResourceByGetRequest(mainPage.getSrcOfElement(MainPage.IFRAME_VIDEO)));
    }
}
