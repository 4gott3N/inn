package ru.inn.autotests.webdriver.tests.playpage;

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.inn.autotests.webdriver.common.PrepareUser;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.composite.pages.*;
import ru.inn.autotests.webdriver.games.Game;
import ru.inn.autotests.webdriver.games.PointBlank;
import ru.inn.autotests.webdriver.tests.AbstractTest;
import ru.inn.autotests.webdriver.toolkit.LocalDriverManager;


public class PlayPageTests extends AbstractTest {

    @Test(description = "test to check the user who has a personage in the PB has ability to play in Shooting")
    public void testShooting() {
        log4j.info("test to check the user who has a personage in the PB has ability to play in Shooting");
        User user = switcherLocation(TypeUser.HARDCORE_USER);
        PrepareUser prepareUser = new PrepareUser();
        //prepareUser.addMoneyByBackend(user, "10");
        Game game = new PointBlank();
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openPage(gpp);
        LocalDriverManager.getDriverController().deleteAllCookies();
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized");
        PlayPage pp = new PlayPage(game);
        Assert.assertTrue(lp.validateElementVisible(PlayPage.LINK_SHOOTING),
                "There is no link to the SHOOTING ()on the pb page");
        pp.click(PlayPage.LINK_SHOOTING);
        pp.validateElementVisible(PlayPage.DIV_PRELOADER_SHOOTING);
        pp.validateElementIsNotVisible(PlayPage.DIV_PRELOADER_SHOOTING);
        /* Assert.assertTrue(lp.validateElementVisible(PlayPage.BUTTON_SHOOT),
                "There is BUTTON_SHOOT and It is disable " + pp.getCurrentUrl());
        Assert.assertTrue(lp.validateElementVisible(PlayPage.MESSAGE_NO_PERSONAGE), "Shooting is enabled");
        Assert.assertTrue(lp.validateElementVisible(PlayPage.BUTTON_SHOOT), "There is no BUTTON_SHOOT  on the SHOOTING page or It is invisible " + pp.getCurrentUrl());
    	double priceShooting = pp.getNumber(PlayPage.BUTTON_SHOOT);
    	pp.click(PlayPage.BUTTON_SHOOT);
    	gpp.clickPopupIfItPresent(game);
    	Assert.assertTrue(lp.validateElementIsNotVisible(PlayPage.BUTTON_SHOOT), "The BUTTON_SHOOT is still visible after the clicking on it");
		ub.assertUserSum(oldMoney, -priceShooting);
    	/*int i=0;
    	while(pp.validateElementIsNotVisible(PlayPage.FORM_PRIZE)){
    		pp.sendPause(10+i);
    		Assert.assertTrue(lp.validateElementIsNotVisible(PlayPage.BUTTON_SHOOT), "The BUTTON_SHOOT is still visible after the clicking on it");
    		ub.assertUserSum(oldMoney, -priceShooting);
    		pp.mouseClick();
    		i+=1;
    		pp.makeScreenshot("shooting " + i);
    		Assert.assertTrue(i<5, "The shooting doesn't work");
    	}*/

    }


    @Test(description = "test to check the new user does not have ability to play in Shooting")
    public void testNewUserShooting() {
        log4j.info("test to check the new user does not have ability to play in Shooting");
        User user = switcherLocation(TypeUser.USER);
        Game game = new PointBlank();
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openPage(gpp);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized");
        UserBarPage ub = new UserBarPage();
        user.setMoney(ub.getCurrentUserMoney());
        PlayPage pp = new PlayPage(game);
        Assert.assertTrue(lp.validateElementVisible(PlayPage.LINK_SHOOTING),
                "There is no link to the SHOOTING ()on the pb page");
        pp.click(PlayPage.LINK_SHOOTING);
        pp.validateElementIsNotVisible(PlayPage.DIV_PRELOADER_SHOOTING);
        Assert.assertTrue(lp.validateElementIsNotVisible(PlayPage.BUTTON_SHOOT),
                "There is a BUTTON_SHOOT and It is visible for user without a char " + pp.getCurrentUrl());
        Assert.assertTrue(lp.validateElementIsNotVisible(PlayPage.MESSAGE_NO_PERSONAGE),
                "New user without chars can't see a message MESSAGE_NO_PERSONAGE");

    }


    @Test(description = "test to check that redirecting to PS2 wiki is works", enabled=false)
    public void testCheckRedirectPS2Wiki() {
        log4j.info("test to check that redirecting to PS2 wiki is works");
        SpecialPages sp = new SpecialPages();
        sp.openUrlIn4game("planetside2/play/error/?code=G17");
        Assert.assertTrue(sp.validateElementIsNotVisible(SpecialPages.CLASS_BODY_OF_404),
                "There is a 404 page, redirecting to PS2 doesn't work");
        Assert.assertTrue(sp.getCurrentUrl().contains("ru.4gamesupport.com/kb/articles/286-g17"),
                "We are not on the ru.4gamesupport.com PS2 page, redirecting to PS2 ru.4gamesupport.com doesn't work. We are on the page " + sp.getCurrentUrl());
    }


}
