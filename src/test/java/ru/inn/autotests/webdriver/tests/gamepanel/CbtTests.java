package ru.inn.autotests.webdriver.tests.gamepanel;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

//import ru.inn.autotests.testapi.system.entities.DragonsProphetDeliveryInfo;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.composite.pages.GamePanelPage;
import ru.inn.autotests.webdriver.composite.pages.LoginPage;
import ru.inn.autotests.webdriver.composite.pages.PlayPage;
import ru.inn.autotests.webdriver.composite.pages.SpecialPages;
import ru.inn.autotests.webdriver.composite.pages.UserBarPage;
import ru.inn.autotests.webdriver.games.DragonsProphet;
import ru.inn.autotests.webdriver.games.Lineage2Classic;
import ru.inn.autotests.webdriver.tests.AbstractTest;
import ru.inn.autotests.webdriver.tests.AbstractTest.TypeUser;
import ru.inn.autotests.webdriver.toolkit.LocalDriverManager;
import ru.inn.autotests.webdriver.toolkit.MailProvider;



public class CbtTests extends AbstractTest {
	
	@BeforeClass
    public void initUser() {
        //OperationsHelper.baseUrl="https://l2-classic-ru.4gametest.com/";
    }

    @Test(description="test to check that user has the ability to get role betatester OFC", enabled=false)
	public void testEnterPinCodeOfc() throws Exception {
    	log4j.info("test to check that user has the ability to get role betatester OFC");
        User   user=switcherLocation(TypeUser.NEW_USER);
    	GamePanelPage gpp = new GamePanelPage(game, language);
    	gpp.openUrlIn4game(gpp.getPageUrl() + "play");
    	LoginPage lp = new LoginPage(game);
    	lp.click(UserBarPage.BUTTON_SIGN_UP);
    	lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
    	Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
    	
        gpp.waitToPlayGame(true);
    	Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_CBT_CODE), 
    			"Button 'Enter CBT Code' is NOT  visible");
    	gpp.click(GamePanelPage.BUTTON_CBT_CODE);
    	gpp.fillCbtForm(String.format(GamePanelPage.ROLE_TESTER,game.getServiceId()), user.getEnvironment());
    	Assert.assertTrue(gpp.validateElementIsNotVisible(GamePanelPage.FORM_CBT), 
    			"CBT Code Activation Form is still visible");
    	Assert.assertTrue(gpp.validateElementIsNotVisible(GamePanelPage.BUTTON_CBT_CODE), 
    			"Button 'Enter CBT Code' is still visible");
	}
    
    @Test(description="The test to check that user has the ability to preorder DP", enabled=false)
	public void testPreorderDp() throws Exception {
    	log4j.info("The test to check that user has the ability to preorder DP");
        game = new DragonsProphet();
        User user = switcherLocation(TypeUser.USER);
    	GamePanelPage gpp = new GamePanelPage(game, language);
    	gpp.openUrlIn4game(gpp.getPageUrl() + "install");
    	Assert.assertTrue(gpp.validateElementVisible(PlayPage.BUTTON_PREORDER_DP_PACK_1), 
    			"The Button 'Preorder DP (pack 1)' is NOT  visible on the page " + gpp.getCurrentUrl());
    	gpp.click(PlayPage.BUTTON_PREORDER_DP_PACK_1);
    	LoginPage lp = new LoginPage(game);
    	lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
    	Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
    	UserBarPage ubp=new UserBarPage();
    	double oldtUserSum=ubp.getCurrentUserMoney();

    	
    	Assert.assertTrue(gpp.validateElementVisible(PlayPage.BUTTON_PREORDER_DP_PACK_1), 
    			"The Button 'Preorder DP (pack 1)' is NOT  visible on the page " + gpp.getCurrentUrl());
    	double sumDpPack1 = gpp.getNumber(PlayPage.DIV_SUM_PREORDER_DP_PACK_1);
    	gpp.click(PlayPage.BUTTON_PREORDER_DP_PACK_1);
		
    	
    	Assert.assertTrue(gpp.validateElementVisible(PlayPage.DIV_PINCODE_DP), 
    			"The div with a gift code for DP 'DIV_PINCODE_DP' is NOT  visible on the page " + gpp.getCurrentUrl());
    	ubp.assertUserSum(oldtUserSum,-sumDpPack1);
    	gpp.logoutHook();
    	gpp.openUrlIn4game(gpp.getPageUrl() + "install");
    	Assert.assertTrue(gpp.validateElementVisible(PlayPage.BUTTON_PREORDER_DP_PACK_1), 
    			"The Button 'Preorder ' DP (pack 1)' is NOT  visible on the page " + gpp.getCurrentUrl());gpp.openUrlIn4game("");
    	Assert.assertTrue(gpp.validateElementVisible(UserBarPage.BUTTON_SIGN_UP), 
    			"The button 'BUTTON_SIGN_UP' is NOT  visible on the page " + gpp.getCurrentUrl());
    	lp.click(UserBarPage.BUTTON_SIGN_UP);
    	lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
    	Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
    	gpp.openUrlIn4game(gpp.getPageUrl() + "install");
    	Assert.assertTrue(gpp.validateElementVisible(PlayPage.DIV_PINCODE_DP), 
    			"The div with a gift code for DP 'DIV_PINCODE_DP' is NOT  visible on the page " + gpp.getCurrentUrl());
	}
    

    
    @Test(description="The test to check that user has the ability to preorder L2 classic", enabled=false)
	public void testPreorderL2Classic() throws Exception {
    	log4j.info("The test to check that user has the ability to preorder L2 classic");
        game = new Lineage2Classic();
        User user = switcherLocation(TypeUser.USER);
    	GamePanelPage gpp = new GamePanelPage(game, language);
    	gpp.openUrlIn4game("");
    	LoginPage lp = new LoginPage(game);
    	String mainMail = user.getMainEmail();
    	lp.click(UserBarPage.BUTTON_SIGN_UP);
    	lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
    	Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
    	UserBarPage ubp=new UserBarPage();
    	double oldtUserSum=ubp.getCurrentUserMoney();
    	gpp.openUrlIn4game(gpp.getPageUrl() + "install");
    	
    	Assert.assertTrue(gpp.validateElementVisible(PlayPage.BUTTON_PREORDER_L2CLASSIC_SUBS_1), 
    			"The Button 'Preorder L2 classic (1 month)' is NOT  visible on the page " + gpp.getCurrentUrl());
    	double sum1MonthPreorder = gpp.getNumber(gpp.findElementWithNumber(PlayPage.DIV_SUM_PREORDER, 1));
    	gpp.click(PlayPage.BUTTON_PREORDER_L2CLASSIC_SUBS_1);
    	Assert.assertTrue(gpp.validateElementVisible(PlayPage.FIELD_FIO_PREORDER_L2CLASSIC), 
    			"The field 'fio' is NOT  visible after clicking button BUTTON_PREORDER_L2CLASSIC_SUBS_1 on the page " + gpp.getCurrentUrl());
    	gpp.type(PlayPage.FIELD_FIO_PREORDER_L2CLASSIC, "Ivan Ivanov");
    	Assert.assertTrue(gpp.validateElementVisible(PlayPage.BUTTON_ACCEPT_FIO), 
    			"The button 'BUTTON_ACCEPT_FIO' is NOT  visible on the page " + gpp.getCurrentUrl());
		int numNewMail = MailProvider.getNumberOfMessages(mainMail);
    	gpp.click(PlayPage.BUTTON_ACCEPT_FIO);
    	Assert.assertTrue(gpp.validateElementVisible(PlayPage.DIV_SUCCESS_PREORDER_FPRM), 
    			"The form 'DIV_SUCCESS_PREORDER_FPRM' is NOT  visible on the page " + gpp.getCurrentUrl());
    	Assert.assertTrue(gpp.validateElementVisible(PlayPage.DIV_MAIN_MAIL), 
    			"The div with main mail 'DIV_MAIN_MAIL' is NOT  visible on the page " + gpp.getCurrentUrl());
    	Assert.assertTrue(gpp.validateElementVisible(PlayPage.BUTTON_VK_SUBSCRIBE_NEWS), 
    			"The button 'BUTTON_VK_SUBSCRIBE_NEWS' is NOT  visible on the page " + gpp.getCurrentUrl());
    	ubp.assertUserSum(oldtUserSum,-sum1MonthPreorder);
    	Assert.assertTrue(MailProvider.isNewMessagePresent(mainMail, numNewMail),"New message 'about buying L2 CL preorder' is not present");
    	gpp.logoutHook();
    	gpp.openUrlIn4game(gpp.getPageUrl() + "install");
    	Assert.assertTrue(gpp.validateElementVisible(PlayPage.BUTTON_PREORDER_L2CLASSIC_SUBS_1), 
    			"The Button 'Preorder L2 classic (1 month)' is NOT  visible on the page " + gpp.getCurrentUrl());gpp.openUrlIn4game("");
    	Assert.assertTrue(gpp.validateElementVisible(UserBarPage.BUTTON_SIGN_UP), 
    			"The button 'BUTTON_SIGN_UP' is NOT  visible on the page " + gpp.getCurrentUrl());
    	lp.click(UserBarPage.BUTTON_SIGN_UP);
    	lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
    	Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
    	gpp.openUrlIn4game(gpp.getPageUrl() + "install");
    	Assert.assertTrue(gpp.validateElementVisible(PlayPage.DIV_SUCCESS_PREORDER_FPRM), 
    			"The form 'DIV_SUCCESS_PREORDER_FPRM' is NOT  visible on the page " + gpp.getCurrentUrl());
	}
    
    @Test(description="The test to check that user can't see the play pafe of  L2 classic", enabled=false)
	public void testPlayPageL2ClassicHidden() throws Exception {
    	log4j.info("The test to check that user can't see the play pafe of  L2 classic");
        game = new Lineage2Classic();
        User user = switcherLocation(TypeUser.USER);
    	GamePanelPage gpp = new GamePanelPage(game, language);
    	gpp.openUrlIn4game("");
    	LoginPage lp = new LoginPage(game);
    	lp.click(UserBarPage.BUTTON_SIGN_UP);
    	lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
    	Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
    	gpp.openUrlIn4game(gpp.getPageUrl() + "play");
    	Assert.assertTrue(gpp.validateElementVisible(SpecialPages.CLASS_BODY_OF_404), 
    			"The user withou a role can see the play page of  L2 classic " + gpp.getCurrentUrl());
    	    	
	}

    @Test(description="The test to check that user can see the icon of game in the Userbar ", enabled=false)
   	public void testBadgeL2Classic() throws Exception {
       	log4j.info("The test to check that user can see the icon of game in the Userbar");
        game = new Lineage2Classic();
        UserBarPage ubp = new UserBarPage();
        ubp.openUrlIn4game(game.getURL() + "install");
       	Assert.assertTrue(ubp.validateElementVisible(UserBarPage.LINK_COMPUTER_GAMES), 
       			"LINK_COMPUTER_GAMES is not visible in the userbar on the page " + ubp.getCurrentUrl());
       	ubp.click(UserBarPage.LINK_COMPUTER_GAMES);
       	Assert.assertTrue(ubp.validateElementVisible(ubp.getGameNavBarLink(game)), "Game " + game.getAppName() + " is not in navigation bar");
       	ubp.click(ubp.getGameNavBarLink(game));
       	Assert.assertTrue(ubp.validateUrlContains("lineage2classic/install/"), 
       			"We are not on the 'lineage2classic/install/' page after clicking on the icon in the userbar");
   	}
}
