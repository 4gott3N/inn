package ru.inn.autotests.webdriver.tests.common;

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.inn.autotests.webdriver.common.PrepareUser;
import ru.inn.autotests.webdriver.common.entity.Message;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.composite.pages.LoginPage;
import ru.inn.autotests.webdriver.composite.pages.MainPage;
import ru.inn.autotests.webdriver.composite.pages.UserBarPage;
import ru.inn.autotests.webdriver.tests.AbstractTest;
import ru.inn.autotests.webdriver.toolkit.MailProvider;

public class BanTests extends AbstractTest {
    User  user = switcherLocation(TypeUser.USER);
    PrepareUser prepareUser = new PrepareUser();

    @Test(description="test to check that notification with ban has come and a user can't log in")
    public void testBanUser()  {
    	log4j.info("test to check that notification with ban have come and a user can't log in");
        String mainMail = user.getMainEmail();
    	int numMail = MailProvider.getNumberOfMessages(mainMail);
    	prepareUser.banUser("36000", user.getId());
    	MainPage mainPage = new MainPage();
		mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
		lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
		Assert.assertTrue(lp.validateElementVisible(LoginPage.LABEL_ERROR_MESSAGE_LOGIN_OR_PASSWORD), 
				"Error message 'The account is blocked' is NOT visible");
		Assert.assertTrue(MailProvider.isNewMessagePresent(mainMail, numMail));
    }
    
    @Test(description="test to check that notification with unban has come and a user has able to log in",
			 dependsOnMethods={"testBanUser"})
    public void testUnbanUser() {
    	log4j.info("test to check that notification with ban have come and a user has able to log in");
        String mainMail = user.getMainEmail();
    	int numMail = MailProvider.getNumberOfMessages(mainMail);
    	prepareUser.unbanUser(user.getId());
    	MainPage mainPage = new MainPage();
		mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
		lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
		Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
		Assert.assertTrue(MailProvider.isNewMessagePresent(mainMail, numMail));
		Message message = MailProvider.getLastMessage(user.getMainEmail());
        message.assertBackgroundColor("GREEN");
    }


}
