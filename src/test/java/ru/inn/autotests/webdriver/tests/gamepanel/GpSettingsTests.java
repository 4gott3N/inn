package ru.inn.autotests.webdriver.tests.gamepanel;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.composite.pages.GamePanelPage;
import ru.inn.autotests.webdriver.composite.pages.LoginPage;
import ru.inn.autotests.webdriver.composite.pages.UserBarPage;
import ru.inn.autotests.webdriver.tests.AbstractTest;
import ru.inn.autotests.webdriver.toolkit.PlatformUtils;
import ru.inn.autotests.webdriver.toolkit.PlatformUtils.Status;

public class GpSettingsTests extends AbstractTest {


    @BeforeClass
    public void initUser() {
        if (System.getenv("plugin") != null)
            isPlugin = Boolean.parseBoolean(System.getenv("plugin"));

    }
    
    
    /**
     * Test validates two buttons on the settings page
     */
    @Test(enabled = false)
    public void testNotAuthorizedStatusUpdate() {
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        PlatformUtils.sendStatus(Status.FULL_UPDATE_REQUIRED, game,isPlugin);
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_SETTINGS), "The button 'Settings' is not visible on the panel 'play'");
        gpp.click(GamePanelPage.BUTTON_SETTINGS);
        gpp.assertText(GamePanelPage.BUTTON_DEFAULT_PATH, "$Settings_default_path",language);
        gpp.assertText(GamePanelPage.BUTTON_FIX_GAME, "$Settings_fix_game",language);
    }

    /**
     * Test checks that for user, who hasn't password for a game, visible button 'Set password'
     */
    @Test(enabled = false)
    public void testAuthorizedWithoutPasswordGame() {
        User user=switcherLocation(TypeUser.HARDCORE_USER);
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        
        PlatformUtils.sendStatus(Status.INSTALLED, game,isPlugin);
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_SETTINGS), "The button 'Settings' is not visible on the panel 'play'");
        gpp.click(GamePanelPage.BUTTON_SETTINGS);
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_SET_PASSWORD), "The button 'Set password' is not visible on the panel 'Settings'");
        gpp.assertText(GamePanelPage.BUTTON_SET_PASSWORD, "$Settings_set_password",language);
    }

    /**
     * Test checks that for user, who has password for a game, visible buttons 'Change password' and 'Remove password'
     */
    @Test(enabled = false)
    public void testAuthorizedWithPasswordGame() {
        User user=switcherLocation(TypeUser.HARDCORE_USER);
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        LoginPage lp = new LoginPage(game);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        
        PlatformUtils.sendStatus(Status.INSTALLED, game,isPlugin);
    }







}
