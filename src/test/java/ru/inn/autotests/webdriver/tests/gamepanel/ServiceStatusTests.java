package ru.inn.autotests.webdriver.tests.gamepanel;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.composite.pages.GamePanelPage;
import ru.inn.autotests.webdriver.composite.pages.LoginPage;
import ru.inn.autotests.webdriver.composite.pages.MainPage;
import ru.inn.autotests.webdriver.composite.pages.UserBarPage;
import ru.inn.autotests.webdriver.games.DragonsProphet;
import ru.inn.autotests.webdriver.tests.AbstractTest;
import ru.inn.autotests.webdriver.toolkit.GameData;
import ru.inn.autotests.webdriver.toolkit.LocalDriverManager;



public class ServiceStatusTests extends AbstractTest {

    @Test(description="test to check that games have correct status", enabled=true)
	public void testCheckServiceStatus() throws Exception {
    	log4j.info("test to check that games have correct status");
        User   user=switcherLocation(TypeUser.HARDCORE_USER);
    	MainPage mpp = new MainPage();
    	mpp.openPage(mpp);
    	Assert.assertTrue(mpp.validateElementVisible(UserBarPage.DIV_USER_BAR), "The userbar is not visible");
    	log4j.info(GameData.getStatusService(new DragonsProphet().getServiceId()));
    	Assert.assertTrue(GameData.getStatusService("22").equals(""));
    	Assert.assertTrue(GameData.getStatusService("28").equals(""));
    	//Assert.assertTrue(GameData.getStatusService("21").contains("OBT"),"DP doesn't have a status OBT");
    	
	}
    


}
