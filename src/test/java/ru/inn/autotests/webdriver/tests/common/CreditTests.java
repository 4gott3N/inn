package ru.inn.autotests.webdriver.tests.common;

import org.eclipse.jetty.util.log.Log;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.inn.autotests.webdriver.common.PrepareUser;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.composite.pages.GiftCodePage;
import ru.inn.autotests.webdriver.composite.pages.LoginPage;
import ru.inn.autotests.webdriver.composite.pages.MainPage;
import ru.inn.autotests.webdriver.composite.pages.UserBarPage;
import ru.inn.autotests.webdriver.tests.AbstractTest;
import ru.inn.autotests.webdriver.toolkit.WebDriverController;


public class CreditTests extends AbstractTest {

    @Test(description = "test to check that new user can't get credit")
    public void testCreditWithNewUser() {
    	log4j.info("test to check that new user can't get credit");
      User  user = switcherLocation(TypeUser.NEW_USER);
        MainPage mp = new MainPage();
        UserBarPage ubp = new UserBarPage();
       /* mp.openPage(mp);
        LocalDriverManager.getDriverController().deleteAllCookies();*/
        mp.openPage(mp);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "Login label is not present");
        
        ubp.click(UserBarPage.BUTTON_USER_BALANCE_DATA_ARROW);
        Assert.assertTrue(ubp.validateElementIsNotVisible(UserBarPage.LABEL_CREDIT_ON), "The new user can switch on the Credit");
        Assert.assertTrue(ubp.validateElementIsNotVisible(UserBarPage.LABEL_CREDIT_OFF), "The new user can switch off the Credit");
    }
    
    @Test(description = "test to check available credit ")
    public void testCreditOn() {
        log4j.info("test to check available credit");
        User user = switcherLocation(TypeUser.TEST_USER);
        MainPage mp = new MainPage();
        UserBarPage ubp = new UserBarPage();
        PrepareUser prepareUser = new PrepareUser();
        prepareUser.setOverdraftLimit(user, "10000", true);
        mp.openPage(mp);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);

        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "Login label is not present");
        ubp.click(UserBarPage.BUTTON_USER_BALANCE_DATA_ARROW);
        ubp.click(UserBarPage.LINK_CREDIT);
        Assert.assertTrue(ubp.validateElementVisible(GiftCodePage.FORM_CREDIT), "Credit FORM is not present");
        if (lp.isElementPresent(GiftCodePage.LABEL_CREDIT_OFF)) {
            ubp.click(GiftCodePage.BUTTON_CREDIT_ON);
            Assert.assertTrue(ubp.validateElementVisible(GiftCodePage.LABEL_CREDIT_ON), "Credit wasn't activated");
        }
        log4j.info("test to check available credit ");
        Assert.assertTrue(ubp.validateElementIsNotVisible(GiftCodePage.LABEL_CREDIT_OFF), "Credit is disabled");
        Assert.assertTrue(ubp.validateElementVisible(GiftCodePage.LABEL_CREDIT_ON), "Credit is not enabled");
        ubp.click(GiftCodePage.BUTTON_CREDIT_OFF);
        Assert.assertTrue(ubp.validateElementVisible(GiftCodePage.LABEL_CREDIT_OFF), "Credit wasn't deactivated");
        ubp.click(UserBarPage.BUTTON_USER_BALANCE_DATA_ARROW);
        Assert.assertTrue(ubp.validateElementVisible(UserBarPage.LABEL_CREDIT_OFF), "Credit is not disabled");
        ubp.refreshPage();
        ubp.click(GiftCodePage.BUTTON_CREDIT_ON);
        Assert.assertTrue(ubp.validateElementVisible(GiftCodePage.LABEL_CREDIT_ON), "Credit wasn't activated");
        ubp.click(UserBarPage.BUTTON_USER_BALANCE_DATA_ARROW);
        Assert.assertTrue(ubp.validateElementVisible(UserBarPage.LABEL_CREDIT_ON), "Credit is not enabled");
    }



}
