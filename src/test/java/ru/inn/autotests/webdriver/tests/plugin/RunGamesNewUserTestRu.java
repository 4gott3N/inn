package ru.inn.autotests.webdriver.tests.plugin;

import org.sikuli.api.DesktopScreenRegion;
import org.sikuli.api.ImageTarget;
import org.sikuli.api.ScreenRegion;
import org.sikuli.api.Target;
import org.sikuli.api.robot.Keyboard;
import org.sikuli.api.robot.Mouse;
import org.sikuli.api.robot.desktop.DesktopKeyboard;
import org.sikuli.api.robot.desktop.DesktopMouse;
import org.sikuli.api.robot.desktop.DesktopScreen;
import org.sikuli.api.visual.ScreenPainter;
import org.testng.Assert;
import org.testng.annotations.*;
import ru.inn.autotests.webdriver.common.PrepareUser;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.common.os.WinRegistry;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.composite.pages.GamePanelPage;
import ru.inn.autotests.webdriver.composite.pages.LoginPage;
import ru.inn.autotests.webdriver.composite.pages.UserBarPage;
import ru.inn.autotests.webdriver.composite.popups.PopupFinalFormalities;
import ru.inn.autotests.webdriver.games.Game;
import ru.inn.autotests.webdriver.games.Game.GameEnum;
import ru.inn.autotests.webdriver.tests.AbstractTest;
import ru.inn.autotests.webdriver.toolkit.LocalDriverManager;
import ru.inn.autotests.webdriver.toolkit.PlatformUtils;

import java.awt.*;
import java.io.File;
import java.util.ArrayList;



public class RunGamesNewUserTestRu extends AbstractTest {
    private Mouse mouse = new DesktopMouse();
    private Keyboard keyboard = new DesktopKeyboard();
    static ScreenPainter painter = new ScreenPainter();

    private final static int TIMEOUT = 30;
    public static String PATH_TO_KEY="SOFTWARE\\Wow6432Node\\4game\\4gameservice";
    public static String NAME_OF_KEY="appId";


    User newUser = new User();
    Game currentGame = games.get(GameEnum.R2ONLINE);

    @DataProvider(name = "initSuperSecurityGames")
    public Object[][] initSuperSecurityGames() {
        // я очень не хотел использовать фиксированное выделение памяти
        // поэтому я сначала считаю все значения, а потом перепишу в правильный массив
        // TODO: перепишите меня если хотите
        ArrayList<ArrayList<Game>> tmpSuperSecurityGames = new ArrayList<ArrayList<Game>>();
        for (Game g : games.values()) {
            String isGameSuperSecurity = PlatformUtils.getStageParameters("superSecurity_" + g.getGameName());

            if (isGameSuperSecurity != null && isGameSuperSecurity.toLowerCase().equals("true")) {
                ArrayList<Game> g2 = new ArrayList<Game>();
                g2.add(g);
                tmpSuperSecurityGames.add(g2);
            }
        }

        Game [][]superSecurityGames = new Game[tmpSuperSecurityGames.size()][1];
        for (int i = 0; i < tmpSuperSecurityGames.size(); i++) {
            superSecurityGames[i][0] = tmpSuperSecurityGames.get(i).get(0);
        }

        return superSecurityGames;
    }

    @DataProvider(name = "initGamesWithoutSuperSecurity")
    public Object[][] initGamesWithoutSuperSecurity() {
        // я очень не хотел использовать фиксированное выделение памяти
        // поэтому я сначала считаю все значения, а потом перепишу в правильный массив
        // TODO: перепишите меня если хотите
        ArrayList<ArrayList<Game>> tmpGames = new ArrayList<ArrayList<Game>>();
        for (Game g : games.values()) {
            String isGameSuperSecurity = PlatformUtils.getStageParameters("superSecurity_" + g.getGameName());

            if (isGameSuperSecurity != null && isGameSuperSecurity.toLowerCase().equals("false")) {
                ArrayList<Game> g2 = new ArrayList<Game>();
                g2.add(g);
                tmpGames.add(g2);
            }
        }

        Game [][]nonSuperSecurityGames = new Game[tmpGames.size()][1];
        for (int i = 0; i < tmpGames.size(); i++) {
            nonSuperSecurityGames[i][0] = tmpGames.get(i).get(0);
        }

        return nonSuperSecurityGames;


    }

    @BeforeMethod
    public void setUp() {
        // prepare new User
//        PrepareUser prepareUser = new PrepareUser();
//        newUser = prepareUser.registrationByBackend(game);
//        newUser = new User();
//        newUser.setMainEmail("notifytest.01368007975344@inn.ru");
//        newUser.setPassword("123456");

        if (OperationsHelper.baseUrl.contains("qa")|| OperationsHelper.baseUrl.contains("demo")) {
            PrepareUser prepareUser = new PrepareUser();
            newUser = prepareUser.registrationByBackend(game, true);

        } else {
            newUser=new User();
            newUser.setMainEmail(PlatformUtils.getStageParameters("test4gameNewUserLogin"));
            newUser.setPassword(PlatformUtils.getStageParameters("test4gameNewUserPass"));
        }
    }

    protected void runGame(Game game) throws Exception {
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openPage(gpp);

//        PlatformUtils.checkGamesStatuses(Status.INSTALLED);

        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(newUser.getMainEmail(), newUser.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized");

        gpp.waitToPlayGame(true);

        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_PLAY), "The button 'Play' is not visible on the panel 'play'");
        gpp.click(GamePanelPage.BUTTON_PLAY);

        if (gpp.isElementPresent(PopupFinalFormalities.IFRAME_FINAL_FORMALITIES)) {
            PopupFinalFormalities pff = new PopupFinalFormalities();
            Assert.assertTrue(pff.validateElementPresent(PopupFinalFormalities.BUTTON_ACCEPT_AND_PLAY), "The button 'BUTTON_ACCEPT_AND_PLAY' is not visible after clicking Play on the panel 'Play'");
            pff.click(PopupFinalFormalities.BUTTON_ACCEPT_AND_PLAY);
        }

    }

    private boolean detectGameRun(Game game, int GAME_LAUNCHED_TIMEOUT) {
        // проверим теперь это средствами скриншотов
        boolean res = false;

        ScreenRegion s = new DesktopScreenRegion();
        Target imageTarget = new ImageTarget(new File(game.getGameSikuliImagesPath() + "successful_connect.png"));
        ScreenRegion r = null;
        //по идее у нас может быть несколько мониторов, и я точно не знаю на каком из них запустится игра
        GraphicsEnvironment g = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice[] devices = g.getScreenDevices();
        for (int i = 0; i < devices.length; i++) {
            s.setScreen(new DesktopScreen(i));

            s.wait(imageTarget, GAME_LAUNCHED_TIMEOUT * 3);
            r = s.find(imageTarget);
            if (r != null && r.getScore() > 0.90) {
                log4j.info("Found target game image (sikuli)");
                res = true;
                break;
            }
            log4j.info("Did not found target image: device " + i);
        }
        if (res == true) {
            // это нужно только для rf online, но по идее другим это не должно помешать
            // можно конечно передавать это параметром
            painter.circle(r.getCenter(), 1000);
            mouse.click(r.getCenter());
        }
        return res;
    }

    private boolean isGameCorrectlyLaunched(Game game) {
        // проверим сначала что процесс запущен
        // если процесс не запущен, то дальше проверять не имеет смысла
        int GAME_LAUNCHED_TIMEOUT = 30000;
        if (    ((this.os.is64bit() != true)&&
                 (game.getGameName() == GameEnum.ATLANTIKA ||
                  game.getGameName() == GameEnum.PLANETSIDE2))
             || (this.os.is64bit()))
        {
            // если 32 битная система и игры (атлантика, пс2)
            // или 64 битная система то детектим процесс
            if (PlatformUtils.detectGameProcess(game, TIMEOUT)) {
                log4j.info("Detected game process");
                // temporary!!!!
                return true;
            }
            else {
                log4j.info("Did not detect game process");
            }
        }

        //TODO: здесь добавить проверку по логам или еще как, дабы достоверно убедится, что игра запустилась

        return false;
    }

    @Test(dataProvider = "initSuperSecurityGames")
    public void testRunSuperSecurityGame(Game game) throws Exception{
        currentGame = game;
        runGame(game);

        // check appID in regedit (and etc)
        String value = WinRegistry.readString(WinRegistry.HKEY_LOCAL_MACHINE,
                                              PATH_TO_KEY,
                                              NAME_OF_KEY);
        Assert.assertNotNull("register key app_id is NULL", value);

        // check game is correctly launched
        boolean isGameLaunched = isGameCorrectlyLaunched(game);
        Assert.assertTrue(isGameLaunched, "The game did not launched: " + game.getGameName());
    }

    @Test(dataProvider = "initGamesWithoutSuperSecurity")
    public void testRunGameWithoutSuperSecurity(Game game) throws Exception{
        currentGame = game;
        runGame(game);

        // check game is correctly launched
        boolean isGameLaunched = isGameCorrectlyLaunched(game);
        Assert.assertTrue(isGameLaunched, "The game did not launched: " + game.getGameName());
    }

    @AfterMethod
    public void tearDown() {
        // killing the game
        try {
            PlatformUtils.killGame(currentGame);
        }
        catch (Exception e) {
            log4j.info(e);
        }
    }

    @AfterClass
    public void logout(){
        LocalDriverManager.getDriverController().shutdown();
    }
}
