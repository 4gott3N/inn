package ru.inn.autotests.webdriver.tests.plugin;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.composite.pages.GamePanelPage;
import ru.inn.autotests.webdriver.composite.popups.PopupDownload;
import ru.inn.autotests.webdriver.games.Game;
import ru.inn.autotests.webdriver.tests.AbstractTest;
import ru.inn.autotests.webdriver.toolkit.PlatformUtils;
import ru.inn.autotests.webdriver.toolkit.YamlProvider;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PluginStatusTest extends AbstractTest {


    @BeforeClass
    public void initGameAndUser() {
     if(System.getenv("plugin")!=null)
            isPlugin= Boolean.parseBoolean(System.getenv("plugin"));
    }

    @Test
    public void pluginStatusTestThroughWs()  {
        OperationsHelper.baseUrl="https://feature-detect-proxy-ru.4gametest.com/";
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openPage(gpp);
        Assert.assertTrue(gpp.validateElementPresent(GamePanelPage.BUTTON_INSTALL), "The button 'Install' is not visible on the panel 'play'");
        log4j.info("click on Install");
        gpp.clickOnStalenessElement(GamePanelPage.BUTTON_INSTALL);
        PopupDownload wd = new PopupDownload();
        Assert.assertTrue(wd.validateElementPresent(PopupDownload.BUTTON_START_INSTALLATION),"The button to start Installation is not visible");
        Assert.assertTrue(wd.validateElementPresent(PopupDownload.CHECKBOX_AGREE_LICENSE),"The checkbox to agree with licence is not visible");
        log4j.info("Popup with agree is present");
        gpp.waitStalenessElement(PopupDownload.FOLDER_PATH);

        wd.type(PopupDownload.FOLDER_PATH, YamlProvider.getParameter("games_dir",YamlProvider.appConfigs));
        log4j.info("type in folder path "+YamlProvider.getParameter("games_dir",YamlProvider.appConfigs));
        wd.click(PopupDownload.CHECKBOX_AGREE_LICENSE);
        log4j.info("click agree with license");
        wd.click(PopupDownload.BUTTON_START_INSTALLATION);
        log4j.info("click on start installation");
        gpp.refreshPage();
        Assert.assertTrue(gpp.validateElementPresent(GamePanelPage.PROGRESS_BAR_PERCENT),"The PROGRESS_BAR_PERCENT is not visible");
        gpp.clickOnStalenessElement(GamePanelPage.PROGRESS_PAUSE);
        log4j.info("click on pause");

        Assert.assertTrue(gpp.validateElementPresent(GamePanelPage.PROGRESS_BAR_ON_PAUSE),"The PROGRESS BAR with pause is not visible");
        gpp.clickOnStalenessElement(GamePanelPage.PROGRESS_RESUME);
        log4j.info("click on resume installation");
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.PROGRESS_BAR),"The PROGRESS BAR is not visible");
        gpp.refreshPage();
        gpp.clickOnStalenessElement(GamePanelPage.PROGRESS_CANCEL);
        log4j.info("click on cancel");
        Assert.assertTrue(gpp.validateElementPresent(GamePanelPage.PROGRESS_BLOCK_CANCEL_STATUS),"The PROGRESS BAR block with button 'Cancel' is not visible");
        gpp.clickOnStalenessElement(GamePanelPage.CANCEL_BLOCK_BUTTON_YES);
        log4j.info("click yes to cancel the installation");
    }


    @Test
    public void pluginStatusTestThroughPlugin()  {
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openPage(gpp);
        gpp.workWithWebSocketsOnPorts(null);
        Assert.assertTrue(gpp.validateElementPresent(GamePanelPage.BUTTON_INSTALL), "The button 'Install' is not visible on the panel 'play'");
        log4j.info("click on Install");
        gpp.clickOnStalenessElement(GamePanelPage.BUTTON_INSTALL);
        PopupDownload wd = new PopupDownload();
        Assert.assertTrue(wd.validateElementPresent(PopupDownload.BUTTON_START_INSTALLATION),"The button to start Installation is not visible");
        Assert.assertTrue(wd.validateElementPresent(PopupDownload.CHECKBOX_AGREE_LICENSE),"The checkbox to agree with licence is not visible");
        log4j.info("Popup with agree is present");
        gpp.waitStalenessElement(PopupDownload.FOLDER_PATH);

        wd.type(PopupDownload.FOLDER_PATH, YamlProvider.getParameter("games_dir",YamlProvider.appConfigs));
        log4j.info("type in folder path "+YamlProvider.getParameter("games_dir",YamlProvider.appConfigs));
        wd.click(PopupDownload.CHECKBOX_AGREE_LICENSE);
        log4j.info("click agree with license");
        wd.click(PopupDownload.BUTTON_START_INSTALLATION);
        log4j.info("click on start installation");
        gpp.refreshPage();
        Assert.assertTrue(gpp.validateElementPresent(GamePanelPage.PROGRESS_BAR_PERCENT),"The PROGRESS_BAR_PERCENT is not visible");
        gpp.clickOnStalenessElement(GamePanelPage.PROGRESS_PAUSE);
        log4j.info("click on pause");

        Assert.assertTrue(gpp.validateElementPresent(GamePanelPage.PROGRESS_BAR_ON_PAUSE),"The PROGRESS BAR with pause is not visible");
        gpp.clickOnStalenessElement(GamePanelPage.PROGRESS_RESUME);
        log4j.info("click on resume installation");
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.PROGRESS_BAR),"The PROGRESS BAR is not visible");
        gpp.refreshPage();
        gpp.clickOnStalenessElement(GamePanelPage.PROGRESS_CANCEL);
        log4j.info("click on cancel");
        Assert.assertTrue(gpp.validateElementPresent(GamePanelPage.PROGRESS_BLOCK_CANCEL_STATUS),"The PROGRESS BAR block with button 'Cancel' is not visible");
        gpp.clickOnStalenessElement(GamePanelPage.CANCEL_BLOCK_BUTTON_YES);
        log4j.info("click yes to cancel the installation");
    }



    @DataProvider
    public Iterator<Object[]> initGames(){
        List<Object[]> data = new ArrayList<Object[]>();
        for(Game game : games.values())
            data.add(new Object[]{game});
        return data.iterator();
    }


    @Test(dataProvider = "initGames")
    public void checkGameSize(Game game) {
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openPage(gpp);
        PlatformUtils.sendStatus(PlatformUtils.Status.NOT_INSTALLED, game, isPlugin);
        Assert.assertTrue(gpp.validateElementPresent(GamePanelPage.BUTTON_INSTALL), "The button 'Install' is not visible on the panel 'play'");
        Assert.assertTrue(gpp.validateElementPresent(GamePanelPage.GAME_SIZE),"The GAME_SIZE in Game panel is not visible");
        System.out.println(gpp.getText(GamePanelPage.GAME_SIZE));
        gpp.validateSize(GamePanelPage.GAME_SIZE, game);
    }

}
