package ru.inn.autotests.webdriver.tests.plugin;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.composite.pages.GamePanelPage;
import ru.inn.autotests.webdriver.composite.pages.LoginPage;
import ru.inn.autotests.webdriver.composite.pages.UserBarPage;
import ru.inn.autotests.webdriver.games.Game;
import ru.inn.autotests.webdriver.tests.AbstractTest;
import ru.inn.autotests.webdriver.toolkit.PlatformUtils;
import ru.inn.autotests.webdriver.toolkit.UserData;
import ru.inn.autotests.webdriver.toolkit.WebDriverController;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class RunGamesTest extends AbstractTest {




    @DataProvider
    public Iterator<Object[]> initGames(){
        List<Object[]> data = new ArrayList<Object[]>();
        for(Game game : games.values())
        data.add(new Object[]{game});
        return data.iterator();
   }


    @Test(dataProvider = "initGames")
    public void runGame(Game game) {
        User user =switcherLocation(TypeUser.MOBILE_USER);
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized");
        
        gpp.addCookie("promoPopupShowed__" + game.getServiceId()+"__" + UserData.getMasterAccountId(), "1");
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        
        if(gpp.isVisible(GamePanelPage.STATUS_MAINTENANCE_BIG))
        {  log4j.info("Game "+game.getAppName()+" on maintenance");
            return;}
        gpp.waitToPlayGame(true);
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_PLAY), "The button 'Play' is not visible on the panel 'play'");
        gpp.click(GamePanelPage.BUTTON_BUTTON_PLAY_OR_PLAY_WITH_PASSWORD);
        gpp.clickPopupIfItPresent(game);
        Assert.assertTrue(PlatformUtils.detectGameProcess(game, WebDriverController.TIMEOUT), "Game " + game.getAppName() + " process wasn't launched");
        PlatformUtils.killGame(game);
    }



}

