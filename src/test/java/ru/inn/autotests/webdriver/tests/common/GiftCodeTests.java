package ru.inn.autotests.webdriver.tests.common;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.composite.common.Captcha;
import ru.inn.autotests.webdriver.composite.pages.*;
import ru.inn.autotests.webdriver.composite.pages.PaymentTerminal.PaymentMethod;
import ru.inn.autotests.webdriver.games.Game;
import ru.inn.autotests.webdriver.games.PointBlank;
import ru.inn.autotests.webdriver.tests.AbstractTest;
import ru.inn.autotests.webdriver.toolkit.LocalDriverManager;
import ru.inn.autotests.webdriver.toolkit.MailProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class GiftCodeTests extends AbstractTest {
    User user;


    @Test(description = "test to check activation code for money", enabled=false)
    public void testMoneyCodeActivation() throws IOException {
        log4j.info("test to check activation code for money was started");
        //OperationsHelper.baseUrl="https://feature-dp-open-ru.4gametest.com/";
        User user = switcherLocation(TypeUser.USER_NO_MONEY);
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");

        GiftCodePage gcp = new GiftCodePage();
        gcp.openPage(gcp);
        UserBarPage ub = new UserBarPage();
        double oldMoney = ub.getCurrentUserMoney();
        gcp.fillGiftCodeForm(GiftCodePage.MONEY_CODE, user.getEnvironment());
        Assert.assertTrue(gcp.validateElementVisible(GiftCodePage.MESSAGE_SUCCESS_ACTIVATION),
                "Green div with success activation message is not visible");
        ub.refreshPage();
        ub.assertUserSum(oldMoney, 300);
    }

    @Test()
    public void testEverythingCodeActivation() throws IOException {
        log4j.info("test to check activation new code for everything");
        //OperationsHelper.baseUrl="https://feature-dp-open-ru.4gametest.com/";
        User user = switcherLocation(TypeUser.USER_NO_MONEY);
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");

        GiftCodePage gcp = new GiftCodePage();
        gcp.openPage(gcp);
        UserBarPage ub = new UserBarPage();
        double oldMoney = ub.getCurrentUserMoney();
        double oldBonus = gcp.getBonusesBalance();
        double oldRubles = gcp.getRublesBalance();
        gcp.fillGiftCodeForm(GiftCodePage.ITEM_MONEY, user.getEnvironment());
        gcp.fillPersonageForm();
        Assert.assertTrue(gcp.validateElementVisible(GiftCodePage.MESSAGE_SUCCESS_ACTIVATION),
                "Green div with success activation message is not visible");
        gcp.assertBonusBalance(oldBonus, 10.0);
        gcp.assertRublesBalance(oldRubles, 30.0);
        ub.assertUserSum(oldMoney, 40);
    }

    @Test(description = "test to check activation code for role beta_tester PB", enabled = true)
    public void testRoleCodeActivation() throws IOException {
        log4j.info("test to check activation code for role beta_tester PB was started");
        //OperationsHelper.baseUrl="https://feature-dp-open-ru.4gametest.com/";
        User user = switcherLocation(TypeUser.USER_NO_MONEY);
        Game game = new PointBlank();
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");

        GiftCodePage gcp = new GiftCodePage();
        gcp.openPage(gcp);
        gcp.fillGiftCodeForm(String.format(GiftCodePage.ROLE_TESTER, game.getServiceId()), user.getEnvironment());
        Assert.assertTrue(gcp.validateElementVisible(GiftCodePage.MESSAGE_SUCCESS_ACTIVATION),
                "Green div with success activation message is not visible");
    }

    @Test(description = "test to check activation code for money and bonus")
    public void testMoneyAndBonusCodeActivation() throws IOException {
        log4j.info("test to check activation code for money and bonus was started");
        //OperationsHelper.baseUrl="https://feature-dp-open-ru.4gametest.com/";
        User user = switcherLocation(TypeUser.USER_NO_MONEY);
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");

        UserBarPage ub = new UserBarPage();
        GiftCodePage gcp = new GiftCodePage();
        gcp.openPage(gcp);
        double oldMoney = ub.getCurrentUserMoney();
        double oldBonus = gcp.getBonusesBalance();
        double oldRubles = gcp.getRublesBalance();
        gcp.fillGiftCodeForm(GiftCodePage.MONEY_AND_BONUS_CODE, user.getEnvironment());
        Assert.assertTrue(gcp.validateElementVisible(GiftCodePage.MESSAGE_SUCCESS_ACTIVATION),
                "Green div with success activation message is not visible");
        gcp.openUrlIn4game("events");
        gcp.assertBonusBalance(oldBonus, 9.0);
        gcp.assertRublesBalance(oldRubles, 36.0);
        ub.assertUserSum(oldMoney, 45);
    }

    @Test(description = "test to check activation code for money and bonus for 4game", enabled=false)
    public void testNewMoneyAndBonusCodeActivation() throws IOException {
        log4j.info("test to check activation code for money and bonus was started");
        //OperationsHelper.baseUrl="https://feature-dp-open-ru.4gametest.com/";
        User user = switcherLocation(TypeUser.USER_NO_MONEY);
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        
        UserBarPage ub = new UserBarPage();
        GiftCodePage gcp = new GiftCodePage();
        gcp.openPage(gcp);
        double oldMoney = ub.getCurrentUserMoney();
        double oldBonus = gcp.getBonusesBalance();
        double oldRubles = gcp.getRublesBalance();
        gcp.fillGiftCodeForm(GiftCodePage.MONEY_AND_BONUS_CODE_4GAME, user.getEnvironment());
        Assert.assertTrue(gcp.validateElementVisible(GiftCodePage.MESSAGE_SUCCESS_ACTIVATION),
                "Green div with success activation message is not visible");
        gcp.openUrlIn4game("events");
        gcp.assertBonusBalance(oldBonus, 2.0);
        gcp.assertRublesBalance(oldRubles, 1.0);
        ub.assertUserSum(oldMoney, 3);
    }
    
    @Test(description = "test to check activation code for Aion's (L2 classic for qa) subscription")
    public void testSubscriptionCodeActivation() throws IOException {
        log4j.info("test to check activation code for Aion's (L2 classic for qa) subscription");
        //OperationsHelper.baseUrl="https://feature-dp-open-ru.4gametest.com/";
        User user = switcherLocation(TypeUser.USER_NO_MONEY);
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");

        GiftCodePage gcp = new GiftCodePage();
        gcp.openPage(gcp);
        gcp.fillGiftCodeForm(GiftCodePage.SUBSCRIPTION_CODE, user.getEnvironment());
        Assert.assertTrue(gcp.validateElementVisible(GiftCodePage.MESSAGE_SUCCESS_ACTIVATION),
                "Green div with success activation message is not visible");
    }

    @Test(description = "test to check activation code for PB's items")
    public void testPbItemCodeActivation() throws IOException {
        log4j.info("test to check activation code for PB's items");
        //OperationsHelper.baseUrl="https://feature-dp-open-ru.4gametest.com/";
        User user = switcherLocation(TypeUser.HARDCORE_USER);
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");

        GiftCodePage gcp = new GiftCodePage();
        gcp.openPage(gcp);
        gcp.fillGiftCodeForm(GiftCodePage.ITEM_PB_CODE, user.getEnvironment());
        Assert.assertTrue(gcp.validateElementVisible(GiftCodePage.MESSAGE_SUCCESS_ACTIVATION),
                "Green div with success activation message is not visible");
    }
    
    @Test(description = "test to check activation code for PS2's items")
    public void testPs2ItemCodeActivation() throws IOException {
        log4j.info("test to check activation code for PS2's items");
        User user = switcherLocation(TypeUser.HARDCORE_USER);
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");

        GiftCodePage gcp = new GiftCodePage();
        gcp.openPage(gcp);
        gcp.fillGiftCodeForm(GiftCodePage.ITEM_PS2_CODE, user.getEnvironment());
        Assert.assertTrue(gcp.validateElementVisible(GiftCodePage.MESSAGE_SUCCESS_ACTIVATION),
                "Green div with success activation message is not visible");
    }

    @Test(description = "test to check activation code for Aion's item")
    public void testAionItemCodeActivation() throws IOException {
        log4j.info("test to check activation code for Aion's item");
        //OperationsHelper.baseUrl="https://feature-dp-open-ru.4gametest.com/";
        User user = switcherLocation(TypeUser.USER_NO_MONEY);
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");

        GiftCodePage gcp = new GiftCodePage();
        gcp.openPage(gcp);
        gcp.fillGiftCodeForm(GiftCodePage.ITEM_CODE, user.getEnvironment());
        gcp.fillPersonageForm();
        Assert.assertTrue(gcp.validateElementVisible(GiftCodePage.MESSAGE_SUCCESS_ACTIVATION),
                "Green div with success activation message is not visible");
    }


    @DataProvider
    public Iterator<Object[]> initPayments() {
        List<Object[]> data = new ArrayList<Object[]>();
        for (PaymentTerminal.PaymentMethod method : paymentsMethods) {
            data.add(new Object[]{method});
        }
        return data.iterator();
    }

    @Test(description = "test to check that user can buy gift certificates through yandex with a parametr 'activate now'",
            dataProvider = "initPayments", enabled = false)
    public void testBuyGiftCodes1500Yandex(PaymentTerminal.PaymentMethod method) throws Exception {
        log4j.info("test to check that user can buy gift certificates through yandex with a parametr 'activation now'");
        //OperationsHelper.baseUrl="https://feature-dp-open-ru.4gametest.com/";
        User user = switcherLocation(TypeUser.USER_NO_MONEY);
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");

        int numMainMail = MailProvider.getNumberOfMessages(user.getMainEmail());
        GiftCodePage gcp = new GiftCodePage();
        gcp.openPage(gcp);
        Assert.assertTrue(gcp.validateElementVisible(GiftCodePage.FORM_GIFT_CODES),
                "Gray form with gift codes is not visible!");
        UserBarPage ubp = new UserBarPage();
        double oldMoney = ubp.getCurrentUserMoney();
        double oldBonus = gcp.getBonusesBalance();
        double oldRubles = gcp.getRublesBalance();
        gcp.click(GiftCodePage.RADIO_CODE1500R_1);
        gcp.selectPayment(method);
        gcp.click(GiftCodePage.CHECK_AUTOACTIVATE);
        gcp.click(GiftCodePage.BUTTON_BUY);
        PaymentTerminal terminal = new PaymentTerminal();
        terminal.addFundsThroughChosenPaymentMethod(method, user, 1500);
        Assert.assertTrue(gcp.validateElementPresent(UserBarPage.DIV_LOGIN_NAME_FIELD),
                "We are not in 4 game after the replenishing. There is no user bar");
        gcp.openPage(gcp);
        ubp.assertUserSum(oldMoney, 1650);
        gcp.assertBonusBalance(oldBonus, 150);
        gcp.assertRublesBalance(oldRubles, 1500);
        MailProvider.isNewMessagePresent(user.getMainEmail(), numMainMail);
        LocalDriverManager.getDriverController().deleteAllCookies();
    }

    @Test(description = "test to check that user can buy gift certificates through payonline")
    public void testBuyGiftCode3000Payonline() throws Exception {
        log4j.info("test to check that user can buy gift certificates through payonline");
        //OperationsHelper.baseUrl="https://feature-dp-open-ru.4gametest.com/";
        User user = switcherLocation(TypeUser.USER_NO_MONEY);
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");

        int numMainMail = MailProvider.getNumberOfMessages(user.getMainEmail());
        GiftCodePage gcp = new GiftCodePage();
        gcp.openPage(gcp);
        Assert.assertTrue(gcp.validateElementVisible(GiftCodePage.FORM_GIFT_CODES),
                "Gray form with gift codes is not visible!");
        int numOfCodes = gcp.getNumOfCodes();
        UserBarPage ubp = new UserBarPage();
        double oldMoney = ubp.getCurrentUserMoney();
        gcp.click(GiftCodePage.RADIO_CODE3000R);
        gcp.selectPayment(PaymentMethod.PLASTIC_CARD);
        gcp.click(GiftCodePage.BUTTON_BUY);
        PaymentTerminal terminal = new PaymentTerminal();
        terminal.addFundsThroughChosenPaymentMethod(PaymentTerminal.PaymentMethod.PLASTIC_CARD, user, 3000);
        Assert.assertTrue(gcp.validateElementPresent(UserBarPage.DIV_LOGIN_NAME_FIELD),
                "We are not in 4 game after the replenishing. There is no user bar");
        gcp.openPage(gcp);
        ubp.assertUserSum(oldMoney, 300);
        gcp.validateNumCodes(numOfCodes, 1);
        MailProvider.isNewMessagePresent(user.getMainEmail(), numMainMail);
        //check letter

    }

    @Test(description = "test to check that user can activate pincode with money after buying on the My codes page",
            enabled = false)
    public void testActivateGiftCodes() throws Exception {
        log4j.info("test to check that user can activate pincode with money after buying on the My codes page");
        //OperationsHelper.baseUrl="https://feature-dp-open-ru.4gametest.com/";
        User user = switcherLocation(TypeUser.USER_NO_MONEY);
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");

        GiftCodePage gcp = new GiftCodePage();
        gcp.openPage(gcp);
        UserBarPage ubp = new UserBarPage();
        double oldMoney = ubp.getCurrentUserMoney();
        gcp.openPage(gcp);
        gcp.click(GiftCodePage.LINK_MY_CODES);
        int numOfCodes = gcp.getNumOfCodes();
        gcp.click(GiftCodePage.BUTTON_ACTIVATE_MY_CODES);
        Captcha captcha = new Captcha();
        captcha.typeCaptcha("987654");
        gcp.click(GiftCodePage.BUTTON_ACTIVATE_POPUP);
        ubp.assertUserSum(oldMoney, 3000);
        gcp.validateNumCodes(numOfCodes, -1);

    }
    
    @Test(description = "test to check that user can activate pincode with limit activation", enabled=false)
    public void testActivateGiftCodeLimit() throws Exception {
        log4j.info("test to check that user can activate pincode with limit activation");
        //OperationsHelper.baseUrl="https://pincode-warning-ru.4gametest.com/";
        User user = switcherLocation(TypeUser.USER_NO_MONEY);
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");

        GiftCodePage gcp = new GiftCodePage();
        gcp.openPage(gcp);
        UserBarPage ub = new UserBarPage();
        double oldMoney = ub.getCurrentUserMoney();
        gcp.fillGiftCodeForm(GiftCodePage.LIMIT_CODE, user.getEnvironment());
        Assert.assertTrue(gcp.validateElementVisible(GiftCodePage.LINK_ACTIVATE_ANOTHER_ONE),
                "Green div with success activation message and LINK_ACTIVATE_ANOTHER_ONE is not visible");
        gcp.click(GiftCodePage.LINK_ACTIVATE_ANOTHER_ONE);
        gcp.fillGiftCodeForm(GiftCodePage.LIMIT_CODE, user.getEnvironment());
        Assert.assertTrue(gcp.validateElementVisible(GiftCodePage.MESSAGE_FAIL_LIMIT_ACTIVATION),
                "The message that user can't activate this code is not present. MESSAGE_FAIL_LIMIT_ACTIVATION is not visible");
    }

    @Test(description = "test to check that user can buy gift certificates through mock on the dev", enabled = false)
    public void testActivateGiftDevAfterBuying() throws Exception {
        log4j.info("test to check that user can buy gift certificates through mock on the dev");
        //OperationsHelper.baseUrl="https://feature-dp-open-ru.4gametest.com/";
        User user = switcherLocation(TypeUser.USER_NO_MONEY);
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");

        GiftCodePage gcp = new GiftCodePage();
        gcp.openPage(gcp);
        UserBarPage ubp = new UserBarPage();
        double oldMoney = ubp.getCurrentUserMoney();
        gcp.openPage(gcp);
        gcp.click(GiftCodePage.LINK_MY_CODES);
        int numOfCodes = gcp.getNumOfCodes();
        gcp.click(GiftCodePage.BUTTON_ACTIVATE_MY_CODES);
        Assert.assertTrue(gcp.validateElementVisible(GiftCodePage.POPUP_ACTIVATE_CODE),
                "Pop-up with form 'Code activation' is NOT visible");
        Captcha captcha = new Captcha();
        captcha.typeCaptcha("987654");
        gcp.click(GiftCodePage.BUTTON_ACTIVATE_POPUP);
        ubp.assertUserSum(oldMoney, 300);
        gcp.validateNumCodes(numOfCodes, -1);


    }

    @Test(description = "test to check text on the page Gift codes")
    public void testTextOnPageGiftCodes() throws Exception {
        log4j.info("test to check text on the page Gift codes");
    }


}
