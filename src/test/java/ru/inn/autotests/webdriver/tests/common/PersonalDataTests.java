package ru.inn.autotests.webdriver.tests.common;

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.composite.common.Captcha;
import ru.inn.autotests.webdriver.composite.pages.LoginPage;
import ru.inn.autotests.webdriver.composite.pages.MainPage;
import ru.inn.autotests.webdriver.composite.pages.SettingsPage;
import ru.inn.autotests.webdriver.composite.pages.UserBarPage;
import ru.inn.autotests.webdriver.tests.AbstractTest;

public class PersonalDataTests extends AbstractTest {


    @Test(description = "test to check that user has ability to change a personal data")
    public void testChangePersonalData(){
        log4j.info("test to check that user has ability to change a personal data");
        User user = switcherLocation(TypeUser.NEW_USER);
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        SettingsPage settingsPage = new SettingsPage(language);
        settingsPage.openPage(settingsPage);
        settingsPage.click(SettingsPage.LINK_PERSONAL_INFO);
        Assert.assertTrue(settingsPage.validateElementVisible(SettingsPage.FORM_SETTINGS_CONTENT), "Settings form is not opened");
        settingsPage.type(SettingsPage.FIELD_NAME_AND_SURNAME, "Testing Name");
        String availableSex=settingsPage.availableSexForUser();
        System.out.println(availableSex);
        settingsPage.click(availableSex);
        settingsPage.type(SettingsPage.FIELD_DOC_NUMBER,"1234");
        settingsPage.type(SettingsPage.FIELD_LAST_DOC_NUMBER,"56");
        settingsPage.type(SettingsPage.FIELD_PASSWORD_IN_PERSONAL_INFO,"123456");
        Captcha captcha=new Captcha();
        captcha.typeCaptchaIfPresent("987654");
        settingsPage.click(SettingsPage.BUTTON_SAVE_SETTINGS);
        Assert.assertTrue(lp.validateElementVisible(settingsPage.DIV_SUCCESS_PERSONAL_INFO), "The div about the success of changing personal data is not visible");
    }
}
