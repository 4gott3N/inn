package ru.inn.autotests.webdriver.tests;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import ru.inn.autotests.webdriver.common.PrepareUser;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.common.language.Language;
import ru.inn.autotests.webdriver.common.os.OperationSystem;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.composite.pages.MainPage;
import ru.inn.autotests.webdriver.composite.pages.PaymentTerminal;
import ru.inn.autotests.webdriver.composite.pages.RegisterPage;
import ru.inn.autotests.webdriver.composite.pages.UserBarPage;
import ru.inn.autotests.webdriver.games.Game;
import ru.inn.autotests.webdriver.games.Game.GameEnum;
import ru.inn.autotests.webdriver.games.Lineage2;
import ru.inn.autotests.webdriver.games.Lineage2eu;
import ru.inn.autotests.webdriver.games.PlanetSide2;
import ru.inn.autotests.webdriver.games.RFOnline;
import ru.inn.autotests.webdriver.toolkit.*;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * Abstract test
 *
 * @author Aleksey Niss,Sergey Kashapov
 */
@Listeners(WebDriverListener.class)
public abstract class AbstractTest {

    protected Game game;
    protected Game randomGame;
    protected Language language;
    protected OperationSystem os = PlatformUtils.getOperationSystem();
    public static Map<GameEnum, Game> games = new HashMap<GameEnum, Game>();
    protected static Logger log4j = Logger.getLogger(AbstractTest.class);
    public static boolean isPlugin = true;
    static ArrayList<YamlProvider.TestsToRun> tests = new ArrayList<YamlProvider.TestsToRun>();
    public static String configName = "";
    static YamlProvider.TestsToRun testRunNow=new YamlProvider.TestsToRun();
    public ArrayList<PaymentTerminal.PaymentMethod> paymentsMethods=new ArrayList<PaymentTerminal.PaymentMethod>();


    public static void initTests() {
        int counter = 0;
        for (Object data : YamlProvider.getTestRun(configName)) {
            if (counter >= 1)
                tests.add((YamlProvider.TestsToRun) data);
            counter++;
        }

    }

    public AbstractTest() {
        configName = System.getenv("config");
        if (configName == null) {
            configName = YamlProvider.getParameter("configName", YamlProvider.appConfigs);
        }
        isPlugin = Boolean.parseBoolean(YamlProvider.getParameter("plugin", configName));
        File theDir = new File("target" + File.separator + "failure_screenshots" + File.separator);
        theDir.mkdirs();

    }

    public static void setNowTest(YamlProvider.TestsToRun nowTest) {
        testRunNow = nowTest;
    }

    @BeforeClass
    public void initData(ITestContext context) {
        language=Language.getLang(PlatformUtils.getStageParameters("lang"));
        games = testRunNow.getGames();
        game = (Game) games.values().toArray()[0];
        paymentsMethods=testRunNow.getPaymentsMethods();
        if(OperationsHelper.baseUrl.contains("eu."))
        	randomGame = new Lineage2eu();
        else 
        	randomGame = new Lineage2();
    }

    @AfterMethod
    public void after(){
        if( LocalDriverManager.getDriverController()!=null)
        LocalDriverManager.getDriverController().deleteAllCookies();
    }


    public enum TypeUser {
        TEST_USER, BAN_USER, USER, CHANGE_USER, HARDCORE_USER, NEW_USER, MOBILE_USER,WITHOUT_MOBILE_USER, USER_NO_MONEY, USER_FOR_TEST_TASK
    }


    public synchronized User switcherLocation(TypeUser typeUser) {
        User user = new User();
        String baseUrl = OperationsHelper.baseUrl;
        // HARDCORE_USER is used in hidden games tests (he has a authority to see them)User has a personage for PS2
        if (typeUser == TypeUser.HARDCORE_USER) {
            user.setMainEmail(PlatformUtils.getStageParameters("hardcoreUser"));
            user.setPassword(PlatformUtils.getStageParameters("hardcorePass"));
            user.setId(PlatformUtils.getStageParameters("hardcoreId"));
            if (!baseUrl.contains("eu"))
                game = new PlanetSide2();
        }
        if (baseUrl.contains("test")) {
            if (typeUser == TypeUser.TEST_USER || typeUser == TypeUser.CHANGE_USER || typeUser == TypeUser.MOBILE_USER) {
                PrepareUser prepareUser = new PrepareUser();
                user = prepareUser.registrationByBackend(game, true);
                prepareUser.addMoneyByBackend(user, "3000");
            } else if (typeUser == TypeUser.USER) {
                PrepareUser prepareUser = new PrepareUser();
                user = prepareUser.registrationByBackend(game, false);
                prepareUser.addMoneyByBackend(user, "3000");
            }else if (typeUser == TypeUser.WITHOUT_MOBILE_USER) {
                PrepareUser prepareUser = new PrepareUser();
                user = prepareUser.registrationByBackend(game, true);
                prepareUser.addMoneyByBackend(user, "3000");
                prepareUser.deleteMobile(user.getId());
                user.setMobileNumber("");
            }

            else if (typeUser == TypeUser.BAN_USER) {
                PrepareUser prepareUser = new PrepareUser();
                user = prepareUser.registrationByBackend(game, true);
                prepareUser.banUser(PrepareUser.Period.FOREVER, game);
            } else if (typeUser == TypeUser.NEW_USER) {
                PrepareUser prepareUser = new PrepareUser();
                user = prepareUser.registrationByBackend(game, false);
                prepareUser.deleteMobileNumber(user);
                user.setMobileNumber("");
                prepareUser.addMoneyByBackend(user, "3000");
            } else if (typeUser == TypeUser.USER_NO_MONEY) {
                PrepareUser prepareUser = new PrepareUser();
                user = prepareUser.registrationByBackend(game, false);

            }
            
        } else {
        	if (typeUser == TypeUser.USER_NO_MONEY) {
        			typeUser = TypeUser.MOBILE_USER;
            }
            if (typeUser == TypeUser.NEW_USER) {
               if(LocalDriverManager.getDriverController()==null)
                LocalDriverManager.setWebDriverController(new WebDriverController());
                MainPage mainPage = new MainPage();
                mainPage.openPage(mainPage);
                RegisterPage registerPage = new RegisterPage();
                user.setMainEmail(OperationsHelper.selectRandomEmail());
                Assert.assertTrue(registerPage.validateElementVisible(UserBarPage.BUTTON_SIGN_UP), "There is no the button 'Sign In or Register' in the userbar. Maybe the User is authorized already.");
                registerPage.click(UserBarPage.BUTTON_SIGN_UP);
                Assert.assertTrue(registerPage.validateElementPresent(RegisterPage.FIELD_REGISTRATION), "There is no the field to typing the email for registration on the register page ");
                registerPage.fillRegistration(user.getMainEmail());
                Assert.assertTrue(registerPage.validateElementIsNotVisible(RegisterPage.BUTTON_CREATE_ACCOUNT), "Register form is still visible");
                String linkToRegist = MailProvider.getLinkFromMail(user.getMainEmail());
                registerPage.openUrlIn4game(linkToRegist);
                user.setPassword("123456");
                registerPage.fillPassForm(user.getPassword());
                Assert.assertTrue(registerPage.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized");
                user.setId("4295228430");
                LocalDriverManager.getDriverController().deleteAllCookies();
            } else if (typeUser == TypeUser.TEST_USER) {
                user.setMainEmail(PlatformUtils.getStageParameters("testMoneyUserMail"));
                user.setPassword(PlatformUtils.getStageParameters("testMoneyUserPass"));
                user.setLogin(PlatformUtils.getStageParameters("testMoneyLogin"));
                user.setServiceAccountId(PlatformUtils.getStageParameters("serviceAccountId"));
                user.setServiceAccountPassword("123456");
            } else if (typeUser == TypeUser.USER) {
                user.setMainEmail(PlatformUtils.getStageParameters("testUserMail"));
                user.setLogin(PlatformUtils.getStageParameters("testUserLogin"));
                user.setPassword(PlatformUtils.getStageParameters("testUserPass"));
            }else if (typeUser == TypeUser.WITHOUT_MOBILE_USER) {
                user.setMainEmail(PlatformUtils.getStageParameters("testUserMail"));
                user.setPassword(PlatformUtils.getStageParameters("testUserPass"));
            }
            else if (typeUser == TypeUser.BAN_USER) {
                user.setMainEmail(PlatformUtils.getStageParameters("testBanUserMail"));
                user.setPassword(PlatformUtils.getStageParameters("testBanUserPass"));
            } else if (typeUser == TypeUser.CHANGE_USER) {
                user.setLogin(PlatformUtils.getStageParameters("testChangeUserLogin"));
                user.setPassword(PlatformUtils.getStageParameters("testChangeUserPass"));
                user.setMobileNumber(PlatformUtils.getStageParameters("testChangeUserMobile"));
            } else if (typeUser == TypeUser.MOBILE_USER) {
                user.setMainEmail(PlatformUtils.getStageParameters("testSmsUserMail"));
                user.setPassword(PlatformUtils.getStageParameters("testSmsUserPass"));
                user.setLogin(PlatformUtils.getStageParameters("testSmsUserLogin"));
                user.setMobileNumber(PlatformUtils.getStageParameters("testSmsUserNumber"));
            }


        }
        if (baseUrl.contains("qa") || baseUrl.contains("4gametest")) {
            user.setEnvironment("qa");
        } else if (baseUrl.contains("dev")) {
            user.setEnvironment("dev");
        } else {
            user.setEnvironment("live");
        }
        return user;
    }
}
