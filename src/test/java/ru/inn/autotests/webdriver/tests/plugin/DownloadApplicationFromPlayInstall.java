package ru.inn.autotests.webdriver.tests.plugin;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.inn.autotests.webdriver.composite.pages.GamePanelPage;
import ru.inn.autotests.webdriver.composite.popups.PopupDownloadApplication;
import ru.inn.autotests.webdriver.games.Game;
import ru.inn.autotests.webdriver.tests.AbstractTest;

/**
 */
public class DownloadApplicationFromPlayInstall extends AbstractTest {

    private void basicTestAction(Game game) {
    	GamePanelPage gpp = new GamePanelPage(game);
        gpp.openPage(gpp);
        log4j.info("\n[TESTCASE]Install page application download for " + game.getAppName());
        
        gpp.click(GamePanelPage.BUTTON_INSTALL_WITH_NO_PLUGIN);

        PopupDownloadApplication pda = new PopupDownloadApplication();
        Assert.assertTrue(pda.validateElementPresent(PopupDownloadApplication.BUTTON_ACCEPT_LICENSE), "The BUTTON_ACCEPT_LICENSE in the popup Download application is not visible");
        pda.click(PopupDownloadApplication.BUTTON_ACCEPT_LICENSE);

        gpp.validateDownloadedApplication(game);
    }

    @DataProvider
    public Object[][] initGames(){
        Game[][] gameList=new Game[games.size()][1]; int i=0;
        for(Game game : games.values())
        {
            gameList[i][0]=game;
            i++;
        }
        return gameList;
    }


    @Test(dataProvider = "initGames")
    public void testDownloadApp(Game game){
        basicTestAction(game);
    }


}
