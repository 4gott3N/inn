package ru.inn.autotests.webdriver.tests.common;

import org.testng.Assert;
import org.testng.annotations.Test;

import ru.inn.autotests.webdriver.common.PrepareUser;
import ru.inn.autotests.webdriver.common.entity.Message;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.composite.common.Captcha;
import ru.inn.autotests.webdriver.composite.pages.RecoveryPasswordPage;
import ru.inn.autotests.webdriver.composite.pages.UserBarPage;
import ru.inn.autotests.webdriver.tests.AbstractTest;
import ru.inn.autotests.webdriver.toolkit.MailProvider;

public class RecoveryPasswordTests extends AbstractTest {

	PrepareUser prepareUser = new PrepareUser();
	
    @Test(description = "test to check that user can recover his password for 4game by sms")
    public void testRecoveryPasswordMobile() throws Exception {
        log4j.info("test to check that user can recover his password for 4game by sms");
        User user = switcherLocation(TypeUser.MOBILE_USER);
        prepareUser.resetCacheIs(user.getEnvironment());
        String newPassword = user.getPassword();
        RecoveryPasswordPage rpp = new RecoveryPasswordPage();
        rpp.openPage(rpp);
        rpp.fillRecoveryPasswordForm(user.getLogin());
        int numOfSms = MailProvider.getNumberOfSms(user.getMobileNumber());
        rpp.fillUserIdentifiedForm();
        Assert.assertTrue(rpp.validateElementVisible(RecoveryPasswordPage.FIELD_SMS_CODE),
                "The Sms with a code to recover password was not sent");
        Assert.assertTrue(MailProvider.isNewSmsPresent(user.getMobileNumber(), numOfSms),
                "there is no new sms notifytest." + user.getMobileNumber() + "@inn.ru.htm");
        Message mess = MailProvider.getSms(user.getMobileNumber());
        rpp.fillSignInWithNewPasswordFormSms(newPassword, newPassword, mess.getCodeFromSMS());
        Assert.assertTrue(rpp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
    }

    @Test(description = "test to check that user can recover his password for 4game by mail")
    public void testRecoveryPasswordMail() throws Exception {
        log4j.info("test to check that user can recover his password for 4game by mail");
        User user = switcherLocation(TypeUser.WITHOUT_MOBILE_USER);
        prepareUser.resetCacheIs(user.getEnvironment());
        String newPassword = user.getPassword();
        RecoveryPasswordPage rpp = new RecoveryPasswordPage();
        rpp.openPage(rpp);
        String mainEmail = user.getMainEmail();
        rpp.fillRecoveryPasswordForm(mainEmail);
        rpp.sendPause(2);
        int numMainMail = MailProvider.getNumberOfMessages(mainEmail);
        rpp.fillUserIdentifiedForm();
        Assert.assertTrue(rpp.validateElementVisible(RecoveryPasswordPage.FORM_AN_EMAIL_FOR_YOU),
                "The Message with a link to recover password was not sent FORM_AN_EMAIL_FOR_YOU is not visible");
        Assert.assertTrue(MailProvider.isNewMessagePresent(mainEmail, numMainMail));
        String linkToRecover = MailProvider.getLinkFromMail(mainEmail);
        rpp.openUrlIn4game(linkToRecover);
        rpp.fillSignInWithNewPasswordFormMail(newPassword, newPassword);
        Assert.assertTrue(rpp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
    }


    @Test(description = "test to check that captcha in the RecoveryForm displayed when nonexistent login was entered")
    public void testCaptchaDisplayInRecoveryForm() throws Exception {
        log4j.info("test to check that captcha in the RecoveryForm displayed when nonexistent login was entered");
        prepareUser.resetCacheIs("qa");
        RecoveryPasswordPage rpp = new RecoveryPasswordPage();
        rpp.openPage(rpp);
        rpp.fillRecoveryPasswordForm("nonexistent1");
        Captcha captcha = new Captcha();
        captcha.typeCaptcha("");
    }


}
