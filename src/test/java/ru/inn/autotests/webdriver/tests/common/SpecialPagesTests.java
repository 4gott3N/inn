package ru.inn.autotests.webdriver.tests.common;

import org.openqa.selenium.Cookie;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.composite.pages.LoginPage;
import ru.inn.autotests.webdriver.composite.pages.MainPage;
import ru.inn.autotests.webdriver.composite.pages.SpecialPages;
import ru.inn.autotests.webdriver.composite.pages.UserBarPage;
import ru.inn.autotests.webdriver.tests.AbstractTest;
import ru.inn.autotests.webdriver.toolkit.LocalDriverManager;
import ru.inn.autotests.webdriver.toolkit.UserData;

/**
 * Created with IntelliJ IDEA.
 * User: Sergey.Kashapov
 * Date: 23.07.13
 * Time: 14:23
 * To change this template use File | Settings | File Templates.
 */
public class SpecialPagesTests extends AbstractTest {



    @Test(description = "test to check 404 page")
    public void test404(){
        log4j.info("test to check 404 page started");

        SpecialPages sp=new SpecialPages();
        sp.open404();
        Assert.assertTrue(sp.validateElementVisible(SpecialPages.CLASS_BODY_OF_404), "404 page is NOT correct");
    }

    @Test(description = "test to check 504 page")
    public void test504(){
        log4j.info("test to check 504 page started");
        SpecialPages sp=new SpecialPages();
        sp.open504();
        Assert.assertTrue(sp.validateElementVisible(SpecialPages.CLASS_BODY_OF_504));
        Assert.assertTrue(sp.validateElementVisible(SpecialPages.LABEL_TIMER_ON_504));
        Cookie lop=new Cookie("lop","1");
        Assert.assertEquals(LocalDriverManager.getDriverController().getCookie("lop"), lop, "Cookie lop is not equals 1");
    }


    @Test(description = "test to check Maintenance page")
    public void testMaintenance(){
        log4j.info("test to check Maintenance page started");
        SpecialPages sp=new SpecialPages();
        sp.openUrlIn4game("/default/maintenance/");
        Assert.assertTrue(sp.validateElementVisible(SpecialPages.CLASS_BODY_OF_MAINT), "Maintenance page is NOT correct");
        if(OperationsHelper.baseUrl.contains("ru") )
        Assert.assertTrue(sp.validateElementVisible(SpecialPages.VK_WIDGET), "VK widget on the Maintenance page is NOT visible");
    }
    
    @Test(description = "test to check 500 page")
    public void test500(){
        log4j.info("test to check 500 page started");
        User user = switcherLocation(TypeUser.HARDCORE_USER);
        SpecialPages sp=new SpecialPages();
        MainPage mp=new MainPage();
        mp.openPage(mp);
              
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementPresent(UserBarPage.DIV_LOGIN_NAME_FIELD),"Login label is not present");
        
        mp.addCookie("promoPopupShowed__1006__" + UserData.getMasterAccountId(), "1");
        mp.openPage(mp);
        
        sp.open500();
        Assert.assertTrue(sp.validateElementVisible(SpecialPages.CLASS_BODY_OF_500), "500 page is NOT correct");
    }

    @Test(description = "test to check scrollbar on size 1031x1031")
    public void testHasScrollbar() throws Exception {
        log4j.info("test to check scrollbar on size 1031x1031 started");
        MainPage mp=new MainPage();
        mp.openPage(mp);
        mp.windowSetSize(1031, 1031);
        Assert.assertFalse(mp.hasScrollbar(), "Scrolling exists!");
        LocalDriverManager.getDriverController().maximizeWindow();
    }

}
