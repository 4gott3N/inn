package ru.inn.autotests.webdriver.tests.playpage;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.inn.autotests.webdriver.common.PrepareUser;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.composite.pages.GamePanelPage;
import ru.inn.autotests.webdriver.composite.pages.LoginPage;
import ru.inn.autotests.webdriver.composite.pages.PlayPage;
import ru.inn.autotests.webdriver.composite.pages.UserBarPage;
import ru.inn.autotests.webdriver.games.Game;
import ru.inn.autotests.webdriver.games.PlanetSide2;
import ru.inn.autotests.webdriver.tests.AbstractTest;
import ru.inn.autotests.webdriver.toolkit.LocalDriverManager;

import java.util.Date;


public class LoyalityTests extends AbstractTest {


    @Test(description="test to check user's loyality level for PS2")
    public void testLoyaltyPS2(){
        User user=switcherLocation(TypeUser.USER);
    	Game game = new PlanetSide2();
    	log4j.info("test to check user's loyality level for " + game.getAppName());
    	PrepareUser prepareUser = new PrepareUser();
    	prepareUser.createServiceAccountId(user, game);
    	Date dateInFuture = OperationsHelper.getChangedDate(5);
    	PlayPage playPage = new PlayPage(game);
        LoginPage lp = new LoginPage(game);
        int k=0;
        for (int i = 0; i < 7; i++) {
			prepareUser.setLoyaltySum(user.getServiceAccountId(), String.valueOf(k), dateInFuture);
	    	playPage.openUrlIn4game(game.getURL() + "play/premium/");
	        lp.click(UserBarPage.BUTTON_SIGN_UP);
	        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
	        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
	    	Assert.assertTrue(playPage.validateElementVisible(String.format(PlayPage.DIV_LEVEL_LOYALTY,i)), "The div with information about loyalty "  + i + " is not present");
	    	LocalDriverManager.getDriverController().deleteAllCookies();
	    	k+=30;
        }
    }
    
    @Test(description="test to check user's loyality is over for PS2")
    public void testLoyaltyPS2IsOver(){
        User user=switcherLocation(TypeUser.USER);
    	Game game = new PlanetSide2();
    	log4j.info("test to check user's loyality is over for " + game.getAppName());
    	PrepareUser prepareUser = new PrepareUser();
    	prepareUser.createServiceAccountId(user, game);
    	int timeToResetLoyalty = 2;
    	Date dateInPast = OperationsHelper.getChangedDate(-timeToResetLoyalty);
    	prepareUser.setLoyaltySum(user.getServiceAccountId(), String.valueOf(100), dateInPast);
    	prepareUser.setSubscriptionDate(user.getServiceAccountId(), dateInPast);
    	PlayPage playPage = new PlayPage(game);
        LoginPage lp = new LoginPage(game);
        playPage.openUrlIn4game(game.getURL() + "play");
	    lp.click(UserBarPage.BUTTON_SIGN_UP);
	    lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
	    Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        playPage.click(PlayPage.LINK_LOYALTY_PS2);
	    Assert.assertTrue(playPage.validateElementVisible(PlayPage.DIV_LOYALTY_TIME_REMAINING), "The div with information about loyalty time remaining is not present");
	    Integer numberDayToResetLoyalty = (int) playPage.getNumber(PlayPage.DIV_LOYALTY_TIME_REMAINING);
		Assert.assertTrue(numberDayToResetLoyalty.equals(30 - timeToResetLoyalty), "The PlayPage.DIV_LOYALTY_TIME_REMAINING " + numberDayToResetLoyalty + " doesn't equal with " + (30 - timeToResetLoyalty));
	    
    }
    

}
