package ru.inn.autotests.webdriver.tests.common;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.inn.autotests.webdriver.common.entity.Message;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.composite.pages.*;
import ru.inn.autotests.webdriver.tests.AbstractTest;
import ru.inn.autotests.webdriver.toolkit.LocalDriverManager;
import ru.inn.autotests.webdriver.toolkit.MailProvider;
import ru.inn.autotests.webdriver.toolkit.UserData;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PaymentsTests extends AbstractTest {
    User user;

    @BeforeClass
    public void initUser() {
        user = switcherLocation(TypeUser.USER);
        //OperationsHelper.baseUrl="https://new-centili-ru.4gametest.com/";
    }



    @DataProvider
    public Iterator<Object[]> initPayments(){
        List<Object[]> data = new ArrayList<Object[]>();
        for(PaymentTerminal.PaymentMethod method: paymentsMethods)
        {
            data.add(new Object[]{method});
        }
       return data.iterator();
    }

    @Test(dataProvider = "initPayments")
    public void testReplenishAccount(PaymentTerminal.PaymentMethod method) {
    	log4j.info("testReplenishAccount started for " + method);
        User user = switcherLocation(TypeUser.USER);
            PaymentTerminal terminal = new PaymentTerminal();
            MainPage mainPage = new MainPage();
            mainPage.openPage(mainPage);
             
            LoginPage lp = new LoginPage(game);
            lp.click(UserBarPage.BUTTON_SIGN_UP);
            lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
            Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized");
            
            mainPage.addCookie("promoPopupShowed__1006__" + UserData.getMasterAccountId(), "1");
            mainPage.openPage(mainPage);
            
            UserBarPage ubp = new UserBarPage();
            double oldMoney = ubp.getCurrentUserMoney();
            int numberOfStringsInMainMail = MailProvider.getNumberOfMessages(user.getMainEmail());
            double sum = terminal.getMinSum(method);
            terminal.replenishAccount(method, user, sum);
            Assert.assertTrue(ubp.validateElementPresent(UserBarPage.DIV_USER_BAR),"We didn't return to the 4game after replanishing. Via " + method);
            EventsPage ep= new EventsPage(language);
            ep.openPage(ep);
            Assert.assertTrue(ep.getFirstEvent().contains(String.valueOf((int)sum)), "The first event does not contain the sum per the replenishing " + sum);
            ubp.assertUserSum(oldMoney, sum);
            MailProvider.isNewMessagePresent(user.getMainEmail(), numberOfStringsInMainMail);
            Message message = MailProvider.getLastMessage(user.getMainEmail());
            message.assertBackgroundColor("GREEN");
            message.assertSum(sum);
            mainPage.openPage(mainPage);
            LocalDriverManager.getDriverController().deleteAllCookies();

    }
    
    @Test(description = "test to check successfull payment url when a user is not authrized")
    public void testCheckPaymentLink() {
        log4j.info("test to check successfull payment url when a user is not authrized");
        MainPage mainPage = new MainPage();
        mainPage.openUrlIn4game("?orderId=13189826&psCode=payonline&returnState=1");
        Assert.assertTrue(mainPage.validateElementIsNotVisible(SpecialPages.CLASS_BODY_OF_500), "There is a 500 page");
        Assert.assertTrue(mainPage.validateElementVisible(UserBarPage.DIV_USER_BAR), "The USER_BAR is not present.");
    }
    
    @Test(description = "test to check unsuccessfull replenishing url when a user is authrized")
    public void testCheckUnsuccessfullPaymentLink() {
        log4j.info("test to check unsuccessfull replenishing url when a user is authrized");
        MainPage mainPage = new MainPage();
        LoginPage lp = new LoginPage(game);
        mainPage.openPage(mainPage);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized");
        mainPage.openUrlIn4game("?orderId=13189826&psCode=payonline&returnState=1");
        mainPage.sendPause(15);
        Assert.assertTrue(mainPage.validateElementVisible(UserBarPage.MESSAGE_REPLANISH_ERROR), 
        		"MESSAGE_REPLANISH_ERROR is not present after unsuccessfull replenishing");
    }
   
    @Test(enabled=true)
    public void testEmptySum() {
            MainPage mainPage = new MainPage();
            mainPage.openPage(mainPage);
            LoginPage lp = new LoginPage(game);
            lp.click(UserBarPage.BUTTON_SIGN_UP);
            lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
            Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized");
            PaymentTerminal terminal = new PaymentTerminal();
            mainPage.openUrlIn4game("?popupWidget=PaymentTerminalWidget");
            Assert.assertTrue(terminal.validateElementVisible(PaymentTerminal.FORM_PAYMENT_TERMINAL),"The PaymentTerminal is not visible");
            String nameOfPaymentMethod = "Yandex_bank_card";
			terminal.clickOnChosenMethodAndVerifyChosenSelection(nameOfPaymentMethod);
            String type = String.format(PaymentTerminal.TYPE_SELECTOR, nameOfPaymentMethod);
            //terminal.type(type, "");
            terminal.submit(type);
            //WebDriverController.pressEnter();
            Assert.assertTrue(terminal.validateElementVisible(PaymentTerminal.MESSAGE_ERROR),"The empty sum is able to be submited in the PaymentTerminal");

    }


}
