package ru.inn.autotests.webdriver.tests.gamepanel;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.composite.pages.GamePanelPage;
import ru.inn.autotests.webdriver.composite.pages.LoginPage;
import ru.inn.autotests.webdriver.composite.pages.UserBarPage;
import ru.inn.autotests.webdriver.games.Game;
import ru.inn.autotests.webdriver.tests.AbstractTest;
import ru.inn.autotests.webdriver.toolkit.PlatformUtils;
import ru.inn.autotests.webdriver.toolkit.PlatformUtils.Status;

public class BanTests extends AbstractTest {

	@BeforeClass
    public void initUser() {
        if (System.getenv("plugin") != null)
            isPlugin = Boolean.parseBoolean(System.getenv("plugin"));

    }

    @Test
    public void testSetBan() {
        User user = switcherLocation(TypeUser.BAN_USER);
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        PlatformUtils.sendStatus(Status.INSTALLED, game, isPlugin);
        gpp.validateBan(GamePanelPage.BAN_INFO_MESSAGE, "$Ban_user_forever");

    }


}
