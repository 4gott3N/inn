package ru.inn.autotests.webdriver.tests.gamepanel;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.inn.autotests.webdriver.common.PrepareUser;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.composite.pages.GamePanelPage;
import ru.inn.autotests.webdriver.composite.pages.LoginPage;
import ru.inn.autotests.webdriver.composite.pages.UserBarPage;
import ru.inn.autotests.webdriver.composite.popups.PopupFinalFormalities;
import ru.inn.autotests.webdriver.composite.popups.PopupRemovePassword;
import ru.inn.autotests.webdriver.composite.popups.PopupSetPassword;
import ru.inn.autotests.webdriver.tests.AbstractTest;
import ru.inn.autotests.webdriver.toolkit.LocalDriverManager;
import ru.inn.autotests.webdriver.toolkit.PlatformUtils;
import ru.inn.autotests.webdriver.toolkit.UserData;

import java.util.HashSet;
import java.util.Set;

public class ForgotPasswordTest extends AbstractTest {
    String newPassword = "1234567";
    String changedPassword = "7654321";
    PrepareUser prepareUser = new PrepareUser();
  
    @Test(enabled=false)
    public void testCheckButtonPassword() {
        log4j.info("Test testCheckButtonPassword started for game " + game.getGameName());
        //OperationsHelper.baseUrl="https://main-w-mobile-ru.4gametest.com/";
        User user = prepareUser.registrationByBackend(game, true);
       
        
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");

        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        
        gpp.addCookie("promoPopupShowed__" + game.getServiceId()+"__" + UserData.getMasterAccountId(), "1");
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        
                Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD),
                        "The user is not authorized.");

        gpp.waitToPlayGame(false);

        gpp.validateElementPresent(GamePanelPage.BUTTON_PLAY);

        gpp.validateElementPresent(GamePanelPage.BUTTON_SETTINGS);
        gpp.click(GamePanelPage.BUTTON_SETTINGS);
        Assert.assertTrue(gpp.validateElementPresent(GamePanelPage.BUTTON_SET_PASSWORD), 
        		"There is no BUTTON_SET_PASSWORD for game " + game.getAppName());

        gpp.click(GamePanelPage.BUTTON_SET_PASSWORD);

        PopupSetPassword psp = new PopupSetPassword();
        psp.validateElementPresent(PopupSetPassword.INPUT_PASSWORD);
        psp.type(PopupSetPassword.INPUT_PASSWORD, newPassword);
        psp.validateElementPresent(PopupSetPassword.BUTTON_CREATE_PASSWORD);
        psp.click(PopupSetPassword.BUTTON_CREATE_PASSWORD);
        psp.validateElementPresent(PopupSetPassword.INPUT_USER_PASSWORD);
        psp.type(PopupSetPassword.INPUT_USER_PASSWORD, user.getPassword());
        psp.click(PopupSetPassword.INPUT_ICON_ENTER);

        gpp.validateElementPresent(GamePanelPage.GAME_PANEL);
        gpp.waitWhileElementIsPresent(GamePanelPage.MAIN_SPINNER_WAITING);
        Assert.assertTrue(lp.validateElementVisible(GamePanelPage.BUTTON_PLAY_WITH_PASSWORD), 
        		"There is no BUTTON_PLAY_WITH_PASSWORD after creating a password for game " + game.getAppName());
        gpp.click(GamePanelPage.BUTTON_PLAY_WITH_PASSWORD);
        Assert.assertTrue(lp.validateElementVisible(GamePanelPage.FIELD_GAME_PASSWORD), 
        		"There is no FIELD_GAME_PASSWORD after clicking on the BUTTON_PLAY_WITH_PASSWORD");
        gpp.validateElementPresent(GamePanelPage.FIELD_GAME_PASSWORD);
        gpp.type(GamePanelPage.FIELD_GAME_PASSWORD, newPassword);
        gpp.click(GamePanelPage.BUTTON_PLAY_WITH_PASSWORD_ARROW);

        gpp.validateElementPresent(PopupFinalFormalities.IFRAME_FINAL_FORMALITIES);

        PopupFinalFormalities pff = new PopupFinalFormalities();
        pff.validateElementPresent(PopupFinalFormalities.BUTTON_ACCEPT_AND_PLAY);
        pff.click(PopupFinalFormalities.BUTTON_ACCEPT_AND_PLAY);
        Assert.assertTrue(PlatformUtils.detectGameProcess(game, 30),
                "Game " + game.getGameName() + " process wasn't launched");
        PlatformUtils.killGame(game);
    }

    @Test(enabled =  false)
    public void testCheckboxForgotPassword() {
        log4j.info("Test testCheckboxForgotPassword started for game " + game.getGameName());
        User user = prepareUser.registrationByBackend(game, true);
        prepareUser.setServicePassword(user);
        //OperationsHelper.baseUrl="https://main-w-mobile-ru.4gametest.com/";
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");

        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        
        gpp.addCookie("promoPopupShowed__" + game.getServiceId()+"__" + UserData.getMasterAccountId(), "1");
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        
                Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD),
                        "The user is not authorized.");

        gpp.waitToPlayGame(false);
        Assert.assertTrue(lp.validateElementVisible(GamePanelPage.BUTTON_PLAY_WITH_PASSWORD), 
        		"There is no BUTTON_PLAY_WITH_PASSWORD");
        gpp.validateElementPresent(GamePanelPage.BUTTON_PLAY_WITH_PASSWORD);
        gpp.click(GamePanelPage.BUTTON_PLAY_WITH_PASSWORD);
        Assert.assertTrue(lp.validateElementVisible(GamePanelPage.FIELD_GAME_PASSWORD), 
        		"There is no FIELD_GAME_PASSWORD after clicking on the BUTTON_PLAY_WITH_PASSWORD");
        gpp.type(GamePanelPage.FIELD_GAME_PASSWORD, newPassword);
        Assert.assertTrue(lp.validateElementVisible(GamePanelPage.BUTTON_PLAY_WITH_PASSWORD_ARROW), 
        		"There is no BUTTON_PLAY_WITH_PASSWORD_ARROW after clicking on the BUTTON_PLAY_WITH_PASSWORD");
        gpp.click(GamePanelPage.CHECKBOX_REMEMBER_PASSWORD);
        gpp.click(GamePanelPage.BUTTON_PLAY_WITH_PASSWORD_ARROW);

        PopupFinalFormalities pff = new PopupFinalFormalities();
        pff.validateElementPresent(PopupFinalFormalities.BUTTON_ACCEPT_AND_PLAY);
        pff.click(PopupFinalFormalities.BUTTON_ACCEPT_AND_PLAY);
        pff.validateElementIsNotVisible(PopupFinalFormalities.BUTTON_ACCEPT_AND_PLAY);
        gpp.validateElementPresent(GamePanelPage.BUTTON_PLAY_REMEMBERED_PASSWORD);
        Assert.assertTrue(PlatformUtils.detectGameProcess(game, 30),
                "Game " + game.getGameName() + " process wasn't launched");
        PlatformUtils.killGame(game);
        
        Assert.assertTrue(lp.validateElementVisible(GamePanelPage.LINK_FORGET_PASSWORD), 
        		"There is no LINK_FORGET_PASSWORD");
        gpp.click(GamePanelPage.LINK_FORGET_PASSWORD);
        Assert.assertTrue(lp.validateElementVisible(GamePanelPage.BUTTON_PLAY_WITH_PASSWORD), 
        		"There is no BUTTON_PLAY_WITH_PASSWORD after clicking on the LINK_FORGET_PASSWORD");
    }


    @Test(enabled=false)
    public void testRemovePassword() {



        PrepareUser prepareUser = new PrepareUser();
        User user = prepareUser.registrationByBackend(game, true);
        prepareUser.setServicePassword(user);
        //OperationsHelper.baseUrl="https://main-w-mobile-ru.4gametest.com/";
        prepareUser.setLastAuthTime(user, "22", OperationsHelper.getChangedDate(-2));
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");

        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        
        gpp.addCookie("promoPopupShowed__" + game.getServiceId()+"__" + UserData.getMasterAccountId(), "1");
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");

        gpp.waitToPlayGame(false);

        gpp.validateElementPresent(GamePanelPage.BUTTON_SETTINGS);
        gpp.click(GamePanelPage.BUTTON_SETTINGS);
        gpp.validateElementPresent(GamePanelPage.BUTTON_REMOVE_PASSWORD);
        gpp.click(GamePanelPage.BUTTON_REMOVE_PASSWORD);

        PopupRemovePassword prp = new PopupRemovePassword();
        prp.validateElementPresent(PopupRemovePassword.INPUT_CURRENT_PASSWORD);
        prp.type(PopupRemovePassword.INPUT_CURRENT_PASSWORD, newPassword);
        prp.click(PopupRemovePassword.BUTTON_CHANGE_PASSWORD);
        prp.validateElementPresent(PopupRemovePassword.INPUT_USER_PASSWORD);
        prp.type(PopupRemovePassword.INPUT_USER_PASSWORD, user.getPassword());
        prp.click(PopupRemovePassword.INPUT_ICON_ENTER);
        gpp.validateElementIsNotVisible(GamePanelPage.FIELD_GAME_PASSWORD);

        gpp.validateElementPresent(GamePanelPage.BUTTON_PLAY);
    }

}
