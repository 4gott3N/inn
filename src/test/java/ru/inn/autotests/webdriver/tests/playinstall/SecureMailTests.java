package ru.inn.autotests.webdriver.tests.playinstall;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import ru.inn.autotests.webdriver.common.PrepareUser;
import ru.inn.autotests.webdriver.common.entity.Message;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.common.os.WinRegistry;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.composite.common.Captcha;
import ru.inn.autotests.webdriver.composite.pages.GamePanelPage;
import ru.inn.autotests.webdriver.composite.pages.LoginPage;
import ru.inn.autotests.webdriver.composite.pages.SettingsPage;
import ru.inn.autotests.webdriver.composite.pages.UserBarPage;
import ru.inn.autotests.webdriver.composite.popups.PopupSecureCode;
import ru.inn.autotests.webdriver.tests.AbstractTest;
import ru.inn.autotests.webdriver.toolkit.LocalDriverManager;
import ru.inn.autotests.webdriver.toolkit.MailProvider;
import ru.inn.autotests.webdriver.toolkit.PlatformUtils;
import ru.inn.autotests.webdriver.toolkit.ProcessProvider;


public class SecureMailTests extends AbstractTest {

    public static String PATH_TO_KEY = "SOFTWARE\\Wow6432Node\\4game\\4gameservice";
    public static String NAME_OF_KEY = "appId";
    User user;
	PrepareUser prepareUser= new PrepareUser();

    @BeforeClass
    public void initUser() {
        user = switcherLocation(TypeUser.NEW_USER);
    	
    }

    @BeforeMethod
    public void startService() {
        ProcessProvider.run4GameServiceProcess();
        prepareUser.switchSuperSecurity(user, true);
    }
    
    @AfterMethod
    public void switchUberSecurityOff() {
        prepareUser.switchSuperSecurity(user, true);
    }

    //@Test(description = "Go to game and check play", priority = 0)
  /*  public void testPreconditionSecurity() {

        log4j.info("Test started for game " + game.getGameName());
        GamePanelPage gpp = new GamePanelPage(game, language);
        LocalDriverManager.getDriverController().deleteAllCookies();
        gpp.openPage(gpp);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");

        log4j.info("Login was successful");
        gpp.waitToPlayGame(true);
    }*/

    @Test(description = "The test to check that new user can switch on the supersecurity without adding the computer to the 'trusted list'", priority = 0)
    public void testSwitchOnSupersecurityWithoutTrusted() throws Exception {
    	log4j.info("The test to check that new user can switch on the supersecurity without adding the computer to the 'trusted list'");
    	prepareUser.deleteMobileNumber(user);
    	prepareUser.switchSuperSecurity(user, false);
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.resetCacheApi();
        LoginPage lp = new LoginPage(game);
        
        lp.openUrlIn4game("/settings/security/");
        //lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized");
    	
		SettingsPage sp = new SettingsPage(language);
		Assert.assertTrue(sp.validateElementVisible(SettingsPage.BUTTON_FIRST_SUPER_SECURITY_ON), "We are not on the Secutity page or it's already on. BUTTON_FIRST_SUPER_SECURITY_ON is not visible");
		Assert.assertTrue(sp.validateElementVisible(SettingsPage.BUTTON_SECOND_SUPER_SECURITY_ON), "BUTTON_SECOND_SUPER_SECURITY_ON is not visible");
		Assert.assertTrue(sp.validateElementVisible(SettingsPage.DIV_COMICS), "DIV_COMICS is not visible on the Security page");
		sp.click(SettingsPage.BUTTON_SECOND_SUPER_SECURITY_ON);
		Captcha captcha = new Captcha();
		captcha.fillCaptchaIfItPresent();
		
		Assert.assertTrue(sp.validateElementVisible(SettingsPage.LINK_USE_MAIL), "LINK_USE_MAIL is not visible in the form 'Switch on the UbSec'");
		sp.click(SettingsPage.LINK_USE_MAIL);
		Assert.assertTrue(sp.validateElementVisible(SettingsPage.BUTTON_SEND_MAIL_CODE), "BUTTON_SEND_MAIL_CODE is not visible in the form 'Switch  the UbSec on through mail'");
		int num = MailProvider.getNumberOfMessages(user.getMainEmail());
		sp.click(SettingsPage.BUTTON_SEND_MAIL_CODE);
		captcha.fillCaptchaIfItPresent();
		Assert.assertTrue(sp.validateElementVisible(SettingsPage.FIELD_CODE_UBER_SECURITY), "FIELD_CODE_UBER_SECURITY is not visible in the form 'Switch  the UbSec on through mail'");
		Assert.assertTrue(MailProvider.isNewMessagePresent(user.getMainEmail(), num));
        Message mess = MailProvider.getLastMessage(user.getMainEmail());
		sp.type(SettingsPage.FIELD_CODE_UBER_SECURITY, mess.getCode());
		//Assert.assertTrue(sp.validateElementVisible(SettingsPage.CHECK_ADD_TRUSTED), "CHECK_ADD_TRUSTED is not visible in the form 'Switch the UbSec on through mail'");
		sp.click(SettingsPage.CHECK_ADD_TRUSTED);
		Assert.assertTrue(sp.validateElementVisible(SettingsPage.BUTTON_SWITCH_ON_UBER_SECURITY), "BUTTON_SWITCH_ON_UBER_SECURITY is not visible in the form 'Switch  the UbSec on through mail'");
		sp.click(SettingsPage.BUTTON_SWITCH_ON_UBER_SECURITY);
		Assert.assertTrue(sp.validateElementIsNotVisible(SettingsPage.DIV_COMICS), "DIV_COMICS is still visible after the switching on a super security");
		Assert.assertTrue(sp.validatElementContainsText(SettingsPage.FORM_TRUSTED_COMPUTERS, "Mozila Firefox"),  "The computer was not added to the 'Trusted computers' list. There is no any 'Mozila Firefox' in the list");
		//Assert.assertTrue(sp.validateElementVisible(SettingsPage.CHECKBOX_TRUSTED_COMPUTER), "The computer was not added to the 'Trusted computers' list. CHECKBOX_TRUSTED_COMPUTER is not visible");
		
		//UserBarPage ub = new UserBarPage();
		//sp.click(UserBarPage.BUTTON_USER_DATA_ARROW);
		//Assert.assertTrue(sp.validateElementVisible(UserBarPage.LINK_PERSONAL_DATA), "The LINK 'PERSONAL_DATA' is not visible in user menu");
		//Assert.assertTrue(sp.validateElementIsNotVisible(UserBarPage.BUTTON_SUPER_SECURITY_ON), "The BUTTON_SUPER_SECURITY_ON in UserBar is visible for user who already switched supersecurity on ");
		
		gpp.openPage(gpp);
		gpp.waitToPlayGame(true);
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_PLAY), "The button 'Play' is not visible on the panel 'play'");
		gpp.click(GamePanelPage.BUTTON_PLAY);
        gpp.clickPopupIfItPresent(game);
        captcha.fillCaptchaIfItPresent();
        Assert.assertTrue(gpp.validateElementPresent(PopupSecureCode.INPUT_CODE),
                "The form Supersecurity with field for secure code is not visible.");
        Assert.assertNotNull(WinRegistry.readString(WinRegistry.HKEY_LOCAL_MACHINE, PATH_TO_KEY, NAME_OF_KEY));
    }

    @Test(description = "The Auto connection for game with SuperSecurity does not exist", enabled=false)
    public void testNotAutoConnectSecurity() throws Exception {

    	log4j.info("The Auto connection for game with SuperSecurity does not exist");
        GamePanelPage gpp = new GamePanelPage(game, language);
        LoginPage lp = new LoginPage(game);
        
        gpp.openPage(gpp);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        gpp.waitToPlayGame(true);
        int num = MailProvider.getNumberOfMessages(user.getMainEmail());
        gpp.click(GamePanelPage.BUTTON_PLAY);
        gpp.clickPopupIfItPresent(game);
        Captcha captcha = new Captcha();
        captcha.fillCaptchaIfItPresent();
        
        captcha.fillCaptchaIfItPresent();
        Assert.assertTrue(gpp.validateElementPresent(PopupSecureCode.INPUT_CODE),
                "The form Supersecurity with field for secure code is not visible.");
        log4j.info("Email form has opened");
        Assert.assertTrue(MailProvider.isNewMessagePresent(user.getMainEmail(), num));
        Message mess = MailProvider.getLastMessage(user.getMainEmail());
        gpp.type(PopupSecureCode.INPUT_CODE, mess.getCode());
        //if (OperationsHelper.baseUrl.contains("test")) captcha.typeCaptchaIfPresent("987654");
        //else 
        captcha.fillCaptchaIfItPresent();
        Assert.assertTrue(lp.validateElementVisible(PopupSecureCode.BUTTON_CONFIRM_AND_PLAY), "The button BUTTON_CONFIRM_AND_PLAY is not visible");
        gpp.click(PopupSecureCode.BUTTON_CONFIRM_AND_PLAY);
        Assert.assertTrue(gpp.validateElementIsNotVisible(PopupSecureCode.FORM_SECURE_CODE),
                "The form FORM_SECURE_CODE is still visible after entering a code");
        //DIV_MESSAGE_ERROR
        
        Assert.assertTrue(PlatformUtils.detectGameProcess(game, 30),
                "Game " + game.getGameName() + " process wasn't launched");
        PlatformUtils.killGame(game);
        
        gpp.click(GamePanelPage.BUTTON_PLAY);
        captcha.fillCaptchaIfItPresent();
        Assert.assertTrue(gpp.validateElementIsNotVisible(PopupSecureCode.FORM_SECURE_CODE),
                "The form FORM_SECURE_CODE is still visible after for the trusted computer");
        Assert.assertTrue(PlatformUtils.detectGameProcess(game, 30),
                "Game " + game.getGameName() + " process wasn't launched. The game wasn't launched for the trusted computer");
        PlatformUtils.killGame(game);
        Assert.assertNotNull(WinRegistry.readString(WinRegistry.HKEY_LOCAL_MACHINE, PATH_TO_KEY, NAME_OF_KEY));
    }

    @Test(description = "The test to Check that the button 'SET_PASSWORD' is not present for game", priority = 1)
    public void testNoPassForGame() throws Exception {
    	log4j.info("The test to Check that the button 'SET_PASSWORD' is not present for game");
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openPage(gpp);
        gpp.click(GamePanelPage.BUTTON_SETTINGS);
        Assert.assertTrue(gpp.validateElementIsNotVisible(GamePanelPage.BUTTON_SET_PASSWORD),
                "The button 'SET_PASSWORD' is present in game panel for game with supersecurity");
        log4j.info("Set password for game " + game.getGameName() + " is disabled");
    }

    @Test(description = "The test to Check that user can add computer to the list 'trusted comuter' through email", priority = 3)
    public void testMailSecurity() throws Exception {
    	log4j.info("The test to Check that user can add computer to the list 'trusted comuter' through email");
        int num = MailProvider.getNumberOfMessages(user.getMainEmail());
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openPage(gpp);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        gpp.waitToPlayGame(true);
        gpp.click(GamePanelPage.BUTTON_PLAY);
        gpp.clickPopupIfItPresent(game);
        Captcha captcha = new Captcha();
        //if (OperationsHelper.baseUrl.contains("test")) captcha.typeCaptchaIfPresent("987654");
        //else 
        captcha.fillCaptchaIfItPresent();
        Assert.assertTrue(gpp.validateElementPresent(PopupSecureCode.FORM_SECURE_CODE),
                "The form Supersecurity with field for secure code is not visible.");
        log4j.info("Email form has opened");
        Assert.assertTrue(MailProvider.isNewMessagePresent(user.getMainEmail(), num));
        Message mess = MailProvider.getLastMessage(user.getMainEmail());
        gpp.type(PopupSecureCode.INPUT_CODE, mess.getCode());
        //if (OperationsHelper.baseUrl.contains("test")) captcha.typeCaptchaIfPresent("987654");
        //else 
        captcha.fillCaptchaIfItPresent();
        Assert.assertTrue(lp.validateElementVisible(PopupSecureCode.BUTTON_CONFIRM_AND_PLAY), "The button BUTTON_CONFIRM_AND_PLAY is not visible");
        gpp.click(PopupSecureCode.BUTTON_CONFIRM_AND_PLAY);
        Assert.assertTrue(gpp.validateElementIsNotVisible(PopupSecureCode.FORM_SECURE_CODE),
                "The form FORM_SECURE_CODE is still visible after entering a code");
        Assert.assertTrue(PlatformUtils.detectGameProcess(game, 30),
                "Game " + game.getGameName() + " process wasn't launched");
        log4j.info("Game " + game.getGameName() + " is launch");
        PlatformUtils.killGame(game);
        // prepareUser.unlinkApp(user);

        ProcessProvider.close4gameProcess();
        WinRegistry.deleteValue(WinRegistry.HKEY_LOCAL_MACHINE, PATH_TO_KEY, NAME_OF_KEY);


    }

    @Test(description = "The test to Check that user can't add computer to the list 'trusted comuter' entering the incorrect code", priority = 3, enabled = false)
    public void testIncorrectMailSecurity() throws Exception {
    	log4j.info("The test to Check that user can't add computer to the list 'trusted comuter' entering the incorrect code");
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openPage(gpp);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        gpp.waitToPlayGame(true);
        gpp.click(GamePanelPage.BUTTON_PLAY);
        gpp.clickPopupIfItPresent(game);
        Captcha captcha = new Captcha();
        captcha.fillCaptchaIfItPresent();
        Assert.assertTrue(gpp.validateElementPresent(PopupSecureCode.FORM_SECURE_CODE),
                "The form Supersecurity with field for secure code is not visible.");
        gpp.type(PopupSecureCode.INPUT_CODE, "1111");
        log4j.info("Input incorrect code");
        captcha.fillCaptchaIfItPresent();
        captcha.typeCaptchaIfPresent("987654");
        gpp.click(PopupSecureCode.BUTTON_CONFIRM_AND_PLAY);
        Assert.assertTrue(gpp.validateElementPresent(PopupSecureCode.CAPTCHA_FIELD),
                "The captcha is not visible after typing incorrect security code");
        log4j.info("Captcha is enabled");
    }

    @Test(description = "The test to Check that user can send another one email with the code", priority = 3)
    public void testMailRepeatSecurity() throws Exception {
    	log4j.info("The test to Check that user can send another one email with the code");
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openPage(gpp);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        int num = MailProvider.getNumberOfMessages(user.getMainEmail());
        gpp.waitToPlayGame(true);
        gpp.click(GamePanelPage.BUTTON_PLAY);
        gpp.clickPopupIfItPresent(game);
        Captcha captcha = new Captcha();
        captcha.fillCaptchaIfItPresent();
        Assert.assertTrue(gpp.validateElementPresent(PopupSecureCode.LINK_SEND_AGAIN),
                "The form Supersecurity with field for secure code is not visible. LINK_SEND_AGAIN is not visible");
        captcha.fillCaptchaIfItPresent();
        Assert.assertTrue(MailProvider.isNewMessagePresent(user.getMainEmail(), num));
        num = MailProvider.getNumberOfMessages(user.getMainEmail());
        Assert.assertTrue(gpp.validateElementPresent(PopupSecureCode.LINK_SEND_AGAIN),
                "The form Supersecurity with field for secure code is not visible. LINK_SEND_AGAIN is not visible");
        gpp.click(PopupSecureCode.LINK_SEND_AGAIN);
        log4j.info("Click to Send Message again");
        captcha.fillCaptchaIfItPresent();
        Assert.assertTrue(MailProvider.isNewMessagePresent(user.getMainEmail(), num));
        log4j.info("New message has come");
    }



}
