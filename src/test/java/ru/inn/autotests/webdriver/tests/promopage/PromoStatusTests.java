package ru.inn.autotests.webdriver.tests.promopage;

import org.testng.annotations.Test;
import ru.inn.autotests.webdriver.composite.pages.GameCell;
import ru.inn.autotests.webdriver.composite.pages.GamePanelPage;
import ru.inn.autotests.webdriver.composite.pages.MainPage;
import ru.inn.autotests.webdriver.composite.pages.UserBarPage;
import ru.inn.autotests.webdriver.games.*;
import ru.inn.autotests.webdriver.tests.AbstractTest;
import ru.inn.autotests.webdriver.toolkit.PlatformUtils;

/**
 * User: pavel.popov
 */
public class PromoStatusTests extends AbstractTest {


    @Test
    public void testStatusProgress(){
        Game game = new PointBlank();
        MainPage mPage = new MainPage();
        mPage.openPage(mPage);

        GameCell gameCell = new GameCell(game);

        PlatformUtils.sendStatus(PlatformUtils.Status.PROGRESS, game,isPlugin);

        gameCell.assertText(gameCell.gameTitle, Game.GameName.getGameName(game.getGameName()),language);
        gameCell.validateStatus(gameCell.gameStatus, "$Promolist_progress");
        gameCell.validateElementPresent(gameCell.gameProgress);
    }

    @Test
    public void testStatusOnPause(){
        Game game = new Aion();
        MainPage mPage = new MainPage();
        mPage.openPage(mPage);

        GameCell gameCell = new GameCell(game);

        PlatformUtils.sendStatus(PlatformUtils.Status.PAUSED, game,isPlugin);

        gameCell.assertText(gameCell.gameTitle, Game.GameName.getGameName(game.getGameName()),language);
        gameCell.validateOnPauseStatus(gameCell.gameStatusOnPause, "$Promolist_progress_onpause");
        gameCell.validateElementIsNotVisible(gameCell.gameStatusDownloadedSpeed);
        gameCell.validateElementPresent(gameCell.gameProgress);
    }

    /** Test checks status unpacking */
    @Test
    public void testStatusUnpacking(){
        Game game = new Lineage2();
        MainPage mPage = new MainPage();
        mPage.openPage(mPage);

        GameCell gameCell = new GameCell(game);

        PlatformUtils.sendStatus(PlatformUtils.Status.UNPACKING, game,isPlugin);

        gameCell.assertText(gameCell.gameTitle, Game.GameName.getGameName(game.getGameName()),language);
        gameCell.validateStatus(gameCell.gameStatus, "$Promolist_progress");
        gameCell.assertText(gameCell.gameProgressUnpacking, "$Promolist_progress_unpacking",language);
        gameCell.validateElementIsNotVisible(gameCell.gameStatusDownloaded);
        gameCell.validateElementIsNotVisible(gameCell.gameStatusDownloadedSize);

        gameCell.validateElementPresent(gameCell.gameProgress);
    }

    /** Test checks, that a status, which sets from the main page, has validate information in the GamePanel */
    @Test
    public void testCheckStatusInGamePanel(){
        Game game = new RFOnline();
        MainPage mPage = new MainPage();
        mPage.openPage(mPage);

        GameCell gameCell = new GameCell(game);

        PlatformUtils.sendStatus(PlatformUtils.Status.PROGRESS, game,isPlugin);

        gameCell.assertText(gameCell.gameTitle, Game.GameName.getGameName(game.getGameName()),language);
        gameCell.validateStatus(gameCell.gameStatus, "$Promolist_progress");

        gameCell.click(gameCell.gameTitle);

        GamePanelPage gpp = new GamePanelPage(game);

        gpp.validateElementPresent(GamePanelPage.PROGRESS_BAR);


    }

    /** Test checks, that a status has validate information in the GameCell to MainPage */
    @Test
    public void testCheckStatusInGameCell(){
        Game game = new APB();

        GamePanelPage gpp = new GamePanelPage(game);
        gpp.openPage(gpp);

        PlatformUtils.sendStatus(PlatformUtils.Status.PAUSED, game,isPlugin);

        gpp.validateElementPresent(GamePanelPage.PROGRESS_BAR_ON_PAUSE);

        UserBarPage userbar = new UserBarPage();
        userbar.click(UserBarPage.LINK_HOME);

        GameCell gameCell = new GameCell(game);

        gameCell.assertText(gameCell.gameTitle, Game.GameName.getGameName(game.getGameName()),language);
        gameCell.validateOnPauseStatus(gameCell.gameStatusOnPause, "$Promolist_progress_onpause");

    }

    /**
     * Test checks that after click to 'back' in the browser, can view information in the GameCell
     */
    @Test
    public void testNavigateBack(){
        Game game = new APB();

        MainPage mPage = new MainPage();
        mPage.openPage(mPage);

        GameCell gameCell = new GameCell(game);
        gameCell.click(gameCell.linkGameCellWithoutProgress);

        PlatformUtils.sendStatus(PlatformUtils.Status.UNPACKING, game,isPlugin);

        GamePanelPage gpp = new GamePanelPage(game);

        gpp.validateElementPresent(GamePanelPage.PROGRESS_BAR);


        gpp.navigateBack();

        gameCell.assertText(gameCell.gameTitle, Game.GameName.getGameName(game.getGameName()),language);
        gameCell.validateStatus(gameCell.gameStatus, "$Promolist_progress");
    }

}
