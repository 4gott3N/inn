package ru.inn.autotests.webdriver.tests.playpage;

import org.openqa.selenium.Cookie;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.inn.autotests.webdriver.common.PrepareUser;
import ru.inn.autotests.webdriver.common.entity.Message;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.composite.pages.*;
import ru.inn.autotests.webdriver.composite.popups.PopupPremium;
import ru.inn.autotests.webdriver.games.Game;
import ru.inn.autotests.webdriver.games.Lineage2eu;
import ru.inn.autotests.webdriver.games.PlanetSide2;
import ru.inn.autotests.webdriver.tests.AbstractTest;
import ru.inn.autotests.webdriver.toolkit.LocalDriverManager;
import ru.inn.autotests.webdriver.toolkit.MailProvider;
import ru.inn.autotests.webdriver.toolkit.UserData;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;



public class BuySerialCodeTest extends AbstractTest {

	String pageSims = "/sims4/";
	String pageGta = "/gta5/";
	
	 @DataProvider
	    public Iterator<Object[]> initPayments(){
	        List<Object[]> data = new ArrayList<Object[]>();
	        for(PaymentTerminal.PaymentMethod method: paymentsMethods)
	        {
	            data.add(new Object[]{method});
	        }
	       return data.iterator();
	    }

    @Test(description = "Check that user can buy Sims without discount ")
    public void testBuySerialCode() {
        log4j.info("Test Check that user can buy Sims without discount");
        //OperationsHelper.baseUrl = "https://sims4-ru.4gametest.com/";
        User  user=switcherLocation(TypeUser.USER);
        BuySerialCodePage bsc = new BuySerialCodePage();
        int num = MailProvider.getNumberOfMessages(user.getMainEmail());
        bsc.openUrlIn4game(pageSims);
        Assert.assertTrue(bsc.validateElementVisible(BuySerialCodePage.BUTTON_BUY), "The button to buy a serial code is not visible on the page " + bsc.getCurrentUrl());
        bsc.mouseClick(BuySerialCodePage.BUTTON_BUY);
        LoginPage lp = new LoginPage(game);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(bsc.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        UserBarPage ubp=new UserBarPage();
        Assert.assertTrue(bsc.validateElementVisible(BuySerialCodePage.DIV_SUM_WITHOUT_DISCOUNT), "The sum for this edition is not visible on the page " + bsc.getCurrentUrl());
        double oldtUserSum=ubp.getCurrentUserMoney();
        double sumWithoutDiscount = bsc.getNumber(BuySerialCodePage.DIV_SUM_WITHOUT_DISCOUNT);
        Assert.assertTrue(bsc.validateElementVisible(BuySerialCodePage.BUTTON_BUY), "The button to buy a serial code is not visible on the page " + bsc.getCurrentUrl());
        bsc.mouseClick(BuySerialCodePage.BUTTON_BUY);
        Assert.assertTrue(bsc.validateElementVisible(BuySerialCodePage.FIELD_SERIAL_CODE), "The field with serial code is not visible after buying it on the page " + bsc.getCurrentUrl());
        String serialCode = bsc.getText(BuySerialCodePage.FIELD_SERIAL_CODE);
        Assert.assertTrue(bsc.validateElementVisible(BuySerialCodePage.DIV_HOW_TO_GET), "The link to open a tooltip with gift code is not visible after buying a serial code on the page " + bsc.getCurrentUrl());
        bsc.mouseClick(BuySerialCodePage.DIV_HOW_TO_GET);
        Assert.assertTrue(bsc.validateElementVisible(BuySerialCodePage.DIV_GIFT_CODE), "The div with gift code is not visible after buying the serial code on the page " + bsc.getCurrentUrl());
        String giftCode = bsc.getText(BuySerialCodePage.FIELD_SERIAL_CODE);
        Assert.assertTrue(bsc.validateElementVisible(BuySerialCodePage.BUTTON_INSTALL_AION), "The button BUTTON_INSTALL_AION is not visible after buying the serial code on the page " + bsc.getCurrentUrl());
        ubp.assertUserSum(oldtUserSum,-(sumWithoutDiscount-100));
        Assert.assertTrue(MailProvider.isNewMessagePresent(user.getMainEmail(), num),"new message about buying serial code is not present in "+user.getMainEmail());
        Message message = MailProvider.getLastMessage(user.getMainEmail());
        message.assertTextPresent(giftCode);
        message.assertTextPresent(serialCode);
       }
    
    @Test(description = "Check that user can preorder GTA")
    public void testBuyPreorder() {
        log4j.info("Test to Check that user can preorder GTA");
        User  user=switcherLocation(TypeUser.USER);
        BuySerialCodePage bsc = new BuySerialCodePage();
        int num = MailProvider.getNumberOfMessages(user.getMainEmail());
        bsc.openUrlIn4game("");
        bsc.click(UserBarPage.BUTTON_SIGN_UP);
        LoginPage lp = new LoginPage(game);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(bsc.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        UserBarPage ubp=new UserBarPage();
        
        bsc.openUrlIn4game(pageGta);
        Assert.assertTrue(bsc.validateElementVisible(BuySerialCodePage.DIV_SUM_WITHOUT_DISCOUNT), "The sum for this edition is not visible on the page " + bsc.getCurrentUrl());
        double oldtUserSum=ubp.getCurrentUserMoney();
        double sumWithoutDiscount = bsc.getNumber(BuySerialCodePage.DIV_SUM_WITHOUT_DISCOUNT);
        Assert.assertTrue(bsc.validateElementVisible(BuySerialCodePage.BUTTON_BUY), "The button to buy a serial code is not visible on the page " + bsc.getCurrentUrl());
        bsc.mouseClick(BuySerialCodePage.BUTTON_BUY);
        Assert.assertTrue(bsc.validateElementVisible(BuySerialCodePage.DIV_USER_MAIL_PREPRDER), "The div with an information about preorder is not visible after buying it on the page " + bsc.getCurrentUrl());
        Assert.assertTrue(bsc.validateElementVisible(BuySerialCodePage.DIV_HOW_TO_GET), "The link to open a tooltip with gift code is not visible after buying a serial code on the page " + bsc.getCurrentUrl());
        bsc.mouseClick(BuySerialCodePage.DIV_HOW_TO_GET);
        Assert.assertTrue(bsc.validateElementVisible(BuySerialCodePage.DIV_GIFT_CODE), "The div with gift code is not visible after buying the serial code on the page " + bsc.getCurrentUrl());
        String giftCode = bsc.getText(BuySerialCodePage.DIV_GIFT_CODE);
        Assert.assertTrue(bsc.validateElementVisible(BuySerialCodePage.BUTTON_INSTALL_APB), "The button BUTTON_INSTALL_APB is not visible after buying the serial code on the page " + bsc.getCurrentUrl());
        ubp.assertUserSum(oldtUserSum,-(sumWithoutDiscount-200));
        Assert.assertTrue(MailProvider.isNewMessagePresent(user.getMainEmail(), num),"new message about buying serial code is not present in "+user.getMainEmail());
        Message message = MailProvider.getLastMessage(user.getMainEmail());
        message.assertTextPresent(giftCode);
       }
  
    @Test(description = "Check that user can buy Sims without discount ", enabled=true)
    public void testBuySerialCodeWithDiscount() {
        log4j.info("Test Check that user can buy Sims with discount");
        User  user=switcherLocation(TypeUser.USER);
        BuySerialCodePage bsc = new BuySerialCodePage();
        int num = MailProvider.getNumberOfMessages(user.getMainEmail());
        bsc.openUrlIn4game("");
        bsc.click(UserBarPage.BUTTON_SIGN_UP);
        LoginPage lp = new LoginPage(game);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(bsc.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        UserBarPage ubp=new UserBarPage();
        bsc.openUrlIn4game(pageSims);
        Assert.assertTrue(bsc.validateElementVisible(BuySerialCodePage.DIV_SUM_WITHOUT_DISCOUNT), "The sum for this edition is not visible on the page " + bsc.getCurrentUrl());
        Assert.assertTrue(bsc.validateElementVisible(BuySerialCodePage.LINK_ENTER_DISCOUNT_CODE), "The link to open field 'discount code' is not visible on the page  " + bsc.getCurrentUrl());
        bsc.click(BuySerialCodePage.LINK_ENTER_DISCOUNT_CODE);
        Assert.assertTrue(bsc.validateElementVisible(BuySerialCodePage.FIELD_DISCOUNT_CODE), "The field for typing discount code is not visible on the page " + bsc.getCurrentUrl());
        bsc.type(BuySerialCodePage.FIELD_DISCOUNT_CODE, "lepra");
        //bsc.submit(BuySerialCodePage.FIELD_DISCOUNT_CODE);
        Assert.assertTrue(bsc.validateElementVisible(BuySerialCodePage.DIV_SUM_WITH_DISCOUNT), "The sum for this edition with discount is not visible on the page " + bsc.getCurrentUrl());
        double oldtUserSum=ubp.getCurrentUserMoney();
        double sumWithDiscount = bsc.getNumber(BuySerialCodePage.DIV_SUM_WITH_DISCOUNT);
        Assert.assertTrue(bsc.validateElementVisible(BuySerialCodePage.BUTTON_BUY), "The button to buy a serial code is not visible on the page " + bsc.getCurrentUrl());
        bsc.mouseClick(BuySerialCodePage.BUTTON_BUY);
        Assert.assertTrue(bsc.validateElementVisible(BuySerialCodePage.FIELD_SERIAL_CODE), "The field with serial code is not visible after buying it on the page " + bsc.getCurrentUrl());
        String serialCode = bsc.getText(BuySerialCodePage.FIELD_SERIAL_CODE);
        Assert.assertTrue(bsc.validateElementVisible(BuySerialCodePage.DIV_HOW_TO_GET), "The link to open a tooltip with gift code is not visible after buying a serial code on the page " + bsc.getCurrentUrl());
        bsc.mouseClick(BuySerialCodePage.DIV_HOW_TO_GET);
        Assert.assertTrue(bsc.validateElementVisible(BuySerialCodePage.DIV_GIFT_CODE), "The div with gift code is not visible after buying the serial code on the page " + bsc.getCurrentUrl());
        String giftCode = bsc.getText(BuySerialCodePage.FIELD_SERIAL_CODE);
        Assert.assertTrue(bsc.validateElementVisible(BuySerialCodePage.BUTTON_INSTALL_AION), "The button BUTTON_INSTALL_AION is not visible after buying the serial code on the page " + bsc.getCurrentUrl());
        //Assert.assertTrue(bsc.validateElementVisible(BuySerialCodePage.FIELD_SERIAL_CODE), "The field with serial code is not visible after refreshing page " + bsc.getCurrentUrl());
        ubp.assertUserSum(oldtUserSum,-sumWithDiscount);
        Assert.assertTrue(MailProvider.isNewMessagePresent(user.getMainEmail(), num),"new message about buying serial code is not present in "+user.getMainEmail());
        Message message = MailProvider.getLastMessage(user.getMainEmail());
        message.assertTextPresent(giftCode);
        message.assertTextPresent(serialCode);
    }
    
    @Test(dataProvider = "initPayments",description = "The Test to Check that user will have to replenish an account to buy Sims ", enabled=true)
    public void testBuySerialCodeWithoutMoney(PaymentTerminal.PaymentMethod method) {
        log4j.info("The Test to Check that user will have to replenish an account to buy Sims ");
        User  user=switcherLocation(TypeUser.USER_NO_MONEY);
        BuySerialCodePage bsc = new BuySerialCodePage();
        //int num = MailProvider.getNumberOfMessages(user.getMainEmail());
        bsc.openUrlIn4game("");
        bsc.click(UserBarPage.BUTTON_SIGN_UP);
        LoginPage lp = new LoginPage(game);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(bsc.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        UserBarPage ubp=new UserBarPage();
        bsc.openUrlIn4game(pageSims);
        Assert.assertTrue(bsc.validateElementVisible(BuySerialCodePage.DIV_SUM_WITHOUT_DISCOUNT), "The sum for this edition is not visible on the page " + bsc.getCurrentUrl());
        double oldtUserSum=ubp.getCurrentUserMoney();
        double sumWithoutDiscount = bsc.getNumber(BuySerialCodePage.DIV_SUM_WITHOUT_DISCOUNT);
        Assert.assertTrue(bsc.validateElementVisible(BuySerialCodePage.BUTTON_BUY), "The button to buy a serial code is not visible on the page " + bsc.getCurrentUrl());
        bsc.mouseClick(BuySerialCodePage.BUTTON_BUY);
        PaymentTerminal ptp = new PaymentTerminal();
        Assert.assertTrue(ptp.validateElementVisible(PaymentTerminal.FORM_PAYMENT_TERMINAL), "Payment terminal is not visible after clicking 'Buy premium' for user who doesn't have money");
        double neededSum = sumWithoutDiscount-oldtUserSum;
		ptp.assertSumInTerminal(neededSum);
        ptp.replenishAccount(method, user, neededSum);
        Assert.assertTrue(ptp.validateElementPresent(UserBarPage.DIV_USER_BAR),"We didn't return to the 4game after replenishing. BALANCE_PANEL is not present. " + method);
        ptp.openCorrectLinkOnDev();
        ubp.assertUserSum(oldtUserSum, neededSum);
        oldtUserSum=ubp.getCurrentUserMoney();
        Assert.assertTrue(ptp.getCurrentUrl().contains(pageSims),"We didn't return to the correct play page after replenishing. " + pageSims);
        Assert.assertTrue(bsc.validateElementVisible(BuySerialCodePage.BUTTON_BUY), "The button to buy a serial code is not visible on the page " + bsc.getCurrentUrl());
        bsc.mouseClick(BuySerialCodePage.BUTTON_BUY);
        Assert.assertTrue(bsc.validateElementVisible(BuySerialCodePage.FIELD_SERIAL_CODE), "The field with serial code is not visible after buying it on the page " + bsc.getCurrentUrl());
        //String serialCode = bsc.getText(BuySerialCodePage.FIELD_SERIAL_CODE);
        //Assert.assertTrue(bsc.validateElementVisible(BuySerialCodePage.DIV_GIFT_CODE), "The div with gift code is not visible after buying the serial code on the page " + bsc.getCurrentUrl());
        //String giftCode = bsc.getText(BuySerialCodePage.FIELD_SERIAL_CODE);
        //Assert.assertTrue(bsc.validateElementVisible(BuySerialCodePage.BUTTON_INSTALL_AION), "The button BUTTON_INSTALL_AION is not visible after buying the serial code on the page " + bsc.getCurrentUrl());
        //Assert.assertTrue(bsc.validateElementVisible(BuySerialCodePage.FIELD_SERIAL_CODE), "The field with serial code is not visible after refreshing page " + bsc.getCurrentUrl());
        ubp.assertUserSum(oldtUserSum,-(sumWithoutDiscount-100));
       }
   
}
