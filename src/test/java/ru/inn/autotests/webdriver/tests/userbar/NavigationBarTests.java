package ru.inn.autotests.webdriver.tests.userbar;


import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.composite.pages.*;
import ru.inn.autotests.webdriver.games.*;
import ru.inn.autotests.webdriver.tests.AbstractTest;
import ru.inn.autotests.webdriver.toolkit.GameData;
import ru.inn.autotests.webdriver.toolkit.LocalDriverManager;
import ru.inn.autotests.webdriver.toolkit.PlatformUtils;
import ru.inn.autotests.webdriver.toolkit.PlatformUtils.Status;

import java.util.ArrayList;


public class NavigationBarTests extends AbstractTest {
    @BeforeClass
    public void initGame() {
        //if (System.getenv("plugin") != null)
          //  isPlugin = Boolean.parseBoolean(System.getenv("plugin"));
    	//OperationsHelper.baseUrl="https://react-userbar-ru.4gametest.com/";
    }


    /**
     * Test navigation on nav bar.
     */
    @Test(description = "test to check correct navigation on nav bar", priority = 6)
    public void testNavigationOnNavBar() {
        log4j.info("test to check correct navigation on nav bar");
        //Game game = new Lineage2();
        MainPage mainPage = new MainPage();
        mainPage.openUrlIn4game(game.getURL() + "play");
        mainPage.scrollOnTop();
        mainPage.click(UserBarPage.getGameNavBarLink(game));
        Assert.assertTrue(mainPage.validateElementVisible(GamePanelPage.GAME_PANEL),
                "We are not on the game panel page. GAME_PANEL is not visible on the " + mainPage.getCurrentUrl());
        Assert.assertTrue(mainPage.getCurrentUrl().contains(game.getAppName()),
                "Navigation on navigation bar is not working");


    }

    /**
     * Test navigation on nav bar.
     */
    @Test(description = "test to check correct navigation on the balance nav bar", priority = 5)
    public void testNavigationBalanceNavBar() {
        log4j.info("test to check navigation via the balance nav bar started");
        User user = switcherLocation(TypeUser.HARDCORE_USER);
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        mainPage.click(UserBarPage.BUTTON_SIGN_UP);
        LoginPage lp = new LoginPage(game);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(mainPage.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD),
                "The user is not authorized.");
        UserBarPage ubp = new UserBarPage();
        ubp.click(UserBarPage.BUTTON_USER_BALANCE_DATA_ARROW);
        Assert.assertTrue(ubp.validateElementVisible(UserBarPage.LINK_GIFT_CODES),
                "The link GIFT_CODES is not visible in the userbar");
        ubp.click(UserBarPage.LINK_GIFT_CODES);
        GiftCodePage gcp = new GiftCodePage();
        Assert.assertTrue(gcp.validateElementVisible(GiftCodePage.FORM_GIFT_CODES),
                "We are not on the Gifts code page after clicking on the link in user menu");
        ubp.click(UserBarPage.BUTTON_USER_BALANCE_DATA_ARROW);
        Assert.assertTrue(ubp.validateElementVisible(UserBarPage.LINK_CREDIT),
                "The link LINK_CREDIT is not visible in the userbar");
        ubp.click(UserBarPage.LINK_CREDIT);
        Assert.assertTrue(ubp.validateElementVisible(GiftCodePage.DIV_CREDIT_ALLOWED_SUM),
                "We are not on the Credit page after clicking on the link in user menu");
        ubp.click(UserBarPage.BUTTON_USER_BALANCE_DATA_ARROW);
        Assert.assertTrue(ubp.validateElementVisible(UserBarPage.BUTTON_ADD_FUNDS),
                "The button ADD_FUNDS is not visible in the userbar");
        ubp.click(UserBarPage.BUTTON_ADD_FUNDS);
        Assert.assertTrue(ubp.validateElementVisible(PaymentTerminal.FORM_PAYMENT_TERMINAL),
                "The payment terminal is not visible after clicking on the button BUTTON_ADD_FUNDS");


    }

    /**
     * Test navigation via user bar.
     */
    @Test(description = "test to check correct navigation via user bar", priority = 4)
    public void testNavigationViaUserBar() {
        User user = switcherLocation(TypeUser.HARDCORE_USER);
        log4j.info("test to check navigation via user started");
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        mainPage.click(UserBarPage.BUTTON_SIGN_UP);
        LoginPage lp = new LoginPage(game);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(mainPage.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD),
                "The user is not authorized.");
        UserBarPage ubp = new UserBarPage();
        ubp.click(UserBarPage.BUTTON_USER_DATA_ARROW);
        Assert.assertTrue(mainPage.validateElementVisible(UserBarPage.LINK_SETTINGS),
                "The LINK 'Settings' is not visible in user menu");
        ubp.click(UserBarPage.LINK_SETTINGS);
        SettingsPage sp = new SettingsPage(language);
        Assert.assertTrue(sp.validateElementVisible(SettingsPage.FORM_SETTINGS_CONTENT),
                "We are not on the Settings page after clicking on the link in user menu");
        ubp.click(UserBarPage.BUTTON_USER_DATA_ARROW);
        Assert.assertTrue(mainPage.validateElementVisible(UserBarPage.LINK_PERSONAL_DATA),
                "The LINK 'PERSONAL_DATA' is not visible in user menu");
        ubp.click(UserBarPage.LINK_PERSONAL_DATA);
        Assert.assertTrue(sp.validateElementVisible(SettingsPage.FIELD_NAME_AND_SURNAME),
                "We are not on the PERSONAL_DATA page after clicking on the link in user menu");
        ubp.click(UserBarPage.BUTTON_USER_DATA_ARROW);
        Assert.assertTrue(mainPage.validateElementVisible(UserBarPage.LINK_NOTIFICATIONS),
                "The LINK 'LINK_NOTIFICATIONS' is not visible in user menu");
        ubp.click(UserBarPage.LINK_NOTIFICATIONS);
        Assert.assertTrue(sp.validateElementPresent(SettingsPage.CHECKBOX_SECURITY_MAIL),
                "We are not on the NOTIFICATIONS page after clicking on the link in user menu");
        ubp.click(UserBarPage.BUTTON_USER_DATA_ARROW);
/*
        Assert.assertTrue(mainPage.validateElementVisible(UserBarPage.LINK_SECURITY), "The link 'LINK_SECURITY' is not visible in user menu");
        ubp.click(UserBarPage.LINK_SECURITY);
        Assert.assertTrue(sp.validateElementVisible(SettingsPage.DIV_COMICS), "We are not on the Secutity page after clicking on the button in user menu. DIV_COMICS is not visible");
        ubp.click(UserBarPage.BUTTON_USER_DATA_ARROW);
        Assert.assertTrue(mainPage.validateElementVisible(UserBarPage.LINK_EVENTS), "The LINK 'LINK_EVENTS' is not visible in user menu");
        ubp.click(UserBarPage.LINK_EVENTS);
        Assert.assertTrue(sp.validateElementVisible(EventsPage.DIV_EVENT_NAVIGATION_PANEL), "We are not on the EVENTS page after clicking on the link in user menu");
        
        ubp.click(UserBarPage.BUTTON_USER_DATA_ARROW);
        Assert.assertTrue(mainPage.validateElementVisible(UserBarPage.BUTTON_SUPER_SECURITY_ON), "The button 'BUTTON_SUPER_SECURITY_ON' is not visible in user menu");
        ubp.click(UserBarPage.BUTTON_SUPER_SECURITY_ON);
        Assert.assertTrue(sp.validateElementVisible(SettingsPage.BUTTON_FIRST_SUPER_SECURITY_ON), "We are not on the Secutity page after clicking on the button in user menu");
*/
        Assert.assertTrue(mainPage.validateElementVisible(UserBarPage.LINK_EVENTS),
                "The LINK 'LINK_EVENTS' is not visible in user menu");
        ubp.click(UserBarPage.LINK_EVENTS);
        Assert.assertTrue(sp.validateElementVisible(EventsPage.DIV_EVENT_NAVIGATION_PANEL),
                "We are not on the EVENTS page after clicking on the link in user menu");

        ubp.click(UserBarPage.BUTTON_USER_DATA_ARROW);
        Assert.assertTrue(mainPage.validateElementVisible(UserBarPage.LINK_LOGOUT),
                "The LINK 'LINK_LOGOUT' is not visible in user menu");
        LocalDriverManager.getDriverController().deleteAllCookies();
    }

    /**
     * Test for maintenance status.
     */
    @Test(description = "test to check games with maintenance status", enabled = false)
    public void testForMaintenanceStatus() {
        log4j.info("test for maint status in nav bar started");
        MainPage mp = new MainPage();
        UserBarPage ubp = new UserBarPage();
        mp.openPage(mp);
        ubp.assertClassMaintenanceForAllGamesInNavBar(games, isPlugin);
    }


    /**
     * Test all games present in nav bar.
     */
    @Test(description = "test to check that all games are present in nav bar", enabled = false)
    public void testAllGamesPresentInNavBar() {
        log4j.info("test to check all games in nav bar started");
        MainPage mainPage = new MainPage();
        mainPage.openUrlIn4game(game.getURL() + "play");
        UserBarPage ubp = new UserBarPage();
        ubp.assertAllGamesInUserBar(games);
    }

    /**
     * Test check installed games.
     */
    @Test(description = "test to check position for installed games without plugin", priority = 3)
    public void testCheckInstalledGames() {
        log4j.info("test to check position for installed games without plugin started");
        MainPage mainPage = new MainPage();
        UserBarPage ubp = new UserBarPage();
        Game game = new PointBlank();
        mainPage.openUrlIn4game(game.getURL() + "play");
        PlatformUtils.sendStatus(PlatformUtils.Status.INSTALLED, game, isPlugin);
        ubp.assertInstalledGameIsInNavBar(game);

    }

    /**
     * Test check position games.
     */
    @Test(description = "test to check position for installed games with plugin", enabled = false)
    public void testCheckPositionGames() {
        log4j.info("test to check position for installed games with plugin started");
        MainPage mainPage = new MainPage();
        UserBarPage ubp = new UserBarPage();
        mainPage.openUrlIn4game(game.getURL() + "play");
        ArrayList<String> installedGames = GameData.getInstalledGames();
        ubp.assertInstalledGamesInNavBarWithPlugin(installedGames, games);

    }

    /**
     * Test yellow border.
     */
    @Test(description = "test to check yellow border game", priority = 1)
    public void testYellowBorder() {
        log4j.info("test to check yellow border game started");
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openUrlIn4game(game.getURL() + "play");
        gpp.setLocalStorage("userbar-mobile-promotion-shoved", "true");
        gpp.openUrlIn4game(game.getURL() + "play");
        Assert.assertTrue(gpp.validateElementVisible(String.format(UserBarPage.BORDER_YELLOW_FOR_ICON_NAV_BAR, game.getServiceId())),
                "Yellow border is not present on game page " + game.getAppName());
    }

    /**
     * Test hover not installed game.
     */
    @Test(description = "test to check tooltip for the game which in progress", priority = 7)
    public void testHoverGameInProgress() {
        log4j.info("test to check tooltip for the game which in progress");
        MainPage mainPage = new MainPage();
        Game game = new PointBlank();
        mainPage.openUrlIn4game(game.getURL() + "play");
        Assert.assertTrue(mainPage.validateElementVisible(String.format(UserBarPage.BORDER_YELLOW_FOR_ICON_NAV_BAR, game.getServiceId())),
                "Yellow border is not present on game page " + game.getAppName());
        Assert.assertTrue(mainPage.validateElementIsNotVisible(GamePanelPage.BUTTON_INSTALL_WITH_NO_PLUGIN),
    			"BUTTON_INSTALL_WITH_NO_PLUGIN is still visible. This machine is too slow");
        mainPage.sendPause(1);

        PlatformUtils.sendStatus(PlatformUtils.Status.PROGRESS, game, isPlugin);
        UserBarPage ubp = new UserBarPage();
        Assert.assertTrue(ubp.assertBadgeForGame(game, UserBarPage.BADGE_PROGRESS),
                "Game " + game.getAppName() + " does not have badge with progress");
        mainPage.moveToElement(UserBarPage.getGameNavBarLink(game));
        Assert.assertTrue(
                mainPage.validateElementVisible(String.format(UserBarPage.DIV_TOOLTIP_NAV_BAR, game.getServiceId())),
                "DIV_TOOLTIP_NAV_BAR is not present for game " + game.getAppName());


    }

    /**
     * Test badge game.
     */
    @Test(description = "test to check all badges for all statuses", priority = 0, dependsOnMethods = {"testHoverGameInProgress"} )
    public void testBadgeGame() {
        log4j.info("test to check all badges for all statuses started");
        MainPage mainPage = new MainPage();
        //mainPage.openPage(mainPage);
        Game game = new Lineage2();
        mainPage.openUrlIn4game(game.getURL() + "play");
        UserBarPage ubp = new UserBarPage();
        Assert.assertTrue(mainPage.validateElementIsNotVisible(GamePanelPage.BUTTON_INSTALL_WITH_NO_PLUGIN),
    			"BUTTON_INSTALL_WITH_NO_PLUGIN is still visible. This machine is too slow");
        PlatformUtils.sendStatus(PlatformUtils.Status.FULL_UPDATE_REQUIRED, game, isPlugin);
        mainPage.sendPause(1);
        Assert.assertTrue(ubp.assertBadgeForGame(game, UserBarPage.BADGE_UPDATE_REQUIRED),
                "Game " + game.getAppName() + " does not have badge with update");
        PlatformUtils.sendStatus(Status.PAUSED, game, isPlugin);
        Assert.assertTrue(ubp.assertBadgeForGame(game, UserBarPage.BADGE_PAUSED),
                "Game " + game.getAppName() + " does not have badge with pause");
        PlatformUtils.sendStatus(PlatformUtils.Status.REPAIR, game, isPlugin);
        Assert.assertTrue(ubp.assertBadgeForGame(game, UserBarPage.BADGE_PROGRESS),
                "Game " + game.getAppName() + " does not have badge with progress");
        PlatformUtils.sendStatus(PlatformUtils.Status.INSTALLED, game, isPlugin);
       // PlatformUtils.setMaintenance(game, isPlugin);
        //Assert.assertTrue(ubp.assertBadgeForGame(game, UserBarPage.BADGE_MAINTENANCE),
               // "Game " + game.getAppName() + " does not have badge with maintenance");

    }


    /**
     * Test change position icon with install.
     */
    @Test(description = "test to check that icon move from not installed to installed container", priority = 4)
    public void testChangePositionIconWithInstall() {
        log4j.info("test to check that icon move from not installed to installed container");
        MainPage mainPage = new MainPage();
        UserBarPage ubp = new UserBarPage();
        Game game = new Lineage2();
        mainPage.openUrlIn4game(game.getURL() + "play");
        PlatformUtils.sendStatus(PlatformUtils.Status.NOT_INSTALLED, game, isPlugin);
        ubp.assertUninstalledGameIsInNavBar(game);
        PlatformUtils.sendStatus(Status.INSTALLED, game, isPlugin);
        OperationsHelper.sendPause(1);
        ubp.assertInstalledGameIsInNavBar(game);
    }

    /**
     * Test text home button.
     */
    @Test(description = "test to check text from button in user bar 4game(last all games)", enabled=false)
    public void testTextHomeButton() {
        log4j.info("test to check text from button in user bar 4game(last all games) started");
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        log4j.info(language);
        mainPage.assertText(UserBarPage.DIV_HOME_TEXT, "$Home_Button", language);

    }

    @Test(description = "test to check can switch the type of games on the main page")
    public void testSwitchMainPage() {
        log4j.info("test to check can switch the type of games on the main page");
        Game game = new PocketFort();
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        Assert.assertTrue(mainPage.isChecked(MainPage.DIV_DESKTOP_GAMES), 
        		"The div to switch mainpage for desktop games is not selected by default");
        //Assert.assertTrue(mainPage.validateElementVisible(MainPage.DIV_MOBILE_GAMES),
        //        "There is no a div to switch mainpage for mobile games. DIV_MOBILE_GAMES is not visible");
        mainPage.click(MainPage.DIV_MOBILE_GAMES);
        //Assert.assertTrue(mainPage.isChecked(MainPage.DIV_MOBILE_GAMES), 
        //		"The div to switch mainpage for desktop games is not selected after clicking on it");
        String tileGame = String.format(MainPage.DIV_TILE_GAME,game.getServiceId());
		Assert.assertTrue(mainPage.validateElementVisible(tileGame),
               "There is no a tile for game " + game.getAppName());
        mainPage.click(tileGame);
        Assert.assertTrue(mainPage.validateUrlContains(game.getURL()), 
        		"The user didn't get to the game page after clicking on the tile " + game.getAppName());
    }
    
    @Test(description = "test to check can switch the type of games on the main page to the hits")
    public void testSwitchToHitsMainPage() {
        log4j.info("test to check can switch the type of games on the main page to the hits");
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        Assert.assertTrue(mainPage.isChecked(MainPage.DIV_DESKTOP_GAMES), 
        		"The div to switch mainpage for desktop games is not selected by default");
        //Assert.assertTrue(mainPage.validateElementVisible(MainPage.DIV_MOBILE_GAMES),
        //        "There is no a div to switch mainpage for mobile games. DIV_MOBILE_GAMES is not visible");
        mainPage.click(MainPage.DIV_HITS_GAMES);
        //Assert.assertTrue(mainPage.isChecked(MainPage.DIV_MOBILE_GAMES), 
        //		"The div to switch mainpage for desktop games is not selected after clicking on it");
        String tileGame = String.format(MainPage.DIV_TILE_GAME,"31-gta5");
		Assert.assertTrue(mainPage.validateElementVisible(tileGame),
               "There is no a tile for game GTA");
        mainPage.click(tileGame);
        Assert.assertTrue(mainPage.validateUrlContains("gta5"), 
        		"The user didn't get to the game page after clicking on the tile GTA");
    }
    
}
