package ru.inn.autotests.webdriver.tests.common;

import org.testng.Assert;
import org.testng.annotations.Test;

import ru.inn.autotests.webdriver.common.PrepareUser;
import ru.inn.autotests.webdriver.common.entity.Message;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.composite.pages.*;
import ru.inn.autotests.webdriver.tests.AbstractTest;
import ru.inn.autotests.webdriver.toolkit.LocalDriverManager;
import ru.inn.autotests.webdriver.toolkit.MailProvider;

/**
 * Created with IntelliJ IDEA.
 * User: Sergey.Kashapov
 * Date: 01.10.13
 * Time: 16:13
 * To change this template use File | Settings | File Templates.
 */
public class ReplicationTests extends AbstractTest {


	PrepareUser prepareUser = new PrepareUser();


    @Test(description = "test to check replication from EU to RU with registration")
    public void testReplicationWithRegistration(){
        log4j.info("Test register new user with open eye started");
        User user = switcherLocation(TypeUser.HARDCORE_USER);
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        RegisterPage registerPage = new RegisterPage();
        Assert.assertTrue(registerPage.validateElementVisible(UserBarPage.BUTTON_SIGN_UP), "There is no the button 'Sign In or Register' in the userbar. Maybe the User is authorized already.");
        registerPage.click(UserBarPage.BUTTON_SIGN_UP);
        String mainEmail= OperationsHelper.selectRandomEmail();
        registerPage.fillRegistration(mainEmail);
        Assert.assertTrue(registerPage.validateElementIsNotVisible(RegisterPage.BUTTON_CREATE_ACCOUNT), "Register form is open");
        Message message = MailProvider.getLastMessage(mainEmail);
        message.assertBackgroundColor("YELLOW");
        String linkToRegist = MailProvider.getLinkFromMail(mainEmail);
        registerPage.openUrlIn4game(linkToRegist);
        registerPage.fillPassOpenEyeForm("123456");
        Assert.assertTrue(registerPage.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized after the registration.");
        LocalDriverManager.getDriverController().deleteAllCookies();
        mainPage.goToAnotherBase();
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(mainEmail,"123456", false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
    }

    @Test(description = "test to check replication from EU to RU with settings")
    public void testReplicationWithSettings(){
    	log4j.info("test to check replication from EU to RU with settings");
    	prepareUser.resetCacheIs("qa");
        User user = switcherLocation(TypeUser.USER);
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        SettingsPage settingsPage = new SettingsPage(language);
        settingsPage.openPage(settingsPage);
        settingsPage.click(SettingsPage.LINK_PERSONAL_INFO);
        Assert.assertTrue(settingsPage.validateElementVisible(SettingsPage.FORM_SETTINGS_CONTENT), "Settings form is not open");
        String availableSex=settingsPage.availableSexForUser();
        System.out.println(availableSex);
        settingsPage.click(availableSex);
        settingsPage.type(SettingsPage.FIELD_PASSWORD_IN_PERSONAL_INFO, user.getPassword());
        settingsPage.click(SettingsPage.BUTTON_SAVE_SETTINGS);
        Assert.assertTrue(lp.validateElementVisible(settingsPage.DIV_SUCCESS_PERSONAL_INFO), "The div about the success of changing personal data is not visible");
        mainPage.goToAnotherBase();
        Assert.assertTrue(settingsPage.findElement(availableSex).isSelected(),"Replication is not working");
    }
    
    
}
