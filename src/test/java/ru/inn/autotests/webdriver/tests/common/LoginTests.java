package ru.inn.autotests.webdriver.tests.common;

import org.eclipse.jetty.util.log.Log;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.inn.autotests.webdriver.common.PrepareUser;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.composite.common.Captcha;
import ru.inn.autotests.webdriver.composite.pages.*;
import ru.inn.autotests.webdriver.composite.popups.PopupFinalFormalities;
import ru.inn.autotests.webdriver.tests.AbstractTest;
import ru.inn.autotests.webdriver.toolkit.LocalDriverManager;
import ru.inn.autotests.webdriver.toolkit.PlatformUtils;
import ru.inn.autotests.webdriver.toolkit.UserData;

/**
 * Created with IntelliJ IDEA.
 * User: Sergey.Kashapov
 * Date: 10.06.13
 * Time: 16:47
 * To change this template use File | Settings | File Templates.
 */
public class LoginTests extends AbstractTest {

	PrepareUser prepareUser = new PrepareUser();
	
	@BeforeClass
	public void initUser() {
		//OperationsHelper.baseUrl="https://api-auth-ru.4gametest.com/";
	}

    @Test(enabled = false)
    public void testLoginFaceBook() {
        User user=switcherLocation(TypeUser.TEST_USER);
        SocialNetworkPage snp=new SocialNetworkPage();
        snp.deleteFbCookies();
        user.setMainEmail(PlatformUtils.getStageParameters("testFbLogin"));
        user.setPassword(PlatformUtils.getStageParameters("testFbPassword"));
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        snp.clickToOpenPopup(UserBarPage.BUTTON_FACEBOOK_LOGIN);
        snp.loginInFaceBook(user.getMainEmail(), user.getPassword());
        Assert.assertTrue(snp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");

    }

    @Test (enabled = false)
    public void testLoginVk() {
        User user=switcherLocation(TypeUser.TEST_USER);
        SocialNetworkPage snp=new SocialNetworkPage();
        snp.deleteVkCookies();
        user.setMainEmail(PlatformUtils.getStageParameters("testVKLoginAuth"));
        user.setPassword(PlatformUtils.getStageParameters("testVKPass"));
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        snp.clickToOpenPopup(UserBarPage.BUTTON_VK_LOGIN);
        snp.loginInVk(user.getMainEmail(), user.getPassword());
        Assert.assertTrue(snp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");

    }


    @Test(description="test to check that user can login into 4game on the game page")
    public void testPositiveLogin()  {
        User  user=switcherLocation(TypeUser.TEST_USER);
        GamePanelPage gpp = new GamePanelPage(game);
        log4j.info("test to check that user can login into 4game on the game page " + game.getGameName());
        gpp.openPage(gpp);
        String oldCurrentUrl = gpp.getCurrentUrl();
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getLogin(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        
        gpp.addCookie("promoPopupShowed__" + game.getServiceId()+"__" + UserData.getMasterAccountId(), "1");
        gpp.openUrl(oldCurrentUrl);
        
        Assert.assertTrue(lp.getCurrentUrl().equals(oldCurrentUrl),
                "The user is not in the correct location after signing - in on the game page " + oldCurrentUrl);
    }

    @Test(description="test to check that user can login into 4game by short link")
    public void testAutoLogin() throws Exception {
        User user=switcherLocation(TypeUser.TEST_USER);
        GamePanelPage gpp = new GamePanelPage(game);
        log4j.info("test to check that user can login into 4game by short link");
        prepareUser.resetCacheApi();
        gpp.openPage(gpp);
        LocalDriverManager.getDriverController().deleteAllCookies();
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getLogin(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        String ticket = LocalDriverManager.getDriverController().getCookie("inn-user").getValue();
        lp.logoutHook();
        lp.openUrlIn4game("/a/?ticket=" + ticket);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD),
                "The user is not authorized after the going from short link ");
    }

    @Test(description="test to check that user can login with his login into 4game on the game page")
    public void testLoginWithLogin() throws Exception {
        User user=switcherLocation(TypeUser.TEST_USER);
        log4j.info("test to check that user can login  with his login into 4game on the game page");
        GamePanelPage gpp = new GamePanelPage(game);
        gpp.openPage(gpp);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getLogin(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
    }

    @Test(description="test to check that captcha on the Login pop-up is displaying when somebody trying to guess password",priority = 1)
    public void testBruteforceLogin() throws Exception {
        log4j.info("test to check that captcha on the Login pop-up is displaying when somebody trying to guess password");
        User user=switcherLocation(TypeUser.TEST_USER);
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LocalDriverManager.getDriverController().deleteAllCookies();
        LoginPage lp = new LoginPage(game);
        //prepareUser.resetCacheIs(user.getEnvironment());
        prepareUser.resetCacheApi();
        Assert.assertTrue(lp.validateElementIsNotVisible(UserBarPage.DIV_LOGIN_NAME_FIELD),
                "The user is authorized already");
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        Assert.assertTrue(!lp.isVisible(Captcha.CAPTCHA), "Captcha is visible but It shouldn't!");
        for (int i = 0; i < 3; i++) {
            lp.fillLoginForm(user.getMainEmail(), user.getPassword() + "bla", false);
            lp.sendPause(2);
        }
        lp.fillLoginForm(user.getMainEmail(), user.getPassword() + "bla", false);
        Assert.assertTrue(lp.validateElementVisible(Captcha.CAPTCHA), "Captcha is NOT visible but It must be visible!!");

    }

    @Test(description="test to check that user can login with opened password into 4game")
    public void testPositiveLoginOpenEye() throws Exception {
        log4j.info("test to check that user can login with opened password into 4game");
        User user=switcherLocation(TypeUser.TEST_USER);
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), true);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
    }

    @Test(description="test to check that error message is visible when user enter the wrong password in pop-up login",priority = 2)
    public void testNegativeLogin() throws Exception {
        log4j.info("test to check that error message is visible when user enter the wrong password in pop-up login");
        User user=switcherLocation(TypeUser.TEST_USER);
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword() + "1", false);
        Assert.assertTrue(lp.validateElementVisible(LoginPage.LABEL_ERROR_MESSAGE_LOGIN_OR_PASSWORD),
                "Error message 'Login or password is incorrect' is NOT visible");
    }

    @Test(description="test to check that error message is visible when user enter too long password in pop-up login",priority = 2)
    public void testTooLongPassword() throws Exception {
        log4j.info("test to check that error message is visible when user enter too long password in pop-up login");
        User user=switcherLocation(TypeUser.TEST_USER);
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        String tooLongPassword = "";
        for (int i = 0; i < 3; i++) {
            tooLongPassword += tooLongPassword + "qaTestTooLongPassword";
        }
        lp.fillLoginForm(user.getMainEmail(), tooLongPassword, false);
        Assert.assertTrue(lp.validateElementVisible(LoginPage.LABEL_ERROR_MESSAGE_PASSWORD),
                "Error message 'Password is too long' is NOT visible");
    }

    @Test(description="test to check that error message is visible when user enter too long email in pop-up login",priority = 2)
    public void testTooLongEmail() throws Exception {
        log4j.info("test to check that error message is visible when user enter too long email in pop-up login");
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        String tooLongEmail = "";
        for (int i = 0; i < 3; i++) {
            tooLongEmail += tooLongEmail + "qaTestTooLongPassword";
        }
        tooLongEmail += tooLongEmail + "@tooLong.ru";
        lp.fillLoginForm(tooLongEmail, "123456", false);
        Assert.assertTrue(lp.validateElementVisible(LoginPage.LABEL_ERROR_MESSAGE_LOGIN_OR_PASSWORD),
                "Error message 'Email is incorrect' is NOT visible");
    }

    @Test(description="test to check that error message is visible when user enter too short login in pop-up login",priority = 2)
    public void testTooShortLogin() throws Exception {
        log4j.info("test to check that error message is visible when user enter too short login in pop-up login");
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm("qaqaq", "123456", false);
        Assert.assertTrue(lp.validateElementVisible(LoginPage.LABEL_ERROR_MESSAGE_LOGIN_OR_PASSWORD),
                "Error message 'Login or password is incorrect' is NOT visible");
    }

    @Test(description="test to check that error message is visible when user enter too long login in pop-up login",priority = 2)
    public void testTooLongLogin() throws Exception {
        log4j.info("test to check that error message is visible when user enter too long login in pop-up login");
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        String tooLongLogin = "qaqaqaqaqaqaq";
        lp.fillLoginForm(tooLongLogin, "123456", false);
        Assert.assertTrue(lp.validateElementVisible(LoginPage.LABEL_ERROR_MESSAGE_LOGIN_OR_PASSWORD),
                "Error message 'Login or password is incorrect' is NOT visible");
    }

    @Test(description="test to check that user has to accept the 4game licence when he trying to login to the 4game",priority = 2)
    public void testAcceptLicense() {
        if(OperationsHelper.baseUrl.contains("br") || OperationsHelper.baseUrl.contains("eu."))
            throw new SkipException("This test is not 4 ru");
        log4j.info("test to check that user has to accept the 4game licence when he trying to login to the 4game");
        User user=switcherLocation(TypeUser.NEW_USER);
        PrepareUser prepareUser = new PrepareUser();
        prepareUser.rejectLicense(user, "0");
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        String mainEmail = user.getMainEmail();
		lp.type(LoginPage.LOGIN_FIELD, mainEmail);
        lp.type(LoginPage.PASSWORD_FIELD, "123456");
        log4j.info(mainEmail);
        Captcha captcha=new Captcha();
        captcha.typeCaptchaIfPresent("987654");
        lp.click(LoginPage.BUTTON_SIGN_UP);
        Assert.assertTrue(lp.validateElementVisible(PopupFinalFormalities.IFRAME_FINAL_FORMALITIES),  "Pop-up with licence for 4game is not visible, but user have not accepted it yet");
        lp.refreshPage();
        Assert.assertTrue(lp.validateElementVisible(PopupFinalFormalities.IFRAME_FINAL_FORMALITIES),  "Pop-up with licence for 4game is not visible after refreshing page, but user have not accepted it yet");
        lp.acceptLicenceIfNeed();
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD),
                "The user is not authorized after the going from short link ");
    }

}
