package ru.inn.autotests.webdriver.tests.common;

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.composite.common.Captcha;
import ru.inn.autotests.webdriver.composite.pages.LoginPage;
import ru.inn.autotests.webdriver.composite.pages.MainPage;
import ru.inn.autotests.webdriver.composite.pages.SettingsPage;
import ru.inn.autotests.webdriver.composite.pages.UserBarPage;
import ru.inn.autotests.webdriver.tests.AbstractTest;

public class NotificationsTests extends AbstractTest {





    @Test(description = "test to check that user has ability to change a Notifications data")
    public void testChangeNotificationsData(){
        log4j.info("test to check that user has ability to change a Notifications data");
        User user = switcherLocation(TypeUser.TEST_USER);
        MainPage mainPage = new MainPage();      
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        SettingsPage settingsPage = new SettingsPage(language);
        settingsPage.openPage(settingsPage);
        settingsPage.click(SettingsPage.LINK_NOTIFICATIONS);
        Assert.assertTrue(settingsPage.validateElementVisible(SettingsPage.FORM_SETTINGS_CONTENT), "Settings form (NOTIFICATIONS) is not opened");
        settingsPage.click(SettingsPage.CHECKBOX_ACCOUNT_MAIL);
        settingsPage.click(SettingsPage.CHECKBOX_NEWS_MAIL);
        Captcha captcha=new Captcha();
        captcha.typeCaptchaIfPresent("987654");
        settingsPage.click(SettingsPage.BUTTON_SAVE_SETTINGS);
        Assert.assertTrue(lp.validateElementVisible(settingsPage.DIV_SUCCESS_PERSONAL_INFO), "The div about the success of changing notifications data is not visible");
    }



}
