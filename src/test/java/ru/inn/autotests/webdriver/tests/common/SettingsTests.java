package ru.inn.autotests.webdriver.tests.common;

import org.eclipse.jetty.util.log.Log;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.inn.autotests.webdriver.common.PrepareUser;
import ru.inn.autotests.webdriver.common.entity.Message;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.composite.common.Captcha;
import ru.inn.autotests.webdriver.composite.pages.*;
import ru.inn.autotests.webdriver.tests.AbstractTest;
import ru.inn.autotests.webdriver.toolkit.MailProvider;
import ru.inn.autotests.webdriver.toolkit.UserData;
import ru.inn.autotests.webdriver.toolkit.WebDriverController;

import static ru.inn.autotests.webdriver.composite.OperationsHelper.selectRandomEmail;
import static ru.inn.autotests.webdriver.composite.OperationsHelper.selectRandomMobileNumber;


public class SettingsTests extends AbstractTest {
    User user = new User();
    PrepareUser prepareUser = new PrepareUser();
    
    @BeforeMethod
    public void initUser() {
        user = switcherLocation(TypeUser.CHANGE_USER);
        
    	//OperationsHelper.baseUrl="https://ubersecurity-settings-ru.4gametest.com/";
    }


    @Test(description = "test to change main mail of user")
    public void testChangeMailOfUser()   {
        log4j.info("Change main mail of user without mobile");
        user = switcherLocation(TypeUser.WITHOUT_MOBILE_USER);

        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        //prepareUser.deleteMobile(UserData.getMasterAccountId());
        SettingsPage settingsPage = new SettingsPage(language);
        settingsPage.openPage(settingsPage);
        String oldMail = settingsPage.getText(SettingsPage.LABEL_MAIL_OF_USER);
        int numOldMail = MailProvider.getNumberOfMessages(oldMail);
        String newMail =selectRandomEmail();
        int numNewMail = MailProvider.getNumberOfMessages(newMail);
        settingsPage.click(SettingsPage.LINK_CHANGE_MAIN_MAIL);
        settingsPage.fillChangeEmailForm(user.getPassword(),newMail);
        Assert.assertTrue(settingsPage.validateElementIsNotVisible(SettingsPage.BUTTON_ACCEPT_CHANGE_MAIL),"Form is not close");
        Assert.assertTrue(MailProvider.isNewMessagePresent(newMail, numNewMail), "New message is not present ");
        String linkToChangeMailFirst = MailProvider.getLinkFromMail(newMail);
        settingsPage.openUrlIn4game(linkToChangeMailFirst);
        Assert.assertTrue(settingsPage.validateElementVisible(SettingsPage.FORM_SETTINGS_CONTENT),
                "We are not on the Settings page after opening the link to confirm mail");
        //captcha.fillCaptchaIfItPresent();
        //Assert.assertTrue(settingsPage.validateElementVisible(SettingsPage.FORM_CHANGE_EMAIL_SEND_LETTER),"Form with message that send new message is not visible");
        Assert.assertTrue(MailProvider.isNewMessagePresent(oldMail, numOldMail),"New message is not present");
        String linkToChangeMail = MailProvider.getLinkFromMail(oldMail);
        log4j.info(linkToChangeMail);
        settingsPage.openUrlIn4game(linkToChangeMail);
        Assert.assertTrue(settingsPage.validateTextEquals(SettingsPage.LABEL_MAIL_OF_USER,newMail));//,"The main mail was not changed. The new mail is " + newMail+ " but we got " + settingsPage.getText(SettingsPage.LABEL_MAIL_OF_USER));
    }

    @Test(description = "change mail of user with mobile number")
    public void testChangeMailOfUserThroughMobile() throws InterruptedException {
        log4j.info("Test to check the ability to Change main mail of user with mobile number");
        Captcha captcha=new Captcha();
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getLogin(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        SettingsPage settingsPage = new SettingsPage(language);
        settingsPage.openPage(settingsPage);
        settingsPage.click(SettingsPage.LINK_CHANGE_MAIN_MAIL);
        String newMail =selectRandomEmail();
        int numNewMail = MailProvider.getNumberOfMessages(newMail); 
        settingsPage.fillChangeEmailForm(user.getPassword(),newMail);
        Assert.assertTrue(settingsPage.validateElementIsNotVisible(SettingsPage.BUTTON_ACCEPT_CHANGE_MAIL), "Form 'change mail' is not close");
        Assert.assertTrue(MailProvider.isNewMessagePresent(newMail, numNewMail),"New message 'to change main mail' is not present");//"blablabla", numNewMail),"New message 'to change main mail' is not present");
        int numOfSms=MailProvider.getNumberOfSms(user.getMobileNumber());
        settingsPage.openUrlIn4game(MailProvider.getLinkFromMail(newMail));
        captcha.fillCaptchaIfItPresent();
        Assert.assertTrue(settingsPage.validateElementVisible(SettingsPage.FIELD_SMS_CODE_CHANGE_MAIL),"FIELD_SMS_CODE_CHANGE_MAIL is not visible. The sms was not sent");
        Assert.assertTrue(MailProvider.isNewSmsPresent(user.getMobileNumber(),numOfSms),"there is no new sms notifytest."+user.getMobileNumber() + "@inn.ru.htm");
        Message mess = MailProvider.getSms(user.getMobileNumber());
        settingsPage.type(SettingsPage.FIELD_SMS_CODE_CHANGE_MAIL, mess.getCodeFromSMS());
        captcha.fillCaptchaIfItPresent();
        settingsPage.click(SettingsPage.BUTTON_CHANGE_MAIL_AFTER_SMS_CODE);
        Assert.assertTrue(settingsPage.validateElementIsNotVisible(SettingsPage.BUTTON_CHANGE_MAIL_AFTER_SMS_CODE),"The form changing main mail was not closed.");
        Assert.assertTrue(settingsPage.validateElementVisible(SettingsPage.LABEL_MAIL_OF_USER),"LABEL_MAIL_OF_USER is not visible");
        Assert.assertTrue(settingsPage.validateTextEquals(SettingsPage.LABEL_MAIL_OF_USER,newMail));//,"The main mail was not changed. The new mail is " + newMail+ " but we got " + settingsPage.getText(SettingsPage.LABEL_MAIL_OF_USER));
    }

    @Test(description = "test to check that user does not have an ability to change a mobile phone number")
    public void testChangeMobileOfUser() throws InterruptedException {
        log4j.info("test to check that user does not have an ability to change a mobile phone number");
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getLogin(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        SettingsPage settingsPage = new SettingsPage(language);
        settingsPage.openPage(settingsPage);
        Assert.assertTrue(lp.validateElementVisible(SettingsPage.LINK_CHANGE_MOBILE),"There is no link to change a mobile phone");
        settingsPage.openUrlIn4game("/settings/?popupWidget=SetContactWidget&contactType=3");
        Assert.assertTrue(lp.validateElementVisible(SettingsPage.FORM_UNABLE_CHANGE_MOBILE), "The user has an ability to change mobile phone. The form FORM_UNABLE_CHANGE_MOBILE is not visible");

    }
    
    @Test(description = "test to check that user can see a correct security level", enabled=true)
    public void testLevelSecurity() {
        log4j.info("test to check that user can see a correct security level");
        User user = switcherLocation(TypeUser.USER);
        PrepareUser prepareUser = new PrepareUser();
        prepareUser.switchSuperSecurity(user, false);
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getLogin(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        SettingsPage settingsPage = new SettingsPage(language);
        settingsPage.openPage(settingsPage);
        Assert.assertTrue(settingsPage.validateElementVisible(SettingsPage.FORM_ACCOUNT_MEDIUM_SECURITY),"The form 'Medium security' for user without a super security is not present");
        prepareUser.switchSuperSecurity(user, true);
        settingsPage.refreshPage();
        Assert.assertTrue(settingsPage.validateElementVisible(SettingsPage.FORM_ACCOUNT_HIGH_SECURITY),"The form 'High level security' for user with a super security and with mobile phone is not present");
        Assert.assertTrue(settingsPage.validateElementVisible(SettingsPage.FORM_SUPER_SECURITY_IS_ON),"The form 'Trusted computers are on' for user with a super security and with mobile phone is not present");
        prepareUser.deleteMobileNumber(user);
        settingsPage.refreshPage();
        Assert.assertTrue(settingsPage.validateElementVisible(SettingsPage.FORM_ACCOUNT_LOW_SECURITY),"The form 'Low level security' for user without a mobile phone is not present");
        prepareUser.switchSuperSecurity(user, false);
        settingsPage.refreshPage();
        Assert.assertTrue(settingsPage.validateElementVisible(SettingsPage.FORM_ACCOUNT_LOW_SECURITY),"The form 'Low level security' for user without a super security and a mobile phone is not present");
    }

    @Test(description = "test to check that deleting mobile number of user is impossible")
    public void testDeleteMobileOfUser() throws InterruptedException {
        log4j.info("test to check that deleting mobile number of user is impossible");
        User user = switcherLocation(TypeUser.MOBILE_USER);
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getLogin(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        mainPage.openUrlIn4game("/settings/?popupWidget=SetContactWidget&contactType=3&state=3");
        Assert.assertTrue(lp.validateElementVisible(SettingsPage.FORM_UNABLE_CHANGE_MOBILE), "The user has an ability to delete mobile phone. The form FORM_UNABLE_CHANGE_MOBILE is not visible");

    }

    @Test(description = "test to check show warning without main mail")
    public void testContactsWithoutMainMail() throws InterruptedException {
        log4j.info("Test contacts without main mail");
        PrepareUser prepareUser = new PrepareUser();
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getLogin(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        prepareUser.deleteMainMail(UserData.getMasterAccountId());
        SettingsPage settingsPage = new SettingsPage(language);
        settingsPage.openPage(settingsPage);
        Assert.assertTrue(settingsPage.validateElementPresent(SettingsPage.LINK_ADD_MAIN_MAIL),"Warning without main mail is not present");
        Assert.assertTrue(settingsPage.validateElementVisible(SettingsPage.FORM_ACCOUNT_IS_NOT_PROTECTED),"The form 'Account is not protected!' for user without main mail is not present");

    }


    @Test(description = "test to change login user")
    public void testChangeLoginUser() {
        log4j.info("Change login of user");
        String newLogin = "new" + OperationsHelper.selectRandomNumber();
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        
        int num = MailProvider.getNumberOfMessages(user.getMainEmail());
        SettingsPage settingsPage = new SettingsPage(language);
        settingsPage.openPage(settingsPage);
        settingsPage.fillChangeLoginForm(newLogin, user.getPassword());
        Assert.assertTrue(settingsPage.validateElementIsNotVisible(SettingsPage.BUTTON_SAVE_SETTINGS),"Form is not close");
        Assert.assertTrue(settingsPage.validateElementVisible(SettingsPage.SUCCESS_DIV),"Success div is not present");
        Assert.assertTrue(MailProvider.isNewMessagePresent(user.getMainEmail(), num),"New message is not present");
        Message message = MailProvider.getLastMessage(user.getMainEmail());
        message.assertBackgroundColor("GRAY");
    }


    @Test(description = "test to change pass of user")
    public void testChangePassUser() {
        log4j.info("Change login of user");
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getLogin(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        
        int num = MailProvider.getNumberOfMessages(user.getMainEmail());
        SettingsPage settingsPage = new SettingsPage(language);
        settingsPage.openPage(settingsPage);
        settingsPage.fillChangePassForm(user.getPassword());
        Assert.assertTrue(settingsPage.validateElementIsNotVisible(SettingsPage.BUTTON_SAVE_SETTINGS),"Form is not close");
        Assert.assertTrue(settingsPage.validateElementVisible(SettingsPage.SUCCESS_DIV),"Success div is not present");
        Assert.assertTrue(MailProvider.isNewMessagePresent(user.getMainEmail(), num),"New message is not present");
        Message message = MailProvider.getLastMessage(user.getMainEmail());
        message.assertBackgroundColor("GRAY");

    }

    @Test(description = "test to add facebook contact and then dell it")
    public void testConnectFbAndDel() {
    	log4j.info("test to add facebook contact and then dell it");
        UserBarPage ubp = new UserBarPage();
        SocialNetworkPage snp = new SocialNetworkPage();
        snp.deleteFbCookies();
        log4j.info("Connect to faceBook");
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getLogin(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        double oldMoney = ubp.getCurrentUserMoney();
        
        SettingsPage settingsPage = new SettingsPage(language);
        settingsPage.openPage(settingsPage);
        snp.clickToOpenPopup(SettingsPage.BUTTON_ADD_FB_CONTACT);
        snp.loginInFaceBook("selenium1byebye@gmail.com", "4game.ru");
        Assert.assertTrue(settingsPage.validateElementVisible(SettingsPage.BUTTON_DEL_FB_CONTACT),"Button delete facebook account is not present");
        settingsPage.deleteFbConnect();
        Assert.assertEquals(oldMoney,ubp.getCurrentUserMoney(),"The bonuses were increased after adding fb contact");


    }

    @Test(description = "test to add VK contact and then dell it", enabled=true)
    public void testConnectVkAndDel() {
    	log4j.info("test to add VK contact and then dell it");
        UserBarPage ubp = new UserBarPage();
        SocialNetworkPage snp = new SocialNetworkPage();
        snp.deleteFbCookies();
        log4j.info("Connect to VK");
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getLogin(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        double oldMoney = ubp.getCurrentUserMoney();
        
        SettingsPage settingsPage = new SettingsPage(language);
        settingsPage.openPage(settingsPage);
        snp.clickToOpenPopup(SettingsPage.BUTTON_ADD_VK_CONTACT);
        snp.loginInVk("selenium1byebye@gmail.com", "4game.ru");
        Assert.assertTrue(settingsPage.validateElementVisible(SettingsPage.BUTTON_DEL_VK_CONTACT),"Button delete vk account is not present");
        settingsPage.deleteVkConnect();
        Assert.assertEquals(oldMoney,ubp.getCurrentUserMoney(),"The bonuses were increased after adding vk contact");

    }

    @Test(description = "test to add mobile number")
    public void testAddMobileNumber() {
        log4j.info("Test add mobile number for user started");
        user=switcherLocation(TypeUser.NEW_USER);
        MainPage mp = new MainPage();
        mp.openPage(mp);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        SettingsPage settingsPage=new SettingsPage(language);
        settingsPage.openPage(settingsPage);
        String mobileNumber = selectRandomMobileNumber();
        log4j.info("New user mobile number is "+mobileNumber);
        Assert.assertTrue(lp.validateElementVisible(SettingsPage.LINK_ADD_MOBILE), "LINK_ADD_MOBILE is not visible");
        settingsPage.click(SettingsPage.LINK_ADD_MOBILE);
        Assert.assertTrue(settingsPage.validateElementPresent(SettingsPage.FIELD_MOBILE_NUMBER),"Field for mobile number is not present");
        settingsPage.addMobileNumber(user,mobileNumber);
        user.setMobileNumber(mobileNumber);
        Assert.assertTrue(settingsPage.validateElementPresent(SettingsPage.FIELD_FOR_CODE_ADD_MOBILE),"Field for code for add mobile is not present");
        int num = MailProvider.getNumberOfMessages(user.getMainEmail());
        settingsPage.getAndTypeCodeFromSms(user);
        settingsPage.sendPause(10);
        Assert.assertTrue(MailProvider.isNewMessagePresent(user.getMainEmail(), num));
        //num = MailProvider.getNumberOfMessages(user.getMainEmail());
        //MailProvider.isNewMessagePresent(user.getMainEmail(), num);
        String linkToAdd = MailProvider.getLinkFromMail(user.getMainEmail());
        settingsPage.openUrlIn4game(linkToAdd);
        Assert.assertTrue(mp.validateElementPresent(UserBarPage.DIV_LOGIN_NAME_FIELD),"Login name is not present in user bar");
        Assert.assertTrue(!settingsPage.isVisible(SettingsPage.FORM_CODE_EXPIRED),"FORM_CODE_EXPIRED is visible. Tha adding mobile did not have success");
        Assert.assertTrue(mp.validateElementPresent(SettingsPage.LINK_CHANGE_MOBILE),"The mobile phone was not added correctly. LINK_CHANGE_MOBILE is not visible");

     }



}
