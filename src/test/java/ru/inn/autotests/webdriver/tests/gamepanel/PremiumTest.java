package ru.inn.autotests.webdriver.tests.gamepanel;

import org.openqa.selenium.Cookie;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.inn.autotests.webdriver.common.PrepareUser;
import ru.inn.autotests.webdriver.common.entity.Message;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.composite.pages.*;
import ru.inn.autotests.webdriver.composite.popups.PopupPremium;
import ru.inn.autotests.webdriver.games.Game;
import ru.inn.autotests.webdriver.games.Lineage2Classic;
import ru.inn.autotests.webdriver.games.Lineage2eu;
import ru.inn.autotests.webdriver.games.PlanetSide2;
import ru.inn.autotests.webdriver.tests.AbstractTest;
import ru.inn.autotests.webdriver.toolkit.LocalDriverManager;
import ru.inn.autotests.webdriver.toolkit.MailProvider;
import ru.inn.autotests.webdriver.toolkit.PlatformUtils;
import ru.inn.autotests.webdriver.toolkit.UserData;
import ru.inn.autotests.webdriver.toolkit.PlatformUtils.Status;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;



public class PremiumTest extends AbstractTest {

    /**
     * TestCase: Check ( a monthly price) and the min pices match
     */
    @Test(description = "Check the min pices match and a monthly price")
    public void testCheckMinPriceAndMonthly() {
        log4j.info("Test Check (a monthly price) the min pices match on the  game "+game.getGameName());
      User  user=switcherLocation(TypeUser.TEST_USER);
      //Game game = new Lineage2eu();
        GamePanelPage gpp = new GamePanelPage(game, language);
        PopupPremium ppr = new PopupPremium(game);
        gpp.openUrlIn4game(game.getURL() + "play");
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.FIELD_PREMIUM_MIN_PRICE), "Div with min price is not visible when user is not authorized");
        String minPriceInGp = gpp.getMinPrice();
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.FIELD_PREMIUM_FEATURES), "The Div with description features of premium  is not visible when user is not authorized");
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");

        gpp.addCookie("promoPopupShowed__" + game.getServiceId()+"__" + UserData.getMasterAccountId(), "1");
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
                
        //Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.FIELD_PREMIUM_MIN_PRICE), "Div with min price is not visible when user is authorized");
        gpp.click(GamePanelPage.BUTTON_BUY_PREMIUM);
        gpp.clickLicenceIfItPresent(game);              
        Assert.assertTrue(gpp.validateElementVisible(PopupPremium.FORM_BUY_PREMIUM), "The premium pop-up is not visible");
        //You have to rewrite that
        //log4j.info(gpp.getText(PopupPremium.FORM_BUY_PREMIUM));
        Assert.assertTrue(gpp.getText(PopupPremium.FORM_BUY_PREMIUM).contains(minPriceInGp), "The Min price " + minPriceInGp +  " does not contains in Popup premium");
        double numberOfMonthsSecondMonth = ppr.getNumber(PopupPremium.LABEL_NUMBER_MONTHS_SECOND_MONTH);
        double priceOfCoupon= Double.parseDouble(ppr.getText(PopupPremium.LABEL_PRICE_SECOND_MONTH_TOTAL));
        double bonusesOfCoupon= ppr.getPriceBonusesIfPresent(PopupPremium.LABEL_PRICE_SECOND_MONTH_BONUSES);
        int rightMonthlyCost = (int)((priceOfCoupon + bonusesOfCoupon)/numberOfMonthsSecondMonth);
        int priceOfCouponMonthly= (int)ppr.getNumber(PopupPremium.LABEL_PRICE_SECOND_MONTHLY);
        Assert.assertTrue(priceOfCouponMonthly == rightMonthlyCost, "The monthly price " + priceOfCouponMonthly +  " does not match with right price " + rightMonthlyCost);
    }
    
    /**
     * TestCase: Check the link to the premium accaumt page
     */
    @Test(description = "Check the link for the premium accaumt page ")
    public void testLinkPremiumPage() {
        User user=switcherLocation(TypeUser.TEST_USER);
    	Game game = new PlanetSide2();
        log4j.info("Test to Check the link for the premium accaumt page "+game.getGameName());
        GamePanelPage gpp = new GamePanelPage(game, language);
        
        gpp.openUrlIn4game(game.getURL() + "play");
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.LINK_PREMIUM_PAGE), "Check the link for the premium accaumt page is not visible " +game.getAppName());
        String oldUrl = gpp.getCurrentUrl();
        gpp.click(GamePanelPage.LINK_PREMIUM_PAGE);
    }
    
    /**
     * TestCase: Check the 'Upgrade Premium' button on the Lineage2 game
     */
    @Test(description = "Check the 'Upgrade Premium' button on the Lineage2 game")
    public void testCheckButtonPremium() {
        log4j.info("Test check the 'Upgrade Premium' button on the  game "+game.getGameName());
        User user=switcherLocation(TypeUser.TEST_USER);
        GamePanelPage gpp = new GamePanelPage(game, language);
        
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        gpp.addCookie("promoPopupShowed__" + game.getServiceId()+"__" + UserData.getMasterAccountId(), "1");
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_BUY_PREMIUM), "Button Buy Premium in game panel is not visible "  + game.getAppName());
    }
    
    /**
     * TestCase: Test to check that the date of duration premium is correct
     */
    @Test(description = "Test to check that the date of duration premium is correct")
    public void testCheckStatusPremium() {
        User user=switcherLocation(TypeUser.TEST_USER);
        log4j.info("Test to check that the date of duration premium is correct for the  game "+game.getGameName());
        GamePanelPage gpp = new GamePanelPage(game, language);
        PrepareUser prepareUser = new PrepareUser();
        prepareUser.createServiceAccountId(user, game);
       
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");

        gpp.addCookie("promoPopupShowed__" + game.getServiceId()+"__" + UserData.getMasterAccountId(), "1");
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.DIV_PREMIUM_FEATURES), "The list features of premium is not visible " + game.getAppName());
        prepareUser.setSubscriptionDate(user.getServiceAccountId(), OperationsHelper.getChangedDate(5));
        gpp.refreshPage();
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.MESSAGE_PREMIUM_ENABLED), "The message 'premium is enabled' is not visible " + game.getAppName());
        prepareUser.setSubscriptionDate(user.getServiceAccountId(), OperationsHelper.getChangedDate(2));
        gpp.refreshPage();
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.MESSAGE_PREMIUM_FINISHES), "The message 'premium finishes' is not visible " + game.getAppName());
        prepareUser.setSubscriptionDate(user.getServiceAccountId(), OperationsHelper.getChangedDate(-1));
        gpp.refreshPage();
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.MESSAGE_PREMIUM_OVER), "The message 'premium finishes' is not visible " + game.getAppName());
        prepareUser.setSubscriptionDate(user.getServiceAccountId(), OperationsHelper.getChangedDate(-3));
        gpp.refreshPage();
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.DIV_PREMIUM_FEATURES), "The list features of premium is not visible " + game.getAppName());
        
    }
    
    /**
     * TestCase: Check the 'Upgrade Premium' button on the Lineage2 game when user is not authorized
     */
    @Test(description = "Check the 'Upgrade Premium' button on the Lineage2 game when user is not authorized", enabled=true)
    public void testCheckButtonPremiumNotLogin() {
        log4j.info("Check the 'Upgrade Premium' button on the Lineage2 game when user is not authorized "+game.getGameName());
        User  user=switcherLocation(TypeUser.TEST_USER);
        GamePanelPage gpp = new GamePanelPage(game, language);
        
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_BUY_PREMIUM), "Button Buy Premium in game panel is not visible when user is not authorized " + game.getAppName());
        gpp.click(GamePanelPage.BUTTON_BUY_PREMIUM);
        LoginPage lp = new LoginPage(game);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        gpp.clickLicenceIfItPresent(game);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        
        gpp.addCookie("promoPopupShowed__" + game.getServiceId()+"__" + UserData.getMasterAccountId(), "1");
        gpp.refreshPage();
        
        Assert.assertTrue(gpp.validateElementVisible(PopupPremium.FORM_BUY_PREMIUM), "The premium pop-up is not visible");
    }
    
    /**
     * TestCase: Buy premium on 1 month
     */
    @Test(description = "Buy premium on 1 month")
    public void testBuyPremiumAccount1Month() {
    	//OperationsHelper.baseUrl="https://registry-ru.4gametest.com/";
        User  user=switcherLocation(TypeUser.TEST_USER);
        UserBarPage ubp=new UserBarPage();
        GamePanelPage gpp = new GamePanelPage(game, language);
        log4j.info("Test buy premium 1 month started for game " + game.getGameName());
        PopupPremium ppr = new PopupPremium(game);    
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");

        gpp.addCookie("promoPopupShowed__" + game.getServiceId()+"__" + UserData.getMasterAccountId(), "1");
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_BUY_PREMIUM), "Button Buy Premium in game panel is not visible");
        gpp.click(GamePanelPage.BUTTON_BUY_PREMIUM);
        gpp.clickLicenceIfItPresent(game);
        int num = MailProvider.getNumberOfMessages(user.getMainEmail());
        Assert.assertTrue(ppr.validateElementVisible(PopupPremium.FORM_BUY_PREMIUM),
                "The premium pop-up is not visible " + game.getAppName());
        double priceOfCoupon= Double.parseDouble(ppr.getText(PopupPremium.LABEL_PRICE_FIRST_MONTH_TOTAL));
        double bonusesOfCoupon= ppr.getPriceBonusesIfPresent(PopupPremium.LABEL_PRICE_FIRST_MONTH_BONUSES);
        log4j.info(bonusesOfCoupon + ", " + priceOfCoupon);
        double oldtUserSum=ubp.getCurrentUserMoney();
        ppr.mouseClick(PopupPremium.RADIO_FIRST_MONTH);
        ppr.click(PopupPremium.BUTTON_BUY);
        Assert.assertTrue(ppr.validateElementIsNotVisible(PopupPremium.FORM_BUY_PREMIUM), "Premium popup is not closed");
        Assert.assertTrue(MailProvider.isNewMessagePresent(user.getMainEmail(), num),"new message is not present in "+user.getMainEmail());
        Message message = MailProvider.getLastMessage(user.getMainEmail());
        ubp.assertUserSum(oldtUserSum,-priceOfCoupon);
        message.assertSum(priceOfCoupon);
        message.assertBackgroundColor("GREEN");
        message.assertSum(priceOfCoupon);
        EventsPage ep= new EventsPage(language);
        ep.openPage(ep);
        Assert.assertTrue(ep.getFirstEvent().contains(String.valueOf((int)priceOfCoupon)), "The first event does not contain the sum per premium " + priceOfCoupon);
    }

    /**
     * TestCase: Buy premium on 1 day
     */
    @Test(description = "Buy premium 1 day with auto continue ")
    public void testBuyPremiumAccountWithAutoPay() {
        User user=switcherLocation(TypeUser.TEST_USER);
        UserBarPage ubp=new UserBarPage();
        PopupPremium ppr = new PopupPremium(game);
        GamePanelPage gpp = new GamePanelPage(game, language);
        log4j.info("Buy premium 1 day with auto continue  " + game.getGameName());
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        
        gpp.addCookie("promoPopupShowed__" + game.getServiceId()+"__" + UserData.getMasterAccountId(), "1");
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_BUY_PREMIUM), "Button Buy Premium in game panel is not visible");
        gpp.click(GamePanelPage.BUTTON_BUY_PREMIUM);
        gpp.clickLicenceIfItPresent(game);
        int num = MailProvider.getNumberOfMessages(user.getMainEmail());
        Assert.assertTrue(ppr.validateElementVisible(PopupPremium.FORM_BUY_PREMIUM), "The premium pop-up is not visible");
        double priceOfCoupon= Double.parseDouble(ppr.getText(PopupPremium.LABEL_PRICE_FIRST_DAY));
        double bonusesOfCoupon= ppr.getPriceBonusesIfPresent(PopupPremium.LABEL_PRICE_FIRST_DAY_BONUSES);
        log4j.info(bonusesOfCoupon + ", " + priceOfCoupon);
        double oldtUserSum=ubp.getCurrentUserMoney();
        //Assert.assertTrue(ppr.validateElementVisible(PopupPremium.RADIO_FIRST_DAY), "The RADIO_FIRST_DAY in pop-up premium is not visible "  + game.getGameName());
        ppr.mouseClick(PopupPremium.RADIO_FIRST_DAY);
        //Assert.assertTrue(ppr.validateElementVisible(PopupPremium.CHECKBOX_AUTOPAY), "The CHECKBOX_AUTOPAY in pop-up premium is not visible "  + game.getGameName());
        ppr.sendPause(1);
        ppr.click(PopupPremium.CHECKBOX_AUTOPAY);
        ppr.click(PopupPremium.BUTTON_BUY);
        Assert.assertTrue(ppr.validateElementIsNotVisible(PopupPremium.FORM_BUY_PREMIUM), "Premium popup is not closed after buying the 1 day's coupon");
        Assert.assertTrue(MailProvider.isNewMessagePresent(user.getMainEmail(), num),"new message is not present in "+user.getMainEmail() + " after buying premium");
        Message message = MailProvider.getLastMessage(user.getMainEmail());
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_BUY_PREMIUM), "Button Buy Premium in game panel is not visible after buying the Premium");
        ubp.assertUserSum(oldtUserSum,-priceOfCoupon);
        message.assertBackgroundColor("GREEN");
        message.assertSum(priceOfCoupon);
        //Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.LINK_AUTO_IMPROVEMENT), "The link to switch AutoImprovement off is not visible " + game.getGameName());
        //Assert.assertTrue(gpp.getAttribute(GamePanelPage.LINK_AUTO_IMPROVEMENT, "data-improvement-state").equals("true"), "The link to switch AutoImprovement doesn't have correct attribute data-improvement-state " + game.getGameName());
        //String oldText = gpp.getText(GamePanelPage.LINK_AUTO_IMPROVEMENT);
        //gpp.click(GamePanelPage.LINK_AUTO_IMPROVEMENT);
        //Assert.assertTrue(gpp.validateTextNotEquals(GamePanelPage.LINK_AUTO_IMPROVEMENT, oldText), "The link to switch AutoImprovement doesn't have correct text after Clicking on the link " + oldText+ " in " + game.getGameName());
        //gpp.refreshPage();
        //Assert.assertTrue(gpp.getAttribute(GamePanelPage.LINK_AUTO_IMPROVEMENT, "data-improvement-state").equals("false"), "The link to switch AutoImprovement on doesn't have correct attribute data-improvement-state after click 'off' " + game.getGameName());
        EventsPage ep= new EventsPage(language);
        ep.openPage(ep);
        Assert.assertTrue(ep.getFirstEvent().contains(String.valueOf((int)priceOfCoupon)), "The first event does not contain the sum per premium " + priceOfCoupon);
    }

    /**
     * TestCase: Buy premium for PS2 from Loyality page
     */
    @Test(description = "Buy premium for PS2 from Loyality page")
    public void testtBuyPremiumAccountPs2FromLoyality() {
        User  user=switcherLocation(TypeUser.TEST_USER);
        Game game = new PlanetSide2();
        UserBarPage ubp=new UserBarPage();
        PopupPremium ppr = new PopupPremium(game);
        GamePanelPage gpp = new GamePanelPage(game, language);
        

        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        LocalDriverManager.getDriverController().deleteAllCookies();
        log4j.info("Buy premium for PS2 from Loyality page  " + game.getGameName());
        gpp.openUrlIn4game("planetside2/play/premium/");
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_LOYALITY_BUY_PREMIUM), "Button Buy Premium on the Loyality page is not visible" + game.getAppName());
        gpp.click(GamePanelPage.BUTTON_LOYALITY_BUY_PREMIUM);
        gpp.clickLicenceIfItPresent(game);
        int num = MailProvider.getNumberOfMessages(user.getMainEmail());
        Assert.assertTrue(ppr.validateElementVisible(PopupPremium.FORM_BUY_PREMIUM), "The premium pop-up is not visible");
        double priceOfCoupon= Double.parseDouble(ppr.getText(PopupPremium.LABEL_PRICE_FIRST_MONTH_TOTAL));
        double bonusesOfCoupon= ppr.getPriceBonusesIfPresent(PopupPremium.LABEL_PRICE_FIRST_MONTH_BONUSES);
        log4j.info(bonusesOfCoupon + ", " + priceOfCoupon);
        double oldtUserSum=ubp.getCurrentUserMoney();
        ppr.mouseClick(PopupPremium.RADIO_FIRST_MONTH);
        ppr.click(PopupPremium.BUTTON_BUY);
        Assert.assertTrue(ppr.validateElementIsNotVisible(PopupPremium.FORM_BUY_PREMIUM), "Premium popup is not closed");
        Assert.assertTrue(MailProvider.isNewMessagePresent(user.getMainEmail(), num),"new message is not present in "+user.getMainEmail() + " after buying premium");
        Message message = MailProvider.getLastMessage(user.getMainEmail());
        ubp.assertUserSum(oldtUserSum,-priceOfCoupon);
        Assert.assertTrue(ppr.validateElementVisible(gpp.GAME_PANEL), "The redirect to play page after buying the premium for ps2, url " + ppr.getCurrentUrl());
        //Assert.assertTrue(ppr.getCurrentUrl().contains("play"), "The redirect to play page after buying the premium for ps2, url " + ppr.getCurrentUrl());
        message.assertSum(priceOfCoupon);
        message.assertBackgroundColor("GREEN");
        message.assertSum(priceOfCoupon);
        EventsPage ep= new EventsPage(language);
        ep.openPage(ep);
        Assert.assertTrue(ep.getFirstEvent().contains(String.valueOf((int)priceOfCoupon)), "The first event does not contain the sum per premium " + priceOfCoupon);
    }
    
    /**
     * TestCase: Buy premium when user is baned
     */
    @Test(description = "Buy premium when user is baned")
    public void testBuyPremiumAccountUserBaned() {

        User user=switcherLocation(TypeUser.BAN_USER);
        PopupPremium ppr = new PopupPremium(game);
        GamePanelPage gpp = new GamePanelPage(game, language);
        log4j.info("Test Buy premium when user is baned " + game.getGameName());
                
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        
        gpp.addCookie("promoPopupShowed__" + game.getServiceId()+"__" + UserData.getMasterAccountId(), "1");
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        
        lp.addUriToCurrentUrl(ppr.URL_POPUP_PREMIUM);
        Assert.assertTrue(ppr.validateElementVisible(PopupPremium.FORM_BUY_PREMIUM), "The premium pop-up is not visible " + game.getAppName());
        ppr.mouseClick(PopupPremium.RADIO_FIRST_MONTH);
        ppr.click(PopupPremium.BUTTON_BUY);
        Assert.assertTrue(ppr.validateElementVisible(PopupPremium.LABEL_ERROR_MESSAGE), "The message 'user can't buy a premium because he is baned' is not visible ");
    }

    @DataProvider
    public Iterator<Object[]> initPayments(){
        List<Object[]> data = new ArrayList<Object[]>();
        for(PaymentTerminal.PaymentMethod method: paymentsMethods)
        {
            data.add(new Object[]{method});
        }
       return data.iterator();
    }
    
    /**
     * TestCase: Buy premium when user doesn't have enough money
     */
    @Test(dataProvider = "initPayments", description = "Buy premium when user doesn't have enough money")
    public void testBuyPremiumAccountNoMoney(PaymentTerminal.PaymentMethod method) {
        User  user=switcherLocation(TypeUser.USER_NO_MONEY);
        //game = new Lineage2eu();
        UserBarPage ubp=new UserBarPage();
        PopupPremium ppr = new PopupPremium(game);
        GamePanelPage gpp = new GamePanelPage(game, language);
        log4j.info("Test Buy premium when user doesn't have enough money " + game.getGameName());
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");

        gpp.addCookie("promoPopupShowed__" + game.getServiceId()+"__" + UserData.getMasterAccountId(), "1");
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_BUY_PREMIUM), "Button Buy Premium in game panel is not visible");
        gpp.click(GamePanelPage.BUTTON_BUY_PREMIUM);
        gpp.clickLicenceIfItPresent(game);
        Assert.assertTrue(ppr.validateElementVisible(PopupPremium.FORM_BUY_PREMIUM), "The premium pop-up is not visible " + game.getAppName());
        double priceOfCoupon= Double.parseDouble(ppr.getText(PopupPremium.LABEL_PRICE_THIRD_DAY));
        double bonusesOfCoupon= ppr.getPriceBonusesIfPresent(PopupPremium.LABEL_PRICE_THIRD_DAY_BONUSES);
        log4j.info(bonusesOfCoupon + ", " + priceOfCoupon);
        double oldtUserSum=ubp.getCurrentUserMoney();
        ppr.mouseClick(PopupPremium.RADIO_THIRD_DAY);
        ppr.click(PopupPremium.BUTTON_BUY);
        PaymentTerminal ptp = new PaymentTerminal();
        Assert.assertTrue(ptp.validateElementVisible(PaymentTerminal.FORM_PAYMENT_TERMINAL), "Payment terminal is not visible after clicking 'Buy premium' for user who doesn't have money");
        ptp.assertSumInTerminal(priceOfCoupon);
        ptp.replenishAccount(method, user, priceOfCoupon);
        Assert.assertTrue(ptp.validateElementPresent(UserBarPage.DIV_USER_BAR),"We didn't return to the 4game after replenishing. BALANCE_PANEL is not present. " + method);
        ptp.openCorrectLinkOnDev();
        ubp.assertUserSum(oldtUserSum, priceOfCoupon);
        Assert.assertTrue(ptp.getCurrentUrl().contains(game.getURL()),"We didn't return to the correct play page after replenishing. " + game.getAppName());
        ppr.mouseClick(PopupPremium.RADIO_THIRD_DAY);
        ppr.click(PopupPremium.BUTTON_BUY);
        Assert.assertTrue(ppr.validateElementIsNotVisible(PopupPremium.FORM_BUY_PREMIUM), "Premium popup is not closed");
        Assert.assertTrue(ppr.validateElementVisible(GamePanelPage.MESSAGE_PREMIUM_STATUS), "The message about premium duration is not visible ");
    }
    
    /**
     * TestCase: Buy premium when user doesn't have enough money and replenishing was not success
     */
    @Test(description = "Buy premium when user doesn't have enough money and replenishing was not success")
    public void testBuyPremiumAccountNoMoneyNoSuccess() {
    	//OperationsHelper.baseUrl="https://main-w-mobile-ru.4gametest.com/";
        User  user=switcherLocation(TypeUser.USER_NO_MONEY);
        PopupPremium ppr = new PopupPremium(game);
        GamePanelPage gpp = new GamePanelPage(game, language);
        log4j.info("Test Buy premium when user doesn't have enough money and replenishing was not success " + game.getGameName());
        
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        
        gpp.addCookie("promoPopupShowed__" + game.getServiceId()+"__" + UserData.getMasterAccountId(), "1");
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");

        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_BUY_PREMIUM), "Button Buy Premium in game panel is not visible");
        gpp.click(GamePanelPage.BUTTON_BUY_PREMIUM);
        gpp.clickLicenceIfItPresent(game);
        Assert.assertTrue(ppr.validateElementVisible(PopupPremium.FORM_BUY_PREMIUM), "The premium pop-up is not visible " + game.getAppName());
        double priceOfCoupon= Double.parseDouble(ppr.getText(PopupPremium.LABEL_PRICE_FIRST_DAY));
        double bonusesOfCoupon= ppr.getPriceBonusesIfPresent(PopupPremium.LABEL_PRICE_FIRST_DAY_BONUSES);
        log4j.info(bonusesOfCoupon + ", " + priceOfCoupon);
        //Assert.assertTrue(ppr.validateElementVisible(PopupPremium.RADIO_FIRST_DAY), "The premium pop-up with RADIO_FIRST_DAY is not visible " + game.getAppName());
        //ppr.scrollToElemant(PopupPremium.RADIO_FIRST_DAY);
        ppr.mouseClick(PopupPremium.RADIO_FIRST_DAY);
        ppr.click(PopupPremium.BUTTON_BUY);
        PaymentTerminal ptp = new PaymentTerminal();
        Assert.assertTrue(ptp.validateElementVisible(PaymentTerminal.FORM_PAYMENT_TERMINAL), "Payment terminal is not visible after clicking 'Buy premium' for user who doesn't have money");
        ptp.assertSumInTerminal(priceOfCoupon);
        ppr.openUrlIn4game("/?orderId=4466&psCode=payonline&returnState=1");
        Assert.assertTrue(ptp.getCurrentUrl().contains(game.getURL()),"We didn't return to the correct play page after replenishing. " + game.getAppName());
        Assert.assertTrue(gpp.validateElementIsNotVisible(PopupPremium.FORM_BUY_PREMIUM), "The premium pop-up is visible after unsuccessful replenishing");
        ppr.sendPause(15);
        Assert.assertTrue(ptp.validateElementVisible(UserBarPage.MESSAGE_REPLANISH_ERROR), "Error message about 'replenishing was not success' is not visible");

    }

    /**
     * TestCase: The test to check that new user can see a pop-up with adv for L2
     */
    @Test(description = "The test to check that new user can see a pop-up with adv for L2")
    public void testBuyPremiumFirstTime() {
        User  user=switcherLocation(TypeUser.TEST_USER);
        UserBarPage ubp=new UserBarPage();
        PopupPremium ppr = new PopupPremium(game);
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        LocalDriverManager.getDriverController().deleteAllCookies();
        log4j.info("The test to check that new user can see a pop-up with adv for L2  " + game.getGameName());
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        PlayPage ppp = new PlayPage(game);
        Assert.assertTrue(lp.validateElementVisible(PlayPage.BUTTON_BUY_TICKET_FOR2DAYS_DISCOUNT),
        		"The adv pop-up with discount for game " + game.getAppName() + " is not visible");
        ppp.click(PlayPage.BUTTON_BUY_TICKET_FOR2DAYS_DISCOUNT);
        Assert.assertTrue(ppr.validateElementVisible(PopupPremium.FORM_BUY_PREMIUM), "The premium pop-up is not visible " + game.getAppName());
        ppp.refreshPage();
        Cookie closedPopUpCookie = ppp.getCookie("promoPopupShowed__" + game.getServiceId()+"__" + UserData.getMasterAccountId());
        String valueCookie = "1"; 
        Assert.assertTrue(closedPopUpCookie.getValue().equals(valueCookie),"Cokie pid" + closedPopUpCookie.getValue() + " doesn't match with " + valueCookie);
        
    }
    
    /**
     * TestCase: The test to check that a new user can buy a subscription for L2 Claccic
     */
    @Test(description = "The test to check that a new user can buy a subscription for L2 Claccic")
    public void testBuySubsL2Classic() {
    	Game game = new Lineage2Classic();
        User  user=switcherLocation(TypeUser.TEST_USER);
        UserBarPage ubp=new UserBarPage();
        PopupPremium ppr = new PopupPremium(game);
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        log4j.info("The test to check that a new user can buy a subscription for L2 Claccic " + game.getGameName());
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        PlatformUtils.sendStatus(Status.INSTALLED, game, isPlugin);
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_PLAY_BLOCKED), "Button Play disabled in game panel L2 Classic is not visible");
        
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_BUY_SUBS), "Button Buy Subscription in game panel is not visible");
        gpp.click(GamePanelPage.BUTTON_BUY_SUBS);
        gpp.clickLicenceIfItPresent(game);
        int num = MailProvider.getNumberOfMessages(user.getMainEmail());
        Assert.assertTrue(ppr.validateElementVisible(PopupPremium.FORM_BUY_PREMIUM),
                "The premium pop-up is not visible " + game.getAppName());
        double priceOfCoupon= Double.parseDouble(ppr.getText(PopupPremium.LABEL_PRICE_THIRD_MONTH_TOTAL));
        double bonusesOfCoupon= ppr.getPriceBonusesIfPresent(PopupPremium.LABEL_PRICE_THIRD_MONTH_BONUSES);
        log4j.info(bonusesOfCoupon + ", " + priceOfCoupon);
        double oldtUserSum=ubp.getCurrentUserMoney();
        ppr.mouseClick(PopupPremium.RADIO_THIRD_MONTH);
        ppr.click(PopupPremium.BUTTON_BUY);
        Assert.assertTrue(ppr.validateElementIsNotVisible(PopupPremium.FORM_BUY_PREMIUM), "Premium popup is not closed");
        PlatformUtils.sendStatus(Status.INSTALLED, game, isPlugin);
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_PLAY), "Button Play in game panel L2 Classic is not visible after buying the subscription");
        Assert.assertTrue(MailProvider.isNewMessagePresent(user.getMainEmail(), num),"new message is not present in "+user.getMainEmail());
        Message message = MailProvider.getLastMessage(user.getMainEmail());
        ubp.assertUserSum(oldtUserSum,-priceOfCoupon);
        message.assertSum(priceOfCoupon);
        message.assertBackgroundColor("GREEN");
        message.assertSum(priceOfCoupon);
    }


}
