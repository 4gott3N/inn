package ru.inn.autotests.webdriver.tests.liveprec;

import org.testng.annotations.Test;
import ru.inn.autotests.webdriver.composite.pages.GamePanelPage;
import ru.inn.autotests.webdriver.tests.AbstractTest;

/**
 *
 */
public class PreconditionDownloadGame extends AbstractTest {


    /*
     * This test is a step for download game, if it's needed.
     * For using this test, add annotation @Test above with parameter
     * (dependsOnMethods={("PreconditionDownloadGame.testPreconditionDownloadGame())))".<br/>
     */
    @Test
    public void testPreconditionUpdateOrPlay() throws Exception {
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openPage(gpp);
        gpp.waitToPlayGame(false);
    }


    
}
