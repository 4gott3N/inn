package ru.inn.autotests.webdriver.tests.gamepanel;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.composite.pages.GamePanelPage;
import ru.inn.autotests.webdriver.composite.pages.LoginPage;
import ru.inn.autotests.webdriver.composite.pages.UserBarPage;
import ru.inn.autotests.webdriver.tests.AbstractTest;
import ru.inn.autotests.webdriver.toolkit.LocalDriverManager;
import ru.inn.autotests.webdriver.toolkit.PlatformUtils;
import ru.inn.autotests.webdriver.toolkit.UserData;
import ru.inn.autotests.webdriver.toolkit.PlatformUtils.Status;

public class StatusTests extends AbstractTest {



    /**
     * I. TestCase: Check button INSTALL when Game isn't installed<br>
     * <b>Prepare:</b><br>
     * Nothing <br>
     * TestCase:<br>
     * <ol>
     * <li> Opens the Game Page;<br>
     * <li> Checks that text on the INSTALL button is present and valid;<br>
     * <li> Checks that the  size is present and valid.
     * </ol>
     * <br>Remarks:<br>
     * After test: clean game folder, stop 4game process, clean register, start 4game process.
     */
    @Test
    public void checkButtonInstall() {
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openPage(gpp);
        gpp.resetMaintenance();
        PlatformUtils.sendStatus(Status.NOT_INSTALLED, game, isPlugin);
        log4j.info("Check status not installed");
        Assert.assertTrue(gpp.validateElementPresent(GamePanelPage.BUTTON_INSTALL), "The button 'Install' is not visible on the panel 'play'");
    }

    /**
     * II. TestCase: Press button INSTALL when Game isn't installed<br>
     * <br>Prepare:</b><br>
     * <p/>
     * TestCase:<br>
     * <ol>
     * <li> Opens the Game Page;<br>
     * <li> Sends status NOT_INSTALLED;<br>
     * <li> Presses button INSTALL;<br>
     * <li> Sends setInstallDir for fakePlugin available;<br>
     * <li> Checks that PopUp 'Download' is present ;<br>
     * <li> Clicks on button 'Start installation';<br>
     * <li> Checks that the progress bar is present and the color of progress bar is yellow;<br>
     * <li> Checks that info into a progress bar (percent) is present;<br>
     * <li> Checks that info under the progress par is present and valid;
     * <li> Checks that links Resume, Cancel are present.<br>
     * </ol>
     */
    @Test
    public void pressButtonInstall() {
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openPage(gpp);
        PlatformUtils.sendStatus(Status.NOT_INSTALLED, game, isPlugin);
        log4j.info("click install game");
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_INSTALL), "The button 'Install' is not visible on the panel 'play'");
        PlatformUtils.sendStatus(Status.PROGRESS, game, isPlugin);

        log4j.info("Check progress bar is present");
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.PROGRESS_BAR),"The PROGRESS BAR is not visible");

        log4j.info("Check text pause and cancel");
        gpp.assertText(GamePanelPage.PROGRESS_PAUSE, "$Pause",language);
        gpp.assertText(GamePanelPage.PROGRESS_CANCEL, "$Cancel",language);
        
        Assert.assertTrue(gpp.validateElementPresent(GamePanelPage.PROGRESS_CANCEL),"The button PROGRESS_CANCEL is not visible");
        gpp.click(GamePanelPage.PROGRESS_CANCEL);
        Assert.assertTrue(gpp.validateElementPresent(GamePanelPage.PROGRESS_BLOCK_CANCEL_STATUS),"The PROGRESS BAR block with button 'Cancel' is not visible");
        log4j.info("Click buttons yes and no");
        Assert.assertTrue(gpp.validateElementPresent(GamePanelPage.CANCEL_BLOCK_BUTTON_YES),"The PROGRESS BAR block with button 'Yes' is not visible");
        log4j.info("Click button yes");
        gpp.click(GamePanelPage.CANCEL_BLOCK_BUTTON_YES);
    }


    /**
     * III. TestCase: Send JSON with status PROGRESS<br>
     * TestCase:<br>
     * <ol>
     * <li> Sends status PROGRESS with actual states<br>
     * <li> Checks that the progress bar is present and the color of progress bar is yellow;<br>
     * <li> Checks that info into a progress bar (percent) is present and valid;<br>
     * <li> Checks that info under the progress par is present and valid;<br>
     * <li> Checks that links Resume, Cancel are present.<br>
     * <ol>
     */
    @Test
    public void sendStatusProgress() {
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openPage(gpp);
        PlatformUtils.sendStatus(Status.PROGRESS, game, isPlugin);
        log4j.info("Check status progress when game is installing");
        Assert.assertTrue(gpp.validateElementPresent(GamePanelPage.PROGRESS_BAR),"The PROGRESS BAR is not visible");
        gpp.assertText(GamePanelPage.PROGRESS_PAUSE, "$Pause",language);
        gpp.assertText(GamePanelPage.PROGRESS_CANCEL, "$Cancel",language);
    }

    /**
     * IV. TestCase: Press button Pause from status PROGRESS<br>
     * TestCase:<br>
     * <ol>
     * <li> Sends status PROGRESS with actual states<br>
     * <li> Checks that the progress bar is present and the color of progress bar is yellow;<br>
     * <li> Clicks on button Pause<br>
     * <li> Checks that the progress bar is present and the color of progress bar is gray;<br>
     * <li> Checks that info into a progress bar (percent) is present and valid;<br>
     * <li> Checks that info under the progress par is present and valid;<br>
     * <li> Checks that links Resume, Cancel are present.<br>
     * </ol>
     */
    @Test
    public void pressButtonPause() {
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openPage(gpp);
        PlatformUtils.sendStatus(Status.PROGRESS, game, isPlugin);

        Assert.assertTrue(gpp.validateElementPresent(GamePanelPage.PROGRESS_BAR),"The PROGRESS BAR is not visible");
        log4j.info("Click pause when progress bar is available");
        gpp.clickOnStalenessElement(GamePanelPage.PROGRESS_PAUSE);
        PlatformUtils.sendStatus(Status.PAUSED, game, isPlugin);

        Assert.assertTrue(gpp.validateElementPresent(GamePanelPage.PROGRESS_BAR_ON_PAUSE),"The PROGRESS BAR with pause is not visible");

        gpp.assertText(GamePanelPage.PROGRESS_RESUME, "$Resume",language);
        gpp.assertText(GamePanelPage.PROGRESS_CANCEL, "$Cancel",language);

        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_SETTINGS), "The button 'Settings' is not visible on the panel 'play'");
    }

    /**
     * V. TestCase: Send JSON status PAUSED <br>
     * TestCase:<br>
     * <ol>
     * <li> Sends status PAUSED with actual states<br>
     * <li> Checks that the progress bar is present and the color of progress bar is gray;<br>
     * <li> Checks that info into a progress bar (percent) is present and valid;<br>
     * <li> Checks that info under the progress par is present and valid;<br>
     * <li> Checks that links Resume, Cancel are present.<br>
     * </ol>
     */
    @Test
    public void sendStatusPaused() {
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openPage(gpp);
        PlatformUtils.sendStatus(Status.PAUSED, game, isPlugin);
        log4j.info("Check status progress bar on pause");
        Assert.assertTrue(gpp.validateElementPresent(GamePanelPage.PROGRESS_BAR_ON_PAUSE),"The PROGRESS BAR with pause is not visible");

        gpp.assertText(GamePanelPage.PROGRESS_RESUME, "$Resume",language);
        gpp.assertText(GamePanelPage.PROGRESS_CANCEL, "$Cancel",language);

        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_SETTINGS), "The button 'Settings' is not visible on the panel 'play'");
    }

    /**
     * VI. TestCase: Send JSON status FINISHED
     * TestCase:<br>
     * <ol>
     * <li> Sends status FINISHED<br>
     * <li> Sends status INSTALLED<br>
     * <li> Checks that link 'Go to game launch page' is visible<br>
     * </ol>
     */
    @Test
    public void sendStatusFinished() {
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openPage(gpp);
        PlatformUtils.sendStatus(Status.FINISHED, game, isPlugin);
        PlatformUtils.sendStatus(Status.INSTALLED, game, isPlugin);
        log4j.info("Check status finished");
        Assert.assertTrue(gpp.validateElementPresent(GamePanelPage.LINK_INSTALL_SUCCESS),"The link about INSTALL was SUCCESS is not visible");

        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_SETTINGS), "The button 'Settings' is not visible on the panel 'play'");
    }

    /**
     * VII. TestCase: Check button 'Play' after status install success<br>
     * <b>Prepare:</b><br>
     * - User logged in 4game
     * TestCase:<br>
     * <ol>
     * <li> Sends status FINISHED<br>
     * <li> Sends status INSTALLED<br>
     * <li> Checks that link 'Go to game launch page' is visible<br>
     * <li> Clicks on the link 'Go to game launch page'<br>
     * <li> Checks that button 'Play' is available<br>
     * <li> Checks that button 'Settings' is available<br>
     * </ol>
     */
    @Test
    public void checkButtonPlay() {
        User user=switcherLocation(TypeUser.HARDCORE_USER);
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openPage(gpp);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized");

        gpp.addCookie("promoPopupShowed__" + game.getServiceId()+"__" + UserData.getMasterAccountId(), "1");
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        
        PlatformUtils.sendStatus(Status.FINISHED, game, isPlugin);
        PlatformUtils.sendStatus(Status.INSTALLED, game, isPlugin);

        Assert.assertTrue(gpp.validateElementPresent(GamePanelPage.LINK_INSTALL_SUCCESS),"The link about INSTALL was SUCCESS is not visible");
        log4j.info("Check status install success");
        gpp.click(GamePanelPage.LINK_INSTALL_SUCCESS);
        log4j.info("Click on link go to game");
        PlatformUtils.sendStatus(Status.INSTALLED, game, isPlugin);
        Assert.assertTrue(gpp.validateElementPresent(GamePanelPage.BUTTON_PLAY),
        		"The button BUTTON_PLAY is not visible");
        gpp.assertText(GamePanelPage.BUTTON_PLAY, "$Play",language);
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_SETTINGS), "The button 'Settings' is not visible on the panel 'play'");
    }

    /**
     * VIII. TestCase: Press button 'Cancel' when status PROGRESS
     * TestCase:<br>
     * <ol>
     * <li> Sends status PROGRESS with actual states<br>
     * <li> Checks that the progress bar is present and the color of progress bar is yellow;<br>
     * <li> Clicks on the button Cancel<br>
     * <li> Checks that Canceled Status is validate on the cancel block <br>
     * <li> Checks that buttons 'Yes', 'No' are validate on the cancel block<br>
     * <li> Checks that 'Download' size is validate<br>
     * <li> Checks that 'Downloaded' size is validate<br>
     * <li> Clicks on the button 'Yes'<br>
     * <li> Checks that button Update is enabled<br>
     * <li> Checks that button 'Settings' is available<br>
     * </ol>
     */
    @Test
    public void pressButtonCancel() {
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openPage(gpp);

        PlatformUtils.sendStatus(Status.PROGRESS, game, isPlugin);

        Assert.assertTrue(gpp.validateElementPresent(GamePanelPage.PROGRESS_BAR),"The PROGRESS BAR is not visible");
        log4j.info("Click cancel when progress bar is available");
        gpp.click(GamePanelPage.PROGRESS_CANCEL);
        Assert.assertTrue(gpp.validateElementPresent(GamePanelPage.PROGRESS_BLOCK_CANCEL_STATUS),"The PROGRESS BAR block with button 'Cancel' is not visible");
        log4j.info("Click buttons yes and no");
        Assert.assertTrue(gpp.validateElementPresent(GamePanelPage.CANCEL_BLOCK_BUTTON_YES),"The PROGRESS BAR block with button 'Yes' is not visible");
        Assert.assertTrue(gpp.validateElementPresent(GamePanelPage.CANCEL_BLOCK_BUTTON_NO),"The PROGRESS BAR block with button 'No' is not visible");
        log4j.info("Click button yes");
        gpp.click(GamePanelPage.CANCEL_BLOCK_BUTTON_YES);
        log4j.info("Check full update require");
        PlatformUtils.sendStatus(Status.FULL_UPDATE_REQUIRED, game, isPlugin);

        gpp.assertText(GamePanelPage.BUTTON_UPDATE, "$Update",language);
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_SETTINGS), "The button 'Settings' is not visible on the panel 'play'");
    }

    /**
     * IX. TestCase: Send JSON status UPDATE
     * TestCase:<br>
     * <ol>
     * <li> Sends status UPDATE<br>
     * <li> Checks that button Update is enabled<br>
     * <li> Checks that button 'Settings' is available<br>
     * </ol>
     */
    @Test
    public void sendStatusUpdate() {
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openPage(gpp);

        PlatformUtils.sendStatus(Status.FULL_UPDATE_REQUIRED, game, isPlugin);
        log4j.info("Check status update");
        gpp.assertText(GamePanelPage.BUTTON_UPDATE, "$Update",language);
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_SETTINGS), "The button 'Settings' is not visible on the panel 'play'");
    }

    /**
     * X. TestCase: Click button 'Cancel' when status 'Paused' in Progress<br>
     * TestCase:<br>
     * <ol>
     * <li> Sends status PAUSED with actual states<br>
     * <li> Checks that Resume, Cancel links are present.<br>
     * <li> Presses on button 'Cancel'
     * <li> Clicks on the button Yes<br>
     * <li> Checks that button Update is enabled<br>
     * <li> Checks that button 'Settings' is available<br>
     * </ol>
     */
    @Test
    public void pressButtonCancelWhenStatusPaused() {
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openPage(gpp);

        PlatformUtils.sendStatus(Status.PAUSED, game, isPlugin);

        gpp.assertText(GamePanelPage.PROGRESS_CANCEL, "$Cancel",language);

        gpp.click(GamePanelPage.PROGRESS_CANCEL);
        gpp.click(GamePanelPage.CANCEL_BLOCK_BUTTON_YES);

        PlatformUtils.sendStatus(Status.FULL_UPDATE_REQUIRED, game, isPlugin);
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_SETTINGS), "The button 'Settings' is not visible on the panel 'play'");
    }

    /**
     * XI. TestCase: Press button 'Update'<br>
     * TestCase:<br>
     * <ol>
     * <li> Sends status UPDATE<br>
     * <li> Checks that button Update is enabled<br>
     * <li> Presses on button 'Update'<br>
     * <li> Checks that the progress bar is present and the color of progress bar is yellow;<br>
     * <li> Checks that info into a progress bar (percent) is present and valid;<br>
     * <li> Checks that info under the progress par is present and valid;<br>
     * <li> Checks that links Pause, Cancel are present.<br>
     * </ol>
     */
    @Test
    public void pressButtonUpdate() {
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openPage(gpp);

        PlatformUtils.sendStatus(Status.FULL_UPDATE_REQUIRED, game, isPlugin);

        gpp.assertText(GamePanelPage.BUTTON_UPDATE, "$Update",language);
        PlatformUtils.sendStatus(Status.PROGRESS, game, isPlugin);

        Assert.assertTrue(gpp.validateElementPresent(GamePanelPage.PROGRESS_BAR),"The PROGRESS BAR is not visible");

        gpp.assertText(GamePanelPage.PROGRESS_PAUSE, "$Pause",language);
        gpp.assertText(GamePanelPage.PROGRESS_CANCEL, "$Cancel",language);
    }

    /**
     * XII. TestCase: Press button 'Resume' when status is PAUSED<br>
     * TestCase:<br>
     * <ol>
     * <li> Sends status PAUSED<br>
     * <li> Checks that links Resume, Cancel are present.<br>
     * <li> Presses on button 'Resume'<br>
     * <li> Checks that the progress bar is present and the color of progress bar is yellow;<br>
     * <li> Checks that info into a progress bar (percent) is present and valid;<br>
     * <li> Checks that info under the progress par is present and valid;<br>
     * <li> Checks that links Resume, Cancel are present.<br>
     * </ol>
     */
    @Test
    public void pressButtonResume() {
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openPage(gpp);

        PlatformUtils.sendStatus(Status.PAUSED, game, isPlugin);

        gpp.assertText(GamePanelPage.PROGRESS_RESUME, "$Resume",language);
        gpp.click(GamePanelPage.PROGRESS_RESUME);

        PlatformUtils.sendStatus(Status.PROGRESS, game, isPlugin);

        Assert.assertTrue(gpp.validateElementPresent(GamePanelPage.PROGRESS_BAR),"The PROGRESS BAR is not visible");

        gpp.assertText(GamePanelPage.PROGRESS_PAUSE, "$Pause",language);
        gpp.assertText(GamePanelPage.PROGRESS_CANCEL, "$Cancel",language);
    }



    /**
     * XV. TestCase: Check button Play after status INSTALLED success and Sign in<br>
     * TestCase:<br>
     * <ol>
     * <li> Sends status FINISHED<br>
     * <li> Sends status INSTALLED<br>
     * <li> Checks that link 'Go to game launch page' is visible<br>
     * <li> Refresh page<br>
     * <li> Checks that button 'Sign In' is available<br>
     * <li> Press button 'Sign In'<br>
     * <li> Logins on page <br>
     * <li> Checks that user has logged <br>
     * <li> Checks that button 'Play' is available<br>
     * <li> Checks that button 'Settings' is available<br>
     * </ol>
     */
    @Test
    public void checkButtonPlayWhenStatusInstalled() {
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openPage(gpp);

        PlatformUtils.sendStatus(Status.FINISHED, game, isPlugin);
        PlatformUtils.sendStatus(Status.INSTALLED, game, isPlugin);

        Assert.assertTrue(gpp.validateElementPresent(GamePanelPage.LINK_INSTALL_SUCCESS),"The link about INSTALL was SUCCESS is not visible");

        gpp.click(GamePanelPage.LINK_INSTALL_SUCCESS);

        PlatformUtils.sendStatus(Status.INSTALLED, game, isPlugin);
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_SETTINGS), "The button 'Settings' is not visible on the panel 'play'");
    }

    /**
     * TestCase: Check Maintenance when Status is INSTALLED<br>
     * TestCase:<br>
     * <ol>
     * <li> Sets status Maintenance into a browser (through cookies) <br>
     * <li> Sends status INSTALLED <br>
     * <li> Checks that status Maintenance is validate <br>
     * <li> Checks that button Play isn't displayed <br>
     * </ol>
     */
    @Test(enabled = false)
    public void checkBigMaintenance() {
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openPage(gpp);
        gpp.setMaintenance(game);
        gpp.refreshPage();
        PlatformUtils.sendStatus(Status.INSTALLED, game, isPlugin);
        gpp.assertText(GamePanelPage.STATUS_MAINTENANCE_BIG, "$Big_maintenance",language);
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_PLAY), "The button 'Play' is not visible on the panel 'play'");

        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_SETTINGS), "The button 'Settings' is not visible on the panel 'play'");

    }
    
    @Test(description = "the test to check that there is no ability to launch game from install page", enabled=true)
    public void testGoToPlay() {
    	log4j.info("the test to check that there is no ability to launch game from install page");
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openUrlIn4game(game.getURL() + "install");
        PlatformUtils.sendStatus(Status.INSTALLED, game, isPlugin);
        
        Assert.assertTrue(gpp.validateElementPresent(GamePanelPage.LINK_INSTALL_SUCCESS),"The link about INSTALL was SUCCESS is not visible");

    }



    @AfterClass
    public void deleteCookies(){
        LocalDriverManager.getDriverController().deleteAllCookies();
    }


}
