package ru.inn.autotests.webdriver.tests.plugin;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.inn.autotests.webdriver.composite.pages.GamePanelPage;
import ru.inn.autotests.webdriver.composite.pages.MainPage;
import ru.inn.autotests.webdriver.composite.popups.PopupDownloadApplication;
import ru.inn.autotests.webdriver.tests.AbstractTest;
import ru.inn.autotests.webdriver.toolkit.PlatformUtils;

/**
 * User: pavel.popov
 */
public class DownloadApplication extends AbstractTest {


    @BeforeClass
    public void initGameAndUser() {
        if(System.getenv("plugin")!=null)
            isPlugin= Boolean.parseBoolean(System.getenv("plugin"));
    }


    /**
     * Test downloads the application from footer
     */
    @Test
    public void testDownloadApplicationFromFooter() {
        MainPage mPage = new MainPage();
        mPage.openPage(mPage);
        log4j.info("\n[TESTCASE]Main page application download from footer");
        mPage.click(MainPage.BUTTON_REINSTALL_APP);

        PopupDownloadApplication pda = new PopupDownloadApplication();
        Assert.assertTrue(pda.validateElementPresent(PopupDownloadApplication.BUTTON_ACCEPT_LICENSE), "The BUTTON_ACCEPT_LICENSE in the popup Download application is not visible");
        pda.click(PopupDownloadApplication.BUTTON_ACCEPT_LICENSE);
        mPage.validateDownloadedApplication(null);
    }

    /**
     * Test downloads the application from game panel
     */
    @Test
    public void testDownloadApplicationFromGame() {
        if(isPlugin)
            throw new SkipException("Because of installed plugin");
        switcherLocation(TypeUser.TEST_USER);
    	GamePanelPage gpp = new GamePanelPage(game);
        gpp.openPage(gpp);
        log4j.info("\n[TESTCASE]Main page application download from game panel for " + game.getAppName());
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_INSTALL_WITH_NO_PLUGIN), "The BUTTON_INSTALL_PLUGIN is not visible");
        gpp.click(GamePanelPage.BUTTON_INSTALL_WITH_NO_PLUGIN);

        PopupDownloadApplication pda = new PopupDownloadApplication();
        Assert.assertTrue(pda.validateElementPresent(PopupDownloadApplication.BUTTON_ACCEPT_LICENSE), "The BUTTON_ACCEPT_LICENSE in the popup Download application is not visible");
        pda.click(PopupDownloadApplication.BUTTON_ACCEPT_LICENSE);

        gpp.validateDownloadedApplication(game);
    }



    @Test
    public void testDownloadApplicationWithError720() {
        GamePanelPage gpp = new GamePanelPage(game);
        gpp.openPage(gpp);
        log4j.info("\n[TESTCASE]Main page application download with 720 error for game " + game.getAppName());
        PlatformUtils.sendError720(isPlugin);
        PopupDownloadApplication pda = new PopupDownloadApplication();
        Assert.assertTrue(pda.validateElementPresent(PopupDownloadApplication.BUTTON_ACCEPT_LICENSE), "The BUTTON_ACCEPT_LICENSE in the popup Download application is not visible");
        pda.click(PopupDownloadApplication.BUTTON_ACCEPT_LICENSE);

        gpp.validateDownloadedApplication(null);
    }

}
