package ru.inn.autotests.webdriver.tests.plugin;


import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.inn.autotests.webdriver.composite.pages.GameCell;
import ru.inn.autotests.webdriver.composite.pages.MainPage;
import ru.inn.autotests.webdriver.composite.pages.UserBarPage;
import ru.inn.autotests.webdriver.composite.popups.PopupDownloadApplication;
import ru.inn.autotests.webdriver.games.Game;
import ru.inn.autotests.webdriver.games.PointBlank;
import ru.inn.autotests.webdriver.tests.AbstractTest;

/**
 * Created with IntelliJ IDEA.
 */
public class DownloadApplicationFromMainPageCells extends AbstractTest {


    @BeforeClass
    public void initGameAndUser() {
        if(System.getenv("plugin")!=null)
            isPlugin= Boolean.parseBoolean(System.getenv("plugin"));
    }


    private void basicTestAction (Game game) {
    	MainPage mPage = new MainPage();
        mPage.openPage(mPage);
        log4j.info("\n[TESTCASE]Main page application download from cells for " + game.getAppName());
        GameCell gameCell = new GameCell(game);
        gameCell.clickOnDownloadButton(game);
        PopupDownloadApplication pda = new PopupDownloadApplication();
        Assert.assertTrue(pda.validateElementPresent(PopupDownloadApplication.BUTTON_ACCEPT_LICENSE), "The BUTTON_ACCEPT_LICENSE in the popup Download application is not visible");
        pda.click(PopupDownloadApplication.BUTTON_ACCEPT_LICENSE);
        gameCell.validateDownloadedApplication(game);
    }


    @DataProvider
    public Object[][] initGames(){
        Game[][] gameList=new Game[games.size()][1]; int i=0;
        for(Game game : games.values())
        {
            gameList[i][0]=game;
            i++;
        }
        return gameList;
    }


    @Test(dataProvider = "initGames")
    public void testDownloadAppFromCells(Game game){
        if(isPlugin)
            throw new SkipException("Because of installed plugin");

        basicTestAction(game);
    }


    @Test(description = "test to check ability download application from nav bar",enabled = false)
    public void testDownloadAppFromNavBar(){
        log4j.info("test to check ability download application from nav bar started");
        Game game=new PointBlank();
        MainPage mainPage=new MainPage();
        UserBarPage ubp=new UserBarPage();
        mainPage.openPage(mainPage);
        ubp.showHoverGameInNavBar(game);
        Assert.assertTrue(ubp.validateElementVisible(String.format(UserBarPage.BUTTON_INSTALL_NAV_BAR, game.getServiceId())),"Button install in nav bar is not visible for game "+game.getAppName());
        ubp.click(String.format(UserBarPage.BUTTON_INSTALL_NAV_BAR, game.getServiceId()));
        PopupDownloadApplication pda = new PopupDownloadApplication();
        Assert.assertTrue(pda.validateElementPresent(PopupDownloadApplication.BUTTON_ACCEPT_LICENSE), "The BUTTON_ACCEPT_LICENSE in the popup Download application is not visible");
        pda.click(PopupDownloadApplication.BUTTON_ACCEPT_LICENSE);
        mainPage.validateDownloadedApplication(game);
    }

}
