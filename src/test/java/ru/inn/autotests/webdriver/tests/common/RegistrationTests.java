package ru.inn.autotests.webdriver.tests.common;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jetty.util.log.Log;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import ru.inn.autotests.webdriver.common.PrepareUser;
import ru.inn.autotests.webdriver.common.entity.Message;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.composite.common.Captcha;
import ru.inn.autotests.webdriver.composite.pages.BuySerialCodePage;
import ru.inn.autotests.webdriver.composite.pages.GamePanelPage;
import ru.inn.autotests.webdriver.composite.pages.LoginPage;
import ru.inn.autotests.webdriver.composite.pages.MainPage;
import ru.inn.autotests.webdriver.composite.pages.RegisterPage;
import ru.inn.autotests.webdriver.composite.pages.SettingsPage;
import ru.inn.autotests.webdriver.composite.pages.UserBarPage;
import ru.inn.autotests.webdriver.composite.popups.PopupPremium;
import ru.inn.autotests.webdriver.games.DragonsProphet;
import ru.inn.autotests.webdriver.games.Game;
import ru.inn.autotests.webdriver.games.Lineage2Classic;
import ru.inn.autotests.webdriver.games.PocketFort;
import ru.inn.autotests.webdriver.tests.AbstractTest;
import ru.inn.autotests.webdriver.tests.AbstractTest.TypeUser;
import ru.inn.autotests.webdriver.toolkit.LocalDriverManager;
import ru.inn.autotests.webdriver.toolkit.MailProvider;
import ru.inn.autotests.webdriver.toolkit.PlatformUtils;
import ru.inn.autotests.webdriver.toolkit.PlatformUtils.Status;
import ru.inn.autotests.webdriver.toolkit.UserData;

/**
 * Created with IntelliJ IDEA.
 * User: Sergey.Kashapov
 * Date: 03.06.13
 * Time: 12:46
 * To change this template use File | Settings | File Templates.
 */
public class RegistrationTests extends AbstractTest {

	PrepareUser prepareUser = new PrepareUser();
	
	
    @Test(description = "test to register new user with open password in form of registration")
    public void testRegisterNewUserWithOpenEye() {
    	//OperationsHelper.baseUrl="https://game-reg-pixel-stript-ru.4gametest.com/";
        log4j.info("Test register new user with open eye started");
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        RegisterPage registerPage = new RegisterPage();
        Assert.assertTrue(registerPage.validateElementVisible(UserBarPage.BUTTON_SIGN_UP), "There is no the button 'Sign In or Register' in the userbar. Maybe the User is authorized already.");
        registerPage.click(UserBarPage.BUTTON_SIGN_UP);
        String mainEmail=OperationsHelper.selectRandomEmail();
        registerPage.fillRegistration(mainEmail);
        Assert.assertTrue(registerPage.validateElementIsNotVisible(RegisterPage.BUTTON_CREATE_ACCOUNT), "Register form is open");
		Message message = MailProvider.getLastMessage(mainEmail);
        message.assertBackgroundColor("YELLOW");
        String linkToRegist = MailProvider.getLinkFromMail(mainEmail);
        registerPage.openUrlIn4game(linkToRegist);
        registerPage.fillPassOpenEyeForm("123456");
        Assert.assertTrue(registerPage.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized after the registration.");

    }


    @Test(description = "test to registration new user", enabled = false)
    public void testRegisterNewUser() {
        log4j.info("Test register from game new user started");
        List<String> listOfEmail = new ArrayList<String>();
        MainPage mainPage = new MainPage();
        RegisterPage registerPage = new RegisterPage();
        for (int i = 0; i < 45; i++) {
	        mainPage.openPage(mainPage);
	
	        String mainEmail=OperationsHelper.selectRandomEmail();
	        Assert.assertTrue(registerPage.validateElementVisible(UserBarPage.BUTTON_SIGN_UP),
	                "There is no the button 'Sign In or Register' in the userbar. Maybe the User is authorized already.");
	        registerPage.click(UserBarPage.BUTTON_SIGN_UP);
	        registerPage.fillRegistration(mainEmail);
	        Assert.assertTrue(registerPage.validateElementIsNotVisible(RegisterPage.BUTTON_CREATE_ACCOUNT), "Register form is open");
	        Message message = MailProvider.getLastMessage(mainEmail);
	        message.assertBackgroundColor("YELLOW");
	        String linkToRegist = MailProvider.getLinkFromMail(mainEmail);
	        listOfEmail.add(mainEmail);
	        registerPage.openUrlIn4game(linkToRegist);
	        registerPage.fillPassForm("s3dsaber3d");
	        Assert.assertTrue(registerPage.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized after the registration.");
	        registerPage.openUrlIn4game("logout");
        }
        for (String email : listOfEmail) {
			System.out.println(email);
		}
    }


    @Test(description = "test to register new user from game page,and check brand of this game")
    public void testRegisterNewUserBrandGame() {
        log4j.info("Test register from game new user started");
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openUrlIn4game(game.getURL() + "install");
        RegisterPage registerPage = new RegisterPage();
        String mainEmail=OperationsHelper.selectRandomEmail();
        Assert.assertTrue(registerPage.validateElementVisible(UserBarPage.BUTTON_SIGN_UP), "There is no the button 'Sign In or Register' in the userbar. Maybe the User is authorized already.");
        registerPage.click(UserBarPage.BUTTON_SIGN_UP);
        registerPage.fillRegistration(mainEmail);
        Assert.assertTrue(registerPage.validateElementIsNotVisible(RegisterPage.BUTTON_CREATE_ACCOUNT),"Register form is still visible");
        Message message = MailProvider.getLastMessage(mainEmail);
        message.assertBackgroundColor("YELLOW");
        message.checkLinkInMessage(".png");
        String linkToRegist = MailProvider.getLinkFromMail(mainEmail);
        registerPage.openUrlIn4game(linkToRegist);
        registerPage.fillPassForm("123456");
        Assert.assertTrue(registerPage.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD),
                "The user is not authorized after the registration.");
    }


    @Test(description = "check that captcha will not show after one incorrect try",enabled = false)
    public void testCaptchaNotDisplay() {
        log4j.info("Test captcha not display when register with incorrect mail");
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        RegisterPage registerPage = new RegisterPage();
        Assert.assertTrue(registerPage.validateElementVisible(UserBarPage.BUTTON_SIGN_UP), "There is no the button 'Sign In or Register' in the userbar. Maybe the User is authorized already.");
        registerPage.click(UserBarPage.BUTTON_SIGN_UP);
        registerPage.fillRegistration("sdsd a s");
        Assert.assertFalse(registerPage.isVisible(Captcha.CAPTCHA),
                "Captcha is visible after one incorrect enter of mail");
    }


    @Test(description = "check that captcha will show after enter existing mail in register form")
    public void testCaptchaDisplay() {
        log4j.info("Test captcha display when register with existing user");
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        RegisterPage registerPage = new RegisterPage();
        Assert.assertTrue(registerPage.validateElementVisible(UserBarPage.BUTTON_SIGN_UP), "There is no the button 'Sign In or Register' in the userbar. Maybe the User is authorized already.");
        registerPage.click(UserBarPage.BUTTON_SIGN_UP);
        User user = switcherLocation(TypeUser.HARDCORE_USER);
        registerPage.fillRegistration(user.getMainEmail());
        Assert.assertTrue(registerPage.validateElementVisible(Captcha.CAPTCHA),
                "Captcha is not visible after enter existing mail in register form");
    }

    
    @Test(description = "test to check registartion with pid L2 Cl,and assert that cookie with partner_pid is enable")
    public void testRegisterNewUserWithPidL2Cl() {
    	OperationsHelper.baseUrl="https://game-reg-pixel-stript-ru.4gametest.com/";
        log4j.info("The test to check registartion with pid L2 Cl,and assert that cookie with partner_pid is enable");
        String pid ="5115666";
        game = new Lineage2Classic();
        MainPage mainPage = new MainPage();
        mainPage.openUrlIn4game(game.getURL() + "play/?pid=" + pid );
        Cookie pidCookie = LocalDriverManager.getDriverController().getCookie("pid");
        Assert.assertTrue(pidCookie.getValue().equals(pid),"Cokie pid" + pidCookie.getValue() + " doesn't match with " + pid);
        RegisterPage registerPage = new RegisterPage();
        String mainEmail=OperationsHelper.selectRandomEmail();
        
        Assert.assertTrue(registerPage.validateElementVisible(UserBarPage.BUTTON_SIGN_UP), "There is no the button 'Sign In or Register' in the userbar. Maybe the User is authorized already.");
        registerPage.click(UserBarPage.BUTTON_SIGN_UP);
        registerPage.fillRegistration(mainEmail);
        Assert.assertTrue(registerPage.validateElementIsNotVisible(RegisterPage.BUTTON_CREATE_ACCOUNT),"Register form is still visible");
        Message message = MailProvider.getLastMessage(mainEmail);
        message.assertBackgroundColor("YELLOW");
        message.checkLinkInMessage(".png");
        Assert.assertTrue(message.getNotificationLink().contains("partner_id="+pid),"register link doesn't contain the pid = " +pid);
        String linkToRegist = MailProvider.getLinkFromMail(mainEmail);
        registerPage.openUrlIn4game(linkToRegist);
        registerPage.fillPassForm("123456");
        Assert.assertTrue(registerPage.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD),
                "The user is not authorized after the registration.");
        
       GamePanelPage gpp = new GamePanelPage(game);
       prepareUser.addMoneyById(UserData.getMasterAccountId(), "2000");
       Assert.assertTrue(gpp.validateUrlContains(game.getURL()), "We are mot on the page " + game.getURL() + " after the branding registration");
       Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_BUY_SUBS), "BUTTON_BUY_SUBS is not visible on the page " + gpp.getCurrentUrl());
       gpp.click(GamePanelPage.BUTTON_BUY_SUBS);
       gpp.clickLicenceIfItPresent(game);
       Assert.assertTrue(gpp.validateElementVisible(PopupPremium.FORM_BUY_PREMIUM),
               "The premium pop-up is not visible " + game.getAppName());
       gpp.mouseClick(PopupPremium.RADIO_THIRD_MONTH);
       gpp.click(PopupPremium.BUTTON_BUY);
       gpp.clickOkInAlert();
       Assert.assertTrue(gpp.validateElementIsNotVisible(PopupPremium.FORM_BUY_PREMIUM), "Premium popup is not closed");
       
       
    }
    
    @Test(description = "test to check registartion with pid,and assert that cookie with partner_pid is enable")
    public void testRegisterNewUserWithPid() {
        log4j.info("Test register new user with PID started");
        String pid ="5114092";
        String subPidName = "name1";
        String subPidValue = "12";
        MainPage mainPage = new MainPage();
        mainPage.openUrlIn4game("/registration/?pid=" +
        		pid + "&" + subPidName + "=" + subPidValue);
        Cookie pidCookie = LocalDriverManager.getDriverController().getCookie("pid");
        Assert.assertTrue(pidCookie.getValue().equals(pid),"Cokie pid" + pidCookie.getValue() + " doesn't match with " + pid);
        //Cookie pidQueryParamsCookie = LocalDriverManager.getDriverController().getCookie("pid-query-params");
        //Assert.assertTrue(pidQueryParamsCookie != null, "Cokie pidQueryParamsCookie is not present");
        //Assert.assertTrue(pidQueryParamsCookie.getValue().contains(subPidName),"Cokie pidQueryParamsCookie " + 
        //		pidQueryParamsCookie.getValue() + " doesn't contain " + subPidName);
        RegisterPage registerPage = new RegisterPage();
        String mainEmail=OperationsHelper.selectRandomEmail();
        Assert.assertTrue(registerPage.validateElementPresent(RegisterPage.FIELD_REGISTRATION), "There is no the field to typing the email for registration on the register page ");
        registerPage.type(RegisterPage.FIELD_REGISTRATION,mainEmail);
        Captcha captcha=new Captcha();
        captcha.typeCaptchaIfPresent("987654");
        registerPage.click(RegisterPage.BUTTON_CREATE_ACCOUNT_FROM_REGISTRATION);
        Message message = MailProvider.getLastMessage(mainEmail);
        Assert.assertTrue(message.getNotificationLink().contains("partner_id="+pid),"register link doesn't contain the pid = " +pid);
        Assert.assertTrue(message.getNotificationLink().contains("referrer="),"register link doesn't contain the referrer= ");
        String linkToRegist = MailProvider.getLinkFromMail(mainEmail);
        registerPage.openUrlIn4game(linkToRegist);
        registerPage.fillPassForm("123456");
        Assert.assertTrue(registerPage.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized after the registration.");
        String pixelScript = "Pixel script";
		Assert.assertTrue(registerPage.getPageSource().contains(pixelScript), "The page doesn't contain a pixel script for pid " + pid + 
        		" .The pixel script was " + pixelScript);
    }

    @Test(description = "test to check a redirect to registration works fine")
    public void testBrandingRedirect(){
    	log4j.info("test to check a redirect to registration works fine");
        RegisterPage registerPage = new RegisterPage();
        registerPage.openUrlIn4game("registration/"+game.getServiceId());
        Assert.assertTrue(registerPage.validateElementPresent(RegisterPage.FIELD_REGISTRATION), 
        		"There is no the field to typing the email for registration on the register page " + game.getAppName());
        Assert.assertTrue(registerPage.validateElementVisible(String.format(RegisterPage.GAME_ICON,game.getServiceId())), 
        		"There is no an game icon on a branding registration page  " + game.getAppName());
    }


    @Test(description = "test to register new user from the mobile game page,and check brand of this game")
    public void testRegisterBrandMobileGame() {
    	Game game = new PocketFort();
        log4j.info("test to register new user from the mobile game page,and check brand of this game " + game.getAppName());
        prepareUser.resetCacheIs("qa");
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openUrlIn4game(game.getURL());
        RegisterPage registerPage = new RegisterPage();
        String mainEmail=OperationsHelper.selectRandomEmail();
        String oldCurrentUrl = gpp.getCurrentUrl();
        Assert.assertTrue(registerPage.validateElementVisible(UserBarPage.BUTTON_SIGN_UP), "There is no the button 'Sign In or Register' in the userbar. Maybe the User is authorized already.");
        registerPage.click(UserBarPage.BUTTON_SIGN_UP);
        registerPage.fillRegistration(mainEmail);
        Assert.assertTrue(registerPage.validateElementIsNotVisible(RegisterPage.BUTTON_CREATE_ACCOUNT),"Register form is still visible");
        Message message = MailProvider.getLastMessage(mainEmail);
        message.assertBackgroundColor("YELLOW");
        message.checkLinkInMessage(".png");
        message.assertTitleGameName(game.getAppName());
        String linkToRegist = MailProvider.getLinkFromMail(mainEmail);
        int numOldMail = MailProvider.getNumberOfMessages(mainEmail);
        registerPage.openUrlIn4game(linkToRegist);
        registerPage.fillPassForm("123456");
        Assert.assertTrue(registerPage.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD),
                "The user is not authorized after the registration.");
        Assert.assertTrue(gpp.getCurrentUrl().contains(oldCurrentUrl),
                "The user is not in the correct location after signing-up from the game page " + oldCurrentUrl+ ". Now we are on the " + gpp.getCurrentUrl());
        Assert.assertTrue(MailProvider.isNewMessagePresent(mainEmail, numOldMail),"Welcome message has not come for " + game.getAppName());
        message = MailProvider.getLastMessage(mainEmail);
        message.assertBackgroundColor("YELLOW");
        message.checkLinkInMessage(".jpg");
        message.assertTitleGameName(game.getAppName());
    }
    
    @Test(description = "The test to register new user from the 'buy serial code' page ", enabled = true)
    public void testRegisterForBuyingSerialCode() {
    	//OperationsHelper.baseUrl = "https://sims4-ru.4gametest.com/";
    	BuySerialCodePage bsc = new BuySerialCodePage();
        log4j.info("The test to register new user from the 'buy serial code' page " + bsc.getPageUrl());
        bsc.openUrlIn4game(bsc.getPageUrl());
        RegisterPage registerPage = new RegisterPage();
        String mainEmail=OperationsHelper.selectRandomEmail();
        int numOldMail = MailProvider.getNumberOfMessages(mainEmail);
        String oldCurrentUrl = bsc.getCurrentUrl();
        Assert.assertTrue(registerPage.validateElementVisible(UserBarPage.BUTTON_SIGN_UP), "There is no the button 'Sign In or Register' in the userbar. Maybe the User is authorized already.");
        registerPage.click(UserBarPage.BUTTON_SIGN_UP);
        registerPage.fillRegistration(mainEmail);
        Assert.assertTrue(registerPage.validateElementIsNotVisible(RegisterPage.BUTTON_CREATE_ACCOUNT),"Register form is still visible");
        Assert.assertTrue(MailProvider.isNewMessagePresent(mainEmail, numOldMail),"The Register message has not come ");
        Message message = MailProvider.getLastMessage(mainEmail);
        message.assertBackgroundColor("YELLOW");
        message.checkLinkInMessage(".png");
        String linkToRegist = MailProvider.getLinkFromMail(mainEmail);
        numOldMail = MailProvider.getNumberOfMessages(mainEmail);
        registerPage.openUrlIn4game(linkToRegist);
        registerPage.fillPassForm("123456");
        Assert.assertTrue(registerPage.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD),
                "The user is not authorized after the registration.");
        Assert.assertTrue(bsc.getCurrentUrl().contains(oldCurrentUrl),
                "The user is not in the correct location after signing-up from the game page " + oldCurrentUrl+ ". Now we are on the " + bsc.getCurrentUrl());
        Assert.assertTrue(MailProvider.isNewMessagePresent(mainEmail, numOldMail),"Welcome message has not come for " + game.getAppName());
        message = MailProvider.getLastMessage(mainEmail);
        message.assertBackgroundColor("YELLOW");
    }

}
