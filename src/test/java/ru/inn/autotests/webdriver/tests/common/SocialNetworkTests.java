package ru.inn.autotests.webdriver.tests.common;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.inn.autotests.webdriver.common.PrepareUser;
import ru.inn.autotests.webdriver.common.entity.Message;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.composite.pages.*;
import ru.inn.autotests.webdriver.tests.AbstractTest;
import ru.inn.autotests.webdriver.tests.AbstractTest.TypeUser;
import ru.inn.autotests.webdriver.toolkit.LocalDriverManager;
import ru.inn.autotests.webdriver.toolkit.MailProvider;
import ru.inn.autotests.webdriver.toolkit.UserData;


public class SocialNetworkTests extends AbstractTest {

    PrepareUser prepareUser = new PrepareUser();
    
    /*@BeforeClass
    public void initUser() {
        OperationsHelper.baseUrl="https://ubersecurity-settings-ru.4gametest.com/";
    }*/

    @Test(description = "test to check that Default Fb Avatar is Present", enabled = true)
    public void testDefaultFbAvatarPresent() throws Exception {
        log4j.info("test to check that Default Fb Avatar is Present");
        //OperationsHelper.baseUrl="https://subimport-play-ru.4gametest.com/";
        String loginFb = "ummolchanov@yandex.ru";
        String passwFb = "4game.ru";
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        //lp.click(UserBarPage.BUTTON_SIGN_UP);
        SocialNetworkPage snp = new SocialNetworkPage();
        snp.clickToOpenPopup(UserBarPage.BUTTON_FACEBOOK_LOGIN);
        snp.loginInFaceBook(loginFb, passwFb);
        Assert.assertTrue(snp.validateElementVisible(SocialNetworkPage.DEFAULT_FB_AVATAR), "Default Facebook' avatar is not present on the pop-up");
        mainPage.openPage(mainPage);
        LocalDriverManager.getDriverController().get(OperationsHelper.baseUrl);
        LocalDriverManager.getDriverController().deleteAllCookies();
        snp.deleteFbCookies();
        snp.deleteVkCookies();
    }

    @Test(description = "test to check that user has the ability to link fb account with another existing 4game account", enabled = true)
    public void testBindFbWithAccount() throws Exception {
        log4j.info("test to check that user has the ability to link fb account with another existing 4game account");
        String loginFb = "testingfacebook@yandex.ru";
        String passwFb = "4game.ru";
       User  user = switcherLocation(TypeUser.USER);
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        SocialNetworkPage snp = new SocialNetworkPage();
        snp.clickToOpenPopup(UserBarPage.BUTTON_FACEBOOK_LOGIN);
        snp.loginInFaceBook(loginFb, passwFb);
        Assert.assertTrue(snp.validateElementVisible(SocialNetworkPage.LINK_BIND_ACCOUNT), "The social pop-up is not visible");
        snp.click(SocialNetworkPage.LINK_BIND_ACCOUNT);
        Assert.assertTrue(snp.validateElementVisible(SocialNetworkPage.FIELD_EMAIL_OR_LOGIN), "The social pop-up to bind with existing account is not visible");
        snp.type(SocialNetworkPage.FIELD_EMAIL_OR_LOGIN, user.getMainEmail());
        snp.type(SocialNetworkPage.FIELD_PASSWORD, user.getPassword());
        snp.click(SocialNetworkPage.BUTTON_BIND_AND_ENTER);
        UserBarPage ubp = new UserBarPage();
        Assert.assertTrue(ubp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized after the binding fb.");
        UserData ud = new UserData();
        prepareUser.deleteFacebookConnect(ud.getMasterAccountId());
        mainPage.openPage(mainPage);
        LocalDriverManager.getDriverController().get(OperationsHelper.baseUrl);
        LocalDriverManager.getDriverController().deleteAllCookies();
        snp.deleteFbCookies();
        snp.deleteVkCookies();
    }

    @Test(description = "test to check that Default Vk Avatar is Present")
    public void testDefaultVkAvatarPresent() throws Exception {
        log4j.info("test to check that Default Vk Avatar is Present");
        String loginVk = "testfb1@yandex.ru";
        String passwVk = "4game.ru";
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        SocialNetworkPage snp = new SocialNetworkPage();
        snp.clickToOpenPopup(UserBarPage.BUTTON_VK_LOGIN);
        snp.loginInVk(loginVk, passwVk);
        Assert.assertTrue(snp.validateElementVisible(SocialNetworkPage.DEFAULT_VK_AVATAR), "Default Vkontakte' avatar is not present on the pop-up");
        mainPage.openPage(mainPage);
        LocalDriverManager.getDriverController().get(OperationsHelper.baseUrl);
        LocalDriverManager.getDriverController().deleteAllCookies();
        snp.deleteFbCookies();
        snp.deleteVkCookies();

    }

    @Test(description = "Test testFbWithChangedCookies on the login pop-up started")
    public void testFbWithChangedCookies() throws Exception {
        log4j.info("Test testFbWithChangedCookies on the login pop-up started");
        String loginFb = "changedcookies@yandex.ru";
        String passwFb = "4game.ru";
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        mainPage.addCookie("inn-user", "21312312");
        mainPage.openPage(mainPage);
        SocialNetworkPage snp = new SocialNetworkPage();
        snp.clickToOpenPopup(UserBarPage.BUTTON_FACEBOOK_LOGIN);
        snp.loginInFaceBook(loginFb, passwFb);
        Assert.assertTrue(snp.validateElementVisible(SocialNetworkPage.FORM_SOCIAL), "The social pop-up is not visible");
        mainPage.openPage(mainPage);
        LocalDriverManager.getDriverController().get(OperationsHelper.baseUrl);
        LocalDriverManager.getDriverController().deleteAllCookies();
        snp.deleteFbCookies();
        snp.deleteVkCookies();
    }

    @Test(description = "Test testVkWithChangedCookies on the login pop-up started")
    public void testVkWithChangedCookies() throws Exception {
        log4j.info("Test testVkWithChangedCookies on the login pop-up started");
        String loginVk = "testfb1@yandex.ru";
        String passwVk = "4game.ru";
        MainPage mainPage = new MainPage();
        mainPage.openPage(mainPage);
        mainPage.addCookie("inn-user", "21312312");
        mainPage.openPage(mainPage);
        SocialNetworkPage snp = new SocialNetworkPage();
        snp.clickToOpenPopup(UserBarPage.BUTTON_VK_LOGIN);
        snp.loginInVk(loginVk, passwVk);
        Assert.assertTrue(snp.validateElementVisible(SocialNetworkPage.FORM_SOCIAL), "The social pop-up is not visible");
        mainPage.openPage(mainPage);
        LocalDriverManager.getDriverController().get(OperationsHelper.baseUrl);
        LocalDriverManager.getDriverController().deleteAllCookies();
        snp.deleteFbCookies();
        snp.deleteVkCookies();

    }

    @Test(description = "test to check that Fb autorization on the 'registration/urlProject/' leads to lineage2")
    public void testFbRegistrationThroughLanding() throws Exception {
        log4j.info("test to check that Fb autorization on the 'registration/urlProject/' leads to lineage2");
        String loginFb = "testingexist@yandex.ru";
        String passwFb = "4game.ru";
        RegisterPage rp = new RegisterPage();
        rp.openUrlIn4game("/registration" + game.getURL());
        SocialNetworkPage snp = new SocialNetworkPage();
        snp.clickToOpenPopup(RegisterPage.BUTTON_FB);
        snp.loginInFaceBook(loginFb, passwFb);
        Assert.assertTrue(rp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD),
                "The user is not authorized after the registration.");
        Assert.assertTrue(rp.getCurrentUrl()
                .contains(game.getURL()),
                "The user is not in the correct location " + game.getGameName());
        MainPage mp = new MainPage();
        mp.openPage(mp);
        LocalDriverManager.getDriverController().get(OperationsHelper.baseUrl);
        LocalDriverManager.getDriverController().deleteAllCookies();
        snp.deleteFbCookies();
        snp.deleteVkCookies();
    }

    @Test(description = "test to register new user through vk")
    public void testRegisterVk() {
        log4j.info("test to register new user through vk");
        String loginVk = "testfb1@yandex.ru";
        String passwVk = "4game.ru";
        String mainEmail = OperationsHelper.selectRandomEmail();
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openPage(gpp);
        UserBarPage ubp = new UserBarPage();
        SocialNetworkPage snp = new SocialNetworkPage();
        snp.clickToOpenPopup(UserBarPage.BUTTON_VK_LOGIN);
        snp.loginInVk(loginVk, passwVk);
        Assert.assertTrue(snp.validateElementVisible(SocialNetworkPage.FORM_SOCIAL), "The social pop-up is not visible");
        //snp.type(SocialNetworkPage.FIELD_VK_EMAIL, mainEmail);
        snp.click(SocialNetworkPage.CHECKBOX_LICENCE_AGREE);
        snp.click(SocialNetworkPage.BUTTON_SOCIAL_REGISTRATION);
        Assert.assertTrue(ubp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized after the registration.");
        Assert.assertTrue(ubp.getCurrentUrl().contains(game.getURL()), "The url of the page after registration doesn't contain game's url " + game.getURL());
        UserData ud = new UserData();
        prepareUser.deleteVkConnect(ud.getMasterAccountId());
        prepareUser.changeMainMail(ud.getMasterAccountId(), mainEmail);
        Message message = MailProvider.getLastMessage(loginVk);
        message.assertBackgroundColor("GRAY");
        SettingsPage sp = new SettingsPage(language);
        sp.openPage(sp);
        Assert.assertTrue(sp.validateElementPresent(SettingsPage.LABEL_MAIL_OF_USER), "The mail was not added correctly");
        gpp.openPage(gpp);
        LocalDriverManager.getDriverController().get(OperationsHelper.baseUrl);
        LocalDriverManager.getDriverController().deleteAllCookies();
        snp.deleteFbCookies();
        snp.deleteVkCookies();
    }


}
