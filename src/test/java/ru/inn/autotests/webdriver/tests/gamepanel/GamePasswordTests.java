package ru.inn.autotests.webdriver.tests.gamepanel;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import ru.inn.autotests.webdriver.common.PrepareUser;
import ru.inn.autotests.webdriver.common.entity.Message;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.composite.pages.GamePanelPage;
import ru.inn.autotests.webdriver.composite.pages.LoginPage;
import ru.inn.autotests.webdriver.composite.pages.UserBarPage;
import ru.inn.autotests.webdriver.games.Game;
import ru.inn.autotests.webdriver.games.Lineage2eu;
import ru.inn.autotests.webdriver.games.PlanetSide2;
import ru.inn.autotests.webdriver.tests.AbstractTest;
import ru.inn.autotests.webdriver.tests.AbstractTest.TypeUser;
import ru.inn.autotests.webdriver.toolkit.GameData;
import ru.inn.autotests.webdriver.toolkit.MailProvider;
import ru.inn.autotests.webdriver.toolkit.PlatformUtils;
import ru.inn.autotests.webdriver.toolkit.UserData;



public class GamePasswordTests extends AbstractTest {

    PrepareUser prepareUser = new PrepareUser();
    
    
    @Test(description = "test to check that user has the ability to set a password for the game", enabled = false)
    public void testSetGamePassword() {
        //Game game = new Lineage2eu();
    	//OperationsHelper.baseUrl="https://auth-fix-ru.4gametest.com/";
        log4j.info("test to check that user has the ability to set a password for the game " + game.getGameName());
        User user = switcherLocation(TypeUser.USER);
        PrepareUser prepareUser = new PrepareUser();
        prepareUser.createServiceAccountId(user, game);
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");

        gpp.addCookie("promoPopupShowed__" + game.getServiceId()+"__" + UserData.getMasterAccountId(), "1");
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        
        gpp.waitToPlayGame(false);
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_SETTINGS),
                "Button 'settings in game panel' is not Visible " + game.getAppName());
        gpp.click(GamePanelPage.BUTTON_SETTINGS);
        gpp.click(GamePanelPage.BUTTON_SET_PASSWORD);
        String newPassword = "123456";
        gpp.fillSetPasswordForm(newPassword, user.getPassword());
        Assert.assertTrue(gpp.validateElementIsNotVisible(GamePanelPage.FIELD_NEW_PASSWORD),
                "Form 'set game password' is still Visible " + game.getAppName());
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_PLAY_WITH_PASSWORD),
                "Button 'Play with a password' is not Visible " + game.getAppName());
        gpp.click(GamePanelPage.BUTTON_PLAY_WITH_PASSWORD);
        Assert.assertTrue(lp.validateElementVisible(GamePanelPage.FIELD_GAME_PASSWORD), 
        		"There is no FIELD_GAME_PASSWORD after clicking on the BUTTON_PLAY_WITH_PASSWORD");
        gpp.type(GamePanelPage.FIELD_GAME_PASSWORD, newPassword);
        gpp.click(GamePanelPage.BUTTON_PLAY_WITH_PASSWORD_ARROW);
        gpp.clickPopupIfItPresent(game);
        Assert.assertTrue(PlatformUtils.detectGameProcess(game, 30),
                "Game " + game.getGameName() + " process wasn't launched");
        PlatformUtils.killGame(game);

    }

    @Test(description = "test to check that user doesn't have to accept the licence of the game", enabled = true)

    public void testPlayForUserWhoPlayedBefore() {
    	//OperationsHelper.baseUrl="https://auth-fix-ru.4gametest.com/";
        log4j.info("test to check that user doesn't have to accept the licence of the game " + game.getGameName());
        //game = new PlanetSide2();
        User user = switcherLocation(TypeUser.HARDCORE_USER);
        prepareUser.setLastAuthTime(user, game.getServiceId(), OperationsHelper.getChangedDate(-1));
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        
        gpp.addCookie("promoPopupShowed__" + game.getServiceId()+"__" + UserData.getMasterAccountId(), "1");
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        
        gpp.waitToPlayGame(false);
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_PLAY),
                "Button 'Play' is not Visible " + game.getAppName());
        gpp.click(GamePanelPage.BUTTON_PLAY);
        Assert.assertTrue(!GameData.getErrorCode(game).equals("8"),
                "The form with the game licence is visible after clicking Play for user who played before");
        Assert.assertTrue(PlatformUtils.detectGameProcess(game, 30),
                "Game " + game.getGameName() + " process wasn't launched");
        PlatformUtils.killGame(game);
    }

    @Test(description = "test to check that user has the ability to change a password for the game", enabled = false, priority=2)
    public void testChangeGamePassword() {
    	//OperationsHelper.baseUrl="https://auth-fix-ru.4gametest.com/";
        log4j.info("test to check that user has the ability to set a password for the game " + game.getGameName());
        User user = switcherLocation(TypeUser.USER);
        PrepareUser prepareUser = new PrepareUser();
        String servicePassword = "123456";
        prepareUser.createServiceAccountId(user, game);
        prepareUser.setServicePassword(user.getServiceAccountId(), user.getPassword(), servicePassword);
        user.setServiceAccountPassword(servicePassword);
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        
        gpp.addCookie("promoPopupShowed__" + game.getServiceId()+"__" + UserData.getMasterAccountId(), "1");
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");

        gpp.waitToPlayGame(false);
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_SETTINGS),
                "Button 'settings in game panel' is not Visible " + game.getAppName());
        gpp.click(GamePanelPage.BUTTON_SETTINGS);
        gpp.click(GamePanelPage.BUTTON_CHANGE_PASSWORD);
        String newPassword = user.getServiceAccountPassword() + "7";
        gpp.fillChangePasswordForm(servicePassword, newPassword, user.getPassword());
        Assert.assertTrue(gpp.validateElementIsNotVisible(GamePanelPage.FIELD_OLD_PASSWORD),
                "Form 'CHANGE game password' is still Visible " + game.getAppName());
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_PLAY_WITH_PASSWORD),
                "Button 'Play with a password' is not Visible " + game.getAppName());
        gpp.click(GamePanelPage.BUTTON_PLAY_WITH_PASSWORD);
        Assert.assertTrue(lp.validateElementVisible(GamePanelPage.FIELD_GAME_PASSWORD), 
        		"There is no FIELD_GAME_PASSWORD after clicking on the BUTTON_PLAY_WITH_PASSWORD");
        gpp.type(GamePanelPage.FIELD_GAME_PASSWORD, newPassword);
        gpp.click(GamePanelPage.BUTTON_PLAY_WITH_PASSWORD_ARROW);
        gpp.clickPopupIfItPresent(game);
        Assert.assertTrue(PlatformUtils.detectGameProcess(game, 30),
                "Game " + game.getGameName() + " process wasn't launched");
        PlatformUtils.killGame(game);
    }

    @Test(description = "test to check that user has the ability to delete a password for the game", enabled = false)
    public void testDeleteGamePassword() {
    	//OperationsHelper.baseUrl="https://auth-fix-ru.4gametest.com/";
        log4j.info("test to check that user has the ability to set a password for the game " + game.getGameName());
        User user = switcherLocation(TypeUser.USER);
        PrepareUser prepareUser = new PrepareUser();
        String servicePassword = "123456";
        prepareUser.createServiceAccountId(user, game);
        prepareUser.setServicePassword(user.getServiceAccountId(), user.getPassword(), servicePassword);
        user.setServiceAccountPassword(servicePassword);
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        
        gpp.addCookie("promoPopupShowed__" + game.getServiceId()+"__" + UserData.getMasterAccountId(), "1");
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");

        gpp.waitToPlayGame(false);
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_SETTINGS),
                "Button 'settings in game panel' is not Visible " + game.getAppName());
        gpp.click(GamePanelPage.BUTTON_SETTINGS);
        gpp.click(GamePanelPage.BUTTON_DELETE_PASSWORD);
        gpp.fillDeletePasswordForm(servicePassword, user.getPassword());
        Assert.assertTrue(gpp.validateElementIsNotVisible(GamePanelPage.BUTTON_DELETE_PASSWORD_POPUP),
                "Form 'Delete game password' is still Visible " + game.getAppName());
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_PLAY),
                "Button 'Play without a password' is not Visible " + game.getAppName());
        gpp.click(GamePanelPage.BUTTON_PLAY);
        gpp.clickPopupIfItPresent(game);
        Assert.assertTrue(PlatformUtils.detectGameProcess(game, 50),
                "Game " + game.getGameName() + " process wasn't launched");
        PlatformUtils.killGame(game);
    }

    @Test(description = "test to check that user has the ability to recover a password (by the mail) for the game",
            enabled = false)
    public void testRecoveryGamePasswordMail() {
    	//OperationsHelper.baseUrl="https://auth-fix-ru.4gametest.com/";
        log4j.info(
                "test to check that user has the ability to recover a password (by the mail) for the game " + game.getGameName());
        User user = switcherLocation(TypeUser.USER);
        PrepareUser prepareUser = new PrepareUser();
        String mainEmail = user.getMainEmail();
        String masterPassword = user.getPassword();
        String servicePassword = "123456";
        prepareUser.createServiceAccountId(user, game);
        prepareUser.setBetaTestProperty(user, true);
        prepareUser.setServicePassword(user.getServiceAccountId(), user.getPassword(), servicePassword);
        prepareUser.deleteMobileNumber(user);
        user.setServiceAccountPassword(servicePassword);
        String newServicePassword = user.getServiceAccountPassword() + "7";
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        LoginPage lp = new LoginPage(game);
        Assert.assertTrue(gpp.validateElementVisible(UserBarPage.BUTTON_SIGN_UP),
                "The butoton 'SIGN_UP' in userbar is not Visible " + game.getAppName());
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        
        gpp.addCookie("promoPopupShowed__" + game.getServiceId()+"__" + UserData.getMasterAccountId(), "1");
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");

        gpp.waitToPlayGame(false);
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_PLAY_WITH_PASSWORD),
                "Button 'Play with a password' is not Visible " + game.getAppName());
        gpp.click(GamePanelPage.BUTTON_SETTINGS);
        lp.click(GamePanelPage.LINK_FORGOT_PASSWORD_SETTINGS);
        int numMainMail = MailProvider.getNumberOfMessages(mainEmail);
        gpp.fillRecoverSendForm(masterPassword);
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.LABEL_ABOUT_SENT_EMAIL),
                "Form 'We sent you a letter with a link' is not Visible " + game.getAppName());
        Assert.assertTrue(MailProvider.isNewMessagePresent(mainEmail, numMainMail));
        String linkToRecover = MailProvider.getLinkFromMail(mainEmail);
        gpp.openUrlIn4game(linkToRecover);
        gpp.fillRecoverFormMail(newServicePassword, masterPassword);
        Assert.assertTrue(gpp.validateElementIsNotVisible(GamePanelPage.FIELD_RECOVER_NEW_PASSWORD),
                "Form 'recover game password (with link from email)' is still Visible " + game.getAppName());
        user.setServiceAccountPassword(newServicePassword);
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_PLAY_WITH_PASSWORD),
                "Button 'Play with a password' is not Visible " + game.getAppName());
        gpp.click(GamePanelPage.BUTTON_PLAY_WITH_PASSWORD);
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.FIELD_GAME_PASSWORD),
                "The field 'Field for game password' is not Visible " + game.getAppName());
        gpp.type(GamePanelPage.FIELD_GAME_PASSWORD, newServicePassword);
        gpp.click(GamePanelPage.BUTTON_PLAY_WITH_PASSWORD_ARROW);
        gpp.clickPopupIfItPresent(game);
        Assert.assertTrue(PlatformUtils.detectGameProcess(game, 50),
                "Game " + game.getGameName() + " process wasn't launched");
        PlatformUtils.killGame(game);
    }

    @Test(description = "test to check that user has the ability to recover a password (by the sms) for the game",
            enabled = false, priority=1)
    public void testRecoveryGamePasswordSms() {
    	//OperationsHelper.baseUrl="https://gta5-ru.4gametest.com";
        log4j.info(
                "test to check that user has the ability to recover a password (by the sms) for the game " + game.getGameName());
        User user = switcherLocation(TypeUser.USER);
        PrepareUser prepareUser = new PrepareUser();
        String masterPassword = user.getPassword();
        prepareUser.createServiceAccountId(user, game);
        String servicePassword = "123456";
        prepareUser.setServicePassword(user.getServiceAccountId(), user.getPassword(), servicePassword);
        user.setServiceAccountPassword(servicePassword);
        String newServicePassword = user.getServiceAccountPassword() + "7";
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        
        gpp.addCookie("promoPopupShowed__" + game.getServiceId()+"__" + UserData.getMasterAccountId(), "1");
        gpp.openUrlIn4game(gpp.getPageUrl() + "play");

        gpp.waitToPlayGame(false);
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_PLAY_WITH_PASSWORD),
                "Button 'Play with a password' is not Visible " + game.getAppName());
        gpp.click(GamePanelPage.BUTTON_SETTINGS);
        lp.click(GamePanelPage.LINK_FORGOT_PASSWORD_SETTINGS);
        int numOfSms = MailProvider.getNumberOfSms(user.getMobileNumber());
        gpp.fillRecoverSendForm(masterPassword);
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.FIELD_RECOVER_SMS_CODE),
                "Form 'recover game password (with sms code)' is not Visible " + game.getAppName());
        Assert.assertTrue(MailProvider.isNewSmsPresent(user.getMobileNumber(), numOfSms), "there is no new sms");
        Message mess = MailProvider.getSms(user.getMobileNumber());
        gpp.fillRecoverFormSms(mess.getCodeFromSMS(), newServicePassword, masterPassword);
        Assert.assertTrue(gpp.validateElementIsNotVisible(GamePanelPage.FIELD_RECOVER_SMS_CODE),
                "Form 'recover game password (with sms code)' is still Visible " + game.getAppName());
        user.setServiceAccountPassword(newServicePassword);
        gpp.click(GamePanelPage.BUTTON_BACK);
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_PLAY_WITH_PASSWORD),
                "Button 'Play with a password' is not Visible " + game.getAppName());
        gpp.click(GamePanelPage.BUTTON_PLAY_WITH_PASSWORD);
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.FIELD_GAME_PASSWORD),
                "The field 'Field for game password' is not Visible " + game.getAppName());
        gpp.type(GamePanelPage.FIELD_GAME_PASSWORD, newServicePassword);
        gpp.click(GamePanelPage.BUTTON_PLAY_WITH_PASSWORD_ARROW);
        gpp.clickPopupIfItPresent(game);
        Assert.assertTrue(PlatformUtils.detectGameProcess(game, 50),
                "Game " + game.getGameName() + " process wasn't launched");
        PlatformUtils.killGame(game);
    }
    


}
