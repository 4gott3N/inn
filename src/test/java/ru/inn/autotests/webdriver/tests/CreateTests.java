package ru.inn.autotests.webdriver.tests;


import com.google.common.base.Function;
import com.google.common.base.Throwables;
import com.google.common.collect.Lists;
import org.testng.*;
import org.testng.annotations.*;
import org.testng.reporters.EmailableReporter;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;
import org.uncommons.reportng.HTMLReporter;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.toolkit.*;

import java.io.IOException;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Sergey.Kashapov
 * Date: 02.08.13
 * Time: 12:05
 * To change this template use File | Settings | File Templates.
 */

public class CreateTests extends AbstractTest {
    LinkedHashSet<ITestResult> fail = new LinkedHashSet<ITestResult>();
    final LinkedHashSet<String> methods = new LinkedHashSet<String>();
    List<XmlSuite> xmlSuites = new ArrayList<XmlSuite>();
    TestListenerAdapter tla = new TestListenerAdapter();

    TestNG tng = new TestNG();
    ArrayList<List<ISuite>> suiteISuites = new ArrayList<List<ISuite>>();
    IReporter reporter = new HTMLReporter();
    EmailableReporter emailableReporter = new EmailableReporter();
    public static int threadCount = 0;

    @BeforeClass
    public void init() {
        AbstractTest.initTests();
        if (System.getenv("threadCount") != null)
            threadCount = Integer.parseInt(System.getenv("threadCount"));
        else
            threadCount = Integer.parseInt(PlatformUtils.getStageParameters("threadCount"));
    }

    @DataProvider(name = "getTests")
    public Iterator<Object[]> createData() throws Exception {
        List<Object[]> data = new ArrayList<Object[]>();
        for (YamlProvider.TestsToRun test : tests)
            data.add(new Object[]{test});
        return data.iterator();
    }

    @Test(dataProvider = "getTests")
    public void createTests(YamlProvider.TestsToRun test) throws IOException {
        OperationsHelper.initBaseUrl();
        YamlProvider.printConfig();
        tng.addListener(reporter);
        System.setProperty("org.uncommons.reportng.escape-output", "false");
        System.setProperty("org.uncommons.reportng.title", "Awesomeness");
        setNowTest(test);
        tng.addListener(tla);
        XmlSuite suite = new XmlSuite();
        suite.setName(test.getNamesClassToRun());
        XmlTest testXml = new XmlTest(suite);
        List<String> excludeGroup = new ArrayList<String>();
        excludeGroup.add(test.getExcludedGroups());
        testXml.setXmlClasses(test.getClassToRun());
        testXml.setName(test.getNamesClassToRun());
        testXml.setExcludedGroups(excludeGroup);
        suite.setParallel("methods");
        suite.setThreadCount(threadCount);
        xmlSuites.add(suite);
        tng.setXmlSuites(Arrays.asList(suite));
        suiteISuites.add(tng.runSuitesLocally());
        List<ITestResult> failedTests = Lists.newArrayList();
        failedTests.addAll(tla.getFailedTests());
        failedTests.addAll(tla.getConfigurationFailures());
        if (!failedTests.isEmpty()) {
            String header = String.format("Combined Messages (Total:%d)", failedTests.size());
            List<String> errorMessages = Lists.newArrayList();
            errorMessages.add(header);
            errorMessages.addAll(Lists.transform(failedTests, new Function<ITestResult, String>() {
                int i = 1;

                @Override
                public String apply(ITestResult testResult) {
                    methods.add(String.valueOf(testResult.getMethod().getMethodName() + "\n"));
                    String stackTraceString = Throwables.getStackTraceAsString(testResult.getThrowable());
                    String template = "Message-%d: %n %s";
                    return String.format(template, i++, stackTraceString);
                }
            }));

            // transform messages to a single combined string
            //  String message = Joiner.on(LINE_SEPARATOR).join(errorMessages);
            // log4j.info(message);
            fail.addAll(failedTests);

        }

        if (!fail.isEmpty())
            Assert.fail("There is " + fail.size() + " errors in xmlSuites\n" + methods);


    }

    @AfterMethod
    public void cleanPool() {
        LocalDriverManager.cleanThreadPool();
    }

    @AfterClass
    public void createReport() {
        List<ISuite> suites = new ArrayList<ISuite>();
        for (List<ISuite> listSuites : suiteISuites) {
            for (ISuite suite : listSuites) {
                suites.add(suite);
            }
        }
        reporter.generateReport(xmlSuites, suites, tng.getOutputDirectory());
        emailableReporter.generateReport(xmlSuites, suites, tng.getOutputDirectory());

        WebDriverController.stopProxy();
        //wait new
/*        if (OperationsHelper.baseUrl.contains("test")) {
            SSHManager instance = new SSHManager("qa", "qatest", "10.33.50.99");
            String errorMessage = instance.connect();
            if (errorMessage != null)
                Assert.fail(errorMessage);
            String result = instance.sendCommand("sudo /usr/bin/find /var/www/notification_service -type f -delete");
            System.out.println(result);
            instance.close();
        }*/

    }


}
