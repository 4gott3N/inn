package ru.inn.autotests.webdriver.tests.playinstall;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.inn.autotests.webdriver.common.PrepareUser;
import ru.inn.autotests.webdriver.common.entity.Message;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.common.os.WinRegistry;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.composite.common.Captcha;
import ru.inn.autotests.webdriver.composite.pages.GamePanelPage;
import ru.inn.autotests.webdriver.composite.pages.LoginPage;
import ru.inn.autotests.webdriver.composite.pages.SettingsPage;
import ru.inn.autotests.webdriver.composite.pages.UserBarPage;
import ru.inn.autotests.webdriver.composite.popups.PopupSecureCode;
import ru.inn.autotests.webdriver.games.PointBlank;
import ru.inn.autotests.webdriver.games.R2Online;
import ru.inn.autotests.webdriver.tests.AbstractTest;
import ru.inn.autotests.webdriver.tests.AbstractTest.TypeUser;
import ru.inn.autotests.webdriver.toolkit.MailProvider;
import ru.inn.autotests.webdriver.toolkit.PlatformUtils;
import ru.inn.autotests.webdriver.toolkit.ProcessProvider;
import ru.inn.autotests.webdriver.toolkit.WebDriverController;


public class SecureOffTests extends AbstractTest {

    public static String PATH_TO_KEY = "SOFTWARE\\Wow6432Node\\4game\\4gameservice";
    public static String NAME_OF_KEY = "appId";
    User user;
    PrepareUser prepareUser = new PrepareUser();
    

    @BeforeMethod
    public void initUser() {
        user = switcherLocation(TypeUser.NEW_USER);
        prepareUser.switchSuperSecurity(user, false);
    }
    
   
    
    @Test(description = "The test to check that new user can switch on the supersecurity and add the mobile phone")
    public void testSwitchOnSupersecurityAndAddMobile() throws Exception {
    	log4j.info("The test to check that new user can switch on the supersecurity and add the mobile phone");
    	prepareUser.deleteMobileNumber(user);
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.resetCacheApi();
        LoginPage lp = new LoginPage(game);
        
        lp.openUrlIn4game("/settings/security/");
        //lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized");
    	
		SettingsPage sp = new SettingsPage(language);
		Assert.assertTrue(sp.validateElementVisible(SettingsPage.BUTTON_FIRST_SUPER_SECURITY_ON), "We are not on the Secutity page or it's already on. BUTTON_FIRST_SUPER_SECURITY_ON is not visible");
		Assert.assertTrue(sp.validateElementVisible(SettingsPage.DIV_COMICS), "DIV_COMICS is not visible on the Security page");
		sp.click(SettingsPage.BUTTON_FIRST_SUPER_SECURITY_ON);
		Captcha captcha = new Captcha();
		captcha.fillCaptchaIfItPresent();
		
		Assert.assertTrue(sp.validateElementVisible(SettingsPage.FIELD_SET_MOBILE_UBER_SECURITY), "FIELD_SET_MOBILE_UBER_SECURITY is not visible in the form 'Switch  the UbSec on'");
		int num = MailProvider.getNumberOfMessages(user.getMainEmail());
		String mobileNumber = gpp.selectRandomMobileNumber();
		sp.type(SettingsPage.FIELD_SET_MOBILE_UBER_SECURITY, mobileNumber);
		int numOfSms = MailProvider.getNumberOfSms(mobileNumber);
		Assert.assertTrue(sp.validateElementVisible(SettingsPage.BUTTON_SEND_SMS_CODE), "BUTTON_SEND_SMS_CODE is not visible in the form 'Switch  the UbSec on'");
		sp.click(SettingsPage.BUTTON_SEND_SMS_CODE);
		captcha.fillCaptchaIfItPresent();
		
		
		Assert.assertTrue(MailProvider.isNewSmsPresent(mobileNumber, numOfSms),
                "there is no new sms notifytest." + mobileNumber + "@inn.ru.htm");
		Message mess = MailProvider.getSms(mobileNumber);
		
		Assert.assertTrue(sp.validateElementVisible(SettingsPage.FIELD_CODE_UBER_SECURITY), "FIELD_CODE_UBER_SECURITY is not visible in the form 'Switch  the UbSec on through mail'");
		sp.type(SettingsPage.FIELD_CODE_UBER_SECURITY, mess.getCodeFromSMS());
		//Assert.assertTrue(sp.validateElementVisible(SettingsPage.CHECK_ADD_TRUSTED), "CHECK_ADD_TRUSTED is not visible in the form 'Switch the UbSec on through mail'");
		Assert.assertTrue(sp.validateElementVisible(SettingsPage.BUTTON_SWITCH_ON_UBER_SECURITY), "BUTTON_SWITCH_ON_UBER_SECURITY is not visible in the form 'Switch  the UbSec on through mail'");
		sp.click(SettingsPage.BUTTON_SWITCH_ON_UBER_SECURITY);
		Assert.assertTrue(MailProvider.isNewMessagePresent(user.getMainEmail(), num));
        String linkToAdd = MailProvider.getLinkFromMail(user.getMainEmail());
        sp.openUrlIn4game(linkToAdd);
        
        Assert.assertTrue(sp.validateElementIsNotVisible(SettingsPage.DIV_COMICS), "DIV_COMICS is still visible after the switching on a super security");
		Assert.assertTrue(sp.validatElementContainsText(SettingsPage.FORM_TRUSTED_COMPUTERS, "Mozila Firefox"),  "The computer was not added to the 'Trusted computers' list. There is no any 'Mozila Firefox' in the list");
		
        Assert.assertTrue(sp.validateElementPresent(UserBarPage.DIV_LOGIN_NAME_FIELD),"Login name is not present in user bar");
        Assert.assertTrue(!sp.isVisible(SettingsPage.FORM_CODE_EXPIRED),"FORM_CODE_EXPIRED is visible. Tha adding mobile did not have success");
        Assert.assertTrue(sp.validateElementPresent(SettingsPage.LINK_CHANGE_MOBILE),"The mobile phone was not added correctly. LINK_CHANGE_MOBILE is not visible");
    }
    
    @Test(description = "The test to check that new user can switch on the supersecurity through mail and can launch the game after that")
    public void testSwitchOnSupersecurityMail() throws Exception {
    	log4j.info("The test to check that new user can switch on the supersecurity and can launch the game after that");
    	prepareUser.deleteMobileNumber(user);
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.resetCacheApi();
        LoginPage lp = new LoginPage(game);
        
        lp.openUrlIn4game("/settings/security/");
        //lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized");
    	
		
		SettingsPage sp = new SettingsPage(language);
		Assert.assertTrue(sp.validateElementVisible(SettingsPage.BUTTON_FIRST_SUPER_SECURITY_ON), "We are not on the Secutity pageor it's on already. BUTTON_FIRST_SUPER_SECURITY_ON is not visible");
		Assert.assertTrue(sp.validateElementVisible(SettingsPage.BUTTON_SECOND_SUPER_SECURITY_ON), "BUTTON_SECOND_SUPER_SECURITY_ON is not visible");
		Assert.assertTrue(sp.validateElementVisible(SettingsPage.DIV_COMICS), "DIV_COMICS is not visible on the Security page");
		sp.click(SettingsPage.BUTTON_SECOND_SUPER_SECURITY_ON);
		Captcha captcha = new Captcha();
		captcha.fillCaptchaIfItPresent();
		
		Assert.assertTrue(sp.validateElementVisible(SettingsPage.LINK_USE_MAIL), "LINK_USE_MAIL is not visible in the form 'Switch on the UbSec'");
		sp.click(SettingsPage.LINK_USE_MAIL);
		Assert.assertTrue(sp.validateElementVisible(SettingsPage.BUTTON_SEND_MAIL_CODE), "BUTTON_SEND_MAIL_CODE is not visible in the form 'Switch  the UbSec on through mail'");
		int num = MailProvider.getNumberOfMessages(user.getMainEmail());
		sp.click(SettingsPage.BUTTON_SEND_MAIL_CODE);
		captcha.fillCaptchaIfItPresent();
		Assert.assertTrue(sp.validateElementVisible(SettingsPage.FIELD_CODE_UBER_SECURITY), "FIELD_CODE_UBER_SECURITY is not visible in the form 'Switch  the UbSec on through mail'");
		Assert.assertTrue(MailProvider.isNewMessagePresent(user.getMainEmail(), num));
        Message mess = MailProvider.getLastMessage(user.getMainEmail());
		sp.type(SettingsPage.FIELD_CODE_UBER_SECURITY, mess.getCode());
		sp.click(SettingsPage.BUTTON_SWITCH_ON_UBER_SECURITY);
		Assert.assertTrue(sp.validateElementIsNotVisible(SettingsPage.DIV_COMICS), "DIV_COMICS is still visible after the switching on a super security");
		Assert.assertTrue(sp.validatElementContainsText(SettingsPage.FORM_TRUSTED_COMPUTERS, "Mozila Firefox"),  "The computer was not added to the 'Trusted computers' list. There is no any 'Mozila Firefox' in the list");
		//Assert.assertTrue(sp.validateElementVisible(SettingsPage.CHECKBOX_TRUSTED_COMPUTER), "The computer was not added to the 'Trusted computers' list. CHECKBOX_TRUSTED_COMPUTER is not visible");
		
		//UserBarPage ub = new UserBarPage();
		//sp.click(UserBarPage.BUTTON_USER_DATA_ARROW);
		//Assert.assertTrue(sp.validateElementVisible(UserBarPage.LINK_PERSONAL_DATA), "The LINK 'PERSONAL_DATA' is not visible in user menu");
		//Assert.assertTrue(sp.validateElementIsNotVisible(UserBarPage.BUTTON_SUPER_SECURITY_ON), "The BUTTON_SUPER_SECURITY_ON in UserBar is visible for user who already switched supersecurity on ");
		
		gpp.openPage(gpp);
		gpp.waitToPlayGame(true);
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_PLAY), "The button 'Play' is not visible on the panel 'play'");
		gpp.click(GamePanelPage.BUTTON_PLAY);
        gpp.clickPopupIfItPresent(game);
        Assert.assertTrue(PlatformUtils.detectGameProcess(game, 30), "Game " + game.getGameName() + " process wasn't launched for a user with super security");
        PlatformUtils.killGame(game);
    }
    
    @Test(description = "The test to check that new user can switch on the supersecurity through mobile")
    public void testSwitchOnSupersecurityMobile() throws Exception {
    	log4j.info("The test to check that new user can switch on the supersecurity and can launch the game after that");
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.resetCacheApi();
        LoginPage lp = new LoginPage(game);
        String mobileNumber = gpp.selectRandomMobileNumber();
		prepareUser.addMobileNumber(user, mobileNumber);
        user.setMobileNumber(mobileNumber);
        int numEmail = MailProvider.getNumberOfMessages(user.getMainEmail());
        lp.openUrlIn4game("/settings/security/");
        //lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized");
    	
		
		SettingsPage sp = new SettingsPage(language);
		Assert.assertTrue(sp.validateElementVisible(SettingsPage.BUTTON_FIRST_SUPER_SECURITY_ON), "We are not on the Secutity page or Uber security is on. BUTTON_FIRST_SUPER_SECURITY_ON is not visible");
		Assert.assertTrue(sp.validateElementVisible(SettingsPage.BUTTON_SECOND_SUPER_SECURITY_ON), "BUTTON_SECOND_SUPER_SECURITY_ON is not visible");
		Assert.assertTrue(sp.validateElementVisible(SettingsPage.DIV_COMICS), "DIV_COMICS is not visible on the Security page");
    	int numOfSms = MailProvider.getNumberOfSms(user.getMobileNumber());
		sp.click(SettingsPage.BUTTON_SECOND_SUPER_SECURITY_ON);

		Assert.assertTrue(sp.validateElementVisible(SettingsPage.FIELD_CODE_UBER_SECURITY), "FIELD_CODE_UBER_SECURITY is not visible in the form 'Switch  the UbSec on through mail'");
		Assert.assertTrue(MailProvider.isNewSmsPresent(user.getMobileNumber(), numOfSms),
                "there is no new sms notifytest." + user.getMobileNumber() + "@inn.ru.htm");
		Message mess = MailProvider.getSms(user.getMobileNumber());
		sp.type(SettingsPage.FIELD_CODE_UBER_SECURITY, mess.getCodeFromSMS());
		sp.click(SettingsPage.BUTTON_SWITCH_ON_UBER_SECURITY);
		Assert.assertTrue(sp.validateElementIsNotVisible(SettingsPage.DIV_COMICS), "DIV_COMICS is still visible after the switching on a super security");
		Assert.assertTrue(sp.validatElementContainsText(SettingsPage.FORM_TRUSTED_COMPUTERS, "Mozila Firefox"),  "The computer was not added to the 'Trusted computers' list. There is no any 'Mozila Firefox' in the list");

    }
    

    @Test(description = "The test to check that new user can switch off the supersecurity and can launch the game after that")
    public void testSwitchOffSupersecurityMail() throws Exception {
    	log4j.info("The test to check that new user can switch off the supersecurity and can launch the game after that");
    	prepareUser.deleteMobileNumber(user);
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.resetCacheApi();
        gpp.openPage(gpp);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized");
    	prepareUser.switchSuperSecurity(user, true);
    	//WinRegistry.deleteValue(WinRegistry.HKEY_LOCAL_MACHINE, PATH_TO_KEY, NAME_OF_KEY);
    	gpp.openUrlIn4game("/settings/security/");
		SettingsPage sp = new SettingsPage(language);
		int num = MailProvider.getNumberOfMessages(user.getMainEmail());
		
		Assert.assertTrue(sp.validateElementVisible(SettingsPage.BUTTON_SUPER_SECURITY_OFF), "We are not on the Secutity page or the uber security is off. BUTTON_SUPER_SECURITY_OFF is not visible");
		sp.click(SettingsPage.BUTTON_SUPER_SECURITY_OFF);
		Captcha captcha = new Captcha();
		captcha.fillCaptchaIfItPresent();
		Assert.assertTrue(MailProvider.isNewMessagePresent(user.getMainEmail(), num));
        Message mess = MailProvider.getLastMessage(user.getMainEmail());
		sp.type(SettingsPage.FIELD_CODE_UBER_SECURITY, mess.getCode());
		sp.click(SettingsPage.BUTTON_SWITCH_OFF_UBER_SECURITY);
		
		//prepareUser.switchSuperSecurity(user, false);
		Assert.assertTrue(sp.validateElementVisible(SettingsPage.BUTTON_FIRST_SUPER_SECURITY_ON), "BUTTON_FIRST_SUPER_SECURITY_ON is not visible after switching uber security off");
		Assert.assertTrue(sp.validateElementVisible(SettingsPage.DIV_COMICS), "DIV_COMICS is not visible on the Security page after switching uber security off");

		gpp.openPage(gpp);
		gpp.waitToPlayGame(true);
        Assert.assertTrue(gpp.validateElementVisible(GamePanelPage.BUTTON_PLAY), "The button 'Play' is not visible on the panel 'play'");
		gpp.click(GamePanelPage.BUTTON_PLAY);
        gpp.clickPopupIfItPresent(game);
        Assert.assertTrue(PlatformUtils.detectGameProcess(game, 30), "Game " + game.getGameName() + " process wasn't launched for a user without super security");
        PlatformUtils.killGame(game);
    }
    
    @Test(description = "The test to check that new user can switch off the supersecurity through the mobile phone")
    public void testSwitchOffSupersecurityMobile() throws Exception {
    	log4j.info("The test to check that new user can switch off the supersecurity through the mobile phone");
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.resetCacheApi();
        gpp.openPage(gpp);
        LoginPage lp = new LoginPage(game);
        String mobileNumber = gpp.selectRandomMobileNumber();
		prepareUser.addMobileNumber(user, mobileNumber);
        user.setMobileNumber(mobileNumber);
        int numEmail = MailProvider.getNumberOfMessages(user.getMainEmail());
    	int numOfSms = MailProvider.getNumberOfSms(user.getMobileNumber());
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized");
    	prepareUser.switchSuperSecurity(user, true);
    	//WinRegistry.deleteValue(WinRegistry.HKEY_LOCAL_MACHINE, PATH_TO_KEY, NAME_OF_KEY);
    	gpp.openUrlIn4game("/settings/security/");
		SettingsPage sp = new SettingsPage(language);
		int num = MailProvider.getNumberOfMessages(user.getMainEmail());
		
		Assert.assertTrue(sp.validateElementVisible(SettingsPage.BUTTON_SUPER_SECURITY_OFF), "We are not on the Secutity page or the uber security is off. BUTTON_SUPER_SECURITY_OFF is not visible");
		sp.click(SettingsPage.BUTTON_SUPER_SECURITY_OFF);
		Captcha captcha = new Captcha();
		captcha.fillCaptchaIfItPresent();
		Assert.assertTrue(MailProvider.isNewSmsPresent(user.getMobileNumber(), numOfSms),
                "there is no new sms notifytest." + user.getMobileNumber() + "@inn.ru.htm");
		Message mess = MailProvider.getSms(user.getMobileNumber());
		sp.type(SettingsPage.FIELD_CODE_UBER_SECURITY, mess.getCodeFromSMS());
		sp.click(SettingsPage.BUTTON_SWITCH_OFF_UBER_SECURITY);

		Assert.assertTrue(sp.validateElementVisible(SettingsPage.BUTTON_FIRST_SUPER_SECURITY_ON), "BUTTON_FIRST_SUPER_SECURITY_ON is not visible after switching uber security off");
		Assert.assertTrue(sp.validateElementVisible(SettingsPage.DIV_COMICS), "DIV_COMICS is not visible on the Security page after switching uber security off");

    }
    
    @Test(description = "The test to check the user can add a computer to the supersecurity list by means of a mobile phone and delete it")
    public void testAddComputerSecurityPageMobile() throws Exception {
    	log4j.info("The test to check the user can add a computer to the supersecurity list by means of a mobile phone and delete it");
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.resetCacheApi();
        String mobileNumber = gpp.selectRandomMobileNumber();
		prepareUser.addMobileNumber(user, mobileNumber);
        user.setMobileNumber(mobileNumber);
        gpp.openPage(gpp);
        LoginPage lp = new LoginPage(game);
    	prepareUser.switchSuperSecurity(user, true);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized");
    	//WinRegistry.deleteValue(WinRegistry.HKEY_LOCAL_MACHINE, PATH_TO_KEY, NAME_OF_KEY);
    	SettingsPage sp = new SettingsPage(language);
    	sp.openUrlIn4game("/settings/security/");
    	Assert.assertTrue(sp.validateElementVisible(SettingsPage.BUTTON_MAKE_TRUSTED), "We are not on the Secutity page or SuperSecurity is off. BUTTON_MAKE_TRUSTED is not visible");
    	int numOfSms = MailProvider.getNumberOfSms(user.getMobileNumber());
    	sp.click(SettingsPage.BUTTON_MAKE_TRUSTED);
    	//some actions to add a computer
    	
    	Captcha captcha = new Captcha();
		captcha.fillCaptchaIfItPresent();
    	Assert.assertTrue(sp.validateElementVisible(SettingsPage.FIELD_CODE_UBER_SECURITY), "FIELD_CODE_UBER_SECURITY is not visible in the form 'Adding computer to the trusted list'");
    	Assert.assertTrue(MailProvider.isNewSmsPresent(user.getMobileNumber(), numOfSms),
                 "there is no new sms notifytest." + user.getMobileNumber() + "@inn.ru.htm");
    	Message mess = MailProvider.getSms(user.getMobileNumber());
    	numOfSms = MailProvider.getNumberOfSms(user.getMobileNumber());
    	sp.type(SettingsPage.FIELD_CODE_UBER_SECURITY, mess.getCodeFromSMS());
    	//Assert.assertTrue(sp.validateElementVisible(SettingsPage.CHECKBOX_TRUSTED_COMPUTER), "There is no trusted computer after adding it. CHECKBOX_TRUSTED_COMPUTER is not visible");
    	sp.click(SettingsPage.BUTTON_ADD_COMPUTER_UBER_SECURITY);
    	Assert.assertTrue(sp.validateElementVisible(SettingsPage.BUTTON_UNLINK_COMPUTER), "BUTTON_UNLINK_COMPUTER is not visible");
    	Assert.assertTrue(sp.validatElementContainsText(SettingsPage.FORM_TRUSTED_COMPUTERS, "Mozila Firefox"),  "The computer was not added to the 'Trusted computers' list. There is no any 'Mozila Firefox' in the list");
    	
    	sp.click(SettingsPage.CHECKBOX_TRUSTED_COMPUTER);
    	sp.click(SettingsPage.BUTTON_UNLINK_COMPUTER);
    	captcha.fillCaptchaIfItPresent();
    	Assert.assertTrue(sp.validateElementVisible(SettingsPage.BUTTON_DELETE_COMPUTER_UBER_SECURITY), "There is no form 'Unlinl the trusted computer' . BUTTON_DELETE_COMPUTER_UBER_SECURITY is not visible");
    	Assert.assertTrue(MailProvider.isNewSmsPresent(user.getMobileNumber(), numOfSms),
                "there is no new sms notifytest." + user.getMobileNumber() + "@inn.ru.htm");
    	mess = MailProvider.getSms(user.getMobileNumber());
    	sp.type(SettingsPage.FIELD_CODE_UBER_SECURITY, mess.getCodeFromSMS());
		sp.click(SettingsPage.BUTTON_DELETE_COMPUTER_UBER_SECURITY);
		Assert.assertTrue(sp.validateElementVisible(SettingsPage.BUTTON_MAKE_TRUSTED), "This Computer is stil on the 'trusted list'. BUTTON_MAKE_TRUSTED is not visible");
    	
    }
    
    @Test(description = "The test to check the user can add a computer to the supersecurity list through mail and delete it")
    public void testDeleteComputerSecurityPageMail() throws Exception {
    	log4j.info("The test to check the user can add a computer to the supersecurity list through mail and delete it");
    	prepareUser.deleteMobileNumber(user);
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.resetCacheApi();
        gpp.openPage(gpp);
        LoginPage lp = new LoginPage(game);
    	prepareUser.switchSuperSecurity(user, true);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized");
    	//WinRegistry.deleteValue(WinRegistry.HKEY_LOCAL_MACHINE, PATH_TO_KEY, NAME_OF_KEY);

        SettingsPage sp = new SettingsPage(language);
    	sp.openUrlIn4game("/settings/security/");
    	Assert.assertTrue(sp.validateElementVisible(SettingsPage.BUTTON_MAKE_TRUSTED), "We are not on the Secutity page or SuperSecurity is off. BUTTON_MAKE_TRUSTED is not visible");
    	int numEmail = MailProvider.getNumberOfMessages(user.getMainEmail());
    	sp.click(SettingsPage.BUTTON_MAKE_TRUSTED);
    	//some actions to add a computer
    	
    	Captcha captcha = new Captcha();
		captcha.fillCaptchaIfItPresent();
    	Assert.assertTrue(sp.validateElementVisible(SettingsPage.FIELD_CODE_UBER_SECURITY), "FIELD_CODE_UBER_SECURITY is not visible in the form 'Adding computer to the trusted list'");
    	Assert.assertTrue(MailProvider.isNewMessagePresent(user.getMainEmail(), numEmail));
        Message mess = MailProvider.getLastMessage(user.getMainEmail());
        numEmail = MailProvider.getNumberOfMessages(user.getMainEmail());
		sp.type(SettingsPage.FIELD_CODE_UBER_SECURITY, mess.getCode());
    	//Assert.assertTrue(sp.validateElementVisible(SettingsPage.CHECKBOX_TRUSTED_COMPUTER), "There is no trusted computer after adding it. CHECKBOX_TRUSTED_COMPUTER is not visible");
    	sp.click(SettingsPage.BUTTON_ADD_COMPUTER_UBER_SECURITY);
    	    	
    	Assert.assertTrue(sp.validateElementVisible(SettingsPage.FORM_TRUSTED_COMPUTERS), "There is no any trusted computers . FORM_TRUSTED_COMPUTERS is not visible");
    	Assert.assertTrue(sp.validatElementContainsText(SettingsPage.FORM_TRUSTED_COMPUTERS, "Mozila Firefox"),  "The computer was not added to the 'Trusted computers' list. There is no any 'Mozila Firefox' in the list");
    	//Assert.assertTrue(sp.validateElementVisible(SettingsPage.CHECKBOX_TRUSTED_COMPUTER), "There is no trusted computer after adding it. CHECKBOX_TRUSTED_COMPUTER is not visible");
    	
    	Assert.assertTrue(MailProvider.isNewMessagePresent(user.getMainEmail(), numEmail));
    	numEmail = MailProvider.getNumberOfMessages(user.getMainEmail());
    	
    	Assert.assertTrue(sp.validateElementVisible(SettingsPage.BUTTON_UNLINK_COMPUTER), "BUTTON_UNLINK_COMPUTER is not visible");
    	sp.click(SettingsPage.CHECKBOX_TRUSTED_COMPUTER);
    	sp.click(SettingsPage.BUTTON_UNLINK_COMPUTER);
    	captcha = new Captcha();
		captcha.fillCaptchaIfItPresent();
    	Assert.assertTrue(sp.validateElementVisible(SettingsPage.BUTTON_DELETE_COMPUTER_UBER_SECURITY), "There is no form 'Delete the trusted computer' . BUTTON_DELETE_COMPUTER_UBER_SECURITY is not visible");
    	Assert.assertTrue(MailProvider.isNewMessagePresent(user.getMainEmail(), numEmail));
        mess = MailProvider.getLastMessage(user.getMainEmail());
		sp.type(SettingsPage.FIELD_CODE_UBER_SECURITY, mess.getCode());
		sp.click(SettingsPage.BUTTON_DELETE_COMPUTER_UBER_SECURITY);
		Assert.assertTrue(sp.validateElementVisible(SettingsPage.BUTTON_MAKE_TRUSTED), "This Computer is stil on the 'trusted list'. BUTTON_MAKE_TRUSTED is not visible");
    	
    	
    	//some actions to unlink a computer
    	
    	
    }
    
}