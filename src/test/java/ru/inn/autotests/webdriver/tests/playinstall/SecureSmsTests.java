package ru.inn.autotests.webdriver.tests.playinstall;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import ru.inn.autotests.webdriver.common.PrepareUser;
import ru.inn.autotests.webdriver.common.entity.Message;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.common.os.WinRegistry;
import ru.inn.autotests.webdriver.composite.common.Captcha;
import ru.inn.autotests.webdriver.composite.pages.GamePanelPage;
import ru.inn.autotests.webdriver.composite.pages.LoginPage;
import ru.inn.autotests.webdriver.composite.pages.UserBarPage;
import ru.inn.autotests.webdriver.composite.popups.PopupSecureCode;
import ru.inn.autotests.webdriver.tests.AbstractTest;
import ru.inn.autotests.webdriver.toolkit.LocalDriverManager;
import ru.inn.autotests.webdriver.toolkit.MailProvider;
import ru.inn.autotests.webdriver.toolkit.PlatformUtils;
import ru.inn.autotests.webdriver.toolkit.ProcessProvider;

import java.lang.reflect.InvocationTargetException;


public class SecureSmsTests extends AbstractTest {

    public static String PATH_TO_KEY = "SOFTWARE\\Wow6432Node\\4game\\4gameservice";
    public static String NAME_OF_KEY = "appId";
    User user;
    PrepareUser prepareUser = new PrepareUser();

    @BeforeClass
    public void initUser() {
        user=switcherLocation(TypeUser.MOBILE_USER);
      }

    @BeforeMethod
    public void startService(){
        ProcessProvider.run4GameServiceProcess();
    }

  /*  @Test(description = "Go to game and check play",priority = 4)
    public void testPreconditionSecurity()  {
        log4j.info("Test started for game " + game.getGameName());
        GamePanelPage gpp = new GamePanelPage(game, language);
        LocalDriverManager.getDriverController().deleteAllCookies();
        gpp.openPage(gpp);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");

        log4j.info("Login was successful");
        gpp.waitToPlayGame(true);
    }*/

    @Test(description = "The test to Check that user can add computer to the list 'trusted comuter' through sms", priority = 0)
    public void testSmsSecurity() throws InvocationTargetException, IllegalAccessException {
    	log4j.info("The test to Check that user can add computer to the list 'trusted comuter' through sms");
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openPage(gpp);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        gpp.waitToPlayGame(true);
        gpp.click(GamePanelPage.BUTTON_PLAY);
        gpp.clickPopupIfItPresent(game);
        Captcha captcha=new Captcha();
        captcha.fillCaptchaIfItPresent();
        Assert.assertTrue(gpp.validateElementPresent(PopupSecureCode.FORM_SECURE_CODE), "The form Supersecurity with field for secure code is not visible.");
        log4j.info("Sms form has opened");
        Message mess = MailProvider.getSms(user.getMobileNumber());
        gpp.type(PopupSecureCode.INPUT_CODE, mess.getCodeFromSMS());
        captcha.typeCaptchaIfPresent("987654");
        Assert.assertTrue(gpp.validateElementPresent(PopupSecureCode.BUTTON_CONFIRM_AND_PLAY),
                "The button 'CONFIRM_AND_PLAY' is not visible on the supersecurity form");
        gpp.click(PopupSecureCode.BUTTON_CONFIRM_AND_PLAY);
        Assert.assertTrue(gpp.validateElementIsNotVisible(PopupSecureCode.FORM_SECURE_CODE),
                "The form FORM_SECURE_CODE is still visible after entering a code");
        Assert.assertTrue(PlatformUtils.detectGameProcess(game, 30),
                "Game " + game.getGameName() + " process wasn't launched");
        log4j.info("Game " + game.getGameName() + " is launch");
        PlatformUtils.killGame(game);
        //prepareUser.unlinkApp(user);

        ProcessProvider.close4gameProcess();
        WinRegistry.deleteValue(WinRegistry.HKEY_LOCAL_MACHINE, PATH_TO_KEY, NAME_OF_KEY);


    }

    @Test(description = "The test to Check that the form 'untrusted comuter' doesn't appear for trusted computer", priority = 6)
    public void testAutoNoFormSecurity() throws Exception {
    	log4j.info("The test to Check that the form 'untrusted comuter' doesn't appear for trusted computer");
        GamePanelPage gpp = new GamePanelPage(game, language);
        gpp.openPage(gpp);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        gpp.waitToPlayGame(true);
        gpp.click(GamePanelPage.BUTTON_PLAY);
        gpp.clickPopupIfItPresent(game);
        Captcha captcha=new Captcha();
        captcha.fillCaptchaIfItPresent();
        Assert.assertTrue(PlatformUtils.detectGameProcess(game, 30), "Game " + game.getGameName() + " process wasn't launched");
        PlatformUtils.killGame(game);
        Assert.assertNotNull(WinRegistry.readString(WinRegistry.HKEY_LOCAL_MACHINE, PATH_TO_KEY, NAME_OF_KEY));

        //prepareUser.unlinkApp(user);

        ProcessProvider.close4gameProcess();
        WinRegistry.deleteValue(WinRegistry.HKEY_LOCAL_MACHINE, PATH_TO_KEY, NAME_OF_KEY);



    }

    @Test(description = "The test to Check the Bruteforce detected after sending a lot of sms", priority = 7, enabled = false)
    public void testBruteForce() throws Exception {
    	log4j.info("The test to Check the Bruteforce detected after sending a lot of sms");
        GamePanelPage gpp = new GamePanelPage(game, language);
        prepareUser.unlinkApp(user);
        gpp.openPage(gpp);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        gpp.waitToPlayGame(true);
        gpp.resetCacheApi();
        gpp.click(GamePanelPage.BUTTON_PLAY);
        gpp.clickPopupIfItPresent(game);
        Captcha captcha=new Captcha();
        captcha.fillCaptchaIfItPresent();
        Assert.assertTrue(gpp.validateElementPresent(PopupSecureCode.FORM_SECURE_CODE), "The form Supersecurity with field for secure code is not visible.");
        gpp.click(PopupSecureCode.LINK_SEND_AGAIN);
        gpp.click(PopupSecureCode.LINK_SEND_AGAIN);
        gpp.click(PopupSecureCode.LINK_SEND_AGAIN);
        Assert.assertTrue(gpp.validateElementPresent(PopupSecureCode.CAPTCHA_FIELD), "The captcha is not visible after sending sms again");
    }





    @Test(description = "The test to Check that user can't add computer to the list 'trusted comuter' entering the incorrect code", priority = 7)
    public void testIncorrectSmsSecurity() throws Exception {
    	log4j.info("The test to Check that user can't add computer to the list 'trusted comuter' entering the incorrect code");
        GamePanelPage gpp = new GamePanelPage(game, language);
        prepareUser.unlinkApp(user);
        gpp.openPage(gpp);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        gpp.waitToPlayGame(true);
        gpp.click(GamePanelPage.BUTTON_PLAY);
        gpp.clickPopupIfItPresent(game);
        Captcha captcha=new Captcha();
        captcha.fillCaptchaIfItPresent();
        Assert.assertTrue(gpp.validateElementPresent(PopupSecureCode.FORM_SECURE_CODE), "The form Supersecurity with field for secure code is not visible.");
        gpp.type(PopupSecureCode.INPUT_CODE, "1111");
        log4j.info("Input incorrect code");
        captcha.fillCaptchaIfItPresent();
        gpp.click(PopupSecureCode.BUTTON_CONFIRM_AND_PLAY);
        Assert.assertTrue(gpp.validateElementPresent(PopupSecureCode.ERROR_MESSAGE),
                "The message about mistake in typing Supersecurity code is not visible.");
        Assert.assertTrue(gpp.validateElementPresent(PopupSecureCode.CAPTCHA_FIELD), "The captcha is not visible after typing incorrect security code");
        log4j.info("captcha is enabled");
    }

    @Test(description = "The test to Check that user can send another one sms with the code", priority = 7)
    public void testRepeatSmsSecurity() throws Exception {
    	log4j.info("The test to Check that user can send another one sms with the code");
        GamePanelPage gpp = new GamePanelPage(game, language);
        prepareUser.unlinkApp(user);
        LoginPage lp = new LoginPage(game);
        lp.click(UserBarPage.BUTTON_SIGN_UP);
        lp.fillLoginForm(user.getMainEmail(), user.getPassword(), false);
        Assert.assertTrue(lp.validateElementVisible(UserBarPage.DIV_LOGIN_NAME_FIELD), "The user is not authorized.");
        gpp.waitToPlayGame(true);
        gpp.click(GamePanelPage.BUTTON_PLAY);
        gpp.clickPopupIfItPresent(game);
        Captcha captcha=new Captcha();
        captcha.fillCaptchaIfItPresent();
        Assert.assertTrue(gpp.validateElementPresent(PopupSecureCode.FORM_SECURE_CODE), "The form Supersecurity with field for secure code is not visible.");
        int num = MailProvider.getNumberOfSms(user.getMobileNumber());
        gpp.click(PopupSecureCode.LINK_SEND_AGAIN);
        captcha.fillCaptchaIfItPresent();
        log4j.info("Click to Send sms again");
        Assert.assertTrue(MailProvider.isNewSmsPresent(user.getMobileNumber(), num));
        log4j.info("New sms has come");
    }


    @AfterMethod
    public void goToGamePage() throws Exception {
        PlatformUtils.killGame(game);
        GamePanelPage gpp = new GamePanelPage(game, language);
        LocalDriverManager.getDriverController().deleteAllCookies();
        gpp.resetCacheApi();
        gpp.openPage(gpp);

    }



}
