package ru.inn.autotests.webdriver.composite.popups;

import ru.inn.autotests.webdriver.composite.IPage;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.games.Game;
import ru.inn.autotests.webdriver.games.GamePremium;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PopupPremium<T> extends OperationsHelper implements IPage {

    private String pageUrl = "";

    public static String FORM_BUY_PREMIUM = "id=premium-popup";

    public static final String URL_POPUP_PREMIUM= "?popupWidget=PremiumUpgradeWidget";
    public static String LABEL_PRICE_FIRST_MONTHLY = "id=premium-monthly-price-month-1";
    public static String LABEL_PRICE_FIRST_MONTH_TOTAL = "id=premium-monthly-price-total-1";
    public static String LABEL_PRICE_FIRST_MONTH_BONUSES = "id=premium-monthly-bonuses-1";
    public static String RADIO_FIRST_MONTH = "id=premium_month_1";

    public static String LABEL_PRICE_SECOND_MONTHLY = "id=premium-monthly-price-month-2";
    public static String LABEL_PRICE_SECOND_MONTH_TOTAL = "id=premium-monthly-price-total-2";
    public static String LABEL_PRICE_SECOND_MONTH_BONUSES = "id=premium-monthly-bonuses-2";
    public static String LABEL_NUMBER_MONTHS_SECOND_MONTH = "id=premium-monthly-months-2";
    public static String RADIO_SECOND_MONTH = "id=premium_month_2";
    
    public static String LABEL_PRICE_THIRD_MONTHLY = "id=premium-monthly-price-month-3";
    public static String LABEL_PRICE_THIRD_MONTH_TOTAL = "id=premium-monthly-price-total-3";
    public static String LABEL_PRICE_THIRD_MONTH_BONUSES = "id=premium-monthly-bonuses-3";
    public static String LABEL_NUMBER_MONTHS_THIRD_MONTH = "id=premium-monthly-months-3";
    public static String RADIO_THIRD_MONTH = "id=premium_month_3";

    public static String LABEL_PRICE_FIRST_DAY = "id=premium-daily-price-1";
    public static String LABEL_PRICE_FIRST_DAY_BONUSES = "id=premium-daily-bonuses-1";
    public static String RADIO_FIRST_DAY = "id=premium_day_1";

    public static String LABEL_PRICE_SECOND_DAY = "id=premium-daily-price-2";
    public static String LABEL_PRICE_SECOND_DAY_BONUSES = "id=premium-daily-bonuses-2";
    public static String RADIO_SECOND_DAY = "id=premium_day_2";

    public static String LABEL_PRICE_THIRD_DAY = "id=premium-daily-price-3";
    public static String LABEL_PRICE_THIRD_DAY_BONUSES = "id=premium-daily-bonuses-3";
    public static String RADIO_THIRD_DAY = "id=premium_day_3";
    
    public static String CHECKBOX_AUTOPAY = "id=premium-autopay";
    public static String BUTTON_BUY = "id=premium-buy";
    
    public static final String LABEL_ERROR_MESSAGE = "css=.bPremiumRates__eErrorMessage";

    private Game game;

    private static Matcher MATCHER;
    private static Pattern PATTERN;

    public PopupPremium(Game game) {

        if (GamePremium.class.isAssignableFrom(game.getClass()))
            this.game = game;
        else
            throw new RuntimeException("Don't cast the Game without a premium to the Game with a premium! See your code.");

    }


    @Override
    public String getPageUrl() {
        return pageUrl;
    }

    public double getPriceBonusesIfPresent(String locatorCoupon) {
        if (isElementPresent(locatorCoupon)) {
            PATTERN = Pattern.compile("\\d+");
            MATCHER = PATTERN.matcher(getText(locatorCoupon));
            MATCHER.find();
            return Double.parseDouble(MATCHER.group(0));
        }
        return 0;
    }


}
