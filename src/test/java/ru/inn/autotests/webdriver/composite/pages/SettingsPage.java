package ru.inn.autotests.webdriver.composite.pages;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import ru.inn.autotests.webdriver.common.PrepareUser;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.common.language.Language;
import ru.inn.autotests.webdriver.composite.IPage;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.composite.common.Captcha;
import ru.inn.autotests.webdriver.toolkit.MailProvider;
import ru.inn.autotests.webdriver.toolkit.UserData;

import java.util.List;

public class SettingsPage extends OperationsHelper implements IPage {

    String pageUrl = "/settings/";

    
    //SECURITY SECTION
    public static final String FORM_ACCOUNT_IS_NOT_PROTECTED = "id=security_widget_level_none";
    public static final String FORM_ACCOUNT_MEDIUM_SECURITY = "id=security_widget_level_medium";
    public static final String FORM_ACCOUNT_HIGH_SECURITY = "id=security_widget_level_high";
    public static final String FORM_ACCOUNT_LOW_SECURITY = "id=security_widget_level_low";
    public static final String FORM_SUPER_SECURITY_IS_ON = "id=security_widget_ubersecurity_block";
    public static final String DIV_COMICS = "id=uber_security_comics";
    public static final String BUTTON_FIRST_SUPER_SECURITY_ON = "id=uber_security_top_btn_turn_on";
    public static final String BUTTON_SECOND_SUPER_SECURITY_ON = "id=uber_security_bottom_btn_turn_on";
    public static final String BUTTON_SUPER_SECURITY_OFF = "id=uber_security_link_turn_off";
    public static final String BUTTON_MAKE_TRUSTED = "id=uber_security_add_btn";
    public static final String CHECKBOX_TRUSTED_COMPUTER = "css=input[id='checkbox_comp_0']";//.bUberSecurity__eList__eBodyRow.bUberSecurity__eList__eBodyRow__mHasContent:first-child
    public static final String FORM_TRUSTED_COMPUTERS = "css=.bInternalContent__eForm";
    public static final String BUTTON_UNLINK_COMPUTER = "id=unlink_computer_btn";
    
    public static final String LINK_USE_MAIL = "id=uber_security_link_use_mail"; 
    public static final String BUTTON_SEND_MAIL_CODE = "id=uber_security_botton_send_mail_code";
    public static final String BUTTON_SEND_SMS_CODE = "id=uber_security_botton_send_sms_code";
    //public static String FIELD_SET_MOBILE_UBER_SECURITY = "id=uber_security_field_set_mobile";
    public static String FIELD_SET_MOBILE_UBER_SECURITY = "name=identity";
    public static String FIELD_CODE_UBER_SECURITY = "name=code";
    public static String CHECK_ADD_TRUSTED = "name=add_to_trusted";
    public static final String BUTTON_SWITCH_ON_UBER_SECURITY = "id=uber_security_botton_switch_on";
    public static final String BUTTON_SWITCH_OFF_UBER_SECURITY = "id=uber_security_botton_switch_off";
    public static final String BUTTON_ADD_COMPUTER_UBER_SECURITY = "id=uber_security_botton_add_computer";
    public static final String BUTTON_DELETE_COMPUTER_UBER_SECURITY = "id=uber_security_botton_delete_computer";
    
    
    public static String LABEL_MAIL_OF_USER = "id=login_options_email";
    public static String FIELD_CHANGE_MAIL = "id=jsSetContactWidget__ChangeEmail__InitEmailInput";
    public static String FIELD_CHANGE_MOBILE = "xpath=//input[contains(@type,'tel')]";
    public static String FIELD_CHANGE_MAIL_PASS = "id=jsSetContactWidget__ChangeEmail__InitPasswordInput";
    public static String FIELD_CHANGE_MOBILE_PASS = "id=jsSetContactWidget__ChangeMobile__InitPasswordInput";
    public static String DIV_SUCCESS_CONTENT = "id=bSetContactLinks__eSuccessContent";
    public static String DIV_SUCCESS_PERSONAL_INFO = "css=.bFormSubmitBlock.mSuccessSave .bSaveOperationMessage.mSuccess";
    public static String BUTTON_ACCEPT_CHANGE_MAIL = "id=jsSetContactWidget__ChangeEmail__InitButton";
    public static String BUTTON_ACCEPT_CHANGE_MOBILE = "id=jsSetContactWidget__ChangeMobile__InitButton";
    public static final String FIELD_PASSWORD_TO_ADD_MOBILE = "id=jsSetContactWidget__AddMobile__InitPasswordInput";
    public static final String BUTTON_ADD_MOBILE_NUMBER = "id=jsSetContactWidget__AddMobile__InitButton";
    public static final String BUTTON_ADD_NUMBER_AFTER_CODE = "id=jsSetContactWidget__AddMobile__SmsCodeButton";
    public static final String FIELD_FOR_CODE_ADD_MOBILE = "xpath=//input[contains(@id,'SmsCodeInput')]";
    public static final String FIELD_MOBILE_NUMBER = "xpath=//input[contains(@type,'tel')]";
    public static final String FIELD_LOGIN = "name=ProfileLoginForm[login]";
    public static final String FIELD_PASSWORD = "xpath=//input[contains(@name,'password')]";
    public static final String BUTTON_SAVE_SETTINGS = "id=jsPersonalData__SaveSettings";
    public static final String SUCCESS_DIV = "css=.jsSaveOperationMessage__mSuccess";
    public static final String FIELD_NEW_PASSWORD = "name=ProfileLoginForm[newPassword]";
    public static final String BUTTON_ADD_FB_CONTACT = "id=add_fb_contact";
    public static final String BUTTON_ADD_VK_CONTACT = "id=add_vk_contact";
    public static final String BUTTON_DEL_FB_CONTACT = "id=del_fb_contact";
    public static final String BUTTON_DEL_VK_CONTACT = "id=del_vk_contact";
    public static final String FIELD_SMS_CODE_CHANGE_MAIL = "id=jsSetContactWidget__ChangeEmail__SmsCodeInput";
    public static final String BUTTON_CHANGE_MAIL_AFTER_SMS_CODE = "id=jsSetContactWidget__ChangeEmail__SmsCodeButton";
    
    public static final String FORM_UNABLE_CHANGE_MOBILE = "id=jsSetContactWidget__UnableToChangeMobile";
    public static final String LINK_ADD_MAIN_MAIL = "id=link_add_email";
    public static final String LINK_ADD_MOBILE = "id=link_add_mobile";
    public static final String LINK_CHANGE_MAIN_MAIL = "id=change_login_options_email";
    public static final String LINK_CHANGE_MOBILE = "id=link_change_mobile";
    public static final String FORM_CHANGE_EMAIL_SEND_LETTER="id=jsSetContactWidget__ChangeEmail__LetterScreen2Text";
    public static final String LINK_PERSONAL_INFO= "css=a.bInternalOptionsPageLayoutNav__eItemLink";
    public static final String FORM_SETTINGS_CONTENT="id=SettingsContent";
    public static final String FORM_CODE_EXPIRED="id=jsSetContactWidget__CodeExpired";
    //Personal data
    public static final String RADIO_MAN="id=ProfilePersonalForm[sex]_1";
    public static final String RADIO_WOMAN="id=ProfilePersonalForm[sex]_2";
    public static final String FIELD_PASSWORD_IN_PERSONAL_INFO="id=jsPersonalData__PasswordInput";
    public static final String FIELD_NAME_AND_SURNAME="name=ProfilePersonalForm[name]";
    public static final String FIELD_DOC_NUMBER="name=ProfilePersonalForm[identDocNumber0New]";
    public static final String FIELD_LAST_DOC_NUMBER="name=ProfilePersonalForm[identDocNumber1New]";
    
    //Notifications
    public static final String LINK_NOTIFICATIONS="css=a[href='/settings/notifications/'][class*='eItemLink']";
    public static final String CHECKBOX_SECURITY_MAIL="id=NotificationsSettingsForm_notificationEmail_2";
    public static final String CHECKBOX_ACCOUNT_MAIL="id=NotificationsSettingsForm_notificationEmail_268";
    public static final String CHECKBOX_PURCHASE_MAIL="id=NotificationsSettingsForm_notificationEmail_48";
    public static final String CHECKBOX_NEWS_MAIL="id=NotificationsSettingsForm_notificationEmail_128";
    public static final String CHECKBOX_SECURITY_MOBILE="id=NotificationsSettingsForm_notificationMobile_2";

    public SettingsPage(Language lang) {
        sendPause(1);//a hack for wait revert gamepanelpage to settingspanelpage (works animation).
        language = lang;
    }

    String mainWindow = "";

    public String  availableSexForUser(){
       if(findElement(RADIO_MAN).isSelected())
           return RADIO_WOMAN;
        else return RADIO_MAN;

    }

    public void fillChangeEmailForm(String pass,String newMail) {
        log.info("fill change email form");
        Assert.assertTrue(validateElementPresent(FIELD_CHANGE_MAIL),"Form with change mail is not present");
        type(FIELD_CHANGE_MAIL, newMail);
        //type(FIELD_CHANGE_MAIL_PASS, pass);
        Captcha captcha=new Captcha();
        captcha.typeCaptchaIfPresent("987654");
        click(BUTTON_ACCEPT_CHANGE_MAIL);
        //rewrite
        sendPause(3);
        if(captcha.validateCaptchaIsVisible() && isVisible(FIELD_CHANGE_MAIL)){
        	captcha.typeCaptchaIfPresent("987654");
            click(BUTTON_ACCEPT_CHANGE_MAIL);
        }

    }

    public void fillChangeMobileForm(String pass,String mobileNumber) {
        log.info("fill change mobile form");
        Assert.assertTrue(validateElementPresent(FIELD_CHANGE_MOBILE),"Form with change mobile is not present");
        type(FIELD_CHANGE_MOBILE, mobileNumber);
        type(FIELD_CHANGE_MOBILE_PASS, pass);
        Captcha captcha=new Captcha();
        captcha.typeCaptchaIfPresent("987654");
        click(BUTTON_ACCEPT_CHANGE_MOBILE);
    }



    public void deleteFbConnect() {
        try {
            click(SettingsPage.BUTTON_DEL_FB_CONTACT);
            Assert.assertTrue(validateElementVisible(SettingsPage.BUTTON_ADD_FB_CONTACT));
        } catch (Exception e) {
            PrepareUser prepareUser = new PrepareUser();
            prepareUser.deleteFacebookConnect(UserData.getMasterAccountId());
        }
    }

    public void deleteVkConnect() {
        try {
            click(SettingsPage.BUTTON_DEL_VK_CONTACT);
            Assert.assertTrue(validateElementVisible(SettingsPage.BUTTON_ADD_VK_CONTACT));
        } catch (Exception e) {
            PrepareUser prepareUser = new PrepareUser();
            prepareUser.deleteVkConnect(UserData.getMasterAccountId());
        }
    }

    public void clickToOpenAddMobileForm() {
        log.info("Open add mobile form");
        click(UserBarPage.DIV_LOGIN_NAME_FIELD);
        validateElementPresent(UserBarPage.BUTTON_ADD_MOBILE_FROM_LOGIN);
        click(UserBarPage.BUTTON_ADD_MOBILE_FROM_LOGIN);
    }


    public void addMobileNumber(User user,String number) {
        log.info("add mobile number " + number);
        waitStalenessElement(FIELD_MOBILE_NUMBER);
        type(FIELD_MOBILE_NUMBER,number);
        validateElementPresent(FIELD_PASSWORD_TO_ADD_MOBILE);
        //type(FIELD_PASSWORD_TO_ADD_MOBILE, user.getPassword());
        Captcha captcha=new Captcha();
        captcha.typeCaptchaIfPresent("987654");
        validateElementPresent(BUTTON_ADD_MOBILE_NUMBER);
        waitStalenessElement(BUTTON_ADD_MOBILE_NUMBER);
        clickOnStalenessElement(BUTTON_ADD_MOBILE_NUMBER);
    }


    public void fillChangeLoginForm(String login, String pass) {
        log.info("fill login field login is " + login);
        type(FIELD_LOGIN, login);
        log.info("fill password field");
        List<WebElement> fieldsPassword = findElements(FIELD_PASSWORD);
        type(fieldsPassword.get(fieldsPassword.size() - 1), pass);
        Captcha captcha=new Captcha();
        captcha.typeCaptchaIfPresent("987654");
        log.info("click on save settings");
        click(BUTTON_SAVE_SETTINGS);
        if (captcha.validateCaptchaIsVisible()) {
            captcha.typeCaptchaIfPresent("987654");
            click(BUTTON_SAVE_SETTINGS);
        }
    }


    public void fillChangePassForm(String pass) {
        log.info("fill new pass field");
        List<WebElement> fieldsPassword = findElements(FIELD_NEW_PASSWORD);
        type(fieldsPassword.get(fieldsPassword.size() - 1), pass+7);
        log.info("fill password field");
        fieldsPassword = findElements(FIELD_PASSWORD);
        type(fieldsPassword.get(fieldsPassword.size() - 1), pass);
        Captcha captcha=new Captcha();
        captcha.typeCaptchaIfPresent("987654");
        log.info("click on save settings");
        click(BUTTON_SAVE_SETTINGS);
        if (captcha.validateCaptchaIsVisible()) {
            captcha.typeCaptchaIfPresent("987654");
            click(BUTTON_SAVE_SETTINGS);
        }
    }


    public void getAndTypeCodeFromSms(User user) {
        log.info("get and type code from sms from number " + user.getMobileNumber());
        String code = MailProvider.getSms(user.getMobileNumber()).getCodeFromSMS();
        log.info("code from number " + code);
        type(FIELD_FOR_CODE_ADD_MOBILE, code);
        validateElementPresent(BUTTON_ADD_NUMBER_AFTER_CODE);
        int num = MailProvider.getNumberOfMessages(user.getMainEmail());
        click(BUTTON_ADD_NUMBER_AFTER_CODE);
        MailProvider.isNewMessagePresent(user.getMainEmail(), num);

    }


/*    public void fillCodeFromSmsForm(String codeFromSms){
        log.info("fill form with code from sms");
        type(SettingsPage.BUTTON_CHANGE_MOBILE_CODE, codeFromSms);
        Captcha captcha=new Captcha();
        captcha.typeCaptchaIfPresent("987654");
        click(SettingsPage.BUTTON_CHANGE_MOBILE_NEXT);
        if (captcha.validateCaptchaIsVisible()) {
            captcha.typeCaptchaIfPresent("987654");
            click(BUTTON_CHANGE_MOBILE_NEXT);
        }
    }

    public void fillOldCodeFromSmsForm(String codeFromSms){
        log.info("fill form with old code from sms");
        type(SettingsPage.FIELD_CHANGE_MOBILE_OLD_CODE, codeFromSms);
        Captcha captcha=new Captcha();
        captcha.typeCaptchaIfPresent("987654");
        click(SettingsPage.BUTTON_CHANGE_MOBILE_OLD_CODE);
        if (captcha.validateCaptchaIsVisible()) {
            captcha.typeCaptchaIfPresent("987654");
            click(BUTTON_CHANGE_MOBILE_OLD_CODE);
        }
    }

    public void fillDeleteMobileForm(String codeFromSms){
        log.info("fill delete mobile form code from sms");
        type(SettingsPage.FIELD_DELETE_MOBILE_CODE,codeFromSms);
        Captcha captcha=new Captcha();
        captcha.typeCaptchaIfPresent("987654");
        click(SettingsPage.BUTTON_DELETE_MOBILE_ACCEPT);
        if (captcha.validateCaptchaIsVisible()) {
            captcha.typeCaptchaIfPresent("987654");
            click(BUTTON_DELETE_MOBILE_ACCEPT);
        }
    }
*/
    /**
     * Returns the link on the current page
     */
    @Override
    public String getPageUrl() {
        return pageUrl;
    }


}
