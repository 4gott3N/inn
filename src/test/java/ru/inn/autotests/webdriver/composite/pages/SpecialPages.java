package ru.inn.autotests.webdriver.composite.pages;

import ru.inn.autotests.webdriver.composite.OperationsHelper;

/**
 * Created with IntelliJ IDEA.
 * User: Sergey.Kashapov
 * Date: 23.07.13
 * Time: 14:23
 * To change this template use File | Settings | File Templates.
 */
public class SpecialPages extends OperationsHelper {

    public static final String CLASS_BODY_OF_404="css=.b404__text";
    public static final String CLASS_BODY_OF_504="css=.b504__text";
    public static final String LABEL_TIMER_ON_504="id=jsOverloadTimer";
    public static final String CLASS_BODY_OF_500="css=.bError";
    public static final String CLASS_BODY_OF_MAINT="css=.bMaintenance";
    public static final String TWITTER_WIDGET="id=twitter-widget-0";
    public static final String VK_WIDGET="id=vk_groups";

    public void open404(){
        log.info("Open 404 page");
        openUrlIn4game("/404");
    }

    public void open504(){
        log.info("Open 504 page");
        openUrlIn4game("504.html");
    }
    public void open500(){
        log.info("Open 500 page");
        openUrlIn4game("licence/view/serviceId/1099/type/2/");
    }

}
