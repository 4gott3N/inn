package ru.inn.autotests.webdriver.composite.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import ru.inn.autotests.webdriver.composite.IPage;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.games.Game;
import ru.inn.autotests.webdriver.toolkit.PlatformUtils;
import ru.inn.autotests.webdriver.toolkit.WebDriverController;

import java.util.ArrayList;
import java.util.Map;


/**
 * The type User bar page.
 */
public class UserBarPage extends OperationsHelper implements IPage {

    /**
     * The constant BUTTON_ADD_FUNDS.
     */
    public static final String BUTTON_ADD_FUNDS = "id=userBar-button-Replenish";
    /**
     * The constant DIV_USER_BAR.
     */
    public static final String DIV_USER_BAR = "id=userBar4Game";
    
    /**
     * The constant SIGN_UP_BUTTON.
     */
    public static final String BUTTON_SIGN_UP = "id=userBar-button-authAndReg";
    /**
     * The constant FACEBOOK_BUTTON_LOGIN.
     */
    public static final String BUTTON_FACEBOOK_LOGIN = "id=userBar-button-FB";
    /**
     * The constant VK_BUTTON_LOGIN.
     */
    public static final String BUTTON_VK_LOGIN = "id=userBar-button-VK";
    /**
     * The constant HOME_LINK.
     */
    public static final String LINK_HOME = "id=userBar-link-Home";
    /**
     * The constant DIV_HOME_TEXT.
     */
    public static final String DIV_HOME_TEXT = "id=userBar-text-Home";
    /**
     * The constant DROPDOWN_LIST_MYGAMES.
     */
    public static final String DROPDOWN_LIST_MYGAMES = "id=jsUserBar__GameList";
    /**
     * The constant BUTTON_MY_GAMES.
     */
    public static final String BUTTON_MY_GAMES = "id=jsUserBar__LinkMyGames";
    /**
     * The constant GAMES_IN_DROPDOWN.
     */
    public static final String GAMES_IN_DROPDOWN = "xpath=//div[contains(@id, 'MyGamesItem_')]";
    /**
     * The constant ICON_HEART.
     */
    public static final String ICON_HEART = "id=MyGames_Heart";
    /**
     * The constant ICON_SPINNER.
     */
    public static final String ICON_SPINNER = "id=MyGames_Spinner";
    /**
     * The constant DROPDOWN_LIST_GAME_PATTERN.
     */
    public static final String DROPDOWN_LIST_GAME_PATTERN = "id=MyGamesItemTitle_";
    /**
     * The constant TEXT_ITEM_STATUS.
     */
    public static final String TEXT_ITEM_STATUS = "xpath=//div[@class='bMyGamesList__eItemStatus']";
    /**
     * The constant USER_BONUS_VALUE.
     */
    public static final String USER_BONUS_VALUE = "id=jsUserBarBalance__eBonusValue";
    /**
     * The constant LABEL_BALANCE_USERBAR.
     */
    public static final String LABEL_BALANCE_USERBAR = "id=jsUserBarBalance__eMoney";
    /**
     * The constant ID_LOGIN_NAME_FIELD.
     */
    public static final String DIV_LOGIN_NAME_FIELD = "id=userBar-div-LoginName";
    /**
     * The constant USER_MONEY_VALUE.
     */
    public static final String USER_MONEY_VALUE = "id=userBar-div-UserBalanceData";
    /**
     * The constant BUTTON_CREDIT.
     */
    public static final String BUTTON_CREDIT = "css=span[data-popup=jsPopupHint__Credit]";

    public static String MESSAGE_REPLANISH_ERROR = "id=userBar-div-Replanish_Error";

    
    /**
     * The constant LABEL_CREDIT_ON.
     */
    public static final String LABEL_CREDIT_ON = "id=userBar-div-CreditStatus_On";

    /**
     * The constant LABEL_CREDIT_OFF.
     */
    public static final String LABEL_CREDIT_OFF = "id=userBar-div-CreditStatus_Off";

    /**
     * The constant DIV_INSTALLED_GAMES_NAV_BAR.
     */
    public static final String DIV_INSTALLED_GAMES_NAV_BAR = "id=navBar-div-InstalledGames";
    /**
     * The constant DIV_UNINSTALLED_GAMES_NAV_BAR.
     */
    public static final String DIV_UNINSTALLED_GAMES_NAV_BAR = "id=navBar-div-UninstalledGames";

    /**
     * The constant DIV_GAME_ICON_NAV_BAR.
     */
    public static final String DIV_GAME_ICON_NAV_BAR = "xpath=//li[@data-service-id=%s]";
    /**
     * The constant HREF_GAME_NAV_BAR.
     */
    public static final String HREF_GAME_NAV_BAR = "xpath=//a[@data-serviceid=%s]";
    
    public static final String BORDER_YELLOW_FOR_ICON_NAV_BAR = "xpath=//li[@data-service-id=%s]/div/div[contains(@class,'active')]";
    /**
     * The constant TAG_FOR_GAME_NAV_BAR.
     */
    public static final String TAG_FOR_GAME_NAV_BAR = "css=#hint-%s > .bGamesNav__eItem__eTags > .bCrossconversion__eTag";
    /**
     * The constant NAME_FOR_GAME_NAV_BAR.
     */
    public static final String NAME_FOR_GAME_NAV_BAR = "css=#hint-%s > .bGamesNav__eItem__eName";
    /**
     * The constant BUTTON_INSTALL_NAV_BAR.
     */
    public static final String BUTTON_INSTALL_NAV_BAR = "css=.bGamesNav__eInstall__mId%s";

    public static final String DIV_TOOLTIP_NAV_BAR = "css=div[class*='tooltip--game_install_%s']";

    /**
     * The constant BADGE_UPDATE_REQUIRED.
     *///BADGES
    public static final String BADGE_UPDATE_REQUIRED = "css=div[class*='full_update_required']";
    /**
     * The constant BADGE_PAUSED.
     */
    public static final String BADGE_PAUSED = "css=div[class*='paused']";
    /**
     * The constant BADGE_PROGRESS.
     */
    public static final String BADGE_PROGRESS = "css=div[class*='progress']";
    /**
     * The constant BADGE_MAINTENANCE.
     */
    public static final String BADGE_MAINTENANCE = "css=div[class*='maint']";
    /**
     * The constant BADGE_ERROR.
     */
    public static final String BADGE_ERROR = "css=div[class*='error']";
    /**
     * The constant BADGE_BETA.
     */
    public static final String BADGE_BETA = "css=div[class*='ext-status-beta']";
    /**
     * The constant BADGE_NEW.
     */
    public static final String BADGE_NEW = "css=div[class*='ext-status-new']";


    public static final String BUTTON_ADD_MOBILE_FROM_LOGIN = "id=userBar-button-AddMobile";

    /**
     * The constant SCRIPT_POSITION_OF_SCROLL.
     */
    public static final String SCRIPT_POSITION_OF_SCROLL="return window.pageYOffset || document.documentElement.scrollTop";
    
    /**
     * The constant BUTTON_USER_DATA_ARROW.
     */
    public static final String BUTTON_USER_DATA_ARROW="id=userBar-arrow-UserData";
    
    /**
     * The constant BUTTON_USER_DATA_ARROW.
     */
    public static final String BUTTON_USER_BALANCE_DATA_ARROW="id=userBar-arrow-UserBalanceData";
    
    public static final String LINK_SETTINGS="css=a[href='/settings/']";
    public static final String LINK_PERSONAL_DATA="css=a[href='/settings/personal/']";
    public static final String LINK_NOTIFICATIONS="css=a[href='/settings/notifications/']";
    public static final String LINK_EVENTS="css=a[href='/events/']";

    public static final String LINK_SECURITY="css=a[href*='/security/']";
    public static final String BUTTON_SUPER_SECURITY_ON="id=dropdown_ubersecurity_turn_on";
    public static final String LINK_LOGOUT="id=userBar-link-Logout";
    public static final String ERROR_MESSAGE_REPLENISH="css=.bUIErrorMessage__mDisplay_Block";

    //public static final String LINK_LOGOUT="id=userBar-link-Logout";

    
    public static final String LINK_GIFT_CODES="css=a[href='/giftcode/']";
    public static final String LINK_CREDIT="css=a[href*='/credit/']";
    
    public static final String LINK_COMPUTER_GAMES="id=userBar-link-ComputerGames";
    /**
     * Gets current user money.
     *
     * @return the current user money
     */
    public double getCurrentUserMoney() {
    	Assert.assertTrue(validateElementPresent(USER_MONEY_VALUE), "The user is not authorized. USER_MONEY_VALUE is not visible");
        String stringMoney = getText(USER_MONEY_VALUE);
		return Double.parseDouble(stringMoney.replace(",", "."));
    }

    /**
     * Assert user sum and bonuses.
     *
     * @param oldMoney - money that were before
     * @param oldBonuses - bonuses that were before
     * @param changeMoneySum - sum that will be write off
     */
    public void assertUserSum(double oldMoney, double changeMoneySum) {
    	Assert.assertTrue(validateElementPresent(DIV_LOGIN_NAME_FIELD), "The user is not authorized. DIV_LOGIN_NAME_FIELD is not visible");
        int i = 0;
        refreshPage();
        while (getCurrentUserMoney() == oldMoney) {
            refreshPage();
            i++;
            if (i > 10)
                Assert.fail("Sum of user is not changed after 30 seconds. User sum hasn't been changed correctly " + changeMoneySum + " and its now " + getCurrentUserMoney());

        }
        Assert.assertEquals(getCurrentUserMoney(), oldMoney + changeMoneySum, "User sum hasn't been changed correctly " + changeMoneySum + " and its now " + getCurrentUserMoney());
    }

    /**
     * Gets game nav bar locator.
     *
     * @param serviceId the service id
     * @return the game nav bar locator
     */
    public static String getGameNavBarLocator(String serviceId) {
        log.info("select game with serviceId " + serviceId);
        String divGameIcon = String.format(DIV_GAME_ICON_NAV_BAR, serviceId);
        log.info(divGameIcon);
		return divGameIcon;

    }

    /**
     * Gets game nav bar link.
     *
     * @param game the game
     * @return the game nav bar link
     */
    public static String getGameNavBarLink(Game game) {
        log.info("select game " + game.getAppName());
        return String.format(HREF_GAME_NAV_BAR, game.getServiceId());

    }

    /**
     * Assert all games in user bar.
     *
     * @param map the map
     */
    public void assertAllGamesInUserBar(Map<Game.GameEnum, Game> map) {
        for (Game game : map.values()) {
            Assert.assertTrue(validateElementVisible(getGameNavBarLink(game)), "Game " + game.getAppName() + " is not in navigation bar");
        }
    }

    /**
     * Assert class maintenance for all games in nav bar.
     *
     * @param map the map
     * @param isPlugin the is plugin
     */
    public void assertClassMaintenanceForAllGamesInNavBar(Map<Game.GameEnum, Game> map, boolean isPlugin) {
        for(Game game : map.values())
        PlatformUtils.setMaintenance(game, isPlugin);
        for (Game game : map.values()) {
            Assert.assertTrue(getAttribute(getGameNavBarLocator(game.getServiceId())+"> div","class").contains("bGamesNav__eItem__mVisible"), "Game " + game.getAppName() + " hasn't attribute in class with maintenance");
        }
    }

    /**
     * Assert installed game is in nav bar.
     *
     * @param game the game
     */
    public void assertInstalledGameIsInNavBar(Game game) {
        log.info("Check installed games in navigation bar");
        if (!findElement(DIV_INSTALLED_GAMES_NAV_BAR).findElement(locatorFactory(getGameNavBarLink(game))).isDisplayed()) {
            Assert.fail("Installed game is not in navigation installed games");
        }
    }

    /**
     * Assert uninstalled game is in nav bar.
     *
     * @param game the game
     */
    public void assertUninstalledGameIsInNavBar(Game game) {
        log.info("Check uninstalled games in navigation bar");
        WebElement divUninstalledGames = findElement(DIV_UNINSTALLED_GAMES_NAV_BAR);
		String gameIcon = getGameNavBarLink(game);
		Assert.assertTrue(divUninstalledGames.findElement(locatorFactory(gameIcon)).isDisplayed(), 
				"Uninstalled game is not in navigation uninstalled games");
    }

    /**
     * Assert badge for game.
     *
     * @param game the game
     * @param locator the locator
     * @return the boolean
     */
    public boolean assertBadgeForGame(Game game, String locator) {
        WebElement badge = null;
        try {
            badge = findElement(DIV_INSTALLED_GAMES_NAV_BAR).findElement(locatorFactory(getGameNavBarLocator(game.getServiceId()))).findElement(locatorFactory(locator));
        } catch (NoSuchElementException e) {
        }
        return badge==null?false:true;
    }

    /**
     * Great method for stage with plugin!
     * @param installedGames the installed games
     * @param map the map
     */
    public void assertInstalledGamesInNavBarWithPlugin(ArrayList<String> installedGames, Map<Game.GameEnum, Game> map) {
        log.info("Check installed games in navigation bar");
        ArrayList<String> installedGamesForStage = new ArrayList<String>();
        for (int i = 0; i < installedGames.size(); i++) {
            for (Game game : map.values()) {
                if (installedGames.get(i).equals(game.getServiceId())) {
                    installedGamesForStage.add(game.getServiceId());
                }
            }
        }
        if (installedGamesForStage.isEmpty())
            Assert.fail("There are no installed games");
        for (String service : installedGamesForStage) {
            if (!findElement(DIV_INSTALLED_GAMES_NAV_BAR).findElement(locatorFactory(getGameNavBarLocator(service))).isDisplayed()) {
                Assert.fail("Installed game is not in navigation installed games");
            }

        }

    }


    @Override
    public String getPageUrl() {
        return "/";
    }
}
