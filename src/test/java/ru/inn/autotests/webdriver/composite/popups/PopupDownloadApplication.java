package ru.inn.autotests.webdriver.composite.popups;

import ru.inn.autotests.webdriver.composite.IPage;
import ru.inn.autotests.webdriver.composite.common.IFrame;

/**
 * User: pavel.popov
 */
public class PopupDownloadApplication extends IFrame implements IPage {

    public static String BUTTON_ACCEPT_LICENSE = "id=jsLicensePopup-btnAgree";
    public static String FORM_WIDGET_DOWNLOAD = "id=WidgetDownloadForm";


    @Override
    public String getPageUrl() {
        throw new UnsupportedOperationException();
    }
}
