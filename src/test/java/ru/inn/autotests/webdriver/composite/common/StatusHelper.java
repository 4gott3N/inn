package ru.inn.autotests.webdriver.composite.common;

import org.openqa.selenium.By;
import org.testng.Assert;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.games.Game;
import ru.inn.autotests.webdriver.tests.AbstractTest;
import ru.inn.autotests.webdriver.toolkit.ProcessProvider;
import ru.inn.autotests.webdriver.toolkit.TextProvider;
import ru.inn.autotests.webdriver.toolkit.WebDriverController;
import ru.inn.autotests.webdriver.toolkit.YamlProvider;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//My dear friend this class is consists from many pages with payment and one popup widget in 4 game
//Plz don't be stunned  from it,but i don't know, where i should put this class.
public abstract class StatusHelper extends OperationsHelper {

    private static final String FOR_GAME_BEGIN_WORD = "4game";
    private final String applicationNameProp = "application_name";
    private final String pathToDownloadApp = "path_to_download_app";

    /**
     * Validates that percent in the status is correct (more than 0 and less than 100)
     * @param locator - status locator
     */
    public void validatePercentFromStatus(String locator){
        log.info("validate percent from status");
        By by = locatorFactory(locator);
        waitForElementPresent(by);
        String digits = getText(by);
        Pattern p = Pattern.compile("(\\d+)"); //get percent without char '%'
        Matcher m = p.matcher(digits); // ex. '15%' after matcher's work -> '15'
        if (m.find()) {
            Double dDigits = Double.valueOf(m.group());
            Assert.assertTrue(dDigits >= 0 ? true : false);
            Assert.assertTrue(dDigits < 100 ? true : false); // this's bug on the game block (the game size isn't displayed correct, comma has been removed) //checking it (ex: L2EU game size: 14.29 Gb, without comma: 1429 Gb) -> magic digit's 200
        } else {
            throw new RuntimeException("Don't cast digits to Double:" + digits);
        }
    }

    /**
     * Validates that text without percent contains with required text
     * @param locator - status locator
     */
    public void validateStatus(String locator, String keyText){
        log.info("validate status,expected "+keyText);
        By by = locatorFactory(locator);
        waitForElementPresent(by);
        String status = getText(by);
        status = status.split(":")[0];
        Assert.assertEquals(TextProvider.translator(keyText, language), status);
    }


    public void validateSize(String locator,Game game) {
        log.info("validate game size game- "+game.getAppName());
        By by = locatorFactory(locator);
        double gameSize= Double.valueOf(game.getGameSize());
        double delt= gameSize*1.5;
        waitForElementPresent(by);
        String digits = getText(by);
        Pattern p = Pattern.compile("(\\d+)"); //get percent without char '%'
        Matcher m = p.matcher(digits); // ex. '15%' after matcher's work -> '15'
        if (m.find()) {
            Double dDigits = Double.valueOf(m.group());
            Assert.assertTrue(dDigits >= gameSize-delt ? true : false);
            Assert.assertTrue(dDigits < gameSize+delt ? true : false);
        } else {
            throw new RuntimeException("Don't cast digits to Double:" + digits);
        }
    }

    /**
     * Validates that application file is downloaded and has current version
     */
    public void validateDownloadedApplication(Game game) {
        log.info("validate downloaded application");
        String applicationName = "";
        if (!baseUrl.contains("eu")) {
            if (game != null) {
                applicationName = game.getAppName() +"_setup-ru.exe";
            }
            else {
                applicationName = FOR_GAME_BEGIN_WORD + "_setup-ru.exe";
            }

        }
        else {
            if (game != null) {
                applicationName = game.getAppName() +"_setup-en.exe";
            }
            else {
                applicationName = FOR_GAME_BEGIN_WORD + "_setup-en.exe";
            }
        }


        String pathFile = YamlProvider.getParameter(pathToDownloadApp, YamlProvider.appConfigs);
        String pathWithFileName = pathFile + File.separator + applicationName;
        File dirApp = new File(pathFile);
        int timeout = 0;
        boolean isPresent = true;
        final String APP_NAME = applicationName;
        FilenameFilter fileDetectPattern = new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				if (name.contains(APP_NAME)) 
					return true;
				else 
					return false;
			}};

		while(dirApp.listFiles(fileDetectPattern).length != 1) {       	
            OperationsHelper.sendPause(1);
            if (timeout > WebDriverController.TIMEOUT) {
                isPresent = false;
                break;
            }
            timeout++;
        }
        if (!isPresent) {
        	File [] fileList = dirApp.listFiles();
        	StringBuffer sb = new StringBuffer();
        	sb.append("List of files detected: ");
        	for (int i = 0; i < fileList.length; i++) {
        		sb.append(fileList[i].getName().toString() + "; ");
        	}
        	Assert.fail("Application install file wasn't found in directory. Expected download directory path: " + pathFile
            		+ ", expected pattern: " + applicationName + ".\n" +
            		sb.toString());
        }
        
        File helper = new File(".");
        String pathHelper = helper.getAbsolutePath();
        String fileVersionPath = pathHelper.substring(0, pathHelper.length() - 1) + YamlProvider.getParameter("file_version_path",YamlProvider.appConfigs)
             + File.separator +  YamlProvider.getParameter("file_version_name", YamlProvider.appConfigs);

        File versionFile = new File(pathFile + File.separator + "version.txt");

        ProcessProvider pp = new ProcessProvider();
        pp.getAppVersionFile(fileVersionPath, pathWithFileName, versionFile.getAbsolutePath());

        String version = "";

        isPresent = true;
        timeout = 0;
        while(!versionFile.exists()){
            OperationsHelper.sendPause(1);
            if (timeout > WebDriverController.TIMEOUT) {
                isPresent = false;
                break;
            }
            timeout++;
        }

        Assert.assertTrue(isPresent, "Version file wasn't found in directory. Expected version file path: " + versionFile);

        BufferedReader br = null;

        try {
            br = new BufferedReader(new FileReader(versionFile));
            version = br.readLine();    // flle contains only one line
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Check the file: " + versionFile.getAbsolutePath());
        } catch (IOException ie){
            throw new RuntimeException("File isn't readable. Check it.");
        }
        if(System.getenv("plugin_version")!=null)
        {
            Assert.assertEquals(version,System.getenv("plugin_version"));
        }
        else
        {
            String  versionApp=System.getenv("versionApp");
            if(versionApp==null)
                versionApp= YamlProvider.getParameter("application_version", AbstractTest.configName);
            Assert.assertEquals(version,versionApp,"Application version is not "+versionApp+" it is "+version);
        }
    }


}
