package ru.inn.autotests.webdriver.composite.popups;

import ru.inn.autotests.webdriver.composite.IPage;
import ru.inn.autotests.webdriver.composite.OperationsHelper;

public class PopupRemovePassword extends OperationsHelper implements IPage {

    public static String INPUT_CURRENT_PASSWORD = "name=ChangeServicePasswordForm[currentPassword]";

    public static String INPUT_USER_PASSWORD = "name=ChangeServicePasswordForm[masterPassword]";
    public static String BUTTON_CHANGE_PASSWORD = "id=jsSetGamePasswordWidget-btnDeletePassword";
    public static String INPUT_ICON_ENTER = "xpath=//input[@class='bBigButton__eEnterKeyIcon']";

    @Override
    public String getPageUrl() {
        return null;
    }

}
