package ru.inn.autotests.webdriver.composite.pages;

import org.hamcrest.core.IsNot;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.TestException;
import ru.inn.autotests.webdriver.common.language.English;
import ru.inn.autotests.webdriver.common.language.Language;
import ru.inn.autotests.webdriver.common.language.Russian;
import ru.inn.autotests.webdriver.composite.IPage;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.composite.common.Captcha;
import ru.inn.autotests.webdriver.composite.common.StatusHelper;
import ru.inn.autotests.webdriver.composite.popups.PopupDownload;
import ru.inn.autotests.webdriver.composite.popups.PopupFinalFormalities;
import ru.inn.autotests.webdriver.composite.popups.PopupInstallGame;
import ru.inn.autotests.webdriver.games.Game;
import ru.inn.autotests.webdriver.toolkit.GameData;
import ru.inn.autotests.webdriver.toolkit.TextProvider;

import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GamePanelPage extends StatusHelper implements IPage {

    private String pageUrl;
    private static Matcher MATCHER;
    private static Pattern PATTERN;

    //Main
    public static final String GAME_PANEL = "id=GamePanel";

    public static final String STATUS_MAINTENANCE_BIG = "id=jsBigMaintenanceMessage";
    public static final String STATUS_MAINTENANCE_SMALL = "id=jsSmallMaintenanceMessage";

    public static final String GAME_STATUS = "id=bGamePanel__status";

    public static final String MAIN_SPINNER_WAITING = "xpath=//*[@class='bGamePanelDefaultState__eWaitingSpinner']";
    public static final String LINK_AUTO_IMPROVEMENT = "id=bPremiumActions__eAutoImprovement";

    //Buttons
    public static final String BUTTON_UNKNOWN = "xpath=//button[contains(@class,'bUIButton__mWidth_full') and not(contains(@id,'BtnInstallPlugin'))]";
    public static final String BUTTON_INSTALL_WITH_NO_PLUGIN = "id=bGamePanel__eBtnInstallPlugin_jsId";
    public static final String BUTTON_INSTALL = "id=bGamePanel__eBtnInstallService_jsId"; //if plugin's not install - id=bGamePanel__eBtnInstallPlugin_jsId
    public static final String BUTTON_INSTALL_PLUGIN = "css=button.bPseudoButton.bPseudoButton__mColor_inherit";
    public static final String BUTTON_PLAY = "id=jsBtnPlay";
    public static final String BUTTON_BUTTON_PLAY_OR_PLAY_WITH_PASSWORD = "css=button[id*='jsBtn']";
    public static final String BUTTON_PLAY_BLOCKED = "id=jsBtnPlayBlocked";
    public static final String BUTTON_PLAY_WITH_PASSWORD = "id=jsBtnWithPassword";
    public static final String BUTTON_SETTINGS = "id=jsGamePanel-btnToSettings";
    public static final String BUTTON_UPDATE = "id=bGamePanel__eBtnUpdateService_jsId";
    public static final String BUTTON_SIGN_IN = "id=bGamePanel__eBtnLogin_jsId";
    public static final String BUTTON_CBT_CODE = "id=BetaTesterPinCodeButton";
    public static final String BUTTON_PLAY_WITH_PASSWORD_ARROW = "xpath=//*[@class='bUIButton__ePlayWithPassword']";
    public static final String BUTTON_REQUEST_TO_DOWNLOAD_APPLICATION = "xpath=//*[@id='jsGamePanelDownloadApp']/button";
    public static final String BUTTON_PLAY_WITH_PASSWORD_DISABLED = "xpath=//*[@id='jsBtnWithPassword' AND @disabled='disabled']";
    public static final String BUTTON_UPDATE_STATUS_PANEL = "xpath=//button[@class='bSmallButton jsGamePanel__eDefaultState__Refresh']";
    public static final String BUTTON_PLAY_REMEMBERED_PASSWORD = "xpath=//*[@class='bUIButton__eLock bUIButton__eLock__mOpened_true']";
    public static final String SPINNER_ON_BUTTON = "css=button[class*='Loading']";
    //INPUTS
    public static final String FIELD_GAME_PASSWORD = "xpath=//*[@type='password']";

    //Information
    public static final String GAME_SIZE = "id=bGamePanel__installServiceSize_jsId";
    public static final String LINK_INSTALL_SUCCESS = "id=jsBtnJustInstalled";
    public static final String PROGRESS_BLOCK_CANCEL_STATUS = "id=jsGamePanel__ProgressBlock__eButton_ProgressStopStatus";
    public static final String AUTOPAY_LINK = "id=";
    public static final String LINK_FORGET_PASSWORD = "id=WantToForgetPasswordLink";
    public static final String STATUS_UPDATE_4GAME_APP = "xpath=//*[@id='bGamePanel__status']/div/span/span";
    public static final String ICON_GAME = "css=img.bGamePanelCommon__eTitleIcon";
    public static final String FIELD_PREMIUM_MIN_PRICE = "id=gamepanel-premium-min-price";
    public static final String LINK_PREMIUM_PAGE = "id=gamepanel-premium-page-link";
    public static final String FIELD_PREMIUM_FEATURES = "css=ul[class='bGamePanelCommon__ePremiumFeatures']";

    //Cancel block
    public static final String CANCEL_BLOCK_BUTTON_YES = "id=jsGamePanel__ProgressBlock__eButton_ProgressStopYes";
    public static final String CANCEL_BLOCK_BUTTON_NO = "id=jsGamePanel__ProgressBlock__eButton_ProgressStopNo";

    public static final String CHECKBOX_REMEMBER_PASSWORD = "css=input[id='chbRememberGamePassword']";
    public static final String LINK_FORGOT_PASSWORD = "xpath=//div[@id='ForgotPasswordBlock']/a";

    //PROGRESS BLOCK
    //PROGRESS BAR
    public static final String PROGRESS_BAR = "xpath=//div[@class='bUIProgressBar__eInner']";
    public static final String PROGRESS_BAR_PERCENT = "xpath=//span[@class='bUIProgressBar__eStatus']";
    public static final String PROGRESS_BAR_ON_PAUSE = "xpath=//div[contains(@class,'bUIProgressBar__mState_pause')]";
    public static final String PROGRESS_BAR_STATUS_DOWNLOADED = "id=jsGamePanel__ProgressBlock__Downloaded";
    public static final String PROGRESS_BAR_STATUS_DOWNLOAD_SIZE = "id=jsGamePanel__ProgressBlock__DownloadSize";

    //LINKS
    public static final String PROGRESS_PAUSE = "id=jsGamePanel__ProgressBlock__eButton_Pause";
    public static final String PROGRESS_RESUME = "id=jsGamePanel__ProgressBlock__eButton_Resume";
    public static final String PROGRESS_CANCEL = "id=jsGamePanel__ProgressBlock__eButton_Cancel";

    //BAN
    public static final String BAN_INFO_MESSAGE = "id=banInfoMessage";

    //PREMIUM
    public static final String BUTTON_BUY_PREMIUM = "id=jsBtnBuyPremiumInGamePanel";
    
    public static final String BUTTON_BUY_SUBS = "css=a[href='?popupWidget=PremiumUpgradeWidget']";

    //Premium Status
    public static final String DIV_PREMIUM_FEATURES = "css=.bGamePanelCommon__ePremiumFeatures";
    public static final String MESSAGE_PREMIUM_STATUS = "id=premiumDurationInfo";
    public static final String MESSAGE_PREMIUM_OVER = "css=span[id='premiumDurationInfo'][class*='error']";
    public static final String MESSAGE_PREMIUM_FINISHES = "css=span[id='premiumDurationInfo'][class*='warning']";
    public static final String MESSAGE_PREMIUM_ENABLED = "css=span[id='premiumDurationInfo'][class*='confirmed']";

    //Settings
    public static String BUTTON_DEFAULT_PATH = "id=jsGamePanel-btnChangeDefaultPath";
    public static String BUTTON_FIX_GAME = "id=jsGamePanel-btnFixGame";
    public static String BUTTON_SET_PASSWORD = "id=jsGamePanel-btnPasswordSet-1";
    public static String BUTTON_CHANGE_PASSWORD = "id=jsGamePanel-btnPasswordChange-1";
    public static String BUTTON_REMOVE_PASSWORD = "id=jsGamePanel-btnPasswordDelete-1";
    public static String FIELD_NEW_PASSWORD = "name=ChangeServicePasswordForm[newPassword]";
    public static String BUTTON_SAVE_PASSWORD = "id=jsSetGamePasswordWidget-btnCreatePassword";
    public static String FIELD_SET_PASSWORD_MASTER = "name=ChangeServicePasswordForm[masterPassword]";
    public static String BUTTON_SUBMIT_MASTER_PASSWORD = "xpath=//*[@type='submit' and @value=' ']";
    public static String FIELD_OLD_PASSWORD = "name=ChangeServicePasswordForm[currentPassword]";
    public static String BUTTON_SAVE_CHANGES = "id=jsSetGamePasswordWidget-btnChangePassword";
    public static String BUTTON_DELETE_PASSWORD = "id=jsGamePanel-btnPasswordDelete-1";
    public static String BUTTON_DELETE_PASSWORD_POPUP = "id=jsSetGamePasswordWidget-btnDeletePassword";
    public static String LINK_FORGOT_PASSWORD_SETTINGS = "id=forgotPasswordLinkInSettings";
    public static String BUTTON_SEND = "css=.bBigButton__eButton";
    public static String LABEL_LOCK_ON_THE_BUTTON = "xpath=//*[@class='bBigButton__eLock']";
    public static String FIELD_RECOVER_PASSWORD_MASTER = "name=RecoveryServicePasswordForm[masterPassword]";
    public static String FIELD_RECOVER_SMS_CODE = "name=RecoveryServicePasswordForm[code]";
    public static String FIELD_RECOVER_NEW_PASSWORD = "name=RecoveryServicePasswordForm[password]";
    public static String BUTTON_RECOVER_ACCEPT = "css=.bBigButton__eButton";
    public static String BUTTON_BACK = "id=jsGamePanel-btnToCommon";
    public static String LABEL_ABOUT_SENT_EMAIL = "xpath=//*[@class='bAddingInfoPopup__eNoteText']";
    public static String BUTTON_SWITCH_ON_SUPERSECURITY = "id=jsGamePanel-btnEnableSupersecurity";
    public static String LABEL_SWITCHED_ON_SUPERSECURITY = "id=SuperSecurityJustEnabled";


    //FILES WITH CODES
    private final static String pathToCode = System.getProperty("user.dir") +
            File.separator + "src" + File.separator + "test" + File.separator +
            "resources" + File.separator + "codes" + File.separator;
    public static final String ROLE_TESTER = "tester_%s.txt";
    

    //CBT code form
    public static final String FORM_CBT = "xpath=//*[@class='bPinCodeForm__eForm']";
    public static final String FIELD_CELL_1 = "name=code[0]";
    public static final String FIELD_CELL_2 = "name=code[1]";
    public static final String FIELD_CELL_3 = "name=code[2]";
    public static final String FIELD_CELL_4 = "name=code[3]";
    public static final String BUTTON_ACTIVATE = "id=bGiftcode__ButtonSubmit";
    public static final String MESSAGE_FAIL_ACTIVATION = "xpath=//*[@class='bInternalPageValidationMessage__eText']";

    //leave a request form betateter
    public static final String BUTTON_LEAVE_REQUEST = "id=bCBTWidget__eParticipateBtn";
    public static final String FORM_LEAVE_REQUEST = "xpath=//*[@class='pOfc_bCBTWidget']";
    public static final String BUTTON_LOYALITY_BUY_PREMIUM = "id=premium-buy-planetside2";
    
    //change env panel
    public static final String DIV_CHANGE_GAME_ENV = "id=UserScript__eGameEnv";
    public static final String DIV_LIVE_ENV = "css=span[data-val=live]";


    /**
     * Creates instance
     *
     * @param game
     */
    public GamePanelPage(Game game, Language language) {
        this.pageUrl = game.getURL();
        super.setLanguage(language);
    }
   
    /**
     * Creates instance, which set language is default (English)
     *
     * @param game
     */
    public GamePanelPage(Game game) {
        this.pageUrl = game.getURL();
    }

    /**
     * Returns current URL page
     */
    @Override
    public String getPageUrl() {
        return pageUrl;
    }


  /*  public  void changeGameEnv(String env) {
    	Assert.assertTrue(validateElementIsNotVisible(GamePanelPage.BUTTON_INSTALL_WITH_NO_PLUGIN),
    			"BUTTON_INSTALL_WITH_NO_PLUGIN is still visible. This machine is too slow");
    	driver.executeScript("javascript:(function(){"+
        "var _breq = require.config({baseUrl: '/js/bookmarklets'});" +
        "_breq(['game_env'], function(b) {b.start();});})()");
    	Assert.assertTrue(validateElementVisible(GamePanelPage.DIV_CHANGE_GAME_ENV),
    			"DIV_CHANGE_GAME_ENV is not visible after script");
    	click(GamePanelPage.DIV_LIVE_ENV);
    	Assert.assertTrue(validateElementIsNotVisible(GamePanelPage.DIV_CHANGE_GAME_ENV),
    			"DIV_CHANGE_GAME_ENV is still visible after click(GamePanelPage.DIV_LIVE_ENV)");
    }*/
    
    public  void changeGameEnv(String env) {
    	log.info("changeGameEnv started");
    	Assert.assertTrue(validateElementIsNotVisible(GamePanelPage.BUTTON_INSTALL_WITH_NO_PLUGIN),
    			"BUTTON_INSTALL_WITH_NO_PLUGIN is still visible. This machine is too slow");
    	driver.executeScript("require(['l4g/assets/app-interface'], function(iface) {"+
        " iface.get().send(" +
    	"'setEnvironment'," +
        "{name: 'live', serviceId:6}" + 
        ");"+
        "});");
    	Assert.assertTrue(validateElementVisible(GamePanelPage.BUTTON_UNKNOWN),
    			"BUTTON_UNKNOWN is not visible after changeGameEnv to " + env);
    }
    
    //TODO Should add compare all message with time, days and date. While compare only the first two words
    public void validatePremiumStatus(String locator, String keyExpected) {
        log.info("validate premium status is " + keyExpected);
        By by = locatorFactory(locator);
        waitForElementPresent(by);

        String status = getText(by);
        String[] word = status.split(" ");

        String expected = TextProvider.translator(keyExpected, language);
        String[] expectedWord = expected.split(" ");

        Assert.assertEquals(expectedWord[0], word[0]);
        Assert.assertEquals(expectedWord[1], word[1]);

    }

    //TODO Should add compare all message with time, date and days.
    //Remind: information isn't the same for Russian and English languages
    public void validateBan(String locator, String keyExpected) {
        log.info("validate ban status  is " + keyExpected);
        By by = locatorFactory(locator);
        waitForElementPresent(by);

        String status = getText(by);
        String[] word = status.split(" ");

        String expected = TextProvider.translator(keyExpected, language);
        String[] expectedWord = expected.split(" ");

        if (language instanceof English) {
            Assert.assertEquals(expectedWord[0], word[0]);
            Assert.assertEquals(expectedWord[1], word[1]);
            Assert.assertEquals(expectedWord[2], word[2]);
            Assert.assertEquals(expectedWord[3], word[3]);
        } else if (language instanceof Russian) {
            Assert.assertEquals(expectedWord[0], word[0]);
            Assert.assertEquals(expectedWord[1], word[1]);
            Assert.assertEquals(expectedWord[2], word[2]);
        }
    }

    public void waitToPlayGame(boolean toInstall) {
        log.info("Waiting to play game and install it " + toInstall);
        waitForElementPresent(locatorFactory(BUTTON_UNKNOWN));
    	if(!isVisible(BUTTON_UNKNOWN))
    		changeGameEnv("live");
        Assert.assertTrue(validateElementIsNotVisible(BUTTON_INSTALL_WITH_NO_PLUGIN), 
        		"There is no plugin or machine is too slow. BUTTON_INSTALL_WITH_NO_PLUGIN is visible");
        String idOfButtonContainer=findElement(BUTTON_UNKNOWN).getAttribute("id");
        log.info("the button in gp = " + idOfButtonContainer);
        if (BUTTON_INSTALL.contains(idOfButtonContainer)) {
            if (toInstall) {
                click(BUTTON_UNKNOWN);
                PopupDownload wd = new PopupDownload();
                wd.validateElementPresent(PopupDownload.GAME_DOWNLOAD_FORM);
                wd.validateElementPresent(PopupDownload.CHECKBOX_AGREE_LICENSE);
                wd.click(PopupDownload.CHECKBOX_AGREE_LICENSE);
                wd.click(PopupDownload.BUTTON_START_INSTALLATION);
                validateElementPresent(PROGRESS_BAR);
                waitForElementPresentForSec(BUTTON_BUTTON_PLAY_OR_PLAY_WITH_PASSWORD, 5000);
                clickIfPresent(LINK_INSTALL_SUCCESS);
            } else {
               throw new TestException("Game (lugin) is not installed");
            }
        }
        else if (BUTTON_UPDATE.contains(idOfButtonContainer)) {
            click(BUTTON_UPDATE);
            waitForElementPresentForSec(BUTTON_BUTTON_PLAY_OR_PLAY_WITH_PASSWORD, 600);
        }

    }

    public void waitToPlayHiddenGame(boolean toInstall) {
        log.info("Waiting to play game and install it " + toInstall);
        waitForElementPresent(locatorFactory(BUTTON_UNKNOWN));
        Assert.assertTrue(validateElementIsNotVisible(BUTTON_INSTALL_WITH_NO_PLUGIN), 
        		"There is no plugin or machine is too slow. BUTTON_INSTALL_WITH_NO_PLUGIN is visible");
        String idOfButtonContainer=findElement(BUTTON_UNKNOWN).getAttribute("id");
        log.info("the button in gp = " + idOfButtonContainer);
        if (BUTTON_INSTALL.contains(idOfButtonContainer)) {
            if (toInstall) {
                click(BUTTON_UNKNOWN);
                PopupDownload wd = new PopupDownload();
                wd.validateElementPresent(PopupDownload.GAME_DOWNLOAD_FORM);
                wd.validateElementPresent(PopupDownload.CHECKBOX_AGREE_LICENSE);
                wd.click(PopupDownload.CHECKBOX_AGREE_LICENSE);
                wd.click(PopupDownload.BUTTON_START_INSTALLATION);
                validateElementPresent(PROGRESS_BAR);
                waitForElementPresentForSec(BUTTON_BUTTON_PLAY_OR_PLAY_WITH_PASSWORD, 5000);
                clickIfPresent(LINK_INSTALL_SUCCESS);
            } else {
               throw new TestException("Game (lugin) is not installed");
            }
        }
        else if (BUTTON_UPDATE.contains(idOfButtonContainer)) {
            click(BUTTON_UPDATE);
            waitForElementPresentForSec(BUTTON_BUTTON_PLAY_OR_PLAY_WITH_PASSWORD, 600);
        }

    }
    
    public  void clickPopupInstall() {
        log.info("Click Begin Installing");
        PopupInstallGame pig = new PopupInstallGame();
        Assert.assertTrue(pig.validateElementVisible(PopupInstallGame.FORM_INSTALL_GAME), "The form 'BEGIN_INSTALL' is not visible on the panel 'play'");
        Assert.assertTrue(pig.validateElementVisible(PopupInstallGame.BUTTON_BEGIN_INSTALL), "The button 'BUTTON_BEGIN_INSTALL' is not visible after clicking Install on the panel 'Play'");
        pig.click(PopupInstallGame.CHECK_AGREE_LICENCE);
        pig.click(PopupInstallGame.BUTTON_BEGIN_INSTALL);
        Assert.assertTrue(pig.validateElementIsNotVisible(PopupInstallGame.FORM_INSTALL_GAME), "The form 'FORM_INSTALL_GAME' is still visible on the panel 'play'");
    }

    public  void clickPopupIfItPresent(Game game) {
        if (GameData.getErrorCode(game).equals("8")) {
            log.info("Click Accept and PLay");
            PopupFinalFormalities pff = new PopupFinalFormalities();
            Assert.assertTrue(pff.validateElementPresent(PopupFinalFormalities.BUTTON_ACCEPT_AND_PLAY), "The button 'BUTTON_ACCEPT_AND_PLAY' is not visible after clicking Play on the panel 'Play'");
            pff.click(PopupFinalFormalities.BUTTON_ACCEPT_AND_PLAY);
            Assert.assertTrue(pff.validateElementIsNotVisible(PopupFinalFormalities.IFRAME_FINAL_FORMALITIES), "The form 'IFRAME_FINAL_FORMALITIES' is still visible after clicking 'Accept and Play' on The form 'IFRAME_FINAL_FORMALITIES'");
        }

    }

    public  void clickLicenceIfItPresent(Game game) {
        if (GameData.getServiceAcc(game) == null || GameData.getServiceAcc(game).equals("0"))
            try {
                log.info("Click Agree with license");
                GamePanelPage gpp = new GamePanelPage(game);
                gpp.validateElementPresent(PopupFinalFormalities.BUTTON_ACCEPT_AND_PLAY);
                gpp.click(PopupFinalFormalities.BUTTON_ACCEPT_AND_PLAY);
            } catch (Exception e) {

            }
    }

    public void fillCbtForm(String nameFileWithCodes, String environment) throws IOException {
        log.info("fillCbtForm started with code from = " + nameFileWithCodes);
        Assert.assertTrue(validateElementVisible(FORM_CBT), "CBT Code Activation Form is not visible");
        //should be rewritten user.env
        int k = 0;
        do {
            String pinCode = getCodeWithPath(pathToCode + environment + "_" + nameFileWithCodes);
            type(FIELD_CELL_1, pinCode.substring(0, 4));
            type(FIELD_CELL_2, pinCode.substring(4, 8));
            type(FIELD_CELL_3, pinCode.substring(8, 12));
            type(FIELD_CELL_4, pinCode.substring(12, pinCode.length()));
            Captcha captcha=new Captcha();
            captcha.typeCaptcha("987654");
            click(BUTTON_ACTIVATE);
            validateElementVisible(SPINNER_ON_BUTTON);
            validateElementIsNotVisible(SPINNER_ON_BUTTON);
            k++;
        } while (isVisible(MESSAGE_FAIL_ACTIVATION) && k < 50);
    }

    public void fillSetPasswordForm(String newPassword, String masterPassword) {
        log.info("fillSetPasswordForm started with new password = " + newPassword);
        Assert.assertTrue(validateElementVisible(LABEL_LOCK_ON_THE_BUTTON),
                "Form 'set game password' is not Visible");
        type(FIELD_NEW_PASSWORD, newPassword);
        Captcha captcha=new Captcha();
        captcha.typeCaptchaIfPresent("987654");
        click(BUTTON_SAVE_PASSWORD);
        type(FIELD_SET_PASSWORD_MASTER, masterPassword);
        click(BUTTON_SUBMIT_MASTER_PASSWORD);
    }

    public void fillChangePasswordForm(String oldPassword,
                                       String newPassword, String masterPassword) {
        log.info("fillChangePasswordForm started with new password = " + newPassword);
        Assert.assertTrue(validateElementVisible(LABEL_LOCK_ON_THE_BUTTON),
                "Form 'change game password' is not Visible");
        type(FIELD_OLD_PASSWORD, oldPassword);
        type(FIELD_NEW_PASSWORD, newPassword);
        Captcha captcha=new Captcha();
        captcha.typeCaptchaIfPresent("987654");
        click(BUTTON_SAVE_CHANGES);
        type(FIELD_SET_PASSWORD_MASTER, masterPassword);
        click(BUTTON_SUBMIT_MASTER_PASSWORD);

    }

    public void fillDeletePasswordForm(String servicePassword, String masterPassword) {
        log.info("fillDeletePasswordForm started");
        Assert.assertTrue(validateElementVisible(LABEL_LOCK_ON_THE_BUTTON),
                "Form 'change game password' is not Visible");
        type(FIELD_OLD_PASSWORD, servicePassword);
        Captcha captcha=new Captcha();
        captcha.typeCaptchaIfPresent("987654");
        click(BUTTON_DELETE_PASSWORD_POPUP);
        type(FIELD_SET_PASSWORD_MASTER, masterPassword);
        click(BUTTON_SUBMIT_MASTER_PASSWORD);
    }

    public void fillRecoverSendForm(String masterPassword) {
        log.info("fillRecoverFirstForm started");
        //you must delete this after fixing bug with Button Password
        try {
            Assert.assertTrue(validateElementVisible(LABEL_LOCK_ON_THE_BUTTON),
                    "Form 'recover game password (send form)' is not Visible");
        } catch (AssertionError e) {
            refreshPage();
        }
        Captcha captcha=new Captcha();
        captcha.typeCaptchaIfPresent("987654");
        click(BUTTON_SEND);
        type(FIELD_RECOVER_PASSWORD_MASTER, masterPassword);
        click(BUTTON_SUBMIT_MASTER_PASSWORD);
    }

    public void fillRecoverFormSms(String codeFromSMS,
                                   String newServicePassword, String masterPassword) {
        log.info("fillRecoverFormSms started with new password " + newServicePassword
                + " and code from sms =  " + codeFromSMS);
        Assert.assertTrue(validateElementVisible(LABEL_LOCK_ON_THE_BUTTON),
                "Form 'recover game password (with sms code)' is not Visible");
        type(FIELD_RECOVER_SMS_CODE, codeFromSMS);
        type(FIELD_RECOVER_NEW_PASSWORD, newServicePassword);
        Captcha captcha=new Captcha();
        captcha.typeCaptchaIfPresent("987654");
        click(BUTTON_RECOVER_ACCEPT);
        type(FIELD_RECOVER_PASSWORD_MASTER, masterPassword);
        click(BUTTON_SUBMIT_MASTER_PASSWORD);
    }

    public void fillRecoverFormMail(String newServicePassword,
                                    String masterPassword) {
        log.info("fillRecoverFormMail started with new password " + newServicePassword);
        Assert.assertTrue(validateElementVisible(LABEL_LOCK_ON_THE_BUTTON),
                "Form 'recover game password (with sms code)' is not Visible");
        type(FIELD_RECOVER_NEW_PASSWORD, newServicePassword);
        Captcha captcha=new Captcha();
        captcha.typeCaptchaIfPresent("987654");
        click(BUTTON_RECOVER_ACCEPT);
        type(FIELD_RECOVER_PASSWORD_MASTER, masterPassword);
        click(BUTTON_SUBMIT_MASTER_PASSWORD);

    }

    public String getMinPrice() {
        Assert.assertTrue(validateElementVisible(GamePanelPage.FIELD_PREMIUM_MIN_PRICE), "Div with min price is not visible");
        PATTERN = Pattern.compile("\\d+");
        MATCHER = PATTERN.matcher(getText(GamePanelPage.FIELD_PREMIUM_MIN_PRICE));
        MATCHER.find();
        return MATCHER.group(0);
    }

}



