package ru.inn.autotests.webdriver.composite.pages;

import ru.inn.autotests.webdriver.composite.IPage;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.composite.common.StatusHelper;


public class YandexMainPage extends StatusHelper implements IPage
{
	private String url = "/";

    public final static String SEARCH_FIELD = "id=text";
   // public final static String SEARCH_BUTTON = "//button[@type='submit']";
    
  
	public String getPageUrl() 
	{
		return url;
	}
	
	/**
	 * 	input value into search field and search
	 */
	public void search(String value)
	{
		 type(SEARCH_FIELD, value);
		 submit(SEARCH_FIELD);
	}	
}
