package ru.inn.autotests.webdriver.composite.pages;

import org.testng.Assert;
import ru.inn.autotests.webdriver.composite.IPage;
import ru.inn.autotests.webdriver.composite.common.Captcha;
import ru.inn.autotests.webdriver.composite.common.StatusHelper;
import ru.inn.autotests.webdriver.toolkit.PlatformUtils;

import java.util.ArrayList;
import java.util.List;

public class MainPage extends StatusHelper implements IPage {

    private String url = "/";
    public final static String BUTTON_REINSTALL_APP = "xpath=//footer//button";
    public final static String VIDEO_SPAN="css=span.bMainPageTeaserBlock__eTitle.bMainPageTeaserBlock__eTitle__mDecorate_dotted";
    public final static String IFRAME_VIDEO="xpath=//iframe[contains(@src,'//www.youtube.com/')]";
    public final static String DIV_DESKTOP_GAMES = "id=platform_mmo_trigger";
    public final static String DIV_MOBILE_GAMES = "id=platform_mobile_trigger";
    public final static String DIV_HITS_GAMES = "id=platform_hits_trigger";
    public final static String DIV_TILE_GAME = "css=a[data-link-service-id='%s']";

    public final static String SINGLE_PLAYER_PRICE_NO_DISCOUNT = "id=div-sumWithoutDiscount";
    public final static String GTA5 = "xpath=(//a[contains(@href, '/gta5/')])[3]";
    public final static String FARCRY4 = "xpath=(//a[contains(@href, '/far-cry-4/')])[3]";
    public final static String ASSASSINS = "xpath=(//a[contains(@href, '/assassins-creed-unity/')])[3]";
    public final static String CIVIL = "xpath=(//a[contains(@href, '/civilization-beyond-earth/')])[3]";
    public final static String COD = "xpath=(//a[contains(@href, '/call-of-duty-advanced-warface/')])[3]";
    public final static String BORDERLANDS = "xpath=(//a[contains(@href, '/borderlands-pre-sequel/')])[3]";
    public final static String DRAGONAGE = "xpath=(//a[contains(@href, '/dragon-age-inquisition/')])[3]";
    public final static String SIMS4 = "xpath=(//a[contains(@href, '/sims4/')])[3]";

    private List<GameCell> cells = new ArrayList<GameCell>();
    public final static String GAME_NAME = "id=";


    /**
     * ngadiyak
     * auth user
     * 07.02.2015
     * @param email
     * @param password
     */
    public void auth(String email, String password)
    {
        log.info("login with email " + email + " and with pass " + password);
        Assert.assertTrue(validateElementVisible(LoginPage.POPUP_LOGIN_FIELD), "Pop-up 'Login and Registration' is not visible");
        type(LoginPage.LOGIN_FIELD , email);
        type(LoginPage.PASSWORD_FIELD, password);
        click(LoginPage.BUTTON_SIGN_UP);
    }

    /**
     *
     * @return
     */
    public String getSinglePlayerGamePrice()
    {
        String text = getText(SINGLE_PLAYER_PRICE_NO_DISCOUNT);
        return text.substring(0, text.indexOf(' '));
    }




    @Override
    public String getPageUrl() {
        return url;
    }

    /**
     * Sets game cells
     * @param cells
     */
    public void setGameCells(List<GameCell> cells){
        this.cells.addAll(cells);
    }

    /**
     * Returns required GameCell
     * @param index
     */
    public GameCell getGameCell(int index){
        return this.cells.get(index);
    }


    /**
     * Returns all GameCells
     */
    public List<GameCell> getGameCells(){
        return cells;
    }
}