package ru.inn.autotests.webdriver.composite.pages;

import ru.inn.autotests.webdriver.common.language.Language;
import ru.inn.autotests.webdriver.composite.IPage;
import ru.inn.autotests.webdriver.composite.OperationsHelper;

public class EventsPage extends OperationsHelper implements IPage {

    String pageUrl = "/events/";

    
    public static String LABEL_FIRST_EVENT = "css=div.bEventsLogItem:nth-of-type(1)";
    public static String DIV_EVENT_NAVIGATION_PANEL = "id=EventsNavWidget";
  

    public EventsPage(Language lang) {
        language = lang;
    }

    public String getFirstEvent() {
        return getText(LABEL_FIRST_EVENT);
    }

    /**
     * Returns the link on the current page
     */
    @Override
    public String getPageUrl() {
        return pageUrl;
    }


}
