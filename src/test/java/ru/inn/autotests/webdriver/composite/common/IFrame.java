package ru.inn.autotests.webdriver.composite.common;

import ru.inn.autotests.webdriver.composite.OperationsHelper;

/**
 * User: pavel.popov
 */
public class IFrame extends OperationsHelper {
    private String iFrame = "A4GFrame";

    @Override
    public boolean validateElementPresent(String locator) {
        //log.info(driver.getWindowHandles().size());
    	switchTo(iFrame);
        super.validateElementPresent(locator);
        switchToMainContent();
		return true;
    }

    @Override
    public void click(String locator) {
        switchTo(iFrame);
        super.click(locator);
        switchToMainContent();
    }

}
