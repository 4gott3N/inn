package ru.inn.autotests.webdriver.composite.popups;

import ru.inn.autotests.webdriver.composite.IPage;
import ru.inn.autotests.webdriver.composite.common.StatusHelper;

public class PopupInstallGame extends StatusHelper implements IPage {

    private String pageUrl = "";

    public static final String FIELD_FOLDER_PATH = "id=FolderPath";
    public static final String CHECK_ADD_SHORTCUTS = "id=chbCreateShortcuts";
    public static final String CHECK_AGREE_LICENCE = "id=chbAcceptLicence";
    public static final String BUTTON_BEGIN_INSTALL = "id=btnBeginInstall";
    public static final String FORM_INSTALL_GAME = "id=jsOverlayContainer";
    
    public PopupInstallGame() {
    }

    public String getPageUrl(){
        return pageUrl;
    }

}
