package ru.inn.autotests.webdriver.composite.pages;

import ru.inn.autotests.webdriver.common.language.Language;
import ru.inn.autotests.webdriver.composite.IPage;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.composite.common.StatusHelper;
import ru.inn.autotests.webdriver.games.Game;
import ru.inn.autotests.webdriver.tests.AbstractTest;

/**
 * @author evgenij.tkachenko
 */
public class BuySerialCodePage extends OperationsHelper implements IPage {

    //private String pageUrl = "/sims4/";
    
    //SIMS
    public static final String BUTTON_BUY = "id=button-buyEdition";
    public static final String FIELD_DISCOUNT_CODE = "id=field-discountCode";
    public static final String BUTTON_SUBMIT_DISCOUNT_CODE = "id=button-enterCode";
    public static final String DIV_SUM_WITHOUT_DISCOUNT = "id=div-sumWithoutDiscount";
    public static final String DIV_SUM_WITH_DISCOUNT = "id=div-sumWithDiscount";
    public static final String LINK_ENTER_DISCOUNT_CODE = "id=link-enterPromoCode";
    
    //YOUR CODE
    public static final String BUTTON_INSTALL_AION = "id=button-installAion";
    public static final String BUTTON_INSTALL_APB = "id=button-installApb";
    public static final String FIELD_SERIAL_CODE = "id=field-serialCode";
    public static final String DIV_GIFT_CODE = "id=field-pinCode";
    public static final String BUTTON_BUY_ANOTHER_ONE = "id=button-buyAnotherOne";
    public static final String DIV_HOW_TO_GET = "id=div-howToGet";
    
    //preorder
    public static final String DIV_USER_MAIL_PREPRDER = "id=div-userMail";
    
    public BuySerialCodePage() {
    }

	@Override
	public String getPageUrl() {
		return "";
	}



    
    
}
