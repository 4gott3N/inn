package ru.inn.autotests.webdriver.composite;

import ru.inn.autotests.webdriver.toolkit.WebDriverController;


/**
 * @author Aleksey Niss
 * @overview Pages interface
 */
public interface IPage {

    public String getPageUrl();

    public void setDriver(WebDriverController webDriverController);

}
