package ru.inn.autotests.webdriver.composite.popups;

import ru.inn.autotests.webdriver.composite.IPage;
import ru.inn.autotests.webdriver.composite.OperationsHelper;

public class PopupChangePassword extends OperationsHelper implements IPage {

    public static String INPUT_CURRENT_PASSWORD = "xpath=(//*[@name='ChangeServicePasswordForm[currentPassword]'])[2]";

    public static String INPUT_NEW_PASSWORD = "xpath=(//*[@name='ChangeServicePasswordForm[newPassword]'])[2]";

    public static String INPUT_TYPE_CAPTCHA = "name=ChangeServicePasswordForm[captcha]";
    public static String BUTTON_CHANGE_PASSWORD = "id=jsSetGamePasswordWidget-btnChangePassword";

    public static String INPUT_USER_PASSWORD = "name=ChangeServicePasswordForm[masterPassword]";
    public static String INPUT_ICON_ENTER = "xpath=//input[@class='bBigButton__eEnterKeyIcon']";

    @Override
    public String getPageUrl() {
        return null;
    }

}
