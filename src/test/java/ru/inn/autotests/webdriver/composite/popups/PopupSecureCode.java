package ru.inn.autotests.webdriver.composite.popups;

import ru.inn.autotests.webdriver.composite.IPage;
import ru.inn.autotests.webdriver.composite.common.StatusHelper;
import ru.inn.autotests.webdriver.toolkit.WebDriverController;

public class PopupSecureCode extends StatusHelper implements IPage {
    private String pageUrl;

    //inputs
    public static final String INPUT_CODE = "id=jsAppBindWidget__ConfirmCode";
    public static final String INPUT_CAPTCHA = "id=jsAppBindWidget__Captcha";
    public static final String CAPTCHA = "xpath=//input[contains(@name, 'captcha') and not(@type='hidden')]";

    //BUTTONS
    public static final String BUTTON_REMEMBER_ME = "id=jsAppBindWidget__RememberComputer";
    public static final String BUTTON_CONFIRM_AND_PLAY = "id=jsAppBindWidget__ConfirmCode__Button";
    public static final String CLOSE_FORM = "xpath=//i[@class='bUIPopup__eCloseIcon']";

    //LINKS
    public static final String LINK_SEND_AGAIN = "css=.bSetContacts__eLink";

    //FORMS
    public static final String CAPTCHA_FIELD = "xpath=//a[@class='bUICaptcha__eLinkImage']";
    public static final String FORM_SECURE_CODE = "id=jsAppBindWidget__ConfirmCode";
    public static final String DIV_MESSAGE_ERROR = "css=.bUIErrorMessage__mDisplay_Block";

    //DIV
    public static final String ERROR_MESSAGE = "xpath=//div[@class='bUIErrorMessage bUIErrorMessage__mDisplay_Block bUIErrorMessage__mVisible_True ']";




    @Override
    public String getPageUrl() {
        return pageUrl;
    }

    @Override
    public void setDriver(WebDriverController webDriverController) {
    }


}
