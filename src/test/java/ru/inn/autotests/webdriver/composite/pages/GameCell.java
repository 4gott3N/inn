package ru.inn.autotests.webdriver.composite.pages;

import org.openqa.selenium.By;
import org.testng.Assert;
import ru.inn.autotests.webdriver.composite.common.StatusHelper;
import ru.inn.autotests.webdriver.games.Game;
import ru.inn.autotests.webdriver.toolkit.TextProvider;

/**
 * @author Aleksey Niss
 * 
 * <p> Used for interacting with main page cell blocks. This code was moved from MainPage to separate class</p>
 */
//TODO Getters and setters...
public class GameCell extends StatusHelper{

    public String gameCell = "xpath=//div[@data-service-id='%s']";
    public String gameCellLink = "xpath=//a[@data-link-service-id='%s']";

    public String gameCellProgress = "xpath=//div[@id='bGamesPromoListItem__eProgress__%s']";
    public String linkGameCellWithoutProgress = "xpath=//div[@class='bMainPage']//a[@data-link-service-id='%s']";

    public String gameTitle = "";
    public String gameStatus = "";
    public String gameStatusOnPause = "";
    public String gameProgress = "";
    public String gameProgressUnpacking = "";
    public String gameInstallLocator="id=bGamesPromoListItem__eControlsInstallButton__%s";
    public String gameStatusDownloaded = "";
    public String gameStatusDownloadedSize = "";
    public String gameStatusDownloadedSpeed = "";
    public String gameStatusDownloadedApp = "";


    public String gameDownloadIOSOld = "xpath=//a[@class='pofo-storeButtons--button pofo-storeButtons--button-appstore']";
    public String gameDownloadAndroidOld = "xpath=//a[@class='pofo-storeButtons--button pofo-storeButtons--button-googleplay']";


    public final static String BUTTON_OPEN_POPUP_VIDEO = "css=div.page-header__play";
    public final static String BUTTON_CLOSE_POPUP_VIDEO = "css=i.chaos-fighters__video-close";


    /**
     * Create instance of game cell
     * @param game
     */
    public GameCell(Game game){
        linkGameCellWithoutProgress = String.format(linkGameCellWithoutProgress, game.getServiceId());
        gameCellProgress = String.format(gameCellProgress, game.getServiceId());
        gameCell = String.format(gameCell, game.getServiceId());
        gameCellLink = String.format(gameCellLink, game.getServiceId());
        gameStatusDownloaded = gameCellProgress + "//span[@class='bGamesPromoListItemProgress__eTrackStatusDownloaded']";
        gameStatusDownloadedSize = gameCellProgress + "//span[@class='bGamesPromoListItemProgress__eTrackStatusSize']";
        gameStatusDownloadedSpeed = gameCellProgress + "//span[@class='bGamesPromoListItemProgress__eTrackStatusSpeed']";
        gameProgress = gameCellProgress + "//*[@class='bUIProgressBar__eInner']";
        gameStatus = gameCellProgress + "//*[@class='bGamesPromoListItemProgress__eStatusText']";
        gameTitle = gameCellProgress + "//*[@class='bGamesPromoListItemProgress__eTitle']";
        gameStatusOnPause = gameCellProgress + "//*[@class='bGamesPromoListItemProgress__eStatus']";
        gameProgressUnpacking = gameCellProgress + "//*[@class='bGamesPromoListItemProgress__eTrackStatusUnpacking']";
        gameStatusDownloadedApp = gameCell + "//button";
        System.out.println("Cell created: game = " + game.getServiceId() + ", resulting locator = " + gameCell);
    }

    /**
     * Validate 'on pause' status in brackets in Status Item
     * @param locator
     */
    public void validateOnPauseStatus(final String locator, final String keyText){
        By by = locatorFactory(locator);
        waitForElementPresent(by);
        String pauseState = getText(by);
        String inBrackets = pauseState.split("\\(")[1].split("\\)")[0];

        Assert.assertEquals(TextProvider.translator(keyText, language), inBrackets);

    }

    /**
     * Click on the download button in the GameCell. It uses hoverOn from Interaction API (WebDriver).
     */
    public void clickOnDownloadButton(Game game) {

        hoverOn(gameCell); //just for fun, all elements are already rendered and present at this stage @aleksey.niss
        getElement(String.format(gameInstallLocator,game.getServiceId())).click();

    }
}
