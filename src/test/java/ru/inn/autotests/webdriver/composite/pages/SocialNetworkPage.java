package ru.inn.autotests.webdriver.composite.pages;


import org.testng.Assert;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.toolkit.LocalDriverManager;

/**
 * Created with IntelliJ IDEA.
 * User: Sergey.Kashapov
 * Date: 19.06.13
 * Time: 16:04
 * To change this template use File | Settings | File Templates.
 */
public class SocialNetworkPage extends OperationsHelper {

    public static final String FB_LOGIN_FIELD = "id=email";
    public static final String FB_PASSWORD_FIELD = "id=pass";
    public static final String FB_LOGIN_BUTTON = "name=login";
    public static final String VK_LOGIN_FIELD = "name=email";
    public static final String VK_PASSWORD_FIELD = "name=pass";
    public static final String VK_LOGIN_BUTTON = "id=install_allow";
    public static final String VK_REGISTER_FORM="xpath=//*[@data-ft='ft_countrySelectRegistrationSocial']";
    public static final String VK_REGISTER_FORM_EMAIL_FIELD="xpath=//*[@data-ft='ft_emailInputRegistrationSocial']";
    public static final String VK_REGISTER_FORM_CONFIRM_BUTTON="xpath=//button[@data-ft='ft_submitButtonRegistrationSocial']";
    public static final String DEFAULT_FB_AVATAR = "css=div[class*='eUserPic']";
    public static final String DEFAULT_VK_AVATAR = "xpath=//*[contains(@style, 'camera_100.gif')]";
    public static final String FORM_SOCIAL = "id=bUIPopup__eLayout";
    public static final String CHECKBOX_LICENCE_AGREE = "id=jsAuthPopupWidgetLayout__eAccept4gameLicense";
    public static final String FIELD_VK_EMAIL="xpath=//*[@data-ft='ft_emailInputRegistrationSocial']";
    public static final String BUTTON_SOCIAL_REGISTRATION="xpath=//*[@data-ft='ft_submitButtonRegistrationSocial']";
    public static final String LINK_BIND_ACCOUNT="xpath=//*[@data-ft='ft_linkAccountLinkRegistrationSocial']";
    public static final String FIELD_EMAIL_OR_LOGIN="xpath=//*[@name='AuthForm[loginOrEmail]']";
    public static final String FIELD_PASSWORD="xpath=//input[@type='password']";
    public static final String BUTTON_BIND_AND_ENTER="xpath=//*[@data-ft='ft_submitButtonLinkSocial']";

    String mainWindow="";

    public void clickToOpenPopup(String locator) {
        log.info("Open  popup");
        validateElementPresent(locator);
        mainWindow = LocalDriverManager.getDriverController().getWindowHandle();
        click(locator);
        selectOtherWindow();
    }

    public void loginInFaceBook(String email, String password){
    	log.info("login in FaceBook frame with params " + email + " , " + password);
        Assert.assertTrue(validateElementVisible(FB_LOGIN_FIELD),"FB popup is not visible");
        type(FB_LOGIN_FIELD, email);
        type(FB_PASSWORD_FIELD, password);
        click(FB_LOGIN_BUTTON);
        log.info("click login in FB");
        selectWindow(mainWindow);
        //Assert.assertTrue(validateElementVisible(UserBarPage.ID_LOGIN_NAME_FIELD), "The user is not authorized");
    }


    public void loginInVk(String email, String password){
        log.info("login in VK frame with params " + email + " , " + password);
        Assert.assertTrue( validateElementVisible(VK_LOGIN_FIELD),"VK popup is not visible");
        type(VK_LOGIN_FIELD, email);
        type(VK_PASSWORD_FIELD, password);
        click(VK_LOGIN_BUTTON);
        log.info("click login in VK");
        selectWindow(mainWindow);
        //Assert.assertTrue(validateElementVisible(UserBarPage.ID_LOGIN_NAME_FIELD), "The user is not authorized");
    }

    public void fillSocialRegisterFormVk(String email){
        log.info("Fill register form after VK");
         validateElementPresent(VK_REGISTER_FORM);
         type(VK_REGISTER_FORM_EMAIL_FIELD, email);
         click(VK_REGISTER_FORM_CONFIRM_BUTTON);
    }

    public void deleteVkCookies(){
        log.info("deleting Vk cookies started ");
        openUrl("https://vk.com");
        LocalDriverManager.getDriverController().deleteAllCookies();

    }
    public void deleteFbCookies(){
        log.info("deleting Fb cookies started ");
        openUrl("https://www.facebook.com/");
        LocalDriverManager.getDriverController().deleteAllCookies();

    }

}
