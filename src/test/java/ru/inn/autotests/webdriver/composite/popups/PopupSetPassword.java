package ru.inn.autotests.webdriver.composite.popups;

import ru.inn.autotests.webdriver.composite.IPage;
import ru.inn.autotests.webdriver.composite.OperationsHelper;

public class PopupSetPassword extends OperationsHelper implements IPage {
    String pageUrl = "";

    public static String INPUT_PASSWORD = "name=ChangeServicePasswordForm[newPassword]";


    public static String INPUT_USER_PASSWORD = "name=ChangeServicePasswordForm[masterPassword]";
    public static String INPUT_ICON_ENTER = "xpath=//input[@class='bBigButton__eEnterKeyIcon']";

    public static String BUTTON_CREATE_PASSWORD = "id=jsSetGamePasswordWidget-btnCreatePassword";




    @Override
    public String getPageUrl() {
        return pageUrl;
    }
}
