package ru.inn.autotests.webdriver.composite.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.toolkit.LocalDriverManager;
import ru.inn.autotests.webdriver.toolkit.PlatformUtils;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Sergey.Kashapov
 * Date: 29.05.13
 * Time: 17:38
 * To change this template use File | Settings | File Templates.
 */
public class PaymentTerminal extends OperationsHelper {
    public static String FORM_PAYMENT_TERMINAL = "xpath=//*[@data-target='bPaymentTerminal']";
    public static String SANDBOX_PAYPAL = "https://developer.paypal.com/";
    public static String LOGIN_BUTTON_PAYPAL = "css=.ppAccessBtn";
    public static String LOGIN_SANDBOX_PAYPAL_FIELD = "name=email";
    public static String PASSWORD_SANDBOX_PAYPAL_FIELD = "name=password";
    public static String LOGIN_SANDBOX_PAYPAL_BUTTON = "name=_eventId_submit";
    public static String PAYMENT_TERMINAL_LINK = "xpath=//a[@href='?popupWidget=PaymentTerminalWidget']";
    public static String PAYMENT_SELECTOR = "xpath=//li[contains(@class,'%s')]";
    public static String SECTION_SELECTOR = "xpath=//section[contains(@class,'%s')]";
    public static String TYPE_SELECTOR = "xpath=//input[contains(@id,'%s')]";
    public static String QIWI_WALLET_ID = "name=qiwiwalletId";
    public static String FIELD_PHONE_CENTILI = "name=phone";
    public static String BUTTON_SELCTOR = "xpath=//button[contains(@id, '%s')]";
    public static String FIELD_AMOUNT = "name=amount";
    public static String LINK_PAYMENT_3 = "xpath=(//span[@class='bPaymentTerminal__eMenuItemLink'])[3]";
    public static String BUTTON_PAY_DISABLED = "css=button[id*='PaymentPopup__ePayButton'][class*='disabled']";
    public static String MESSAGE_ERROR = "css=div[class='bUIErrorMessage bUIErrorMessage__mDisplay_Block jsPaymentPopup__eAmount__Error']";
    //QIWI CLASS SELECTORS
    public static String QIWI_METHOD = "id=method-QIWI";
    public static String QIWI_PASSWORD_FIELD = "name=password";
    public static String QIWI_BUTTON_TAG = "tag=button";
    public static String QIWI_SUM_WITH_COMISSION = "xpath=//div[@class='providerComm alignLeft']";

    //PAYONLINE LOCATORS
    public static String PAYONLINE_CARD_FORM = "id=miniCardForm_cardNumberA";
    public static String PAYONLINE_CARD_FORM_HEADER = "id=miniCardForm_header_amount";
    public static String PAYONLINE_MASTER_CARD_TYPE = "id=miniCardForm_cardTypeMc";
    public static String PAYONLINE_VISA_TYPE = "id=miniCardForm_cardTypeVisa";
    public static String PAYONLINE_MINICARD_FIELD_A = "id=miniCardForm_cardNumberA";
    public static String PAYONLINE_MINICARD_FIELD_B = "id=miniCardForm_cardNumberB";
    public static String PAYONLINE_MINICARD_FIELD_C = "id=miniCardForm_cardNumberC";
    public static String PAYONLINE_MINICARD_FIELD_D = "id=miniCardForm_cardNumberD";
    public static String PAYONLINE_DROPDOWN_EXPIRE_MONTH = "id=miniCardForm_expDateMonth";
    public static String PAYONLINE_DROPDOWN_EXPIRE_YEAR = "id=miniCardForm_expDateYear";
    public static String PAYONLINE_CARDHOLDER_NAME_FIELD = "id=miniCardForm_cardHolderName";
    public static String PAYONLINE_CARD_BANK_NAME_FIELD = "id=miniCardForm_bankName";
    public static String PAYONLINE_CARD_BANK_CVC_FIELD = "id=miniCardForm_cardCVC";
    public static String PAYONLINE_CARD_BANK_PROCESS = "id=miniCardForm_cmdProcess";
    public static String PAYONLINE_CARD_BANK_COMPLETE = "xpath=//div[@class='pay_compl']/a";


    //YANDEX LIVE LOCATORS
    public static String YANDEX_ACC_LOGIN = "name=login";
    public static String YANDEX_ACC_PASS = "name=passwd";
    public static String YANDEX_CVV = "id=cvv";
    public static String YANDEX_AGREE_CHECKBOX = "id=is-new-offer-signed";
    public static String YANDEX_RETURN = "css=a[class='b-link']";


    //PAYPAL
    public static String PAYPAL_LOGIN_BUTTON = "name=login_button";
    public static String PAYPAL_PANEL_MASK = "id=panelMask";
    public static String PAYPAL_LOGIN_EMAIL = "xpath=//input[@name='login_email']";
    public static String PAYPAL_LOGIN_PASSWORD = "name=login_password";
    public static String PAYPAL_LOGIN_INPUT = "xpath=//input[@value='Log In']";
    public static String PAYPAL_CONTINUE_BUTTON = "xpath=//input[@name='continue']";

    //PAYMENT SAFE CARD LOCATORS
    public static String SAFE_CARD_ROWS_PIN = "xpath=//div[@class='row-0 wrap-pin-row']";
    public static String SAFE_CARD_TEXTBOXES = "xpath=.//input[@type='text']";
    public static String SAFE_CARD_CHECKBOX_AGREE = "xpath=//div[@class='input-wrapper']/span";
    public static String SAFE_CARD_SUBMIT_BUTTON = "xpath=//input[@type='submit']";

    //PAYMENT SCRILL

    public static String SCRILL_HAVE_ACCOUNT = "xpath=//a[@class='have_account']";
    public static String SCRILL_LOGIN_FIELD = "xpath=//input[@name='email']";
    public static String SCRILL_PASS_FIELD = "xpath=//input[@name='password']";
    public static String SCRILL_LOGIN_BUTTON = "id=loginPayNow_button";
    public static String SCRILL_RADIO_BUTTON_BALANCE = "xpath=//input[@value='BALANCE']";
    public static String SCRILL_CONFIRM_BUTTON = "xpath=//button[@value='Confirm']";
    public static String SCRILL_HAS_ACCOUNT_ID = "id=already_has_account";
    public static String SCRILL_CHANGE_ID = "id=change";
    public static String SCRILL_EMAIL = "id=email";
    public static String SCRILL_PASS = "id=password";
    public static String SCRILL_LOGIN = "id=login";
    public static String SCRILL_CONFIRM_AND_PLAY = "id=confirm_and_pay";
    
    //CENTILI
    public static final String SPINNER_ON_BUTTON = "css=button[class*='Loading']";
    public static final String MESSAGE_SMS_SENT = "id=mobileMsg_sent";
    public static final String MESSAGE_SUCCESS = "id=mobileMsg_success";
    public static final String MESSAGE_WRONG_NUMBER = "id=mobileMsg_success";
    public static final String LABEL_CENTILI_SUM = "css=span[class='mobileMsg_amount']";
    public static final String DIV_TILE_OPERATOR_MTS = "css=a[data-operator='Mts']";

    //Payment Through Two Pay Safe Cards
    public static String TWO_SAFE_ENTER_PIN = "id=pinForm:show-pins";
    public static String TWO_SAFE_ROWS_PIN = "xpath=//div[@class='row-1 wrap-pin-row ']";
    public static String TWO_SAFE_CHECKBOX_AGREE = "xpath=//div[@class='input-wrapper']/span";
    public static String TWO_SAFE_SUBMIT_BUTTON = "xpath=//input[@type='submit']";


    //MOCK DEV FOR MONEY
    public static String MOCK_DEV_MONEY_USER_ID = "id=PayForm_userId";
    public static String MOCK_DEV_MONEY_CURRENCY_QTY = "name=PayForm[currencyQty]";
    public static String BUTTON_MOCK_DEV_MONEY_SUBMIT = "name=submit1";

    //MOCK DEV FOR CODES
    public static String MOCK_DEV_PIN_USER_ID = "id=BuyForm_userId";
    public static String MOCK_DEV_PIN_CURRENCY_QTY = "name=BuyForm[currencyQty]";
    public static String BUTTON_MOCK_PIN_DEV_SUBMIT = "name=submit2";


    interface Payment {

        public void addFunds();
    }

    public class PaymentQiwiWallet implements Payment {

        public PaymentQiwiWallet() {

        }

        @Override
        public void addFunds() {
            log.info("add funds through qiwi wallet");
            Assert.assertTrue(validateElementIsNotVisible(FORM_PAYMENT_TERMINAL),"U r still in 4 game site, but u have to be in PaymentQiwiWallet");
            String qiwiPassword = PlatformUtils.getStageParameters("qiwiPassword");
            click(QIWI_METHOD);
            click(QIWI_BUTTON_TAG);
            type(QIWI_PASSWORD_FIELD, qiwiPassword);
            log.info("fill password field");
            click(QIWI_BUTTON_TAG);
            validateElementPresent(QIWI_SUM_WITH_COMISSION);
            click(QIWI_BUTTON_TAG);
            log.info("replenish by qiwi and return to 4game");
        }

    }


    public class PaymentPayOnline implements Payment {

        public PaymentPayOnline() {

        }

        @Override
        public void addFunds() {
            log.info("add funds through payonline");
            Assert.assertTrue(validateElementIsNotVisible(FORM_PAYMENT_TERMINAL),"U r still in 4 game site, but u have to be in PaymentPayOnline");
            String cardNumber = PlatformUtils.getStageParameters("psPayOnlineCard");
            int countDigitsInBlock = 4;
            int countBlocks = 4;
            String[] numberOfCard = new String[countBlocks];
            for (int i = 0; i < 4; i++) {
                numberOfCard[i] = cardNumber.substring(i * countDigitsInBlock, i * countDigitsInBlock + countBlocks);
            }
            String cardHolderName = PlatformUtils.getStageParameters("psPayOnlineName");
            String[] expDate = PlatformUtils.getStageParameters("psPayOnlineExpDate").split(","); //month, year
            String cardCVC = PlatformUtils.getStageParameters("psPayOnlineCVC");
            String amount = getText(PAYONLINE_CARD_FORM_HEADER);
            int end = amount.indexOf(",");
            log.debug("indexof ',' " + end);
            amount = amount.substring(0, end);
            if (PlatformUtils.getStageParameters("psPayOnlineCardType").equals("mastercard")) {
                click(PAYONLINE_MASTER_CARD_TYPE);
            } else {
                click(PAYONLINE_VISA_TYPE);
            }
            type(PAYONLINE_MINICARD_FIELD_A, numberOfCard[0]);
            type(PAYONLINE_MINICARD_FIELD_B, numberOfCard[1]);
            type(PAYONLINE_MINICARD_FIELD_C, numberOfCard[2]);
            type(PAYONLINE_MINICARD_FIELD_D, numberOfCard[3]);
            log.info("Fill number card");
            click(PAYONLINE_CARDHOLDER_NAME_FIELD);
            type(PAYONLINE_CARDHOLDER_NAME_FIELD, cardHolderName);
            log.info("Fill cardHolder name");
            selectValueInDropDown(PAYONLINE_DROPDOWN_EXPIRE_MONTH, expDate[0]);
            selectValueInDropDown(PAYONLINE_DROPDOWN_EXPIRE_YEAR, expDate[1]);
            log.info("select expire date");
            type(PAYONLINE_CARD_BANK_NAME_FIELD, "Payment center");
            type(PAYONLINE_CARD_BANK_CVC_FIELD, cardCVC);
            click(PAYONLINE_CARD_BANK_PROCESS);
            click(PAYONLINE_CARD_BANK_COMPLETE);
            log.info("click complete replenish");
        }

    }


    public class PaymentYandexLive implements Payment {

        public PaymentYandexLive() {

        }

        @Override
        public void addFunds() {
            log.info("add funds through yandex money LIVE");
            Assert.assertTrue(validateElementIsNotVisible(FORM_PAYMENT_TERMINAL),"U r still in 4 game site, but u have to be in PaymentYandexLive");
            String accountYandex = PlatformUtils.getStageParameters("psYandexAcc");
            String accPassw = PlatformUtils.getStageParameters("psYandexPassw");
            String payPassw = PlatformUtils.getStageParameters("psYandexPayPassw");
            String psCodeCVV = PlatformUtils.getStageParameters("psPayOnlineCVC");
            log.info("fill account yandex");
            type(YANDEX_ACC_LOGIN, accountYandex);
            log.info("fill yandex pass");
            type(YANDEX_ACC_PASS, accPassw);
            log.info("submit form");
            submit(YANDEX_ACC_PASS);
            try {
                By by = locatorFactory("css=.unblock-form-was-me");
                WebDriverWait wait = new WebDriverWait(LocalDriverManager.getDriverController().getDriver(), 4);
                wait.until(ExpectedConditions.elementToBeClickable(by));
                submit("css=.unblock-form-was-me");
            } catch (Exception e) {
            }
          /*  log.info("fill cvv");
            type(YANDEX_CVV, psCodeCVV);
            type(YANDEX_ACC_PASS, payPassw);
            log.info("submit form");
            submit(YANDEX_ACC_PASS);
           */
            type(YANDEX_ACC_PASS, payPassw);
            submit(YANDEX_ACC_PASS);
            log.info("return to 4game");
            click(YANDEX_RETURN);
        }

    }

    public class PaymentYandex implements Payment {

        public PaymentYandex() {

        }

        @Override
        public void addFunds() {
            log.info("add funds through yandex money");
            Assert.assertTrue(validateElementIsNotVisible(FORM_PAYMENT_TERMINAL),"U r still in 4 game site, but u have to be in PaymentYandex");
            String accountYandex = PlatformUtils.getStageParameters("psYandexAcc");
            String accPassw = PlatformUtils.getStageParameters("psYandexPassw");
            String payPassw = PlatformUtils.getStageParameters("psYandexPayPassw");
            log.info("fill account yandex");
            type(YANDEX_ACC_LOGIN, accountYandex);
            log.info("fill yandex pass");
            type(YANDEX_ACC_PASS, accPassw);
            log.info("submit form");
            submit(YANDEX_ACC_PASS);
            type(YANDEX_ACC_PASS, payPassw);
            submit(YANDEX_ACC_PASS);
            log.info("return to 4game");
            String returnUrl = getAttribute(YANDEX_RETURN, "href");
			int beginPos = getMatchPosition(returnUrl, "\\w\\/", 1);
            returnUrl = returnUrl.substring(beginPos+1);
            openUrlIn4game(returnUrl);
        }

    }


    public class PaymentPayPal implements Payment {

        public PaymentPayPal() {

        }

        @Override
        public void addFunds() {
            log.info("add funds through PayPal");
            Assert.assertTrue(validateElementVisible(PAYPAL_LOGIN_BUTTON),"Youa ere not on the PaymentPayPal page");
            log.info("click PAYPAL_LOGIN_BUTTON  PayPal");
            click(PAYPAL_LOGIN_BUTTON);
            log.info("waitForNotAttribute");
            Assert.assertTrue(validateElementAttribute(PAYPAL_PANEL_MASK, "class", "accessAid"),
            		"The loader on the PaymentPayPal page doesn't work");
            log.info("fill login email");
            type(PAYPAL_LOGIN_EMAIL, PlatformUtils.getStageParameters("psLoginPayPal"));
            log.info("fill password field");
            type(PAYPAL_LOGIN_PASSWORD, PlatformUtils.getStageParameters("psPasswordPayPal"));
            click(PAYPAL_LOGIN_INPUT);
            log.info("waitForNotAttribute");
            Assert.assertTrue(validateElementAttribute(PAYPAL_PANEL_MASK, "class", "accessAid"),
            		"The loader on the PaymentPayPal page doesn't work");
            log.info("return to 4game");
            click(PAYPAL_CONTINUE_BUTTON);

        }

    }


    public class PaymentPaySafeCard implements Payment {

        public PaymentPaySafeCard() {

        }

        @Override
        public void addFunds() {
            log.info("add funds through PaySafeCard");
            Assert.assertTrue(validateElementIsNotVisible(FORM_PAYMENT_TERMINAL),"U r still in 4 game site, but u have to be in PaySafeCard");
            validateElementPresent(SAFE_CARD_TEXTBOXES);
            String codePin = "0000000009903692";
            String partCodePin = codePin.substring(0, 4);
            List<WebElement> rowsPin = LocalDriverManager.getDriverController().findElements(locatorFactory(SAFE_CARD_ROWS_PIN));
            List<WebElement> textBoxes = rowsPin.get(0).findElements(locatorFactory(SAFE_CARD_TEXTBOXES));
            int i = 4;
            for (WebElement textBox : textBoxes) {
                type(textBox, partCodePin);
                if (i < codePin.length()) {
                    partCodePin = codePin.substring(i, 4 + i);
                    i += 4;
                }
            }
            click(SAFE_CARD_CHECKBOX_AGREE);
            click(SAFE_CARD_SUBMIT_BUTTON);

        }

    }


    public class PaymentScrill implements Payment {

        public PaymentScrill() {

        }

        @Override
        public void addFunds() {
            log.info("add funds through Scrill");
            Assert.assertTrue(validateElementIsNotVisible(FORM_PAYMENT_TERMINAL),"U r still in 4 game site, but u have to be in PaymentScrill");
            click(SCRILL_HAVE_ACCOUNT);
            type(SCRILL_LOGIN_FIELD, PlatformUtils.getStageParameters("psLoginMoneybookers"));
            type(SCRILL_PASS_FIELD, PlatformUtils.getStageParameters("psPasswordMoneybookers"));
            click(SCRILL_LOGIN_BUTTON);
            click(SCRILL_RADIO_BUTTON_BALANCE);
            click(SCRILL_CONFIRM_BUTTON);
        }

    }


    public class PaymentScrillLive implements Payment {

        public PaymentScrillLive() {

        }

        @Override
        public void addFunds() {
            log.info("add funds through ScrillLive");
            Assert.assertTrue(validateElementIsNotVisible(FORM_PAYMENT_TERMINAL),"U r still in 4 game site, but u have to be in ScrillLive");
            click(SCRILL_HAS_ACCOUNT_ID);
            type(SCRILL_EMAIL, PlatformUtils.getStageParameters("psLoginMoneybookers"));
            type(SCRILL_PASS, PlatformUtils.getStageParameters("psPasswordMoneybookers"));
            click(SCRILL_LOGIN);
            click(SCRILL_CONFIRM_AND_PLAY);
        }

    }


    public class PaymentThroughTwoPaySafeCards implements Payment {

        public PaymentThroughTwoPaySafeCards() {

        }

        @Override
        public void addFunds() {
            log.info("add funds through twoPaySafeCards");
            Assert.assertTrue(validateElementIsNotVisible(FORM_PAYMENT_TERMINAL),"U r still in 4 game site, but u have to be in PaymentThroughTwoPaySafeCards");
            click(TWO_SAFE_ENTER_PIN);
            if (baseUrl.contains("qa2") || baseUrl.contains("4gametest")) {
                String codePin1 = PlatformUtils.getStageParameters("psPin1Paysafecard");
                String codePin2 = PlatformUtils.getStageParameters("psPin2Paysafecard");
                String partCodePin1 = codePin1.substring(0, 4);
                String partCodePin2 = codePin2.substring(0, 4);
                List<WebElement> rowsPin = LocalDriverManager.getDriverController().findElements(locatorFactory(TWO_SAFE_ROWS_PIN));
                List<WebElement> textBoxes1 = rowsPin.get(0).findElements(By.xpath(".//input[@type='text']"));
                List<WebElement> textBoxes2 = rowsPin.get(1).findElements(By.xpath(".//input[@type='text']"));
                System.out.println(textBoxes2);
                int i = 4;
                for (WebElement textBox : textBoxes1) {
                    type(textBox, partCodePin1);
                    if (i < codePin1.length()) {
                        partCodePin1 = codePin1.substring(i, 4 + i);
                        i += 4;
                    }
                }
                i = 4;
                for (WebElement textBox : textBoxes2) {
                    type(textBox, partCodePin2);
                    if (i < codePin2.length()) {
                        partCodePin2 = codePin2.substring(i, 4 + i);
                        i += 4;
                    }
                }
            }
            click(TWO_SAFE_CHECKBOX_AGREE);
            click(TWO_SAFE_SUBMIT_BUTTON);
            waitForTextPresent(":-)");
        }

    }

    public class PaymentMockDev implements Payment {

        public PaymentMockDev() {

        }

        @Override
        public void addFunds() {
            log.info("add funds through PaymentMockDev");
            LocalDriverManager.getDriverController().get(PlatformUtils.getStageParameters("demoPayRu"));
            type(MOCK_DEV_MONEY_USER_ID, PlatformUtils.getStageParameters("accountIdForPayment"));
            type(MOCK_DEV_MONEY_CURRENCY_QTY, "100");
            click(BUTTON_MOCK_DEV_MONEY_SUBMIT);
        }


    }
    
    public class XsollaDev implements Payment {

        public XsollaDev() {

        }

        @Override
        public void addFunds() {
        }


    }

    public enum PaymentMethod {
        YANDEX_MONEY, QIWI_WALLET, WEBMONEY, PLASTIC_CARD, TERMINAL, FORPAY, SCRILL, PAYSAFECARD, PAYSAFECARDTWO, PAYPAL, MOCK_DEV, XSOLLA_DEV, CENTILI_QA
    }


    private String selectorForPaymentPlan(final PaymentMethod paymentMethod) {
        if (paymentMethod == null) {
            throw (new IllegalArgumentException());
        }

        String nameOfMethod = "";
        switch (paymentMethod) {
            case FORPAY:
                nameOfMethod = "Forpay";
                break;
            case PLASTIC_CARD:
                nameOfMethod = "Yandex_bank_card";
                break;
            case QIWI_WALLET:
                nameOfMethod = "Osmppull";
                break;
            case TERMINAL:
                nameOfMethod = "Osmp";
                break;
            case WEBMONEY:
                nameOfMethod = "Webmoney";
                break;
            case YANDEX_MONEY:
                nameOfMethod = "Yandex";
                break;
            case PAYPAL:
                nameOfMethod = "Paypal";
                break;
            case PAYSAFECARD:
                nameOfMethod = "Paysafecard";
                break;
            case SCRILL:
                nameOfMethod = "Moneybookers";
                break;
            case PAYSAFECARDTWO:
                nameOfMethod = "Paysafecard";
                break;
            case MOCK_DEV:
                nameOfMethod = "PaymentMockDev";
                break;
            case XSOLLA_DEV:
                nameOfMethod = "Xsolla";
                break;
            case CENTILI_QA:
                nameOfMethod = "Centili";
                break;
        }

        return nameOfMethod;
    }


    public void clickOnChosenMethodAndVerifyChosenSelection(final String nameOfPaymentMethod) {
        if (nameOfPaymentMethod.equals("") || nameOfPaymentMethod == null) {
            throw (new IllegalArgumentException());
        }
        log.info("Choose selection " + nameOfPaymentMethod);
        String select = String.format(PAYMENT_SELECTOR, nameOfPaymentMethod);
        click(select);
        String section = String.format(SECTION_SELECTOR, nameOfPaymentMethod);
        Assert.assertTrue(validateElementVisible(section), "The form for " + nameOfPaymentMethod + " is not Visible ");
    }

    private void enterDataIntoChosenSelection(final String nameOfPaymentMethod, final String mobilePhone, final String CentiliMobilePhone, final double sum) {
        log.info("enter data in forms " + nameOfPaymentMethod + " " + mobilePhone + " " + CentiliMobilePhone + " " + sum);
        if (nameOfPaymentMethod.equals("") || nameOfPaymentMethod == null) {
            throw new IllegalArgumentException("Invalid variable nameOfPaymentMethod (value " + nameOfPaymentMethod + ")");
        }
        if (nameOfPaymentMethod.equals("Centili")) {
        	Assert.assertTrue(validateElementVisible(DIV_TILE_OPERATOR_MTS), "The div with MTS for " + nameOfPaymentMethod + " is not Visible ");
        	click(DIV_TILE_OPERATOR_MTS);
        	Assert.assertTrue(validateElementVisible(FIELD_PHONE_CENTILI), "The field with a mobile phone for " + nameOfPaymentMethod + " is not Visible ");
            type(FIELD_PHONE_CENTILI, CentiliMobilePhone);
        }
        String type = String.format(TYPE_SELECTOR, nameOfPaymentMethod);
        type(type, Double.toString(sum));
        if (nameOfPaymentMethod.equals("Osmppull")) {
            type(QIWI_WALLET_ID, mobilePhone);
        }
        
    }

    private void clickOnButtonReplenish(final String selectedMethod) {
        log.info("click on button replenish");
        if (selectedMethod == null || selectedMethod.equals("")) {
            throw (new IllegalArgumentException());
        }
        String buttonSelector = String.format(BUTTON_SELCTOR, selectedMethod);
        System.out.println(buttonSelector);
        click(buttonSelector);
    }

    public void addFundsThroughChosenPaymentMethod(PaymentMethod paymentMethod, User user, double sum) {
        if (paymentMethod == null) {
            throw (new IllegalArgumentException());
        }
        log.info("Runs add funds for account through " + paymentMethod);
        Payment payment;

        switch (paymentMethod) {
            case FORPAY:
                throw new IllegalArgumentException(paymentMethod + " don't support addFunds ");
            case PLASTIC_CARD:
                payment = new PaymentPayOnline();
                payment.addFunds();
                break;
            case QIWI_WALLET:
                payment = new PaymentQiwiWallet();
                payment.addFunds();
                break;
            case TERMINAL:
                throw new IllegalArgumentException(paymentMethod + " don't support addFunds ");
            case WEBMONEY:
                throw new IllegalArgumentException(paymentMethod + " don't support addFunds ");
            case YANDEX_MONEY: {
                if (baseUrl.contains("qa") || baseUrl.contains("4gametest")) {
                    payment = new PaymentYandex();
                    payment.addFunds();
                    break;
                } else {
                    payment = new PaymentYandexLive();
                    payment.addFunds();
                    break;
                }

            }
            case PAYPAL:
                payment = new PaymentPayPal();
                payment.addFunds();
                break;
            case PAYSAFECARD:
                payment = new PaymentPaySafeCard();
                payment.addFunds();
                break;
            case SCRILL: {
                if (baseUrl.contains("qa") || baseUrl.contains("4gametest")) {
                    payment = new PaymentScrill();
                    payment.addFunds();
                    break;
                } else {
                    payment = new PaymentScrillLive();
                    payment.addFunds();
                    break;
                }
            }
            case PAYSAFECARDTWO:
                payment = new PaymentThroughTwoPaySafeCards();
                payment.addFunds();
                break;
            case MOCK_DEV:
                payment = new PaymentMockDev();
                payment.addFunds();
                break;
            case CENTILI_QA:
                addFundsCentiliQa(user, sum);
                break;
        }
    }


    public void replenishAccount(PaymentMethod paymentMethod, User user, double sum) {
        UserBarPage ubp = new UserBarPage();
        log.info("Replenish account through " + paymentMethod.name());
        if (paymentMethod == PaymentMethod.PAYPAL) {
            if (baseUrl.contains("qa") || baseUrl.contains("4gametest")) {
                LocalDriverManager.getDriverController().get(SANDBOX_PAYPAL);
                String mainWindow = LocalDriverManager.getDriverController().getWindowHandle();
                click(LOGIN_BUTTON_PAYPAL);
                selectOtherWindow();
                type(LOGIN_SANDBOX_PAYPAL_FIELD, PlatformUtils.getStageParameters("psLoginSandbox"));
                click(PASSWORD_SANDBOX_PAYPAL_FIELD);
                type(PASSWORD_SANDBOX_PAYPAL_FIELD, PlatformUtils.getStageParameters("psPasswordSandbox"));
                click(LOGIN_SANDBOX_PAYPAL_BUTTON);
                selectWindow(mainWindow);
            }
        }else
	        if(paymentMethod == PaymentMethod.MOCK_DEV){
	            if(!baseUrl.contains("dev"))
	                Assert.fail("You are not on dev stage");
	            ubp = new UserBarPage();
	            log.info("Replenish account through MockDev");
	            //numberOfStringsInMainMail = MailProvider.getNumberOfMessages(user.getMainEmail());
	            if(!isVisible(FORM_PAYMENT_TERMINAL))
	            	openUrlIn4game("?popupWidget=PaymentTerminalWidget");
	            Assert.assertTrue(validateElementVisible(FORM_PAYMENT_TERMINAL), "Payment terminal is not visible");
	            double oldMoney = ubp.getCurrentUserMoney();
	            addFundsMockDev(user, sum);
	            return;
		        }
        if(!isVisible(FORM_PAYMENT_TERMINAL))
        	openUrl(getCurrentUrl() + "?popupWidget=PaymentTerminalWidget");
        String selectedMethod = selectorForPaymentPlan(paymentMethod);
        clickOnChosenMethodAndVerifyChosenSelection(selectedMethod);
        enterDataIntoChosenSelection(selectedMethod, PlatformUtils.getStageParameters("qiwiLogin"), PlatformUtils.getStageParameters("xsollaPhone"), sum);
        clickOnButtonReplenish(selectedMethod);
        addFundsThroughChosenPaymentMethod(paymentMethod, user, sum);
    }

   
    private void addFundsCentiliQa(User user, double sum) {
    	Assert.assertTrue(validateElementVisible(SPINNER_ON_BUTTON), "The spinner on a button 'Pay' in Xsolla form is not visible");
    	Assert.assertTrue(validateElementVisible(MESSAGE_SMS_SENT), "The message 'sms was sent' in Centili form is not visible");   
    	log.info("add funds through CentiliQa");
    	
    	openNewWindow(baseUrl);
    	replenishAccount(PaymentMethod.PLASTIC_CARD,user, sum);
    	selectWindowAndCloseCurrent();
    	//executeScript("require(['eventBus'], function(eventBus) {" +
    		//	"	eventBus.trigger('updateUserbarBalance');});");
    	//Assert.assertTrue(validateElementVisible(MESSAGE_SUCCESS), "The message success of the replenishing in Centili form is not visible");   
    	//Assert.assertTrue(validateElementVisible(LABEL_CENTILI_SUM), "The span with sum in message about success of the replenishing in Centili form is not visible");
    	//Assert.assertTrue(Double.parseDouble(getText(LABEL_CENTILI_SUM)) == sum, "The sum in message about success of the replenishing in CentiliCentilidoesn't match with " + sum);
	}


	public void addFundsMockDev(User user, double sum) {
        log.info("add funds through PaymentMockDev");
        openUrl(PlatformUtils.getStageParameters("demoPayRu"));
        type(MOCK_DEV_MONEY_USER_ID, user.getId());
        type(MOCK_DEV_MONEY_CURRENCY_QTY, String.valueOf(sum));
        click(BUTTON_MOCK_DEV_MONEY_SUBMIT);
    }

    public void buyPinMockDev(User user, double sum) {
        log.info("buy pin through PaymentMockDev");
        openUrl(PlatformUtils.getStageParameters("demoPayRu"));
        type(MOCK_DEV_PIN_USER_ID, user.getId());
        type(MOCK_DEV_PIN_CURRENCY_QTY, String.valueOf(sum));
        click(BUTTON_MOCK_PIN_DEV_SUBMIT);
    }


	public void openCorrectLinkOnDev() {
		if (baseUrl.contains("dev")){
			String partOfUrl = getCurrentUrl().substring(getCurrentUrl().indexOf("?"));
			openUrlIn4game("/" + partOfUrl);
		}			
	}


	public void assertSumInTerminal(double priceOfCoupon) {
		String stringPriceOfCoupon = "";
		if(baseUrl.contains("eu."))
			stringPriceOfCoupon = String.valueOf(priceOfCoupon);
		else 
			stringPriceOfCoupon = String.valueOf((int)priceOfCoupon);
		Assert.assertTrue(validateElementAttribute(FIELD_AMOUNT, "value", stringPriceOfCoupon), "The sum in Payment terminal doesn't match with " + priceOfCoupon);
		
	}


	public double getMinSum(PaymentMethod method) {
		 log.info(method);
         int sum = 0;
		if (OperationsHelper.baseUrl.contains("ru")){
         	if(method == PaymentMethod.CENTILI_QA)
	            	sum = 16;
	            else
	                sum = 10;
		}else 
			if (OperationsHelper.baseUrl.contains("eu"))
				sum = 1;
         return sum;
	}
}
