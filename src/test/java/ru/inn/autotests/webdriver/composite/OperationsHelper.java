package ru.inn.autotests.webdriver.composite;

import net.sourceforge.htmlunit.corejs.javascript.JavaScriptException;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import ru.inn.autotests.webdriver.common.PrepareUser;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.common.language.English;
import ru.inn.autotests.webdriver.common.language.Language;
import ru.inn.autotests.webdriver.composite.pages.PlayPage;
import ru.inn.autotests.webdriver.games.Game;
import ru.inn.autotests.webdriver.toolkit.LocalDriverManager;
import ru.inn.autotests.webdriver.toolkit.PlatformUtils;
import ru.inn.autotests.webdriver.toolkit.TextProvider;
import ru.inn.autotests.webdriver.toolkit.WebDriverController;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Aleksey Niss, pavel.popov
 */
public abstract class OperationsHelper {

    protected static Logger log = Logger.getLogger(OperationsHelper.class);

    protected WebDriverController driver = LocalDriverManager.getDriverController();
    private WebElement frame = null;
    public static String baseUrl = PlatformUtils.getStageParameters("url4game");
    private static final String isLocation = PlatformUtils.getStageParameters("isLocation");
    private IPage currentPage = null;
    protected Language language = new English(); // default state language
    protected PrepareUser prepareUser = new PrepareUser(); 

    protected void setLanguage(Language language) {
        this.language = language;
    }

    public static void initBaseUrl() {
    	log.info("tests.baseUrl = ${tests.baseUrl}");
    	log.info("PlatformUtils.getStageParameters(url4game) = " + PlatformUtils.getStageParameters("url4game"));
    	log.info("System.getProperty(baseUrl) = " + System.getProperty("baseUrl"));
    	log.info("System.getenv(url4game); = " + System.getenv("url4game"));
    	
    	log.info("Base Url is " + baseUrl);
    	baseUrl = System.getProperty("baseUrl");
        log.info("Base Url is " + baseUrl + " from System.getProperty");
        if (baseUrl == null || baseUrl.equals("${tests.baseUrl}")) {
            baseUrl = PlatformUtils.getStageParameters("url4game");
            log.info("baseurl is from stage parameters");
        }
        if (System.getenv("url4game") != null && (System.getProperty("baseUrl") == null)) {
            baseUrl = System.getenv("url4game");
            log.info("baseurl is " + baseUrl +  " from env parameters");
        }

    }

    public void goToAnotherBase() {
        if (driver.getPageAdress().contains("ru")) {
            openUrl(driver.getPageAdress().replace("ru", "eu"));
        } else {
            openUrl(driver.getPageAdress().replace("eu", "ru"));
        }
    }

    public void setDriver(WebDriverController driver) {
        this.driver = driver;
    }

    /**
     * Exits from current user
     */
    public static void logoutHook() {
        if (LocalDriverManager.getDriverController() != null) {
            LocalDriverManager.getDriverController().goToUrl(baseUrl);
            LocalDriverManager.getDriverController().deleteAllCookies();
        }

    }

    private boolean isElementPresent(By by) {
        return driver.isElementPresent(by);
    }

    private boolean isEnable(By by) {
        return driver.isEnable(by);
    }

    private boolean isVisible(By by) {
    	//log.info(driver.getAttributeValue(by, "outerHTML"));
        //String code = driver.getAttributeValue(By.id("userBar4Game"), "outerHTML");
        //String yourText = code.substring(1, code.indexOf("<"));
        //log.info(yourText);
        return driver.isVisible(by);
    }

    public WebElement findElement(String locator) {
        By by = locatorFactory(locator);
        validateElementVisible(locator);
        return driver.findElement(by);
    }

    public WebElement getElement(String locator) {
        By by = locatorFactory(locator);
        validateElementPresent(locator);
        return driver.findElement(by);
    }


    public static String selectRandomEmail() {
        long currentTime = System.currentTimeMillis();
        String longNumber = Long.toString(currentTime);
        String randomName = "notifytest." + "0" + longNumber + "@inn.ru";
        return randomName;
    }

    // The method returns the position of the occurrence of the number with the number in the text
    public static int getMatchPosition(String inputText, String match, int number) {
        int result = -1;
        Pattern pattern = Pattern.compile(match);
        Matcher matcher = pattern.matcher(inputText);
        int i = 0;
        while (matcher.find()) {
            if (++i == number) {
                result = matcher.start();
                break;
            }
        }
        return result;
    }

    public static String selectRandomLogin() {
        int currentTime = (int) System.currentTimeMillis();
        String longNumber = Integer.toString(currentTime);
        String randomName = "login" + longNumber.substring(4, 9);
        return randomName;
    }

    public static String selectRandomMobileNumber() {
        long currentTime = System.currentTimeMillis();
        String longNumber = Long.toString(currentTime);
        String randomNumber = "+01" + longNumber.substring(3, 10);
        return randomNumber;
    }

    public static String selectRandomNumber() {
        long currentTime = System.currentTimeMillis();
        String longNumber = Long.toString(currentTime);
        String randomNumber = longNumber.substring(3, 10);
        return randomNumber;
    }

    public void executeScript(String script) {
    	driver.executeScript(script);
    }
    
    public void setLocalStorage(String name, String value) {
    	driver.executeScript("window.localStorage.setItem('"+ name +"', "+ value +");");
    }
    

    public static User initNewRandomUser() {
        User user = new User();
        user.setMainEmail(selectRandomEmail());
        user.setLogin(user.getMainEmail());
        user.setPassword("123456");
        return user;
    }

    public void windowSetSize(Dimension windowSize) {
        WebDriver.Window window = driver.getDriver().manage().window();
        Dimension size = window.getSize();
        log.debug("Current windowSize = " + size);
        window.setSize(windowSize);
        log.debug("New windowSize = " + size);
    }

    protected String getAlertText() {
        // Get a handle to the open alert, prompt or confirmation
        Alert alert = driver.getDriver().switchTo().alert();
        // Get the text of the alert or prompt
        return alert.getText();
    }

    public void clickOkInAlert() {
        // Get a handle to the open alert, prompt or confirmation
        Alert alert = driver.getDriver().switchTo().alert();
        // Get the text of the alert or prompt
        log.debug("alert: " + alert.getText());
        // And acknowledge the alert (equivalent to clicking "OK")
        alert.accept();
    }

    public boolean hasScrollbar() {
        driver.executeScript("alert(document.body.scrollWidth)");
        int scrollWidth = Integer.valueOf(getAlertText());
        clickOkInAlert();
        driver.executeScript("alert(document.getElementById('UserBar4Game').clientWidth)");
        int clientWidth = Integer.valueOf(getAlertText());
        clickOkInAlert();
        return scrollWidth != clientWidth;
    }

    public void windowSetSize(int widthWindow, int heightWindow) {
        java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        if (((screenSize.width >= widthWindow) && (screenSize.height >= heightWindow)) ||
                ((0 >= widthWindow) && (0 >= heightWindow))) {
            Dimension targetSize = new Dimension(widthWindow, heightWindow);
            windowSetSize(targetSize);
        } else {
            log.debug("it is impossible");
        }
    }

    protected boolean waitForElementPresent(By by) {
        for (int i = 0; i < 20; i++) {
            if (isVisible(by)) {
                return true;
            }
            sendPause(1);
        }

        return false;
    }


    public void waitElementForSec(String locator, int seconds) {
        for (int i = 0; i < seconds; i++) {
            if (!findElements(locator).isEmpty()) {
                break;
            } else {
                sendPause(1);
            }
        }

    }

    protected boolean waitForElementPresentForSec(String locator, int seconds) {
        for (int i = 0; i < seconds; i++) {
            if (isElementPresent(locator)) {
                return true;
            }
            sendPause(1);
        }
        return false;
    }

    public void waitStalenessElement(final String locator) {
        final By by = locatorFactory(locator);
        new WebDriverWait(driver.getDriver(), 15).until(new ExpectedCondition<Object>() {
            @Override
            public Object apply(WebDriver webDriver) {
                try {
                    final WebElement element = driver.findElement(by);
                    if (element != null && element.isDisplayed()) {
                        return element;
                    }
                } catch (StaleElementReferenceException e) {

                }
                return null;
            }
        });
    }


    public void clickOnStalenessElement(String locator) {
        final By by = locatorFactory(locator);

        new WebDriverWait(driver.getDriver(), 15).until(new ExpectedCondition<Object>() {
            @Override
            public Object apply(WebDriver webDriver) {
                try {
                    final WebElement element = driver.findElement(by);
                    if (element != null && element.isDisplayed() && element.isEnabled()) {
                        element.click();
                        return element;
                    }
                } catch (StaleElementReferenceException e) {
                }
                return null;
            }
        });
    }

    protected void clickCancelInAlert() {
        // Get a handle to the open alert, prompt or confirmation
        Alert alert = driver.getDriver().switchTo().alert();
        // Get the text of the alert or prompt
        log.debug("alert: " + alert.getText());
        // And acknowledge the alert (equivalent to clicking "Cancel")
        alert.dismiss();
    }

    protected void waitForNotAttribute(final String locator,
            final String attribute,
            final String value) {
        WebDriverController.getInstanceWaitDriver().until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver d) {
                return d.findElement(locatorFactory(locator))
                        .getAttribute(attribute)
                        .equals(value);
            }
        });
    }


    protected void waitForTextPresent(final String text) {
        WebDriverController.getInstanceWaitDriver().until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver d) {
                return d.getPageSource().contains(text);
            }
        });
    }


    public java.util.List<WebElement> findElements(final String locator) {
        By by = locatorFactory(locator);
        //validateElementPresent(locator);
        return driver.findElements(by);
    }


    protected boolean waitForVisible(By by) {
        return waitForVisible(by, true);
    }

    protected boolean waitForNotVisible(By by) {
        return waitForVisible(by, false);
    }


    private boolean waitForVisible(By by, boolean isVisible) {
        for (int i = 0; i < WebDriverController.TIMEOUT; i++) {
            if (isVisible(by) && isVisible) {
                return true;
            } else if (!isVisible(by) && !isVisible) {
                return true;
            }
            sendPause(1);
            if (i > 5) {
                break;
            }
        }
        if (isVisible == false) {
            Assert.fail("Element " + by.toString() + " stil displayed after " + WebDriverController.TIMEOUT + " sec");
        }
        if (isVisible == true) {
            Assert.fail("Element " + by.toString() + " wasn't loaded after " + WebDriverController.TIMEOUT + " sec");
        }
        return false;
    }

    public static void sendPause(int sec) {
        try {
            Thread.sleep(sec * 1000);
        } catch (InterruptedException iex) {
            Thread.interrupted();
        }
    }

    public String getSrcOfElement(String locator) {
        return driver.findElement(locatorFactory(locator)).getAttribute("src");
    }

    public boolean checkResourceByGetRequest(String requestURL) {
        HttpURLConnection httpConn = null;
        URL url = null;
        boolean result = false;
        try {
            url = new URL(requestURL);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try {
            httpConn = (HttpURLConnection) url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        httpConn.setUseCaches(false);
        httpConn.setDoInput(true); // true if we want to read server's response
        httpConn.setDoOutput(false); // false indicates this is a GET request
        try {
            if (httpConn.getResponseCode() == 200 && httpConn.getResponseMessage().equals("OK"))
                result = true;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;

    }

    public static By locatorFactory(final String locator) {
        if (locator == null || locator.equals("")) {
            throw new IllegalArgumentException();
        }

        By by = null;

        if (locator.contains("xpath=") || locator.contains("id=") || locator.contains("css=") || locator.contains(
                "name=") || locator.contains("tag=")) {
            String mSplitter = locator.split("=")[0];
            String mLocator = locator.replace(mSplitter + "=", "");
            if (mSplitter.equals("xpath")) {
                by = By.xpath(mLocator);
            } else if (mSplitter.equals("id")) {
                by = By.id(mLocator);
            } else if (mSplitter.equals("css")) {
                by = By.cssSelector(mLocator);
            } else if (mSplitter.equals("name")) {
                by = By.name(mLocator);
            } else if (mSplitter.equals("tag")) {
                by = By.tagName(mLocator);
            } else {
                throw new IllegalArgumentException("Check locator = " + locator);
            }
        } else {
            by = By.id(locator);
        }
        return by;
    }

    protected String getText(By by) {
        waitForElementPresent(by);
        return driver.findElement(by).getText().trim().replace("\n", " ");
    }
    
    public String getText(WebElement element) {
        return element.getText().trim().replace("\n", " ");
    }

    public double getNumber(String locator) {
        Pattern PATTERN = Pattern.compile("\\d+");
        Matcher MATCHER = PATTERN.matcher(getText(locator));
        MATCHER.find();
        return Double.parseDouble(MATCHER.group(0));
    }

    public double getNumber(WebElement element) {
        Pattern PATTERN = Pattern.compile("\\d+");
        Matcher MATCHER = PATTERN.matcher(getText(element));
        MATCHER.find();
        return Double.parseDouble(MATCHER.group(0));
    }
    
    public String getCodeFromFile(String pathToFileWithCodes) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(pathToFileWithCodes));
        String line = reader.readLine(), lineForWhile = "", oldtext = "", noDigits = "";
        while ((lineForWhile = reader.readLine()) != null) {
            if (lineForWhile.matches("\\D")) {
                noDigits = lineForWhile;
                continue;
            }
            oldtext += lineForWhile + "\r\n";

        }
        reader.close();
        String newtext = oldtext.replace(line, "");
        newtext = oldtext.replace(noDigits, "");
        FileWriter writer = new FileWriter(pathToFileWithCodes);
        writer.write(newtext);
        writer.close();
        return line;
    }

    /**
     * Gets the absolute URL of the current page.
     *
     * @return the absolute URL of the current page
     */
    public String getCurrentUrl() {
        return driver.getDriver().getCurrentUrl();
    }

    protected void selectValueInDropDown(String locator, String optionValue) {
        By by = locatorFactory(locator);
        Select select = new Select(driver.findElement(by));
        select.selectByValue(optionValue);
    }

    /**
     * Click hidden element
     *
     * @param className     - class of the element which will be clicked
     * @param numberElement - number of the element which will be clicked
     */
    protected void clickInvisibleElementByJs(String className, int numberElement) {
        driver.executeScript("document.getElementsByClassName('" + className + "')[" + numberElement + "].click();");
    }

    public void submit(String locator) {
        log.debug("Submit:" + locator);
        By by = locatorFactory(locator);
        waitForElementPresent(by);
        driver.findElement(by).submit();
    }


    public void resetCacheApi() {
    	if (baseUrl.contains("4gametest")) {
	            //driver.get("http://testapi-q.qa.inn.ru:8088//operation=clearISCache&environment=ru_qa");
	        	prepareUser.resetCacheApi();
	        	prepareUser.resetCacheIs("qa");
    	}
    }

    /**
     * Sends to API browser command back
     */
    public void navigateBack() {
        driver.navigationBack();
    }


    public void moveToElement(String locator) {
        Actions actions = new Actions(driver.getDriver());
        By by = locatorFactory(locator);
        waitForElementPresent(by);
        actions.moveToElement(driver.findElement(by)).build().perform();
    }

    public void mouseClick() {
        Actions actions = new Actions(driver.getDriver());
        // By by = locatorFactory(locator);
        // waitForElementPresent(by);
        actions.click().build().perform();//(driver.findElement(by)).build().perform();
    }

    public void mouseClick(String locator) {
        Actions actions = new Actions(driver.getDriver());
        By by = locatorFactory(locator);
        waitForElementPresent(by);
        actions.click(driver.findElement(by)).build().perform();
    }

    public void highlightTheElement(By by) {
        //WebElement element = driver.findElement(by);
        //driver.executeScript("arguments[0].style.border='2px solid yellow'", element);
    }

    /**
     * Clicks on element
     *
     * @param locator - locator of element contains xpath= or id=
     */
    public void click(String locator) {
        log.debug("Click on: " + locator);
        By by = locatorFactory(locator);
        //Assert.assertTrue(validateElementVisible(locator), "The element " + locator + " is not Visible");
        validateElementVisible(locator);
        highlightTheElement(by);
        driver.findElement(by).click();
    }
    
    public void clickViaNumber(String locator, int number) {
        log.debug("Click on: " + locator);
        By by = locatorFactory(locator);
        //Assert.assertTrue(validateElementVisible(locator), "The element " + locator + " is not Visible");
        java.util.List<WebElement> list = findElements(locator);
        list.get(number - 1).click();
    }
    
    public WebElement findElementWithNumber(String locator, int number) {
        By by = locatorFactory(locator);
        //Assert.assertTrue(validateElementVisible(locator), "The element " + locator + " is not Visible");
        java.util.List<WebElement> list = findElements(locator);
        return list.get(number - 1);
    }

    /**
     * check on element
     *
     * @param locator - locator of element contains xpath= or id=
     */
    public void check(String locator) {
        log.debug("Click on: " + locator);
        By by = locatorFactory(locator);
        //Assert.assertTrue(validateElementVisible(locator), "The element " + locator + " is not Visible");
        validateElementVisible(locator);
        highlightTheElement(by);
        driver.findElement(by).submit();
    }
    
    public void workWithWebSocketsOnPorts(String ports) {
        if (ports == null)
            ports = "9543, 9544, 9545";
        String script = "require(['l4g-application'], function(l4gApp) {\n" +
                "    l4gApp\n" +
                "        .getAppIface()\n" +
                "        .updateConfig('ws', {\n" +
                "            port: [" + ports + "]\n" +
                "        });\n" +
                "});";
        LocalDriverManager.getDriverController().executeScript(script);
    }

    public void clickIfPresent(String locator) {
        log.debug("Click on: " + locator);
        By by = locatorFactory(locator);
        //Assert.assertTrue(validateElementVisible(locator), "The element " + locator + " is not Visible");
        if (isVisible(by)) {
            highlightTheElement(by);
            driver.findElement(by).click();
        }
    }

    /**
     * Get text from element
     *
     * @param locator - locator of element contains xpath= or id=
     */
    public String getText(String locator) {
        log.debug("Text from: " + locator);
        By by = locatorFactory(locator);
        waitForVisible(by);
        return driver.findElement(by).getText();
    }


    public String getAttribute(String locator, String nameAttribute) {
        log.debug("Text from: " + locator);
        By by = locatorFactory(locator);
        waitForVisible(by);
        return driver.findElement(by).getAttribute(nameAttribute);
    }

    //it will remove in refactoring
    public String getTextWithoutWait(String locator) {
        log.debug("Text from: " + locator);
        Assert.assertTrue(isElementPresent(locator), "getTextWithoutWait is impossible. " +
                "Element " + locator + "is not present");
        By by = locatorFactory(locator);
        return driver.findElement(by).getText();
    }

    public String getPageSource() {
        return driver.getDriver().getPageSource();
    }

    public String getCodeWithPath(String pathToFileWithCodes) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(pathToFileWithCodes));
        String line = reader.readLine(), lineForWhile = "", oldtext = "", noDigits = "";
        while ((lineForWhile = reader.readLine()) != null) {
            if (lineForWhile.matches("\\D")) {
                noDigits = lineForWhile;
                continue;
            }
            oldtext += lineForWhile + "\r\n";

        }
        reader.close();
        String newtext = oldtext.replace(line, "");
        newtext = oldtext.replace(noDigits, "");
        FileWriter writer = new FileWriter(pathToFileWithCodes);
        writer.write(newtext);
        writer.close();
        return line;
    }


    protected void selectWindow(String windowId) {
        for (String handle : driver.getWindowHandles()) {
            if (handle.equals(windowId)) {
                driver.switchToWindow(handle);
                break;
            }
        }
    }

    protected void selectOtherWindow() {
        String current = driver.getWindowHandle();
        int timer = 0;
        while (timer < WebDriverController.TIMEOUT) {
            if (driver.getWindowHandles().size() > 1)
                break;
            else {
                sendPause(1);
                timer++;
            }

        }
        for (String handle : driver.getWindowHandles()) {
            try {
                if (handle != current)
                    driver.switchToWindow(handle);
            } catch (Exception e) {

                Assert.fail("Unable to select window");
            }
        }


    }

    protected void selectWindowAndCloseCurrent() {
        String current = driver.getWindowHandle();
        driver.closeWindow();
        int timer = 0;
        while (timer < WebDriverController.TIMEOUT) {
            if (driver.getWindowHandles().size() > 1)
                break;
            else {
                sendPause(1);
                timer++;
            }

        }
        for (String handle : driver.getWindowHandles()) {
            try {
                if (handle != current)
                    driver.switchToWindow(handle);
            } catch (Exception e) {

                Assert.fail("Unable to select window");
            }
        }


    }

    /**
     * Returns element present or not
     *
     * @param locator - locator of element contains xpath= or id=
     */
    public boolean isElementPresent(String locator) {
        By by = locatorFactory(locator);
        return driver.isElementPresent(by);
    }

    /**
     * Returns element visible or not
     *
     * @param locator - locator of element contains xpath= or id= or css
     */
    public boolean isVisible(String locator) {
        By by = locatorFactory(locator);
        return driver.isVisible(by);
    }

    public boolean isChecked(String locator) {
        By by = locatorFactory(locator);
        return driver.isChecked(by);
    }

    /**
     * Types text to element
     *
     * @param locator  - locator of element contains xpath= or id=
     * @param someText - text, which should be typing
     */
    public void type(String locator, String someText) {
        log.debug("Type:" + someText + " to:" + locator);
        By by = locatorFactory(locator);
        validateElementVisible(locator);
        highlightTheElement(by);
        driver.findElement(by).clear();
        driver.findElement(by).sendKeys(someText);
    }


    public void type(WebElement element, String someText) {
        log.debug("Type:" + someText + " to:" + element.getText());
        element.clear();
        element.sendKeys(someText);
    }

    public static void makeScreenshot(String methodName) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formater = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");
        Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
        BufferedImage capture;
        try {
            capture = new Robot().createScreenCapture(screenRect);
            File fileScreenshot = new File("target" + File.separator + "failure_screenshots" +
                    File.separator + methodName + "_" + formater.format(calendar.getTime()) + "_javarobot.jpg");
            fileScreenshot.getParentFile().mkdirs();
            ImageIO.write(capture, "jpg", fileScreenshot);
            File scrFile = ((TakesScreenshot) LocalDriverManager.getDriverController().getDriver()).getScreenshotAs(
                    OutputType.FILE);
            FileUtils.copyFile(scrFile, new File("target" + File.separator + "failure_screenshots" +
                    File.separator + methodName + "_" + formater.format(calendar.getTime()) + "_webdriver.png"));
        } catch (AWTException awte) {
            awte.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }

    }

    /**
     * Open page
     */
    public void openUrl(String url) {
        log.info("Open page: " + url);
        driver.get(url);
    }

    public void addUriToCurrentUrl(String uri) {
        openUrl(driver.getPageAdress() + uri);
    }

    /**
     * Open url in 4game
     */
    public void openUrlIn4game(String subUrl) {
        if (subUrl.length() > 0 && subUrl.indexOf("/") == 0)
            if (baseUrl.lastIndexOf("/") == baseUrl.length() - 1)
                openUrl(baseUrl + subUrl.substring(1));
            else
                openUrl(baseUrl + subUrl);
        else if (baseUrl.lastIndexOf("/") == baseUrl.length() - 1)
            openUrl(baseUrl + subUrl);
        else
            openUrl(baseUrl + "/" + subUrl);
    }

    /**
     * Open page in 4game
     */
    public void openPage(IPage page) {
        log.info("Open page: " + page.getPageUrl());
        this.currentPage = page;
        String url = currentPage.getPageUrl();
        if (url.length() > 0 && url.indexOf("/") == 0)
            if (baseUrl.lastIndexOf("/") == baseUrl.length() - 1)
                openUrl(baseUrl + url.substring(1));
            else
                openUrl(baseUrl + url);
        else if (baseUrl.lastIndexOf("/") == baseUrl.length() - 1)
            openUrl(baseUrl + url);
        else
            openUrl(baseUrl + "/" + url);
        currentPage.setDriver(driver);
        // TODO you will have to remove it after 27/04/2014
        clickIfPresent(PlayPage.BUTTON_CLOSE_POPUP);
    }

    /**
     * Open url in new window
     */
    public void openInNewWindow(String url) {
        log.info("Open page in new window : " + url);
        openTab(url);
        selectOtherWindow();
    }


    /**
     * Opens a new tab for the given URL
     *
     * @param url The URL to
     * @throws JavaScriptException If unable to open tab
     */
    public void openTab(String url) {
        String script = "var d=document,a=d.createElement('a');a.target='_blank';a.href='%s';a.innerHTML='.';d.body.appendChild(a);return a";
        Object element = LocalDriverManager.getDriverController().executeScript(String.format(script, url));
        if (element instanceof WebElement) {
            WebElement anchor = (WebElement) element;
            anchor.click();
            LocalDriverManager.getDriverController().executeScript("var a=arguments[0];a.parentNode.removeChild(a);",
                    anchor);
        } else {
            throw new JavaScriptException(element, "Unable to open tab", 1);
        }
    }
    
    public void openNewWindow(String url) {
    	Set<String> windowHandles = driver.getWindowHandles();
        String script = String.format("window.open('/', '_blank');", url);
        executeScript(script);
        Set<String> newWindowHandles = driver.getWindowHandles();
        for(String windowHandle:newWindowHandles)
        	if(!windowHandles.contains(windowHandle))
        		selectWindow(windowHandle);
    }
    

    public Object getLastElement(final Collection c) {
        final Iterator itr = c.iterator();
        Object lastElement = itr.next();
        while (itr.hasNext()) {
            lastElement = itr.next();
        }
        return lastElement;
    }

/*	*//**
     * Switches to the non-current window
     *//*
    public void switchWindow() throws NoSuchWindowException, NoSuchWindowException {

        selectOtherWindow();
	    Set<String> handles = driver.getWindowHandles();
	    String current = driver.getWindowHandle();
	    handles.remove(current);
	    log.info(handles);
	    log.info(current);
	    int size = handles.size();
	    String newTab = handles.iterator().next();
	    log.info(newTab);
	    driver.switchTo(newTab);
	}*/

    /**
     * Validates that element has correct value of attribute
     *
     * @param locator - locator of element contains xpath= or id=
     */
    public boolean validateElementAttribute(String locator, String attribute, String value) {
        for (int i = 0; i < WebDriverController.TIMEOUT; i++) {
            if (findElement(locator).getAttribute(attribute).equals(value)) {
                return true;
            }
            sendPause(1);
        }
        return false;
    }

    /**
     * Validates that element is displayed
     *
     * @param locator - locator of element contains xpath= or id=
     */
    public boolean validateElementPresent(String locator) {
        By by = locatorFactory(locator);
        for (int i = 0; i < WebDriverController.TIMEOUT; i++) {
            if (isElementPresent(by)) {
                return true;
            }
            sendPause(1);
        }

        return false;
    }

    /**
     * Validates that element isn't visible
     *
     * @param locator - locator of element contains xpath= or id=
     * @return
     */
    public boolean validateElementIsNotVisible(String locator) {
        By by = locatorFactory(locator);
        for (int i = 0; i < WebDriverController.TIMEOUT; i++) {
            if (!isVisible(by))
                return true;
            sendPause(1);
        }
        return false;
    }

    /**
     * Validates that element is visible
     * returns true if locator is visible
     * returns false if locator is not visible for a while
     *
     * @param locator - locator of element contains xpath= or id= or css
     */
    public boolean validateElementVisible(String locator) {
        By by = locatorFactory(locator);
        for (int i = 0; i < WebDriverController.TIMEOUT; i++) {
            if (isVisible(by))
                return true;
            sendPause(1);
        }
        return false;

    }

    public boolean validateElementEnable(String locator) {
        By by = locatorFactory(locator);
        for (int i = 0; i < WebDriverController.TIMEOUT; i++) {
            if (isEnable(by))
                return true;
            sendPause(1);
        }
        return false;

    }

    public boolean validateTextEquals(String locator, String text) {
        validateElementVisible(locator);
        for (int i = 0; i < WebDriverController.TIMEOUT; i++) {
            if (getText(locator).equals(text))
                return true;
            sendPause(1);
        }
        return false;

    }

    public boolean validateTextNotEquals(String locator, String text) {
        validateElementVisible(locator);
        for (int i = 0; i < WebDriverController.TIMEOUT; i++) {
            if (!getText(locator).equals(text))
                return true;
            sendPause(1);
        }
        return false;

    }
    
    public boolean validatElementContainsText(String locator, String text) {
        validateElementVisible(locator);
        for (int i = 0; i < WebDriverController.TIMEOUT; i++) {
            if (getText(locator).contains(text))
                return true;
            sendPause(1);
        }
        return false;

    }
    
    public boolean validateUrlContains(String url) {
        for (int i = 0; i < WebDriverController.TIMEOUT; i++) {
            if (getCurrentUrl().contains(url))
                return true;
            sendPause(1);
        }
        return false;

    }
    
    

    /**
     * Waits while element is present on the page
     *
     * @param locator - locator of element contains xpath= or id=
     */
    public void waitWhileElementIsPresent(String locator) {
        By by = locatorFactory(locator);
        for (int i = 0; i < WebDriverController.TIMEOUT; i++) {
            if (!isElementPresent(by)) {
                return;
            }
            sendPause(1);
        }
        throw new RuntimeException("The time is out, " + locator + " isn't closed.");
    }

    /**
     * Validates that text in element equals with expected text
     *
     * @param locator  - locator of element contains xpath= or id=
     * @param expected - expected text
     */
    public void assertText(String locator, String expected, Language lang) {
        By by = locatorFactory(locator);
        validateElementVisible(locator);
        if (expected.indexOf("$") != -1) {
            expected = TextProvider.translator(expected, lang);
        }
        Assert.assertEquals(getText(by), expected);
    }

    /**
     * Validates that integer expected equals with count of elements on a page
     *
     * @param locator       - locator of element contains xpath= or id=
     * @param expectedValue - expected value
     */
    public void validateNumber(String locator, int expectedValue) {
        By by = locatorFactory(locator);
        Assert.assertEquals(getCountElements(by), expectedValue);
    }


    /**
     * Reloads page
     */
    public void refreshPage() {
        driver.refresh();
    }

    /**
     * Sets Maintenance for current game
     *
     * @param game - the game, which has maintenance
     */
    public void setMaintenance(Game game) {
        driver.changeCookie("L4G.Maintenance.statuses", "{ \"" + game.getServiceId() + "\":false}");
    }

    // Set a cookie
    public void addCookie(String key, String value) {
    	log.info("The Cokie with " + key + " and value " + value );
        driver.addCookie(key, value);
    }

    // Get a cookie
    public Cookie getCookie(String key) {
        Cookie cookie = driver.getCookie(key);
        log.info("The Cokie has key " + key + " and value " + cookie.getValue() );
        return cookie;
    }
    
    /**
     * Removes all cookies
     */
    public void resetMaintenance() {
        driver.deleteAllCookies();
    }

    /**
     * Switches to iFrame
     *
     * @param iFrame
     */
    public void switchTo(String iFrame) {
        sendPause(2); // for correct open iFrame and switch there
        //log.info(driver.getWindowHandles().size());
        driver.switchTo(iFrame);
    }

    /**
     * Returns to the main content in the page
     */
    public void switchToMainContent() {
        driver.switchToMainContent();
        sendPause(2); //for correct switch to MainContent
    }

    /**
     * Hovers on element
     *
     * @param locator
     */
    public void hoverOn(String locator) {
        Actions action = new Actions(driver.getDriver());
        action.moveToElement(findElement(locator)).build().perform();
        log.info("Action - hover on to locator: " + locator);
    }

    public void showHoverGameInNavBar(Game game) {
        LocalDriverManager.getDriverController().executeScript(
                "$(\".bGamesNav__ePlaceholder__mId" + game.getServiceId() + " .bGamesNav__eItem__eBlock\").addClass(\"bGamesNav__eItem__eBlock__mHover\")");
    }

    public void showHoverDayPremium() {
        LocalDriverManager.getDriverController().executeScript(
                "$(\".bPremiumRates__eDailyCoupon\").addClass(\"bPremiumRates__eDailyCoupon__mState_hover\")");
    }

    public void scrollOnTop() {
        LocalDriverManager.getDriverController().executeScript("window.scrollTo(0,0)");
    }
    
    public void scrollToElemant(String elementToClick) {
    	int yPositionOfElement = driver.findElement(locatorFactory(elementToClick)).getLocation().y;
        LocalDriverManager.getDriverController().executeScript(String.format("window.scrollTo(0, $s);", yPositionOfElement));
    }

    /**
     * Returns count of elements on a page with this locator
     */
    private int getCountElements(By by) {
        waitForElementPresent(by);
        return driver.findElements(by).size();
    }

    /**
     * Returns count of elements on a page with this locator
     */
    public int getCountElements(String locator) {
        By by = locatorFactory(locator);
        waitForElementPresent(by);
        return driver.findElements(by).size();
    }

    /**
     * Sets element to frame
     *
     * @param locator
     */
    public void setFrame(String locator) {
        By by = locatorFactory(locator);
        frame = driver.findElement(by);
    }

    /**
     * Returns the frame
     */
    public WebElement getFrame() {
        if (frame != null)
            return frame;
        else
            throw new RuntimeException("");
    }

    public static void main(String args[]) {
//		String percent = "15 %";

//		for (Field e: domain.getClass().getFields())

//		try {
//			System.out.println(domain.getClass().getField(domain).getName());
//		} catch (NoSuchFieldException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (SecurityException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		Pattern p = Pattern.compile("(\\d+)"); //get percent without char '%'
//		Matcher m = p.matcher(percent); // ex. '15%' after matcher's work -> '15'
//		if (m.find()) {
//			Double dPercent = Double.valueOf(m.group());
//			System.out.println(dPercent);
//		}
    }

    public static Date getChangedDate(int countDay) {
        Calendar calendar = Calendar.getInstance();
        Date lifeTimeDateFinish = new Date();
        calendar.setTime(lifeTimeDateFinish);
        calendar.add(Calendar.DATE, countDay);
        return calendar.getTime();
    }

}
