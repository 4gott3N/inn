package ru.inn.autotests.webdriver.composite.pages;

import org.hamcrest.core.IsNot;
import org.testng.Assert;
import ru.inn.autotests.webdriver.composite.IPage;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.composite.common.Captcha;
import ru.inn.autotests.webdriver.composite.pages.PaymentTerminal.PaymentMethod;
import ru.inn.autotests.webdriver.toolkit.PlatformUtils;

import java.io.File;
import java.io.IOException;

/**
 * @author evgenij.tkachenko
 */
public class GiftCodePage extends OperationsHelper implements IPage{

	 String pageUrl= "/giftcode/";
	 
	 //CREDIT
	 public static final String DIV_CREDIT_ALLOWED_SUM = "id=bCredit__eSumOverdraftLimit";
	
	 /**
	   * The constant LABEL_CREDIT_ON on the Credit page
	   */
	 public static final String LABEL_CREDIT_ON = "id=bCredit__eSum__mSwitch_On";
	 
	 /**
	   * The constant LABEL_CREDIT_OFF on the Credit page
	   */
	 public static final String LABEL_CREDIT_OFF = "id=bCredit__eSum__mSwitch_Off";
	 
	 /**
	   * The constant FORM_CREDIT.
	   */
	 public static final String FORM_CREDIT = "css=.bCredit";
	 
	 /**
	     * The constant BUTTON_CREDIT_ON.
	     */
	    public static final String BUTTON_CREDIT_ON = "css=.bUISwitcher__eAreaRightSide";
	   
	    /**
	     * The constant BUTTON_CREDIT_OFF.
	     */
	    public static final String BUTTON_CREDIT_OFF = "css=.jsUISwitcher__eAreaLeftSide";
	   
	 
	 //ACCOUNT BLOCK	
	 public static final String LABEL_TOTAL_BALANCE = "css=.bInternalSidebarBalance__eSum";
	 public static final String LABEL_RUBLES_BALANCE = "id=bInternalSidebarBalance__eMoney";
	 public static final String LABEL_BONUSES_BALANCE = "id=bInternalSidebarBalance__eBonuses";
	 public static final String BUTTON_ADD_FUNDS = "id=bInternalSidebarBalance__eButtonBuy";
	 
	//FILES WITH CODES
	private final static String pathToCode =  System.getProperty("user.dir") +
	        File.separator + "src" + File.separator + "test" + File.separator +
		 "resources" + File.separator + "codes" + File.separator;
	public static final String MONEY_CODE = "300r.txt";
	public static final String ITEM_MONEY = "30r_10b_item.txt";
	public static final String MONEY_AND_BONUS_CODE = "36r_9b.txt";
	public static final String MONEY_AND_BONUS_CODE_4GAME = "1r_2b.txt";
	public static final String ITEM_CODE = "item.txt";
	public static final String ITEM_PB_CODE = "item_pb.txt";
	public static final String ITEM_PS2_CODE = "item_ps2.txt";
	public static final String SUBSCRIPTION_CODE = "subscr.txt";
	public static final String ROLE_TESTER = "tester_%s.txt";
	public static final String LIMIT_CODE = "subscr_limit.txt";
	
	//TOP GIFT CODES PAGE
	public static final String LINK_MY_CODES = "xpath=//*[@href='/giftcode/my/']";
	public static final String LABEL_NUM_CODES = "xpath=//*[@class='bInternalOptionsPageLayoutNav__eItemSup']";
	
	//TOP BUYING AND ACTIVATION GIFT CODES PAGE
	public static final String LINK_BUYING_AND_ACTIVATION = "xpath=//*[@href='/giftcode/']";
	
	//MY CODES FORM
	public static final String FORM_MY_CODES = "id=BonusCodeStatusWidget";
	public static final String LABEL_FIRST_SUM_CODE = "xpath=//div/strong";
	public static final String BUTTON_ACTIVATE_MY_CODES = "xpath=//*[contains(@href, 'BonusCodeActivationWidget')]";
	public static final String POPUP_ACTIVATE_CODE = "xpath=//*[@class='bGiftCodeActivation__eForm']";
	public static final String BUTTON_ACTIVATE_POPUP = "xpath=//*[@id='']";
		
	//GIFT CODES FORM
	public static final String FORM_GIFT_CODES = "id=GiftCodesContent";
	public static final String RADIO_CODE300R = "xpath=//*[@value='0']";
	public static final String RADIO_CODE1500R_1 = "xpath=//*[@value='11']";
	public static final String RADIO_CODE1500R_2 = "xpath=//*[@value='12']";
	public static final String RADIO_CODE3000R = "xpath=//*[@value='13']";
    public static final String RADIO_PAYONLINE = "id=payonline";
    public static final String RADIO_WEBMONEY = "id=webmoney";
    public static final String RADIO_YANDEX = "id=yandex";
    public static final String CHECK_AUTOACTIVATE = "id=autoActivateCode";
    public static final String BUTTON_BUY = "id=bGiftCodes__eBottomButtonBuy";
	
    //FORM ACTIVATION CODE
	public static final String FORM_CODE_ACTIVATION = "id=BonusCodeActivationWidget";
    public static final String FIELD_CELL_1 = "name=code[0]";
    public static final String FIELD_CELL_2 = "name=code[1]";
    public static final String FIELD_CELL_3 = "name=code[2]";
    public static final String FIELD_CELL_4 = "name=code[3]";
    public static final String BUTTON_ACTIVATE = "id=ftGiftCodeActivationButton";
    public static final String MESSAGE_SUCCESS_ACTIVATION = "xpath=//*[@class='bGiftCodeActivation__eSuccessMessageText']";
    public static final String LINK_ACTIVATE_ANOTHER_ONE ="css=.bGiftCodeActivation__eSuccessMessageHide";
    public static final String MESSAGE_FAIL_ACTIVATION="css=.bUIErrorMessage__mDisplay_Block";
    public static final String MESSAGE_FAIL_LIMIT_ACTIVATION="css=a[href='https://4gameforum.ru/showthread.php?t=789479']";
    public static final String SPINNER_ON_BUTTON = "css=button[class*='Loading']";
    
    //FORM PERSONAGE
    public static final String FORM_PERSONAGE_ITEM_ACTIVATION = "xpath=//*[@class='bGiftCodePersonage']";
    public static final String SELECT_SERVER = "jsUISelect__eItem jsUISelect__eItemFilterCountry bUISelect__eItem";
    public static final String SELECT_SERVERS = "name=AionPincodeItemForm[serverId]";
    public static final String FIELD_PERSONAGE_NAME = "name=AionPincodeItemForm[personageName]";
    public static final String BUTTON_CONFIRM = "xpath=//*[@class='bExternalPageFormItem  bGiftCodePersonage__eButtonWrap']/button";

	
  
    
    
    
    public void fillGiftCodeForm(String nameFileWithCodes, String environment) throws IOException {
    	log.info("fillGiftCodeForm started with code from = " +  nameFileWithCodes);
    	Assert.assertTrue(validateElementVisible(FORM_CODE_ACTIVATION), "GiftCodeForm is not visible");
    	//should be rewritten user.env
    	int k =0;
    	do{
    	String pinCode = getCodeWithPath(pathToCode + environment + "_" + nameFileWithCodes);
    	log.info("pinCode is " + pinCode);
    	type(FIELD_CELL_1, pinCode.substring(0, 4));
		type(FIELD_CELL_2, pinCode.substring(4, 8));
		type(FIELD_CELL_3, pinCode.substring(8, 12));
		type(FIELD_CELL_4, pinCode.substring(12, pinCode.length()));
            Captcha captcha=new Captcha();
            captcha.typeCaptcha("987654");
		click(BUTTON_ACTIVATE);
		validateElementVisible(SPINNER_ON_BUTTON);
		validateElementIsNotVisible(SPINNER_ON_BUTTON);
		k++;
    	}while(!isVisible(MESSAGE_FAIL_LIMIT_ACTIVATION) && isVisible(MESSAGE_FAIL_ACTIVATION) && k<50);
	}
    
    public void fillPersonageForm() throws IOException {
    	String gardarikaPersonageName = PlatformUtils.getStageParameters("gardarikaPersonage");
    	log.info("fillPersonageForm started with " + gardarikaPersonageName);
    	validateElementVisible(FORM_PERSONAGE_ITEM_ACTIVATION);
    	selectGardarikaServer();
		type(FIELD_PERSONAGE_NAME, gardarikaPersonageName);
		click(BUTTON_CONFIRM);
	}
    
    public int getNumOfCodes() {
    	if(isVisible(LABEL_NUM_CODES))
    		return Integer.valueOf(getText(GiftCodePage.LABEL_NUM_CODES));
    	else
    		return 0;
	}
    
    public double getTotalBalance() {
    	Assert.assertTrue(validateElementPresent(LABEL_TOTAL_BALANCE), "The user is not on the gift code page, LABEL_TOTAL_BALANCE is not present");
    	return getNumber(LABEL_TOTAL_BALANCE);
	}
    
    public double getRublesBalance() {
    	Assert.assertTrue(validateElementPresent(LABEL_RUBLES_BALANCE), "The user is not on the gift code page, LABEL_RUBLES_BALANCE is not present");
    	return getNumber(LABEL_RUBLES_BALANCE);
	}
    
    public double getBonusesBalance() {
    	Assert.assertTrue(validateElementPresent(LABEL_BONUSES_BALANCE), "The user is not on the gift code page,  LABEL_BONUSES_BALANCE is not present");
    	return getNumber(LABEL_BONUSES_BALANCE);
	}
    
    public void selectPayment(PaymentMethod paymentMethod) {
		if (paymentMethod == null){
			throw (new IllegalArgumentException());
		}
		log.info("Runs select payment for buy gift codes through " + paymentMethod);
		switch(paymentMethod){
			case PLASTIC_CARD: click(RADIO_PAYONLINE); break; 
			case WEBMONEY: click(RADIO_WEBMONEY); break; 
			case YANDEX_MONEY: click(RADIO_YANDEX); break;
			case MOCK_DEV: click(RADIO_WEBMONEY); break;
		default:
			break;	
		}
	}
    
    private void selectGardarikaServer() {
		clickInvisibleElementByJs(SELECT_SERVER, 2);
	}
    
    public void validateNumCodes(int numOfCodes, int changeNum) {
    	
    	int mustHaveNum = numOfCodes + changeNum;
		if(isVisible(LABEL_NUM_CODES)){
			int newNum = getNumOfCodes();
			Assert.assertTrue(newNum == mustHaveNum, "Number of codes had to be  " + 
				mustHaveNum + " but now " + newNum);
		}else
			Assert.assertTrue(0 == mustHaveNum, "Number of codes had to be  " + 
					mustHaveNum + " but now " + 0);
		
	}

	@Override
    public String getPageUrl() {
        return pageUrl;
    }

	public void assertBonusBalance(double oldBonus, double differenceBonuses) {
		// TODO Auto-generated method stub
		
	}

	public void assertRublesBalance(double oldBonus, double differenceRubles) {
		// TODO Auto-generated method stub
		
	}

	
}
