package ru.inn.autotests.webdriver.composite.popups;

import ru.inn.autotests.webdriver.composite.IPage;
import ru.inn.autotests.webdriver.composite.OperationsHelper;

/**
 * @author pavel.popov
 */
public class PopupDownload extends OperationsHelper implements IPage {

    private String pageUrl = "";

    public static final String GAME_DOWNLOAD_FORM="css=.bWidgetDownload__eInnerContent";

    public static final String BUTTON_START_INSTALLATION = "xpath=//*[@id='btnBeginInstall']/div/div";

    public static final String CHECKBOX_AGREE_LICENSE = "id=chbAcceptLicence";

    public static final String FOLDER_PATH = "id=FolderPath";

    public PopupDownload() {
        sendPause(2);//hack to open popup
    }

    @Override
    public String getPageUrl() {
        return pageUrl;
    }


}
