package ru.inn.autotests.webdriver.composite.pages;

import org.testng.Assert;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.composite.common.Captcha;

/**
 * Created with IntelliJ IDEA.
 * User: Sergey.Kashapov
 * Date: 30.05.13
 * Time: 16:41
 * To change this template use File | Settings | File Templates.
 */
public class RegisterPage extends OperationsHelper {

    //FIELDS
    public static final String FIELD_REGISTRATION = "name=ConfirmMainEmailForm[email]";
    public static final String FIELD_CREATE_NEW_PASSWORD_OPEN_EYE = "xpath=(//input[@name='RegisterForm[password]'])";
    public static final String FIELD_CREATE_NEW_PASSWORD = "xpath=(//input[@name='RegisterForm[password]'])[2]";
    public static final String FIELD_CREATE_NEW_PASSWORD_CLOSE_EYE =  "xpath=//input[@type='password']";

    //BUTTONS
    public static final String BUTTON_CREATE_ACCOUNT = "id=jsLoginPopupWidget__Registration";
    public static final String BUTTON_OPEN_EYE_FOR_PASSWORD = "css=span.bUIInput__ePasswordEye.jsUIInput__ePasswordEye";
    public static final String BUTTON_LOGIN_AND_PLAY = "id=jsRegistration__LoginAndPlay";
    public static final String BUTTON_SIGN_UP = "id=bUserBar__eAuthAndRegLink";

    //SPANS
    public static final String SPAN_UP_SECURITY = "xpath=//span[@class='bDropDown__eMorePowerDescription']";
    
    //REGISTRATION PAGE
    public static final String BUTTON_CREATE_ACCOUNT_FROM_REGISTRATION="id=jsRegistration__CreateAccount";
    public static final String BUTTON_VK = "xpath=//*[@data-ft='ft_regButtonSocial_vk']";
    public static final String BUTTON_FB = "xpath=//*[@data-ft='ft_regButtonSocial_facebook']";

    public static final String CHECKBOX_TERMS_ACCEPT = "id=jsRegistration__AcceptLicense";
	public static final String GAME_ICON = "css=img[src*='icon-%s']";
	public void fillRegistration(String mail){
        log.info("fill email registration form with email "+mail);
        Assert.assertTrue(validateElementPresent(RegisterPage.FIELD_REGISTRATION),
                "The form 'New User Registration' is not visible");
        type(FIELD_REGISTRATION, mail);
        Captcha captcha=new Captcha();
        captcha.typeCaptchaIfPresent("987654");
        Assert.assertTrue(validateElementPresent(BUTTON_CREATE_ACCOUNT),
                "The button BUTTON_CREATE_ACCOUNT on the form 'New User Registration' is not visible");
        click(BUTTON_CREATE_ACCOUNT);


    }

    public void fillPassOpenEyeForm(String pass){
        log.info("fill open eye form after reg link");
        Assert.assertTrue(validateElementPresent(CHECKBOX_TERMS_ACCEPT), "The checkbox CHECKBOX_TERMS_ACCEPT on the form 'New User Registration' is not visible");
        click(CHECKBOX_TERMS_ACCEPT);
        Assert.assertTrue(validateElementPresent(BUTTON_OPEN_EYE_FOR_PASSWORD),
                "The box BUTTON_OPEN_EYE_FOR_PASSWORD on the form 'New User Registration' is not visible");
        click(BUTTON_OPEN_EYE_FOR_PASSWORD);
        Assert.assertTrue(validateElementPresent(FIELD_CREATE_NEW_PASSWORD_OPEN_EYE),
                "The box FIELD_CREATE_NEW_PASSWORD_OPEN_EYE on the form 'New User Registration' is not visible");
        type(FIELD_CREATE_NEW_PASSWORD_OPEN_EYE, pass);
        Assert.assertTrue(validateElementPresent(BUTTON_LOGIN_AND_PLAY),
                "The button BUTTON_LOGIN_AND_PLAY on the form 'New User Registration' is not visible");
        click(BUTTON_LOGIN_AND_PLAY);
    }


    public void fillPassForm(String pass){
        log.info("fill open eye form after reg link");
        Assert.assertTrue(validateElementPresent(CHECKBOX_TERMS_ACCEPT), "The checkbox CHECKBOX_TERMS_ACCEPT on the form 'New User Registration' is not visible");
        click(CHECKBOX_TERMS_ACCEPT);
        Assert.assertTrue(validateElementPresent(FIELD_CREATE_NEW_PASSWORD_CLOSE_EYE),
                "The box FIELD_CREATE_NEW_PASSWORD_CLOSE_EYE on the form 'New User Registration' is not visible");
        type(FIELD_CREATE_NEW_PASSWORD,pass);
        Assert.assertTrue(validateElementPresent(BUTTON_LOGIN_AND_PLAY),
                "The button BUTTON_LOGIN_AND_PLAY on the form 'New User Registration' is not visible");
        Captcha captcha=new Captcha();
        captcha.typeCaptchaIfPresent("987654");
        click(BUTTON_LOGIN_AND_PLAY);
    }
}
