package ru.inn.autotests.webdriver.composite.pages;

import org.testng.Assert;

import com.cybozu.labs.langdetect.Detector;
import com.cybozu.labs.langdetect.DetectorFactory;
import com.cybozu.labs.langdetect.LangDetectException;

import ru.inn.autotests.webdriver.composite.IPage;
import ru.inn.autotests.webdriver.composite.common.Captcha;
import ru.inn.autotests.webdriver.composite.popups.PopupFinalFormalities;
import ru.inn.autotests.webdriver.games.Game;
import ru.inn.autotests.webdriver.toolkit.GameData;
import ru.inn.autotests.webdriver.toolkit.LocalDriverManager;

public class LoginPage extends GamePanelPage implements IPage {

    public LoginPage(Game game) {
		super(game);
	}



    //Elements
	public static final String POPUP_LOGIN_FIELD = "id=bAuthPopupWidget";
    public static final String LOGIN_FIELD = "id=AuthFormLogin";
    public static final String PASSWORD_FIELD = "id=AuthFormPassword";
    private static final String PASSWORD_FIELD_OPENED_EYE = "name=AuthForm[password]";
    public static final String BUTTON_SIGN_UP = "id=jsLoginPopupWidget__SignIn";
    private static final String OPEN_EYE = "xpath=//span[@class='bUIInput__ePasswordEye jsUIInput__ePasswordEye']";
    public static final String LABEL_ERROR_MESSAGE_LOGIN_OR_PASSWORD = "id=AuthForm_loginOrEmail";
    public static final String LABEL_ERROR_MESSAGE_PASSWORD = "id=AuthForm_password";
    public static final String LINK_FORGOT_PASSWORD = "xpath=//a[@href='/recovery-password']";
    public static final String LOGIN_CAPTCHA="id=AuthForm_captcha";
    public static final String BUTTON_I_AGREE = "id=jsLicensePopup-btnPlay";
    public static final String POP_UP_4GAMELICENCE = "id=A4GFrame";
    
    public void fillLoginForm(String email, String password, boolean openEye) {
        log.info("fill the login form with email "+email+ " and with pass "+password);
        Assert.assertTrue(validateElementVisible(POPUP_LOGIN_FIELD), "Pop-up 'Login and Registration' is not visible");
        type(LOGIN_FIELD, email);
        if(openEye){
        	click(OPEN_EYE);
        	type(PASSWORD_FIELD_OPENED_EYE, password);
        }
        else
        	type(PASSWORD_FIELD, password);
        Captcha captcha=new Captcha();
        captcha.typeCaptchaIfPresent("987654");
        click(BUTTON_SIGN_UP);
        if(isVisible(LOGIN_CAPTCHA))
        {
            captcha.typeCaptchaIfPresent("987654");
            fillLoginForm(email, password, openEye);
        }

        acceptLicenceIfNeed();
    }

	public void acceptLicenceIfNeed() {
		/*Detector detector;
		try {
			DetectorFactory.loadProfile("src\\test\\resources\\extensions\\profiles");
		} catch (LangDetectException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			detector = DetectorFactory.create();
			detector.append("русский");
			String lang = detector.detect();
			log.info("lang = " + lang);
		} catch (LangDetectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
      LocalDriverManager.getDriverController().waitForPageLoaded();
      PopupFinalFormalities pff = new PopupFinalFormalities();
      pff.waitElementForSec(POP_UP_4GAMELICENCE,4);
		if(isVisible(POP_UP_4GAMELICENCE) && !isVisible(LABEL_ERROR_MESSAGE_LOGIN_OR_PASSWORD) && !isVisible(LABEL_ERROR_MESSAGE_PASSWORD)){
        	
        	Assert.assertTrue(pff.validateElementVisible(PopupFinalFormalities.IFRAME_FINAL_FORMALITIES),  "Pop-up with licence for 4game is not visible");
        	switchTo("A4GFrame");
        	click(BUTTON_I_AGREE);
        	switchToMainContent();
        	Assert.assertTrue(pff.validateElementIsNotVisible(PopupFinalFormalities.IFRAME_FINAL_FORMALITIES),  "Pop-up with licence for 4game is still visible");
        }
	}





    
}
