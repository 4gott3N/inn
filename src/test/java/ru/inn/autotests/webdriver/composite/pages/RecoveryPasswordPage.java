package ru.inn.autotests.webdriver.composite.pages;

import org.testng.Assert;
import ru.inn.autotests.webdriver.composite.IPage;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.composite.common.Captcha;

public class RecoveryPasswordPage extends OperationsHelper implements IPage {

    String pageUrl = "/recovery-password/";

    //Elements
    public static final String FIELD_CONTACT = "xpath=//input[@name='RecoveryPasswordForm[contact]' and @type='text']";
    public static final String FORM_RECOVERY_PASSWORD = "css=.bRecoveryPasswordPage__eWhitePanel";
    public static final String BUTTON_VERIFY_IDENTITY = "css=.bExternalPageSubmitButton__eInput";

    //FORM USER IDENTIFIED
    public static final String FORM_USER_IDENTIFIED = "xpath=//div[@data-target='bRecoveryPasswordPage__ContactForm']";
    //should be rewrite
    public static final String BUTTON_SEND = "css=.bExternalPageSubmitButton__eInput";

    //Sign in with new password FORM
    public static final String FIELD_PASSWORD = "name=ResetAndLoginForm[password]";
    public static final String FIELD_PASSWORD_CONFIRM = "name=ResetAndLoginForm[passwordConfirm]";
    public static final String FIELD_SMS_CODE = "name=ResetAndLoginForm[code]";
    public static final String CHECK_REMEMBER_ME = "name=ResetAndLoginForm[rememberMe]";
    public static final String BUTTON_SIGN_IN = "css=.bExternalPageSubmitButton__eInput";

    //'An e-mail for you' form
    public static final String FORM_AN_EMAIL_FOR_YOU = "css=.bExternalPageLetterPanel__eHeader";

    public void fillRecoveryPasswordForm(String contact) {
        log.info("fill the RecoveryPassword form with contact "+contact);
        Assert.assertTrue(validateElementVisible(FORM_RECOVERY_PASSWORD), "RecoveryPassword form is not visible");
        type(FIELD_CONTACT, contact);
        Captcha captcha=new Captcha();
        captcha.typeCaptchaIfPresent("987654");
        click(BUTTON_VERIFY_IDENTITY);
        //rewrite
        sendPause(3);
       if (captcha.validateCaptchaIsVisible() && isVisible(FORM_RECOVERY_PASSWORD)) {
            captcha.typeCaptchaIfPresent("987654");
            click(BUTTON_VERIFY_IDENTITY);
        }
    }

    public void fillUserIdentifiedForm() {
        log.info("fill the UserIdentified form with Sms");
        sendPause(5);
        if(!isVisible(FIELD_PASSWORD)){
	        Assert.assertTrue(validateElementVisible(FORM_USER_IDENTIFIED), "UserIdentified form is not visible");
	        Captcha captcha=new Captcha();
	        captcha.typeCaptchaIfPresent("987654");
	        click(BUTTON_SEND);
        }
    }

    public void fillSignInWithNewPasswordFormSms(String pass, String passConfirm, String codeSms) {
        log.info("fill the 'Sign in with new password' form by sms");
        Assert.assertTrue(validateElementVisible(FIELD_PASSWORD), "'Sign in with new password' form by sms is not visible");
        type(FIELD_PASSWORD, pass);
        type(FIELD_PASSWORD_CONFIRM, passConfirm);
        type(FIELD_SMS_CODE, codeSms);
        Captcha captcha=new Captcha();
        captcha.typeCaptchaIfPresent("987654");
        click(BUTTON_SIGN_IN);


    }

    public void fillSignInWithNewPasswordFormMail(String pass, String passConfirm) {
        log.info("fill the 'Sign in with new password' form by mail");
        Assert.assertTrue(validateElementVisible(FIELD_PASSWORD), "'Sign in with new password' form by mail is not visible");
        type(FIELD_PASSWORD, pass);
        type(FIELD_PASSWORD_CONFIRM, passConfirm);
        Captcha captcha=new Captcha();
        captcha.typeCaptchaIfPresent("987654");
        click(BUTTON_SIGN_IN);
        if (captcha.validateCaptchaIsVisible()) {
            captcha.typeCaptchaIfPresent("987654");
            click(BUTTON_SIGN_IN);
        }
    }

    @Override
    public String getPageUrl() {
        return pageUrl;
    }

}
