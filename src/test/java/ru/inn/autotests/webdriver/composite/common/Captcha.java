package ru.inn.autotests.webdriver.composite.common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.composite.popups.PopupSecureCode;
import ru.inn.autotests.webdriver.toolkit.LocalDriverManager;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Sergey.Kashapov
 * Date: 10.06.13
 * Time: 17:20
 * To change this template use File | Settings | File Templates.
 */
public class Captcha extends OperationsHelper {
    public static final String CAPTCHA = "xpath=//input[contains(@name, 'captcha') and not(@type='hidden')]";
    WebDriverWait wait = new WebDriverWait(LocalDriverManager.getDriverController().getDriver(), 4);

    public List<WebElement> getListCaptcha(String locator) {
        return findElements(locator);
    }

    //use this function only for popup with one captcha on it
    public void fillCaptchaIfItPresent() {
        By captchaLocator = locatorFactory(CAPTCHA);
        try {
            wait.until(ExpectedConditions.elementToBeClickable(captchaLocator));
            log.info("type captcha and submit");
            List<WebElement> someCaptcha = getListCaptcha(CAPTCHA);
            for (WebElement element : someCaptcha)
                element.sendKeys("987654");
            someCaptcha.get(someCaptcha.size() - 1).submit();

        } catch (Exception e) {

        }
    }

    public void typeCaptcha(String codeCaptcha) {
        By captchaLocator = locatorFactory(PopupSecureCode.CAPTCHA);
        try {
            Assert.assertTrue(validateElementVisible(CAPTCHA), "Captcha is NOT visible but It must be visible!");
            wait.until(ExpectedConditions.elementToBeClickable(captchaLocator));
            List<WebElement> someCaptcha = getListCaptcha(CAPTCHA);
            for (WebElement element : someCaptcha)
                element.sendKeys(codeCaptcha);
        } catch (Exception e) {

        }
    }

    public void typeCaptchaIfPresent(String codeCaptcha) {
        By captchaLocator = locatorFactory(CAPTCHA);
        try {
            wait.until(ExpectedConditions.elementToBeClickable(captchaLocator));
            log.info("type captcha");
            List<WebElement> someCaptcha = getListCaptcha(CAPTCHA);
            for (WebElement element : someCaptcha)
                element.sendKeys(codeCaptcha);
        } catch (Exception e) {

        }
    }

    public boolean validateCaptchaIsVisible(){
        By captchaLocator = locatorFactory(CAPTCHA);
        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(captchaLocator));
           return isVisible(CAPTCHA);
        } catch (Exception e) {
          return false;
        }
    }


}
