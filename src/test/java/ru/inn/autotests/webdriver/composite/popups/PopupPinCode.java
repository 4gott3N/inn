package ru.inn.autotests.webdriver.composite.popups;

import ru.inn.autotests.webdriver.composite.IPage;
import ru.inn.autotests.webdriver.composite.OperationsHelper;

public class PopupPinCode extends OperationsHelper implements IPage {

    String pageUrl = "";

    public static String INPUT_CELL_1 = "name=code[0]";
    public static String INPUT_CELL_2 = "name=code[1]";
    public static String INPUT_CELL_3 = "name=code[2]";
    public static String INPUT_CELL_4 = "name=code[3]";

    public static String INPUT_CAPTCHA = "id=PincodeForm_captcha";

    public static String BUTTON_ACTIVATE = "id=bGiftcode__ButtonSubmit";

    @Override
    public String getPageUrl() {
        return pageUrl;
    }


}



