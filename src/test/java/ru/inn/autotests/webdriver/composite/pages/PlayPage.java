package ru.inn.autotests.webdriver.composite.pages;

import ru.inn.autotests.webdriver.common.language.Language;
import ru.inn.autotests.webdriver.composite.IPage;
import ru.inn.autotests.webdriver.composite.common.StatusHelper;
import ru.inn.autotests.webdriver.games.Game;

/**
 * @author evgenij.tkachenko
 */
public class PlayPage extends StatusHelper implements IPage {

    private String pageUrl;
    
    //LOYALTY ACCOUNT PS2
    public static final String LINK_LOYALTY_PS2 = "css=a[class='bPlayLayout__eMenuItem'][href*='premium']";
    public static final String DIV_LEVEL_LOYALTY = "css=div[data-loyality-lvl='%s']";
    public static final String DIV_LOYALTY_TIME_REMAINING = "css=h3[class='pPS2_bPremium__eTitle']";
    
    //PAGE FEEDS
    public static final String VK_FEED_SWITCHE = "xpath=//*[@data-ft='widgetVk']";
    public static final String TWITTER_FEED_SWITCHE = "xpath=//*[@data-ft='widgetTwitter']";
    
    //shooting range pb
    public static final String DIV_PRELOADER_SHOOTING = "id=game_preloader_text";
    public static final String LINK_SHOOTING = "css=a[href*='shooting']";
    public static final String DISABLED_BUTTON_SHOOT="css=button.bUIButton__mState_disabled";
    public static final String BUTTON_SHOOT = "css=button[class='jsUIButton bUIButton bUIButton__eFrame']";
    public static final String FORM_PRIZE = "id=prize_image";
    public static final String BUTTON_PLAY_AGAIN = "css=form.pPb_bShootingRange__eGame__ePaymentForm:nth-of-type(2)";
    public static final String MESSAGE_NO_PERSONAGE = "css=.pPb_bShootingRange__eGame__eMessage .bInfoMessage";
    
    //Pop-up
    public static final String BUTTON_CLOSE_POPUP = "css=.bUIPopup__eCloseIcon";
    
    
    //Game Store
    public static final String LINK_HOW_TO_BUY = "xpath=//*[@data-ft='howtobuylink']";
    public static final String STORE_ITEM1 = "xpath=//*[@data-ft='gameStoreItem1']";
    public static final String STORE_ITEM2 = "xpath=//*[@data-ft='gameStoreItem2']";
    public static final String STORE_ITEM3 = "xpath=//*[@data-ft='gameStoreItem3']";
    public static final String STORE_ITEM4 = "xpath=//*[@data-ft='gameStoreItem4']";
    
    //Game FORUMS
    public static final String FORUMS_SECTION = "xpath=//*[@data-ft='gameForums']";
   
    //Game Communities
    public static final String COMMUNITIES_SECTION = "xpath=//*[@data-ft='gameCommunities']";

    //ADV pop-up
    public static final String BUTTON_BUY_TICKET_FOR2DAYS_DISCOUNT = "id=pb-promo-popup-btn";
   
    //Preorder l2 classic
	public static final String BUTTON_PREORDER_L2CLASSIC_SUBS_1 = "css=button[class*='bUIButton__mCoupon-1']";
	public static final String BUTTON_PREORDER_L2CLASSIC_SUBS_2 = "css=button[class*='bUIButton__mCoupon-2']";
	public static final String FIELD_FIO_PREORDER_L2CLASSIC = "id=field_fio";
	public static final String BUTTON_ACCEPT_FIO = "id=button_accepte_fio";
	public static final String LINK_CANCEL_FIO = "id=link_cancel_fio";
	public static final String LINK_GO_TO_L2 = "id=link_go_to_l2";
	public static final String DIV_MAIN_MAIL = "id=div_main_mail";
	public static final String BUTTON_VK_SUBSCRIBE_NEWS = "id=button_vk_subscribe_news";
	public static final String DIV_SUCCESS_PREORDER_FPRM = "id=L2prebuy_classic__step3block";
	public static final String DIV_SUM_PREORDER = "css=.bPreorderSubscription__eCoupon__ePrice";

	//Preorder DP
	public static final String BUTTON_PREORDER_DP_PACK_1 = "css=.buy1packDp";
	public static final String BUTTON_PREORDER_DP_PACK_2 = "css=.buy2packDp";
	public static final String BUTTON_APPLY_DP_FREE = "css=.applyForFreeDp"; 
	public static final String DIV_SUM_PREORDER_DP_PACK_1 = "css=.pricePack1";
	public static final String DIV_SUM_PREORDER_DP_PACK_2 = "css=.pricePack2";
	public static final String DIV_PINCODE_DP = "xpath=//p[@class='jsPincodes' and not(.='')]";
	public static final String DIV_MAIL_USER_DP = "xpath=//span[@class='jsUserMail' and not(.='')]";
	
	
    /**
     * Creates instance
     *
     * @param game
     */
    public PlayPage(Game game, Language language) {
        this.pageUrl = game.getURL();
        super.setLanguage(language);
    }

    /**
     * Creates instance, which set language is default (English)
     *
     * @param game
     */
    public PlayPage(Game game) {
        this.pageUrl = game.getURL();
    }

    /**
     * Returns current URL page
     */
    @Override
    public String getPageUrl() {
        return pageUrl;
    }

    
    
}
