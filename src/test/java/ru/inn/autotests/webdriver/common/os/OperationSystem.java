package ru.inn.autotests.webdriver.common.os;

public abstract class OperationSystem {

    private OS osName;
    private boolean is64bit;

    public static final String REGISTER_PATH_64 = "HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\4game";
    public static final String REGISTER_PATH_32 = "HKEY_LOCAL_MACHINE\\SOFTWARE\\4game";//TODO check it

    /**
     * Creates OperationSystem with parameters
     *
     * @param osName  - the name of OS
     * @param is64bit -
     */
    protected OperationSystem(OS osName, boolean is64bit) {
        this.osName = osName;
        this.is64bit = is64bit;
    }

    /**
     * Returns OS name
     */
    public OS getOSName() {
        return osName;
    }

    /**
     * Returns is system 64 bit or not
     */
    public boolean is64bit() {
        return is64bit;
    }

    public String getRegPathToPlugin() {
        if (this.is64bit()) {
            return REGISTER_PATH_64;
        } else {
            return REGISTER_PATH_32;
        }

    }

    public enum OS {
        W764, W732, WV64, WV32, WXP64, WXP32, W864, W832
    }
}
