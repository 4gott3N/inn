package ru.inn.autotests.webdriver.common.os;

public class WindowsSeven64 extends OperationSystem {

    private final static OS osName = OS.W764;
    private final static boolean is64bit = true;

    public WindowsSeven64() {
        super(osName, is64bit);
    }

}
