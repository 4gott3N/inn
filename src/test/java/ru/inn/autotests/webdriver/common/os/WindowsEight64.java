package ru.inn.autotests.webdriver.common.os;

public class WindowsEight64 extends OperationSystem {
    private final static OS osName = OS.W864;
    private final static boolean is64bit = true;

    public WindowsEight64() {
        super(osName, is64bit);
    }
}
