package ru.inn.autotests.webdriver.common.entity;

import org.apache.log4j.Logger;
import org.testng.Assert;
import ru.inn.autotests.webdriver.common.language.Language;
import ru.inn.autotests.webdriver.toolkit.TextProvider;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * The handler for save all attributes of message.
 * Contains specific functions for working with Messages.
 */
/**
 * @author Sergey.Kashapov
 *
 */
public class Message {
	private String from = "";
	private String fromName = "";
	private String toName = "";
    private String subject = "";
    private String body = "";
    private Language language;
    private Color color;
    private String logoLink;
    private String messageId = "";
    private String textInFooter = "";
    private String notificationLink = "";
    private String gameName = "";
    private String titleGameName = "";


	private String contact = "";
    private String sum = "";
    private String mobileNumber="";
    private String codeFromSMS="";
    Logger log =Logger.getLogger("ru.inn.autotests.webdriver.common.Message");


    /**
     * Creates instance for Message class
     * @param subject - message subject
     * @param body - the body of message
     */
    public Message(String subject, String body) {
        this.subject = subject;
        this.body = body;
    }
    
    public Message() {
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getFromName() {
		return fromName;
	}

	public void setFromName(String fromName) {
		this.fromName = fromName;
	}

	public String getToName() {
		return toName;
	}

	public void setToName(String toName) {
		this.toName = toName;
	}

    public String getTitle() {
        return subject;
    }

    public String getBody() {
        return body;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setLogoLink(String logoLink) {
        this.logoLink = logoLink;
    }

    public String getLinkLogo() {
        return logoLink;
    }

    public Color getColor() {
        return color;
    }

    @Override
    public String toString() {
        return subject + " " + body;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getTextInFooter() {
        return textInFooter;
    }

    public void setTextIntoFooter(String textInFooter) {
        this.textInFooter = textInFooter;
    }

    public String getNotificationLink() {
        return notificationLink;
    }

    public void setNotificationLink(String notificationLink) {
        this.notificationLink = notificationLink;
    }

    public String getTitleGameName() {
		return titleGameName;
	}

	public void setTitleGameName(String titleGameName) {
		this.titleGameName = titleGameName;
	}
    
    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getSum() {
        return sum;
    }

    public void setSum(String sum) {
        this.sum = sum;
    }



    public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}



	public String getCodeFromSMS() {
		return codeFromSMS;
	}

	public void setCodeFromSMS(String codeFromSMS) {
		this.codeFromSMS = codeFromSMS;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language lang) {
		this.language = lang;
	}

	/**
     * Enumeration colors of an email
     */
    public static enum Color {
        GRAY("e8e8e8"),BLUE("111111"),GREEN("#4ea53a"),YELLOW("#ffda22");

        String hexColor;

        Color(String hexColor) {
            this.hexColor = hexColor;
        }

        /**
         * Returns the color by hexColor.
         * @param hexColor - Color in the HEX format
         */
        public static Color getColor(String hexColor) {
            for (Color color : Color.values()){
                if (color.hexColor.equals(hexColor)) {
                    return color;
                }
            }
            return GRAY;
        }
    }
    
      public String getCode(){
           return getBody().split("\\D+")[1];
      }
	
    


    /**
     * Returns true if actual message id compares with expected
     * @param messageId - the expected message id
     */
    public boolean compareMessageId(final String messageId) {
        if (messageId.equals("") || messageId == null) {
            throw new IllegalArgumentException("The messageId is null or empty:" + messageId);
        }
        if (this.messageId.equals(messageId)){
            return true;
        } else {
            return false;
        }
    }
    
    
    /** compare color
     * @param color
     */
    public void assertBackgroundColor(final String color) {
        log.info("compare background color "+color);
        if (color.equals("") || color == null) {
            throw new IllegalArgumentException("The color is null or empty:" + color);
        }
        Assert.assertTrue(this.color.equals(Color.valueOf(color)),"Color of the Email is not "+color);
    }
    
    
    /**
     * Just checks from name for English and Russians letters
     */
    public  void checkFromName(String fromName) {
        String from=TextProvider.translator("$From_name", language);
             Assert.assertEquals(from, fromName);
       
    }

    //TODO write all themes in tsl
    public  void checkSubjectMessage(String subj,String expectedTheme) {
       expectedTheme=TextProvider.translator(expectedTheme, language);
        Assert.assertEquals(subj,expectedTheme);
   }
    
    public boolean checkLinkInMessage(final String partOfLink) {

        if (partOfLink.equals("") || partOfLink == null) {
            throw new IllegalArgumentException("The partOfLink is null or empty:" +partOfLink);
        }
        if (this.notificationLink.contains(partOfLink)){
            return true;
        } else {
            return false;
        }
    }
    
    

    /**
     * Returns true if actual sum equals with expected. This method's working with environment and did change, which
     * environment (English, Russian or someone) is viewed.
     * @param sumInMail - the expected sum
     */
    public void assertSum(double sumInMail) {
        log.info("Compare sum from message ");
        Assert.assertEquals(sumInMail,Double.parseDouble(this.sum),"Sum in letter is not equals "+sumInMail);
    }
    
    public void assertContact(String contactInEmail) {
    	Assert.assertTrue(contactInEmail.equals(contact),"The contact in message is not "+contactInEmail + ". It is " + contact);
    }

    public void assertGameName(String game) {
        Assert.assertTrue(game.equals(gameName),"Game name in message is not "+game + ". It is " + gameName);
    }

    public void assertTitleGameName(String gameName) {
        Assert.assertTrue(gameName.equals(gameName),"Game name in message is not "+gameName + ". It is " + gameName);
    }
    
    public void assertTextPresent(String text) {
        Assert.assertTrue(body.contains(text),"The Text " + text + " is not in the message");
    }

    public boolean compareNotificationSettingsLink() {
	   if(notificationLink!=null)
		   return true;
		   else return false;	
	} 

    /**
     * Returns true if email contains russian letters.
     */
    public boolean checkRussianLetters() {
        if (body != null) {
        	Pattern pattern = Pattern.compile("/^[А-Яа-яЁё]+$/u");
            Matcher matcher = pattern.matcher(body);
            if (matcher.find()) {
                return false;
            }
            return true;
        }
        throw new UnsupportedOperationException("Body message is null in the email.");
    }
      
    /**
     * Wait for new message will come
     * @param mailLogin
     * @param numberOfMessagesThatWere
     * @return
     */


	
}