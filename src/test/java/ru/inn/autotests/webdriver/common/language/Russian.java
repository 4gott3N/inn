package ru.inn.autotests.webdriver.common.language;

import java.io.File;

/**
 * Handler for Russian language
 *
 * @author pavel.popov
 */
public class Russian extends Language {
    private final String pathTranslateFile = System.getProperty("user.dir") +
            File.separator + "src" + File.separator + "test" + File.separator + "resources" +
            File.separator + "translate" + File.separator + "Russian.tsl";
    private final String languageName = "Russian";

    public Russian() {
        super();
        super.setLanguageName(languageName);
        super.setPathTranslateFile(pathTranslateFile);
    }
}