package ru.inn.autotests.webdriver.common.os;

public class WindowsVista32 extends OperationSystem {
    private final static OS osName = OS.WV32;
    private final static boolean is64bit = false;

    public WindowsVista32() {
        super(osName, is64bit);
    }
}
