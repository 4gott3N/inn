package ru.inn.autotests.webdriver.common.entity;

import java.util.LinkedList;
import java.util.List;

/**
 * @author pavel.popov
 */
public class TagCase {

    private int caseNumber;

    private String caseDefinition = new String();

    private XMLParameter xmlParameter;

    /**
     * Default constructor
     */
    public TagCase() {
    }

    /**
     * Returns the case number of XML for the current Case
     */
    public int getCaseNumber() {
        return caseNumber;
    }

    /**
     * Set a case number
     *
     * @param caseNumber - the case number into the XML
     */
    public void setCaseNumber(int caseNumber) {
        this.caseNumber = caseNumber;
    }

    /**
     * Returns the definition of case
     */
    public String getCaseDefinition() {
        return caseDefinition;
    }

    /**
     * Sets a case definition
     */
    public void setCaseDefinition(String caseText) {
        this.caseDefinition = caseText;
    }

    /**
     * Returns the instance of XMLParameter
     */
    public XMLParameter getXmlParameters() {
        return xmlParameter;
    }

    /**
     * Sets instance of XMLParameter
     */
    public void setXmlParameter(XMLParameter xmlParameter) {
        this.xmlParameter = xmlParameter;
    }

    /**
     * Returns 'true' if instance is null
     */
    public boolean isNull() {
        return (this.equals(null));
    }

    /**
     * Handler for work with XML.
     *
     * @author pavel.popov
     */
    public static class XMLParameter {
        private List<XMLObject> listXMLObject = new LinkedList<XMLObject>(); //TODO Check with JsonObject

        /**
         * Returns required instance of the XMLObject
         *
         * @param index - number of required instance
         */
        public XMLObject getXMLObject(int index) {
            return listXMLObject.get(index);
        }

        /**
         * Adds instance of the XMLObject into the XMLParameter
         *
         * @param xmlObject - the instance for add into the XMLParameter
         */
        public void addXMLObject(final XMLObject xmlObj) {
            listXMLObject.add(xmlObj);
        }

        /**
         * Returns size of XMLObjects
         */
        public int size() {
            return listXMLObject.size();
        }

        /**
         * Handler for XML Objects. Contains elements, which work with XML.<br>
         * May contain a primitive object:<br>
         * <code>
         * &lt;object&gt;<br>
         * &lt;key&gt;key&lt;/key&gt;<br>
         * &lt;value&gt;value&lt;/value&gt;<br>
         * &lt;/object&gt; <br>
         * </code>
         * or more difficultly:<br>
         * &lt;object&gt;<br>
         * &lt;key&gt;key&lt;/key&gt;<br>
         * &lt;object&gt;<br>
         * &lt;key&gt;key&lt;/key&gt;<br>
         * &lt;value&gt;value&lt;/value&gt;<br>
         * &lt;/object&gt; <br>
         * &lt;/object&gt; <br>
         *
         * @author pavel.popov
         */
        public static class XMLObject {
            private String key = new String();
            private String value = new String();
            private XMLParameter xmlParameter = new XMLParameter();

            /**
             * Returns key value
             */
            public String getKey() {
                return key;
            }

            /**
             * Sets key value
             *
             * @param key - the value of key
             */
            public void setKey(String key) {
                this.key = key;
            }

            /**
             * Returns value of 'value' tag
             */
            public String getValue() {
                return value;
            }

            /**
             * Sets value
             *
             * @param value - the value of 'value' tag
             */
            public void setValue(String value) {
                this.value = value;
            }

            /**
             * Returns instance of xmlParameter
             */
            public XMLParameter getXmlParameter() {
                return xmlParameter;
            }

            /**
             * Sets instance of xmlParameter
             *
             * @param xmlParameter - the child for current xmlObject
             */
            public void setXmlParameter(XMLParameter xmlParameter) {
                this.xmlParameter = xmlParameter;
            }

            /**
             * Returns has or not the child tag in this xmlObject.
             * If it has tag value then child hasn't. If it's another: it has object xmlParameter instead of value.
             */
            public boolean hasChildTag() {
                return value.isEmpty();
            }

            /**
             * Returns 'true' if has xmlParameter into.
             */
            public boolean isChildXMLParameterEnabled() {
                return (xmlParameter != null);
            }
        }
    }
}
