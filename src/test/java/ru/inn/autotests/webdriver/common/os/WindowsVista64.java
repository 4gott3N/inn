package ru.inn.autotests.webdriver.common.os;

public class WindowsVista64 extends OperationSystem {
    private final static OS osName = OS.WV64;
    private final static boolean is64bit = true;

    public WindowsVista64() {
        super(osName, is64bit);
    }
}
