package ru.inn.autotests.webdriver.common.os;

public class WindowsSeven32 extends OperationSystem {
    private final static OS osName = OS.W832;
    private final static boolean is64bit = false;

    public WindowsSeven32() {
        super(osName, is64bit);
    }
}
