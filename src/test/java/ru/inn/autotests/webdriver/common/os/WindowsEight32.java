package ru.inn.autotests.webdriver.common.os;

public class WindowsEight32 extends OperationSystem {
    private final static OS osName = OS.W832;
    private final static boolean is64bit = false;

    public WindowsEight32() {
        super(osName, is64bit);
    }
}
