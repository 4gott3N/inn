package ru.inn.autotests.webdriver.common;


//import ru.inn.autotests.testapi.BackendAPI;
import ru.inn.autotests.webdriver.common.entity.User;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.games.Game;
import ru.inn.autotests.webdriver.toolkit.PlatformUtils;
import ru.inn.autotests.webdriver.toolkit.YamlProvider;

import java.util.Date;

/**
 * Handler for creating and preparing new users.
 *
 * @author Evgeny Tkachenko
 */
public class PrepareUser {

   // private BackendAPI backendAPI;
    private String operatorId;
    // Password for new created user
    final private static String OLD_PASSW = "123456";

    //Another password for user
    final private static String NEW_PASSW = "1234567";

    //How much add money for user
    final private static String MONEY = "1000";


    /**
     * Creates instance for PrepareUser
     */
    public PrepareUser() {
        //  this.backendAPI = new BackendAPI(YamlProvider.getParameter("backend_api_adress", YamlProvider.appConfigs), PlatformUtils.getStageParameters("environment"));
        //  this.operatorId =PlatformUtils.getStageParameters("operatorId");
    }

    /**
     * Creates instance for PrepareUser
     */
    public PrepareUser(String backendApiAdress, String environment, String operatorId) {
        // this.backendAPI = new BackendAPI(backendApiAdress, environment);
        // this.operatorId = operatorId;
    }

    /**
     * Bans user on required period.
     *
     * @param banPeriod - ban time (in seconds)
     * @param game the game
     */
    public void banUser(String banPeriod, Game game) {
        // System.out.println(game.getServiceAccountId());
        // //backendAPI.banUser(game.getServiceAccountId(), game.getServiceId(), banPeriod, false, operatorId);
    }

    /**
     * Bans user on required period.
     *
     * @param banPeriod - ban time (in seconds)
     * @param serviceAccountId the service account id
     */
    public void banUser(String banPeriod, String serviceAccountId) {
        //backendAPI.banUser(serviceAccountId, "0", banPeriod, false, operatorId);
    }

    /**
     * Unbans user
     * @param game the game
     */
    public void unbanUser(Game game) {
        //backendAPI.unbanUser(game.getServiceAccountId(), game.getServiceId(), false, operatorId);
    }

    /**
     * Unbans user
     * @param serviceAccountId the service account id
     */
    public void unbanUser(String serviceAccountId) {
        //backendAPI.unbanUser(serviceAccountId, "", false, operatorId);
    }

    /**
     * Create mobile number.
     *
     * @param user the user
     * @param mobileNumber the mobile number
     */
    public void addMobileNumber(User user,String mobileNumber){
        //backendAPI.addContact(user.getId(), mobileNumber, 3);

    }

    /**
     * Delete mobile number.
     *
     * @param user the user
     */
    public void deleteMobileNumber(User user){
        //backendAPI.deleteContact(user.getId(), 3);

    }

    /**
     * Unlink app.
     *
     * @param user the user
     */
    public void unlinkApp(User user){
        //backendAPI.unlinkApplication(user.getId());

    }


    /**
     * Set master password.
     *
     * @param user the user
     * @param pass the pass
     */
    public void setMasterPassword(User user,String pass){
        //backendAPI.setMasterPassword(user.getId(), pass);

    }

    public void setLastAuthTime(User user, String serviceId, Date lastSuccessfulAuthTime){
        //backendAPI.setLastAuthTime(user.getId(), serviceId, lastSuccessfulAuthTime);

    }


    /**
     * Set beta test property.
     *
     * @param user the user
     * @param state the state
     */
    public void setBetaTestProperty(User user,boolean state){
        //backendAPI.setBetaTestProperty(user.getId(), state);

    }

    /**
     * Set subscription date.
     *
     * @param serviceAccount the service account
     * @param lifeTimeDateFinish the life time date finish
     */
    public void setSubscriptionDate(String serviceAccount,Date lifeTimeDateFinish){
        //backendAPI.setSubscriptionDate(serviceAccount, lifeTimeDateFinish);

    }

    /**
     * Set test account property.
     *
     * @param user the user
     * @param state the state
     */
    public void setTestAccountProperty(User user,boolean state){
        //backendAPI.setTestAccountProperty(user.getId(), state);

    }

    /**
     * Set overdraft limit.
     *
     * @param user the user
     * @param overdraftLimit the overdraft limit
     * @param overdraftState the overdraft state
     */
    public void setOverdraftLimit(User user,String overdraftLimit, boolean overdraftState){
        try{
            //backendAPI.setOverdraftLimit(user.getId(), overdraftLimit,overdraftState);
        }catch (Exception e){}
    }

    /**
     * Delete facebook connect.
     *
     * @param masterAcc the master acc
     */
    public void deleteFacebookConnect(String masterAcc) {
        //backendAPI.deleteContact(masterAcc, 4);

    }

    /**
     * Delete vk connect.
     *
     * @param masterAcc the master acc
     */
    public void deleteVkConnect(String masterAcc) {
        //backendAPI.deleteContact(masterAcc, 5);

    }

    /**
     * Delete mobile.
     *
     * @param masterAcc the master acc
     */
    public void deleteMobile(String masterAcc) {
        //backendAPI.deleteContact(masterAcc, 3);

    }

    /**
     * Changing a main mail
     *
     * @param masterAcc the master acc
     * @param email the email
     */
    public void changeMainMail(String masterAcc, String email) {
        //backendAPI.deleteContact(masterAcc, 1);
        //backendAPI.addContact(masterAcc, email, 1);

    }

    /**
     * Delete main mail.
     *
     * @param masterAcc the master acc
     */
    public void deleteMainMail(String masterAcc) {
        //backendAPI.deleteContact(masterAcc, 1);

    }

    /**
     * Set super security for user.
     *
     * @param user the user
     * @param enable the enable
     */
    public void setSuperSecurityForUser(User user,boolean enable){
        //backendAPI.setSuperSecurityFlag(user.getId(), enable);

    }

    /**
     * Returns created user with added the extra Email, the mobile phone and the game.
     *
     * @param mobilePhone - if variable is empty, mobile phone will be deleted.
     * @param game - currentGame. If variable is empty, service account for current user will not be created.
     * @return the user
     */
    public User prepareUser(String mobilePhone, Game game) {
        User user = new User();
        setPasswordAccount(user.getId(), OLD_PASSW);
        user.setServiceAccountPassword(OLD_PASSW);
        user.setPassword(OLD_PASSW);
        String mainEmail = generateEmail();
        //backendAPI.addContact(user.getId(), mainEmail, 1);
        user.setMainEmail(mainEmail);
        if (game != null) {
            user = createServiceAccountId(user, game);
        }
        if (mobilePhone.equals("") || mobilePhone == null) {
            //backendAPI.deleteContact(user.getId(), 3);
            user.setMobileNumber("");
        } else {
            if (mobilePhone.equals("randomMobilePhone")) {
                mobilePhone = "+" + selectNumber();
            }
            //backendAPI.addContact(user.getId(), mobilePhone, 3);
            user.setMobileNumber(mobilePhone);
        }
        pause(1);
        return user;
    }

    /**
     * Create service account id.
     *
     * @param user the user
     * @param game the game
     * @return the user
     */
    public User createServiceAccountId(User user, Game game) {
        String serviceAccountId = user.getServiceAccountId();
        try{
            //    serviceAccountId = //backendAPI.createServiceAccount(user.getId(), selectNumber(), Integer.parseInt(game.getServiceId()));
        }catch(Exception e){
            System.out.println("There was an error. We can't create a serviceAccount . " + e);
        }
        game.setServiceAccountId(serviceAccountId);
        user.setGame(game);
        user.setServiceAccountId(serviceAccountId);
        return user;
    }

    private void setPasswordAccount(String id, String newMasterPassword) {
        try {
            //backendAPI.setServicePassword(id, NEW_PASSW, newMasterPassword);
        } catch (RuntimeException e) {
            throw new RuntimeException(e.getCause());
        }
    }

    /**
     * Delete all user's personal data
     *
     * @param user - current user
     */
    public void deletePersonalData(User user) {
        //backendAPI.deletePersonalData(user.getId());
        pause(1);
    }

    /**
     * Sets service password.
     *
     * @param serviceAccountId the service account id
     * @param loyaltyPoints the loyalty points
     * @param loyaltyDate the loyalty date
     */
    public void setLoyaltySum(String serviceAccountId, String loyaltyPoints, Date loyaltyDate) {
        //backendAPI.setLoyaltyBonus(serviceAccountId, loyaltyPoints, loyaltyDate);
    }

    /**
     * Sets service password.
     *
     * @param user the user
     */
    public void setServicePassword(User user) {
        if (user.getGame().getServiceAccountId().equals("")) {
            throw new IllegalArgumentException("Service account is empty for current user " + user.toString());
        }
        user.setServiceAccountPassword(NEW_PASSW);
        //backendAPI.setServicePassword(user.getGame().getServiceAccountId(), user.getPassword(),
        // user.getServiceAccountPassword());
    }

    /**
     * Sets service password.
     *
     * @param serviceAccountId the service account id
     * @param masterPassword password for 4game
     * @param newServicePassword new password for game
     */
    public void setServicePassword(String serviceAccountId, String masterPassword, String newServicePassword) {
        if (serviceAccountId.equals("")) {
            throw new IllegalArgumentException("Service account is empty for current user ");
        }
        //backendAPI.setServicePassword(serviceAccountId, masterPassword, newServicePassword);
    }

    /**
     * Add money by backend.
     *
     * @param user the user
     * @param balance TODO
     */
    public void addMoneyByBackend(User user, String balance) {
        //backendAPI.addMoneyToUser(user.getId(), (balance == null) ? MONEY : balance);
        user.setMoney(Double.parseDouble(balance));
    }

    /**
     * Add money by backend.
     *
     * @param id the user id
     * @param balance TODO
     */
    public void addMoneyById(String id, String balance) {
        //backendAPI.addMoneyToUser(id, balance);
    }


    /**
     * Add login to acc.
     *
     * @param user the user
     * @param nameOfLogin the name of login
     * @return the user
     */
    public User addLoginToAcc(User user,String nameOfLogin) {
        //backendAPI.addContact(user.getId(), nameOfLogin, 0);
        user.setLogin(nameOfLogin);
        return user;
    }


    /**
     * Registration by backend.
     *
     * @param game the game
     * @param createServiceAccount the create service account
     * @return the user
     */
    public synchronized User registrationByBackend(Game game, boolean createServiceAccount){
        String mainEMail = generateEmail();
        String login = selectRandomLogin();
        String passw = OLD_PASSW;
        User user = new User();
        //   user.setId(//backendAPI.registerUser(mainEMail, login, passw));
        user.setMainEmail(mainEMail);
        user.setLogin(login);
        user.setPassword(passw);
        String mobileNumber="+001" + selectNumber().substring(4, 12);
        if(createServiceAccount){
            user = createServiceAccountId(user, game);
        }
        //backendAPI.addContact(user.getId(),mobileNumber, 3);
        user.setMobileNumber(mobileNumber);
        setCountry(user, "504"); //Russian's id - 504 or 508
        //backendAPI.acceptLicense(user.getId(), "0", null, null);
        //backendAPI.acceptLicense(user.getId(), "0", "109", null);
        return user;
    }

    /**
     * Registration by backend without mobile.
     *
     * @param game the game
     * @return the user
     */
    public User registrationByBackendWithoutMobile(Game game){
        String mainEMail = generateEmail();
        String login = selectRandomLogin();
        String passw = OLD_PASSW;
        User user = new User();
        //  user.setId(//backendAPI.registerUser(mainEMail, login, passw));
        user.setMainEmail(mainEMail);
        user.setLogin(login);
        user.setPassword(passw);
        setCountry(user, "504"); //Russian's id - 504 or 508
        return user;
    }

    /**
     * Registration by backend.
     *
     * @param game the game
     * @return the user
     */
    public User registrationByBackend(Game game) {
        return registrationByBackend(game, false);
    }

    public String registrationByBackendWithMail(String mainEMail, String password){
        String login = selectRandomLogin();
        //String masterAccountId = //backendAPI.registerUser(mainEMail, login, password);
        //backendAPI.setCountry(masterAccountId, "504"); //Russian's id - 504 or 508
        return "";
        //masterAccountId;
    }

    /**
     * Generate email.
     *
     * @return the string
     */
    public String generateEmail() {
        String randomName = "notifytest.ub" + selectNumber() + "@inn.ru";
        return randomName;
    }

    /**
     * Select number.
     *
     * @return the string
     */
    protected String selectNumber() {
        long currentTime= System.currentTimeMillis();
        String longNumber = Long.toString(currentTime);
        return "1"+longNumber;
    }

    /**
     * Sets country.
     *
     * @param user the user
     * @param countryId the country id
     */
    public void setCountry(User user, String countryId) {
        //backendAPI.setCountry(user.getId(), countryId);
    }

    /**
     * Switch off super security.
     *
     * @param user the user
     * @param flag the flag
     */
    public void switchSuperSecurity(User user, boolean flag) {
        //backendAPI.setSuperSecurityFlag(user.getId(), flag);
    }

    /**
     * Accept a license for service
     *
     * @param user the user
     * @param serviceId the id of the service
     */
    public void acceptLicense(User user, String serviceId) {
        //backendAPI.acceptLicense(user.getId(), serviceId, null, null);
    }

    /**
     * Reject a license for service
     *
     * @param user the user
     * @param serviceId the id of the service
     */
    public void rejectLicense(User user, String serviceId) {
        //backendAPI.rejectLicense(user.getId(), serviceId, null);
    }

    /**
     * Pause void.
     *
     * @param timeOutInSeconds the time out in seconds
     */
    protected void pause(long timeOutInSeconds) {
        try {
            Thread.sleep(timeOutInSeconds * 1000);
        } catch (InterruptedException e) {
            Thread.interrupted();
        }
    }

    /**
     * Select random login.
     *
     * @return the string
     */
    protected String selectRandomLogin() {
        return "qa" + selectNumber().toString().substring(7);
    }

    public void resetCacheIs(String env) {
        //	if(OperationsHelper.baseUrl.contains("test") && !env.contains("live"))
        //	//backendAPI.clearISCache();
    }

    public void resetCacheApi() {
        //	//backendAPI.clearAPICache("api2");
    }

    /**
     * Creates all service accounts for current user
     *
     * @param masterId - the master id for user
     * @deprecated Now this work's done prepareUser(String, String, Game)
     */
    @Deprecated
    public void createAllServiceAccounts(String masterId) {
        for (int i = 1006; i < 1007; i++) {
            //backendAPI.createServiceAccount(masterId, selectNumber(), i);
        }
        for (int i = 1; i < 18; i++) {
            // if (i != 9)
            //backendAPI.createServiceAccount(masterId, selectNumber(), i);
        }
    }

    /**
     * Replicates time from second to another periods
     *
     * @author pavel.popov
     */
    public static class Period {
        /**
         * The constant AN_HOUR.
         */
        public final static String AN_HOUR = "3600";
        /**
         * The constant A_DAY.
         */
        public final static String A_DAY = "86400";
        /**
         * The constant A_MINUTE.
         */
        public final static String A_MINUTE = "60";
        /**
         * The constant A_YEAR.
         */
        public final static String A_YEAR = "31536000";
        /**
         * The constant TWO_DAYS.
         */
        public final static String TWO_DAYS = "172800";
        /**
         * The constant THREE_DAYS.
         */
        public final static String THREE_DAYS = "259200";
        /**
         * The constant FOREVER.
         */
        public final static String FOREVER = "999999999";
    }
}
