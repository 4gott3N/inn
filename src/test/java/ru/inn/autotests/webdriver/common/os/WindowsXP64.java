package ru.inn.autotests.webdriver.common.os;

public class WindowsXP64 extends OperationSystem {
    private final static OS osName = OS.W832;
    private final static boolean is64bit = true;

    public WindowsXP64() {
        super(osName, is64bit);
    }
}
