package ru.inn.autotests.webdriver.common.entity;

import ru.inn.autotests.webdriver.games.Game;

public class User {

    private String id = "";
    private String login = "";
    private String password = "";
    private String mobileNumber = "";
    private String mainEmail = "";
    private String extraEmail = "";
    private Game game;
    private String serviceAccountId = "";
   	private String serviceAccountPassword = "";
    private double money;
    private double bonuses;
    private String environment = "";

  	/**
     * Default constructor
     */
    public User() {

    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * @param login the login to set
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * @return the mobileNumber
     */
    public String getMobileNumber() {
    	return mobileNumber;
    }

    /**
     * @return the ServiceAccountId
     */
    public String getServiceAccountId() {
		return serviceAccountId;
	}

    /**
     * @param ServiceAccountId 
     */
	public void setServiceAccountId(String serviceAccountId) {
		this.serviceAccountId = serviceAccountId;
	}
    
    /**
     * @param mobileNumber the mobileNumber to set
     */
    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    /**
     * @return the mainEmail
     */
    public String getMainEmail() {
        return mainEmail;
    }

    /**
     * @param mainEmail the mainEmail to set
     */
    public void setMainEmail(String mainEmail) {
        this.mainEmail = mainEmail;
    }

    /**
     * @return the extraEmail
     */
    public String getExtraEmail() {
        return extraEmail;
    }

    /**
     * @param extraEmail the extraEmail to set
     */
    public void setExtraEmail(String extraEmail) {
        this.extraEmail = extraEmail;
    }

    /**
     * @return the game
     */
    public Game getGame() {
        return game;
    }
    
    /**
     * @return the environment (qa, dev or live)
     */
    public String getEnvironment() {
  		return environment;
  	}

    /**
     * @param environment (qa, dev or live)
     */
  	public void setEnvironment(String environment) {
  		this.environment = environment;
  	}


    /**
     * @param game the game to set
     */
    public void setGame(Game game) {
        this.game = game;
    }

    /**
     * @return the serviceAccountPassword
     */
    public String getServiceAccountPassword() {
        return serviceAccountPassword;
    }

    /**
     * @param serviceAccountPassword the serviceAccountPassword to set
     */
    public void setServiceAccountPassword(String serviceAccountPassword) {
        this.serviceAccountPassword = serviceAccountPassword;
    }

    /**
     * @return the money
     */
    public double getMoney() {
        return money;
    }

    /**
     * @param money the money to set
     */
    public void setMoney(double money) {
        this.money = money;
    }

    /**
     * @return the bonuses
     */
    public double getBonuses() {
        return bonuses;
    }

    /**
     * @param bonuses the bonuses to set
     */
    public void setBonuses(double bonuses) {
        this.bonuses = bonuses;
    }

}

