package ru.inn.autotests.webdriver.common.os;

/**
 * Instance for Windows XP 32bit
 *
 * @author pavel.popov
 */
public class WindowsXP32 extends OperationSystem {

    private final static OS osName = OS.WXP32;
    private final static boolean is64bit = false;

    public WindowsXP32() {
        super(osName, is64bit);
    }
}
