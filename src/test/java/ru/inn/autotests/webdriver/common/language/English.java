package ru.inn.autotests.webdriver.common.language;

import java.io.File;

/**
 * Handler for English language
 *
 * @author pavel.popov
 */
public class English extends Language {
    private final String pathTranslateFile = System.getProperty("user.dir") +
            File.separator + "src" + File.separator + "test" + File.separator + "resources" +
            File.separator + "translate" + File.separator + "English.tsl";
    private final String languageName = "English";

    /**
     * Default constructor
     */
    public English() {
        super();
        super.setLanguageName(languageName);
        super.setPathTranslateFile(pathTranslateFile);
    }
}