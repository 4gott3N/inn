package ru.inn.autotests.webdriver.common.language;

/**
 * Handler for language translator
 *
 * @author pavel.popov
 */
public abstract class Language {

    private String pathTranslateFile = new String();
    private String languageName = new String();

    /**
     * Default constructor
     */
    protected Language() {
    }

    public static Language getLang(String languageName) {
        if (languageName.equals("ru")) {
            return new Russian();
        }
        else if(languageName.equals("eu")){
            return new English();
        }
        else
        {
            return new Brazilian();
        }

    }

    /**
     * Sets path to translate file
     *
     * @param pathTranslateFile
     */
    protected void setPathTranslateFile(String pathTranslateFile) {
        this.pathTranslateFile = pathTranslateFile;
    }

    /**
     * Sets name of Language
     *
     * @param languageName
     */
    protected void setLanguageName(String languageName) {
        this.languageName = languageName;
    }

    /**
     * Returns the translate file path
     */
    public String getTranslateFilePath() {
        return pathTranslateFile;
    }

    /**
     * Returns the name of Language
     */
    public String getNameLanguage() {
        return languageName;
    }
}