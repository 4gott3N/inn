package ru.inn.autotests.webdriver.common.language;

import java.io.File;

/**
 * Handler for German language
 *
 * @author pavel.popov
 */
public class German extends Language {
    private final String pathTranslateFile = System.getProperty("user.dir") +
            File.separator + "src" + File.separator + "test" + File.separator + "resources" +
            File.separator + "translate" + File.separator + "German.tsl";
    private final String languageName = "German";

    /**
     * Default constructor
     */
    public German() {
        super();
        super.setLanguageName(languageName);
        super.setPathTranslateFile(pathTranslateFile);
    }
}