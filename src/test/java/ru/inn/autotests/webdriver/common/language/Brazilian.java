package ru.inn.autotests.webdriver.common.language;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: Sergey.Kashapov
 * Date: 25.11.13
 * Time: 11:53
 * To change this template use File | Settings | File Templates.
 */
public class Brazilian extends Language  {
    private final String pathTranslateFile = System.getProperty("user.dir") +
            File.separator + "src" + File.separator + "test" + File.separator + "resources" +
            File.separator + "translate" + File.separator + "Brazilian.tsl";
    private final String languageName = "Brazilian";

    /**
     * Default constructor
     */
    public Brazilian() {
        super();
        super.setLanguageName(languageName);
        super.setPathTranslateFile(pathTranslateFile);
    }
}
