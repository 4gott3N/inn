package ru.inn.autotests.webdriver.games;

public class Lineage2de extends Game {
    private final static GameEnum gameName = GameEnum.LINEAGE2DE;

    private final static String serviceId = "1007";

    private final static String gameSize = "6.31";

    private final static String url = "/lineage2de/";

    private final static String processName = "L2.exe";

    private final static String appName = "lineage2de";

    private final static String gameApplicationTitle = "Lineage II";

    @Override
    public String getProcessName() {
        return processName;
    }

    public Lineage2de() {
        super(gameName, url, serviceId, gameSize, appName);
    }

    public void setServiceAccountId(String serviceAccountId) {
        super.serviceAccountId = serviceAccountId;
    }

    @Override
    public String getGameApplicationTitle() {
        return gameApplicationTitle;
    }
}
