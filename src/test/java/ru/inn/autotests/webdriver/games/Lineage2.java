package ru.inn.autotests.webdriver.games;

public class Lineage2 extends GamePremium {
    private final static GameEnum gameName = GameEnum.LINEAGE2;

    private final static String serviceId = "6";

    private final static String url = "/lineage2/";

    private final static String gameSize = "7.11";

    private final static String processName = "L2.exe";

    private final String[] lineage2Coupons = {"1 Day", "2 Days", "7 Days", "1 Month", "3 Months", "6 Months"};

    private final static String appName = "lineage2";

    private final static String gameApplicationTitle = "Lineage II";

    private Premium premium = new Premium();

    @Override
    public String getProcessName() {
        return processName;
    }

    public Lineage2() {
        super(gameName, url, serviceId, gameSize, appName);
    }

    public void setServiceAccountId(String serviceAccountId) {
        super.serviceAccountId = serviceAccountId;
    }

    @Override
    public Premium getPremium() {
        return premium.generatePremiumCoupons(lineage2Coupons);
    }

    @Override
    public String getGameApplicationTitle() {
        return gameApplicationTitle;
    }
}
