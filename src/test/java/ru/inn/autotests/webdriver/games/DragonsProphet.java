package ru.inn.autotests.webdriver.games;

/**
 * Created with IntelliJ IDEA.
 * User: Sergey.Kashapov
 * Date: 18.07.13
 * Time: 17:59
 * To change this template use File | Settings | File Templates.
 */
public class DragonsProphet  extends Game{
    private final static Game.GameEnum gameName = GameEnum.DRAGONS_PROPHET;

    private final static String serviceId = "17";

    private final static String url = "/dragonsprophet/";

    private final static String gameSize = "???";

    private final static String processName = "dragonsprophet.exe";

    private final static String appName = "dragonsprophet";

    private final static String gameApplicationTitle = "Dragon's Prophet";

    @Override
    public String getProcessName() {
        return processName;
    }

    public DragonsProphet() {
        super(gameName, url, serviceId, gameSize, appName);
    }

    @Override
    public void setServiceAccountId(String serviceAccountId) {
        super.serviceAccountId = serviceAccountId;
    }

    @Override
    public String getGameApplicationTitle() {
        return gameApplicationTitle;
    }
}
