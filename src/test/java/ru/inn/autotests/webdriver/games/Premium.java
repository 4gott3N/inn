package ru.inn.autotests.webdriver.games;

import java.util.ArrayList;
import java.util.List;

/**
 * Handler for Premium coupons for a premium game
 *
 * @author pavel.popov
 */
public class Premium {

    private List<TimeSlot> coupons = new ArrayList<TimeSlot>();

    public void setCoupon(TimeSlot period) {
        coupons.add(period);
    }

    /**
     * Returns coupons from array list for required index
     *
     * @param index - the required index
     */
    public TimeSlot getCoupon(int index) {
        return coupons.get(index);
    }

    /**
     * Generates premium coupons by String array. <br>
     * <code>ex: arrCoupons = {'1 Day', '5 Days', '1 Month', '3 Years'}.<br>
     * setCoupons returns: List&lt;TimeSlot&gt; {new Day(1), new Day(5), new Month(1), new Year(3)}
     * </code>
     *
     * @param arrCoupons
     * @return premium - premium with generate's coupons
     */
    public Premium generatePremiumCoupons(String[] arrCoupons) {
        Premium premium = new Premium();
        if (arrCoupons.length == 0) {
            throw new IllegalArgumentException("arrCoupons is empty");
        }

        for (String strCoupon : arrCoupons) {
            if (strCoupon.contains(Day.NAME)) {
                String day = strCoupon.split(" ")[0]; // '1 Day' = '1'
                premium.setCoupon(new Day(Integer.parseInt(day)));
            } else if (strCoupon.contains(Month.NAME)) {
                String month = strCoupon.split(" ")[0]; // '1 Month' = '1'
                premium.setCoupon(new Month(Integer.parseInt(month)));
            } else if (strCoupon.contains(Year.NAME)) {
                String year = strCoupon.split(" ")[0]; // '1 Month' = '1'
                premium.setCoupon(new Year(Integer.parseInt(year)));
            } else {
                throw new IllegalArgumentException("Check arrCoupons, error: " + strCoupon);
            }
        }

        return premium;
    }

    /**
     * Handler for Time periods. Contains period and transforms <br>
     * it to the required period of the time (Day, Month or Year)
     *
     * @author pavel.popov
     */
    public class TimeSlot {
        private int countOfPeriod;

        public TimeSlot(int countOfPeriod) {
            this.countOfPeriod = countOfPeriod;
        }

        public int getCountOfPeriod() {
            return countOfPeriod;
        }
    }

    /**
     * @author pavel.popov
     */
    public class Day extends TimeSlot {
        public static final String NAME = "Day";

        public Day(int countOfPeriod) {
            super(countOfPeriod);
        }
    }

    /**
     * @author pavel.popov
     */
    public class Month extends TimeSlot {
        public static final String NAME = "Month";

        public Month(int countOfPeriod) {
            super(countOfPeriod);
        }
    }

    /**
     * @author pavel.popov
     */
    public class Year extends TimeSlot {
        public static final String NAME = "Year";

        public Year(int countOfPeriod) {
            super(countOfPeriod);
        }
    }
}
