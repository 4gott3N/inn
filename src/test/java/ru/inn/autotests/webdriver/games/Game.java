package ru.inn.autotests.webdriver.games;

/**
 * Handler for the Game. <br>
 * Contains a name of the game and information about the game.
 *
 * @author pavel.popov
 */
public abstract class Game {

    private GameEnum gameName;

    private String serviceId = "";

    private String url = "";

    private String gameSize = "";

    private String gamePath = "";

    private String appName = "";

    private String processName = "";
    
    private boolean superSecurity = false;

    private String gameSikuliImagesPath = "";

    private String gameApplicationTitle = "";

	protected String serviceAccountId = "";


/** fields of mobile game **/

    public  String iOsButtonLocator = "";
    public  String googlePlayButtonLocator = "";
    public  String gameCellVideoLocator = "";
    public  String appStoreId = "";
    public  String googlePlayId = "";

/***********/

    /**
     * Constructor for game
     */
    public Game(GameEnum gameName, String url, String serviceId, String gameSize, String appName) {
        this.gameName = gameName;
        this.serviceId = serviceId;
        this.url = url;
        this.gameSize = gameSize;
        this.appName = appName;

        this.gameSikuliImagesPath = System.getProperty("user.dir") +
                                    "\\src\\test\\resources\\images.sikuli\\games\\" + gameName + "\\";
    }

    /**
     * Constructor for mobile game
     */
    public Game(GameEnum gameName, String url, String serviceId)
    {
        this.gameName = gameName;
        this.serviceId = serviceId;
        this.url = url;
    }

    /**
     * Returns the process name for specific game
     */


    public abstract String getProcessName();

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    /**
     * Sets the game name.
     *
     * @param gameName - single enumeration of the game
     */
    public void setGameName(GameEnum gameName) {
        this.gameName = gameName;
    }

    /**
     * Returns app name for current game
     */
    public String getAppName(){
        return appName;
    }

    /**
     * Sets the game path
     *
     * @param path
     */
    public void setGamePath(String path) {
        this.gamePath = path;
    }

    /**
     * Sets service id for the game
     *
     * @param serviceId
     */
    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    /**
     * Sets URL of the game
     *
     * @param url
     */
    public void setURL(String url) {
        this.url = url;
    }

    /**
     * Sets the game size
     *
     * @param gameSize
     */
    public void setGameSize(String gameSize) {
        this.gameSize = gameSize;
    }

    /**
     * Returns the game name
     */
    public GameEnum getGameName() {
        return gameName;
    }

    /**
     * Returns the service id
     */
    public String getServiceId() {
        return serviceId;
    }

    /**
     * Returns the URL from the game
     */
    public String getURL() {
        return url;
    }

    /**
     * Returns the game size
     */
    public String getGameSize() {
        return gameSize;
    }

    /**
     * Returns the game path
     */
    public String getGamePath() {
        return gamePath;
    }

    public boolean isSuperSecurity() {
		return superSecurity;
	}

	public void setSuperSecurity(boolean superSecurity) {
		this.superSecurity = superSecurity;
	}

    public String getGameSikuliImagesPath() {
        return gameSikuliImagesPath;
    }

    public void setGameSikuliImagesPath(String gameSikuliImagesPath) {
        this.gameSikuliImagesPath = gameSikuliImagesPath;
    }

    public abstract String getGameApplicationTitle();

    /**
	 * Sets the service account for each game
	 *
	 * @param serviceAccountId
	 */
	public abstract void setServiceAccountId(String serviceAccountId);

	/**
	 * Returns the service account id
	 */
	public String getServiceAccountId() {
	    return serviceAccountId;
	}

	public void setGameApplicationTitle(String gameApplicationTitle) {
        this.gameApplicationTitle = gameApplicationTitle;
    }

    public static Game getGame(GameEnum gameEnum) {
        switch (gameEnum) {
            case AION:
                return new Aion();
            case APB:
                return new APB();
            case ATLANTIKA:
                return new Atlantika();
            case LINEAGE2:
                return new Lineage2();
            case LINEAGE2DE:
                return new Lineage2de();
            case LINEAGE2EU:
                return new Lineage2eu();
            case LINEAGE2PL:
                return new Lineage2pl();
            case POINTBLANK:
                return new PointBlank();
            case R2ONLINE:
                return new R2Online();
            case RFONLINE:
                return new RFOnline();
            case PLANETSIDE2:
                return new PlanetSide2();
            case OFC:
                return new Ofc();
            case OFCBR:
                return new Ofcbr();
            case XXX:
                return new XXX();
            case LINEAGE2CLASSIC:
                return new Lineage2Classic();
            case CHAOSFIGHTERS:
                return new ChaosFighters();
            case POCKETFORT:
                return new PocketFort();

            default:
                throw new IllegalArgumentException("Didn't find the game " + gameEnum);
        }
    }

	/**
     * Enumeration of the Name of Game
     *
     * @author pavel.popov
     */
    public enum GameEnum {
        AION, LINEAGE2, LINEAGE2EU, LINEAGE2DE,LINEAGE2PL, POINTBLANK, R2ONLINE, ATLANTIKA, ACEONLINE, FREESTYLE, BLOODLINECHAMPIONS, APB, RFONLINE, PLANETSIDE2, OFC,DRAGONS_PROPHET, OFCBR, XXX, POCKETFORT,
        LINEAGE2CLASSIC,
        CHAOSFIGHTERS


    }

    /**
     * @author pavel.popov
     */
    public static class GameName {
        private static final String AION = "Aion";
        private static final String LINEAGE2 = "Lineage 2";
        private static final String LINEAGE2EU = "Lineage 2 EU";
        private static final String LINEAGE2DE = "Lineage 2 DE";
        private static final String LINEAGE2PL = "Lineage 2 PL";
        private static final String POINTBLANK = "Point Blank";
        private static final String R2ONLINE = "R2 Online";
        private static final String ATLANTIKA = "atlantica";
        private static final String ACEONLINE = "Ace Online";
        private static final String FREESTYLE = "FreeStyle";
        private static final String BLOODLINECHAMPIONS = "Bloodline Champions";
        private static final String APB = "APB Reloaded";
        private static final String RFONLINE = "RF Online";
        private static final String PLANETSIDE2="PlanetSide 2";
        private static final String OFC="Offensive Combat";
        private static final String OFCBR="Offensive CombatBR";
        private static final String DRAGONS_PROPHET="Dragons Prophet";
        private static final String XXX="XXXOnline";
        private static final String POCKETFORT="Pocket Fort";
        private static final String LINEAGE2CLASSIC="Lineage 2 Classic";


        private static final String CHAOSFIGHTERS = "Chaos Fighters";
        /**
         * Returns the game name by GameEnum
         *
         * @param gameEnum
         */
        public static String getGameName(GameEnum gameEnum) {
            switch (gameEnum) {
                case ACEONLINE:
                    return ACEONLINE;
                case AION:
                    return AION;
                case APB:
                    return APB;
                case ATLANTIKA:
                    return ATLANTIKA;
                case BLOODLINECHAMPIONS:
                    return BLOODLINECHAMPIONS;
                case FREESTYLE:
                    return FREESTYLE;
                case LINEAGE2:
                    return LINEAGE2;
                case LINEAGE2DE:
                    return LINEAGE2DE;
                case LINEAGE2PL:
                    return LINEAGE2PL;
                case LINEAGE2EU:
                    return LINEAGE2EU;
                case POINTBLANK:
                    return POINTBLANK;
                case R2ONLINE:
                    return R2ONLINE;
                case RFONLINE:
                    return RFONLINE;
                case PLANETSIDE2:
                    return PLANETSIDE2;
                case OFC:
                    return OFC;
                case OFCBR:
                    return OFCBR;
                case XXX:
                    return XXX;
                case POCKETFORT:
                    return POCKETFORT;
                case LINEAGE2CLASSIC:
                    return LINEAGE2CLASSIC;
                case CHAOSFIGHTERS:
                    return CHAOSFIGHTERS;

                default:
                    throw new IllegalArgumentException("Didn't find the game " + gameEnum);
            }
        }




        public static GameEnum selectByGameName(String gameName) {
            if (gameName.equals(ACEONLINE)) {
                return GameEnum.ACEONLINE;
            } else if (gameName.equals(AION)) {
                return GameEnum.AION;
            } else if (gameName.equals(APB)) {
                return GameEnum.APB;
            } else if (gameName.equals(ATLANTIKA)) {
                return GameEnum.ATLANTIKA;
            } else if (gameName.equals(BLOODLINECHAMPIONS)) {
                return GameEnum.BLOODLINECHAMPIONS;
            } else if (gameName.equals(FREESTYLE)) {
                return GameEnum.FREESTYLE;
            } else if (gameName.equals(LINEAGE2)) {
                return GameEnum.LINEAGE2;
            } else if (gameName.equals(LINEAGE2DE)) {
                return GameEnum.LINEAGE2DE;
            }else if (gameName.equals(LINEAGE2PL)) {
                return GameEnum.LINEAGE2PL;
            }else if (gameName.equals(LINEAGE2EU)) {
                return GameEnum.LINEAGE2EU;
            } else if (gameName.equals(POINTBLANK)) {
                return GameEnum.POINTBLANK;
            } else if (gameName.equals(R2ONLINE)) {
                return GameEnum.R2ONLINE;
            } else if (gameName.equals(RFONLINE)) {
                return GameEnum.RFONLINE;
            } else if (gameName.equals(PLANETSIDE2)) {
                return GameEnum.PLANETSIDE2;
            } else if (gameName.equals(OFC)) {
                return GameEnum.OFC;
            } else if (gameName.equals(OFCBR)) {
                return GameEnum.OFCBR;
            } else if (gameName.equals(XXX)) {
                return GameEnum.XXX;
            } else if (gameName.equals(POCKETFORT)) {
                return GameEnum.POCKETFORT;
            } else if (gameName.equals(LINEAGE2CLASSIC)) {
                return GameEnum.LINEAGE2CLASSIC;
            }
            else if (gameName.equals(CHAOSFIGHTERS)) {
                    return GameEnum.CHAOSFIGHTERS;
            } else {
                throw new IllegalArgumentException("Didn't find the game " + gameName);
            }
        }
    }
    

}
