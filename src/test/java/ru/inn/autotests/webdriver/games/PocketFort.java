package ru.inn.autotests.webdriver.games;

public class PocketFort extends Game {
    private final static GameEnum gameName = GameEnum.POCKETFORT;

    private final static String serviceId = "26";

    private final static String url = "/pocket-fort/";

    @Override
    public String getProcessName() {
        return null;
    }

    @Override
    public String getGameApplicationTitle() {
        return null;
    }


    public PocketFort() {
        super(gameName, url, serviceId);

        appStoreId = "908194157";
        googlePlayId = "eu.inn.pocketfort.live";

        iOsButtonLocator = "xpath=//a[contains(@href, 'https://itunes.apple.com/app/id908194157')]";
        googlePlayButtonLocator = "xpath=//a[contains(@href, 'https://play.google.com/store/apps/details?id=eu.inn.pocketfort.live')]";
    }

    @Override
    public void setServiceAccountId(String serviceAccountId) {
        super.serviceAccountId = serviceAccountId;
    }


}
