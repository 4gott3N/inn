package ru.inn.autotests.webdriver.games;

public class Atlantika extends Game {
    private final static GameEnum gameName = GameEnum.ATLANTIKA;

    private final static String serviceId = "13";

    private final static String url = "/atlantica/";

    private final static String gameSize = "3.97";

    private final static String processName = "atlantica.exe";

    private final static String appName = "atlantica";

    private final static String gameApplicationTitle = "Атлантика Онлайн";

    @Override
    public String getProcessName() {
        return processName;
    }

    public Atlantika() {
        super(gameName, url, serviceId, gameSize, appName);
    }

    @Override
    public void setServiceAccountId(String serviceAccountId) {
        super.serviceAccountId = serviceAccountId;
    }

    @Override
    public String getGameApplicationTitle() {
        return gameApplicationTitle;
    }
}
