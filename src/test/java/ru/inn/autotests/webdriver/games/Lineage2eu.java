package ru.inn.autotests.webdriver.games;

public class Lineage2eu extends GamePremium {

    private final static GameEnum gameName = GameEnum.LINEAGE2EU;

    private final static String serviceId = "1006";

    private final static String url = "/lineage2/";

    private final static String gameSize = "6.86";

    private final String[] lineage2Coupons = {"1 Day", "2 Days", "7 Days", "1 Month", "3 Months", "6 Months"};

    private Premium premium = new Premium();

    private final static String processName = "L2.exe";

    public final static String appName = "lineage2eu";

    private final static String gameApplicationTitle = "Lineage II";

    @Override
    public String getProcessName() {
        return processName;
    }

    public Lineage2eu() {
        super(gameName, url, serviceId, gameSize, appName);
    }

    public void setServiceAccountId(String serviceAccountId) {
        super.serviceAccountId = serviceAccountId;
    }

    @Override
    public Premium getPremium() {
        return premium.generatePremiumCoupons(lineage2Coupons);
    }

    @Override
    public String getGameApplicationTitle() {
        return gameApplicationTitle;
    }
}
