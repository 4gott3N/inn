package ru.inn.autotests.webdriver.games;

public class Ofc extends Game {

    private final static GameEnum gameName = GameEnum.OFC;

    private final static String serviceId = "20";

    private final static String url = "/ofc/";

    private final static String gameSize = "";

    private final static String processName = "";

    private final static String appName = "ofc";

    private final static String gameApplicationTitle = "";

    @Override
    public String getProcessName() {
        return processName;
    }

    public Ofc() {
        super(gameName, url, serviceId, gameSize, appName);
    }

    @Override
    public void setServiceAccountId(String serviceAccountId) {
        super.serviceAccountId = serviceAccountId;
    }

    @Override
    public String getGameApplicationTitle() {
        return gameApplicationTitle;
    }
}
