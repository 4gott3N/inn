package ru.inn.autotests.webdriver.games;

public class XXX extends Game {
    private final static GameEnum gameName = GameEnum.XXX;

    private final static String serviceId = "501";

    private final static String url = "/xxx/";

    private final static String gameSize = "";

    private final static String processName = "DisplayArgs.exe";

    private final static String appName = "XXXOnline";

    private final static String gameApplicationTitle = "XXXOnline";

    @Override
    public String getProcessName() {
        return processName;
    }

    public XXX() {
        super(gameName, url, serviceId, gameSize, appName);
    }

    @Override
    public void setServiceAccountId(String serviceAccountId) {
        super.serviceAccountId = serviceAccountId;
    }

    @Override
    public String getGameApplicationTitle() {
        return gameApplicationTitle;
    }
}
