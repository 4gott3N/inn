package ru.inn.autotests.webdriver.games;

public class PointBlank extends Game {

    private final static GameEnum gameName = GameEnum.POINTBLANK;

    private final static String serviceId = "8";

    private final static String url = "/pointblank/";

    private final static String gameSize = "736.25";

    private final static String processName = "PointBlank.exe";

    private final static String appName = "pointblank";

    private final static String gameApplicationTitle = "Point Blank";

    @Override
    public String getProcessName() {
        return processName;
    }

    public PointBlank() {
        super(gameName, url, serviceId, gameSize, appName);
    }

    @Override
    public void setServiceAccountId(String serviceAccountId) {
        super.serviceAccountId = serviceAccountId;
    }

    @Override
    public String getGameApplicationTitle() {
        return gameApplicationTitle;
    }
}
