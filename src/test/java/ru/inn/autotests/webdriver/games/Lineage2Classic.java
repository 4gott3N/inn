package ru.inn.autotests.webdriver.games;

public class Lineage2Classic extends GamePremium {
    private final static GameEnum gameName = GameEnum.LINEAGE2CLASSIC;

    private final static String serviceId = "33";

    private final static String url = "/lineage2classic/";

    private final static String gameSize = "7.11";

    private final static String processName = "L2.exe";

    private final String[] lineage2Coupons = {"1 Month", "3 Months"};

    private final static String appName = "lineage2classic";

    private final static String gameApplicationTitle = "Lineage II";

    private Premium premium = new Premium();

    @Override
    public String getProcessName() {
        return processName;
    }

    public Lineage2Classic() {
        super(gameName, url, serviceId, gameSize, appName);
    }

    public void setServiceAccountId(String serviceAccountId) {
        super.serviceAccountId = serviceAccountId;
    }

    @Override
    public Premium getPremium() {
        return premium.generatePremiumCoupons(lineage2Coupons);
    }

    @Override
    public String getGameApplicationTitle() {
        return gameApplicationTitle;
    }
}
