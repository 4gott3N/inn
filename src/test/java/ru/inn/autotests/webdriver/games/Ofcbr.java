package ru.inn.autotests.webdriver.games;

public class Ofcbr extends Game {

    private final static GameEnum gameName = GameEnum.OFCBR;

    private final static String serviceId = "3020";

    private final static String url = "/ofc/";

    private final static String gameSize = "";

    private final static String processName = "";

    private final static String appName = "ofcbr";

    private final static String gameApplicationTitle = "";

    @Override
    public String getProcessName() {
        return processName;
    }

    public Ofcbr() {
        super(gameName, url, serviceId, gameSize, appName);
    }

    @Override
    public void setServiceAccountId(String serviceAccountId) {
        super.serviceAccountId = serviceAccountId;
    }

    @Override
    public String getGameApplicationTitle() {
        return gameApplicationTitle;
    }
}
