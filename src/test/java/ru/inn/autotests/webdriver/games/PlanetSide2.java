package ru.inn.autotests.webdriver.games;

public class PlanetSide2 extends GamePremium {

    private final static GameEnum gameName = GameEnum.PLANETSIDE2;

    private final static String serviceId = "19";

    private final static String url = "/planetside2/";

    private final static String gameSize = "9.85";

    private final static String processName = "PS2.exe";

    private final static String appName = "planetside2";

    private final static String gameApplicationTitle = "Planetside2";
    
    private Premium premium = new Premium();
    
    private final String[] ps2Coupons = {"1 Month", "3 Months", "6 Months", "12 Months"};

    @Override
    public String getProcessName() {
        return processName;
    }

    public PlanetSide2() {
        super(gameName, url, serviceId, gameSize, appName);
    }

    @Override
    public void setServiceAccountId(String serviceAccountId) {
        super.serviceAccountId = serviceAccountId;
    }

    @Override
    public String getGameApplicationTitle() {
        return gameApplicationTitle;
    }

    @Override
    public Premium getPremium() {
        return premium.generatePremiumCoupons(ps2Coupons);
    }
}
