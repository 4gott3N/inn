package ru.inn.autotests.webdriver.games;

public class APB extends GamePremium {
    private final static GameEnum gameName = GameEnum.APB;

    private final static String serviceId = "17";
    
    private final static String url = "/apb/";

    private final static String gameSize = "4.03";

    private final static String processName = "APB.exe";

    private final static String appName = "apb";

    private final static String gameApplicationTitle = "APB Reloaded";

    private final String[] apbCoupons = {"1 Day", "2 Days", "7 Days", "1 Month", "3 Months", "6 Months"};

    private Premium premium = new Premium();
    @Override
    public String getProcessName() {
        return processName;
    }

    public APB() {
        super(gameName, url, serviceId, gameSize, appName);
    }

    @Override
    public Premium getPremium() {
        return premium.generatePremiumCoupons(apbCoupons);
    }

    @Override
    public void setServiceAccountId(String serviceAccountId) {
        super.serviceAccountId = serviceAccountId;
    }

    @Override
    public String getGameApplicationTitle() {
        return gameApplicationTitle;
    }
}
