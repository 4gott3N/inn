package ru.inn.autotests.webdriver.games;

public class RFOnline extends Game {
    private final static GameEnum gameName = GameEnum.RFONLINE;

    private final static String serviceId = "1";

    private final static String url = "/rf/";

    private final static String gameSize = "2.05";

//    private String startProcessName = "RFOnlineUI.exe";
//
//    private String gameProcessName = "rf_online.bin.exe";

    private String processName = "rf.exe";

    private final static String appName = "rf";

    private final static String gameApplicationTitle = "RF Online";

    @Override
    public String getProcessName() {
        return processName;
    }

    @Override
    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public RFOnline() {
        super(gameName, url, serviceId, gameSize, appName);
    }

    @Override
    public void setServiceAccountId(String serviceAccountId) {
        super.serviceAccountId = serviceAccountId;
    }

    @Override
    public String getGameApplicationTitle() {
        return gameApplicationTitle;
    }
}
