package ru.inn.autotests.webdriver.games;

/**
 * Created with IntelliJ IDEA.
 * User: Sergey.Kashapov
 * Date: 31.07.13
 * Time: 17:25
 * To change this template use File | Settings | File Templates.
 */
public class Lineage2pl extends Game {
    private final static GameEnum gameName = GameEnum.LINEAGE2DE;

    private final static String serviceId = "1008";

    private final static String gameSize = "6.31";

    private final static String url = "/lineage2pl/";

    private final static String processName = "L2.exe";

    private final static String appName = "lineage2pl";

    private final static String gameApplicationTitle = "Lineage II";

    @Override
    public String getProcessName() {
        return processName;
    }

    public Lineage2pl() {
        super(gameName, url, serviceId, gameSize, appName);
    }

    public void setServiceAccountId(String serviceAccountId) {
        super.serviceAccountId = serviceAccountId;
    }

    @Override
    public String getGameApplicationTitle() {
        return gameApplicationTitle;
    }
}
