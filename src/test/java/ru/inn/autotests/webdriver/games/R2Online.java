package ru.inn.autotests.webdriver.games;

public class R2Online extends Game {

    private final static GameEnum gameName = GameEnum.R2ONLINE;

    private final static String serviceId = "2";

    private final static String url = "/r2/";

    private final static String gameSize = "2.96";

    //private final static String processName = "r2client.exe";

    private final static String processName = "r2client.exe";
    
    private final static String appName = "r2";

    private final static String gameApplicationTitle = "R2";

    public R2Online() {
        super(gameName, url, serviceId, gameSize, appName);
    }

    @Override
    public String getProcessName() {
        return processName;
    }

    @Override
    public void setServiceAccountId(String serviceAccountId) {
        super.serviceAccountId = serviceAccountId;
    }

    @Override
    public String getGameApplicationTitle() {
        return gameApplicationTitle;
    }

}
