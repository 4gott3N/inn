package ru.inn.autotests.webdriver.games;

/**
 * Handler for Game with Premium
 *
 * @author pavel.popov
 */
public abstract class GamePremium extends Game {

    /**
     * Returns Premium for the Game
     */
    public abstract Premium getPremium();

    /**
     * Constructor for game with Premium
     */
    public GamePremium(GameEnum gameName, String url, String serviceId, String gameSize, String appName) {
        super(gameName, url, serviceId, gameSize, appName);
    }

    /**
     * Sets the service account for each game
     *
     * @param serviceAccountId
     */
    @Override
    public abstract void setServiceAccountId(String serviceAccountId);
}
