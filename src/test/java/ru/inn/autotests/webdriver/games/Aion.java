package ru.inn.autotests.webdriver.games;

public class Aion extends GamePremium {

    private final static GameEnum gameName = GameEnum.AION;

    private final static String serviceId = "9";

    private final static String url = "/aion/";

    private final static String gameSize = "17.8";

    private final String[] premiumAion = {"1 Day", "30 Days", "90 Days", "180 Days", "360 Days"};

    private Premium premium = new Premium();

    private final static String processName = "AION.bin";

    private final static String appName = "aion";

    private final static String gameApplicationTitle = "SplashWindowClass";

    @Override
    public String getProcessName() {
        return processName;
    }

    public Aion() {
        super(gameName, url, serviceId, gameSize, appName);
    }

    public void setServiceAccountId(String serviceAccountId) {
        super.serviceAccountId = serviceAccountId;
    }

    @Override
    public Premium getPremium() {
        return premium.generatePremiumCoupons(premiumAion);
    }

    @Override
    public String getGameApplicationTitle() {
        return gameApplicationTitle;
    }
}

