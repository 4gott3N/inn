package ru.inn.autotests.webdriver.toolkit;

import org.openqa.selenium.By;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: Sergey.Kashapov
 * Date: 09.07.13
 * Time: 16:35
 * To change this template use File | Settings | File Templates.
 */
public class UserData {

    private static Matcher MATCHER;
    private static Pattern PATTERN;

    public static String getMasterAccountId(){
        String gameData = LocalDriverManager.getDriverController().findElement(By.tagName("body")).getAttribute("data-user-data");
        PATTERN= Pattern.compile("\"id\":(\\d+)");
        MATCHER = PATTERN.matcher(gameData);
        MATCHER.find();
        String masterAccount=MATCHER.group(1);
        return masterAccount;
    }
    public static String getEmail(){
        String gameData = LocalDriverManager.getDriverController().findElement(By.tagName("body")).getAttribute("data-user-data");
        PATTERN= Pattern.compile("\"email\":\"(.+?)\"");
        MATCHER = PATTERN.matcher(gameData);
        MATCHER.find();
        String email=MATCHER.group(1);
        return email;
    }
    public static String getLogin(){
        String gameData = LocalDriverManager.getDriverController().findElement(By.tagName("body")).getAttribute("data-user-data");
        PATTERN= Pattern.compile("\"login\":\"(.+?)\"");
        MATCHER = PATTERN.matcher(gameData);
        MATCHER.find();
        String login=MATCHER.group(1);
        return login;
    }
    public static String getBalance(){
        String gameData = LocalDriverManager.getDriverController().findElement(By.tagName("body")).getAttribute("data-user-data");
        PATTERN= Pattern.compile("\"balance\":(\\d+)");
        MATCHER = PATTERN.matcher(gameData);
        MATCHER.find();
        String masterAccount=MATCHER.group(1);
        return masterAccount;
    }
}
