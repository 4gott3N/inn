package ru.inn.autotests.webdriver.toolkit;

import com.google.common.base.Throwables;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;
import ru.inn.autotests.webdriver.common.os.OperationSystem;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.tests.AbstractTest;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.ConcurrentSkipListSet;


public class WebDriverListener implements IInvokedMethodListener {
    protected static Logger log4j = Logger.getLogger(WebDriverListener.class);
    protected static OperationSystem os = PlatformUtils.getOperationSystem();


    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
        if (LocalDriverManager.getDriverController() == null &&
                !method.getTestMethod().toString().contains("CreateTests") &&
                method.isTestMethod()) {
        	if (LocalDriverManager.getDriverController() == null) LocalDriverManager.setWebDriverController(new WebDriverController());

        }
    }


    private static ConcurrentSkipListSet<Integer> invocateds = new ConcurrentSkipListSet<Integer>();
    @Override
    public  void afterInvocation(IInvokedMethod method, ITestResult testResult) {
        if (invocateds.add(System.identityHashCode(testResult))) {
            if (!testResult.isSuccess() && !method.getTestMethod().toString().contains(
                    "CreateTests") && method.isTestMethod()) {
                makeScreenshot(testResult.getName());
                log4j.error(
                        "Test FAILED! Method:" + testResult.getName() + ". StackTrace is " + Throwables.getStackTraceAsString(
                                testResult.getThrowable()));
                OperationsHelper.logoutHook();
            }
            deleteDownloadedApplication();
            if (AbstractTest.isPlugin && method.getTestMethod().toString().contains(
                    "pluginStatusTestThrough")) {
                PluginController pluginController = new PluginController();
                pluginController.tearDown(YamlProvider.getParameter("games_dir", YamlProvider.appConfigs), os);
            }
        }
    }

    public void makeScreenshot(String methodName) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formater = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");
        Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit()
                .getScreenSize());
        BufferedImage capture;
        try {
            capture = new Robot().createScreenCapture(screenRect);
            File fileScreenshot = new File("target" + File.separator + "failure_screenshots" +
                    File.separator + methodName + "_" + formater.format(calendar.getTime()) + "_javarobot.jpg");
            fileScreenshot.getParentFile()
                    .mkdirs();
            ImageIO.write(capture, "jpg", fileScreenshot);
            if (LocalDriverManager.getDriverController() != null) {
                File scrFile = ((TakesScreenshot) LocalDriverManager.getDriverController()
                        .getDriver()).getScreenshotAs(OutputType.FILE);
                FileUtils.copyFile(scrFile, new File("target" + File.separator + "failure_screenshots" +
                        File.separator + methodName + "_" + formater.format(calendar.getTime()) + "_webdriver.png"));
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }

    }

    public  void deleteDownloadedApplication() {
        String pathFile = YamlProvider.getParameter("path_to_download_app", YamlProvider.appConfigs);
        File applicationPath = new File(pathFile);
        if (applicationPath.exists()) {
            File[] fileList = applicationPath.listFiles();
            for (int i = 0; i < fileList.length; i++) {
                fileList[i].delete();
            }
        }
    }


}
