package ru.inn.autotests.webdriver.toolkit;

import org.apache.log4j.Logger;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlInclude;
import org.yaml.snakeyaml.Yaml;
import ru.inn.autotests.webdriver.composite.pages.PaymentTerminal;
import ru.inn.autotests.webdriver.games.Game;
import ru.inn.autotests.webdriver.tests.AbstractTest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Sergey.Kashapov
 * Date: 31.07.13
 * Time: 17:05
 * To change this template use File | Settings | File Templates.
 */
public class YamlProvider {
    public static Yaml yaml = new Yaml();
    static Logger log = Logger.getLogger(YamlProvider.class);
    static String configFilePath = "src\\test\\resources\\configs\\";
    public static String appConfigs = "../../../../application.yml";
    static String nameTestClass = "ru.inn.autotests.webdriver.tests.";


    public synchronized static String getParameter(String parameter, String fileName) {
        try {
        	//log.info(fileName);
            Iterable<Object> params = yaml.loadAll(new FileInputStream(new File(configFilePath + fileName)));
            LinkedHashMap map = (LinkedHashMap) params.iterator().next();
            return map.get(parameter)!=null?String.valueOf(map.get(parameter)):null;
        }
        catch (FileNotFoundException e) {
           return null;
        }
    }

    public static void printConfig() {

        try {
            File file = new File(configFilePath + AbstractTest.configName);
            FileReader reader = new FileReader(file);
            int b;
            System.out.println("////////////////////\n CONFIG \n////////////////////");
            while ((b = reader.read()) != -1) {
                System.out.print((char) b);
            }
            System.out.println("\n////////////////////");
        } catch (Exception ex) {

        }
    }

    public static Iterable<Object> getTestRun(String fileName) {
        Iterable<Object> runClasses = null;
        try {
            runClasses = yaml.loadAll(new FileInputStream(new File(configFilePath + fileName)));
        } catch (FileNotFoundException e) {
            log.info("file with configs not found " + configFilePath + fileName);
            e.printStackTrace();
        }
        return runClasses;
    }


    public static class TestsToRun {
        public ArrayList<String> games = new ArrayList<String>();
        public String excludedGroups = "";
        public ArrayList<String> classToRun = null;
        public ArrayList<String> payments = new ArrayList<String>();
        public ArrayList<XmlInclude> methodsToRun = new ArrayList<XmlInclude>();
        List<XmlClass> listClasses = new ArrayList<XmlClass>();
        Map<Game.GameEnum, Game> gamesList = new HashMap<Game.GameEnum, Game>();

        public void setGames(ArrayList<String> games) {
            this.games = games;
        }

        public String getExcludedGroups() {
            return excludedGroups;
        }

        public void setExcludedGroups(String excludedGroups) {
            this.excludedGroups = excludedGroups;
        }

        public void setClassToRun(ArrayList<String> classToRun) {
            this.classToRun = classToRun;
        }

        public ArrayList<XmlInclude> getMethodsToRun() {
            return methodsToRun;
        }

        public void setMethodsToRun(ArrayList<XmlInclude> methodsToRun) {
            this.methodsToRun = methodsToRun;
        }


        public ArrayList<String> getPayments() {
            return payments;
        }

        public void setPayments(ArrayList<String> payments) {
            this.payments = payments;
        }

        public TestsToRun() {
           games.add("Chaos Fighters");//Lineage 2");Lineage 2 EU PlanetSide 2 R2 Online
            //games.add("Pocket Fort");
            payments.add("PLASTIC_CARD");//PAYPAL YANDEX_MONEY CENTILI_QA QIWI_WALLET PLASTIC_CARD 

        }


        public Map<Game.GameEnum, Game> getGames() {
            for (int i = 0; i < games.size(); i++) {
                Game.GameEnum gameEnum = Game.GameName.selectByGameName(games.get(i));
                gamesList.put(gameEnum, Game.getGame(gameEnum));
            }
            return gamesList;
        }

        public ArrayList<PaymentTerminal.PaymentMethod> getPaymentsMethods() {
            ArrayList<PaymentTerminal.PaymentMethod> methods = new ArrayList<PaymentTerminal.PaymentMethod>();
            if (!payments.isEmpty()) {
                for (String method : payments)
                    methods.add(PaymentTerminal.PaymentMethod.valueOf(method));
                return methods;
            }
            return methods;
        }


        public List<XmlClass> getClassToRun() {
            try {
                for (String className : classToRun) {
                    XmlClass classXml = new XmlClass(Class.forName(nameTestClass + className));
                    classXml.setIncludedMethods(methodsToRun);
                    listClasses.add(classXml);
                }
            } catch (ClassNotFoundException e) {
                log.info("there is no class from config");
                e.printStackTrace();
            }
            return listClasses;
        }

        public String getNamesClassToRun() {
            String names = "";
            int i = 0;
            for (String a : classToRun) {
                if (i != classToRun.size())
                    names += a + ",";
                else {
                    names += a;
                }
                i++;
            }
            return names;
        }


    }


}
