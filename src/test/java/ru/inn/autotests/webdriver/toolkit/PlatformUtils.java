package ru.inn.autotests.webdriver.toolkit;

import org.apache.log4j.Logger;
import ru.inn.autotests.webdriver.common.os.*;
import ru.inn.autotests.webdriver.common.os.OperationSystem.OS;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.games.Game;
import ru.inn.autotests.webdriver.games.Game.GameEnum;
import ru.inn.autotests.webdriver.tests.AbstractTest;

import java.io.*;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class PlatformUtils extends AbstractTest {


    public static Logger log =Logger.getLogger(PlatformUtils.class);


    /**
     * Checks that all games have one status for each
     *
     * @param status - the status from {@link Status} class
     */
    public static void checkGamesStatuses(String status) {
        List<Map<String, String>> objects = APIProvider.execGetGameStatuses();         //Gets all statuses in view: [{"serviceId"="1","status"="not_installed"},{"serviceId"="2",
        //"status"="full_update_required"}, ... , {...}]
        List<Map<String, String>> errors = new LinkedList<Map<String, String>>();
        Iterator<Map<String, String>> iterator = objects.iterator();
        while (iterator.hasNext()) {
            Map<String, String> game = iterator.next();
            Iterator<String> statuses = game.keySet().iterator();
            while (statuses.hasNext()) {
                String serviceId = statuses.next(); // serviceId
                String number = game.get(serviceId);
                String gameStatus = statuses.next();
                String value = game.get(gameStatus);
                if (!value.equals(status) && !number.equals("7")) { // Should be deleted second condition (delete 7 when ace online will be removed from statuses) TODO
                    errors.add(game);
                }
            }
        }
        if (errors.size() != 0) {
            throw (new UnsupportedOperationException("Errors: " + errors.toString() + " expected status: " + status));
        }
    }




    /**
     * Closes the game process for a game
     *
     * @param game - the game, which should close process
     */
    public static void killGame(final Game game) {
        log.info("kill game "+game.getProcessName());
        if (game.getGameName() == GameEnum.OFC || game.getGameName() == GameEnum.OFCBR){
        	LocalDriverManager.getDriverController().refresh();
        }else{
	        ProcessProvider processProvider = new ProcessProvider();
	        processProvider.killTask(game.getProcessName());
        }
    }
    
    /**
     * Detect game process presence
     *
     * @param game - target game process
     * @param timeout - seconds to wait
     * @return true if process was found
     */
    public static boolean detectGameProcess(final Game game, int timeout) {
        boolean isFound = false;
    	if (game.getGameName() == GameEnum.OFC || game.getGameName() == GameEnum.OFCBR){
        	 for (int sec = 0; sec < timeout; sec++) {
        		 if(LocalDriverManager.getDriverController().getPageAdress().contains("run")){
        			 isFound = true;
             		break;
             	}
             	OperationsHelper.sendPause(1);
             }
        	 return isFound;
        }
    	ProcessProvider processProvider = new ProcessProvider();
        for (int sec = 0; sec < timeout; sec++) {
        	if (processProvider.isPresent(game.getProcessName())) {
        		isFound = true;
        		break;
        	}
        	OperationsHelper.sendPause(1);
        }
        return isFound;
    }

    /**
     * @param browser
     * @deprecated Not used
     *             Starts selenium-server-standalone for current browser
     */
    @Deprecated
    public static void startSeleniumServer(String browser) {
        try {
            String path = System.getProperty("user.dir");
            if (browser.equals("ie")) {
                Runtime.getRuntime().exec(path + File.separator + "lib" + File.separator + "IEDriverServer.exe");
            } else if (browser.equals("opera")) {

            }
            Runtime.getRuntime().exec("java -jar " + path + File.separator + "lib" + File.separator + "selenium-server-standalone-2.25.0.jar");
        } catch (IOException e) {
            throw new RuntimeException("");
        }
    }


    /**
     * Sends an error
     */
    public static void sendError720(boolean isPlugin) {
        APIProvider jsProvider = new APIProvider();
        jsProvider.execSendError720(isPlugin);
    }

    public static void setMaintenance(Game game, boolean isPlugin){
        APIProvider js=new APIProvider();
        js.execSendMaintenanceStatus(game, isPlugin);
    }

    /**
     * Sends a request with set version of plugin
     */
    public static void sendSetVersion(boolean isPlugin) {
        APIProvider jsProvider = new APIProvider();
        jsProvider.execSetVersion(isPlugin);
    }

    /**
     * Sends status through BasicConsole API
     */
    public static void sendStatus(String jsonName, Game game,boolean isPlugin) {
        APIProvider jsProvider = new APIProvider();
        jsProvider.execSendJSON(jsonName, game,isPlugin);
        OperationsHelper.sendPause(3);
    }



    /**
     * Returns the operation system, which convert from system environment variable
     */
    public static OperationSystem getOperationSystem() {
        log.info("get operation system");
        String osName = System.getProperty("os_4game");
        if (osName == null) {
            osName = OS.W764.toString();
        }
        String command = System.getProperty("user.dir") + "\\lib\\" + "bitness-checker.exe";
        String bittness = "";
        try {
            Process process = Runtime.getRuntime().exec(command);

            String line = "";
            InputStream is = null;
            BufferedReader br = null;

            try {
                is = process.getInputStream();
                br = new BufferedReader(new InputStreamReader(is));

                bittness = br.readLine();
            } finally {
                is.close();
                br.close();
            }
        } catch (IOException e) {
            // throw new RuntimeException("Didn't run " + command);
            System.out.println("Didn't run " + command);
            bittness = "unknown";
        }

        osName = System.getProperty("os.name") + " " + bittness;

        System.out.println(osName);
//        OS os = OS.valueOf(osName);
        if (osName == "Windows 7 64") {
            return new WindowsSeven64();
        } else if (osName.equals("Windows 7 32")) {
            return new WindowsSeven32();
        } else if (osName.equals("Windows 8 32")) {
            return new WindowsEight32();
        } else if (osName.equals("Windows 8 64")) {
            return new WindowsEight64();
        } else if (osName.equals("Windows Vista 32")) {
            return new WindowsVista32();
        } else if (osName.equals("Windows Vista 64")) {
            return new WindowsVista64();
        } else if (osName.equals("XP 64")) {
            return new WindowsXP64();
        } else if (osName.equals("XP 32")) {
            return new WindowsXP32();
        } else {
            System.out.println("Unknown system name: " + osName);
            return new WindowsSeven64();
        }
    }

    /**
     * Returns all parameters from the XLS
     */
    public static String getStageParameters(String parameter) {
       	String location = System.getenv("stage_parameters");
        if (location == null) {
            location = YamlProvider.getParameter("paramsPath",configName);
        }
        else {
          location="\\params\\"+location;
        }
        return YamlProvider.getParameter(parameter,location);
    }



    /**
     * Sets all installed games
     *
     * @param os
     */
    public static void setAllInstalledGames(OperationSystem os) {
        ProcessProvider pp = new ProcessProvider();
        pp.setRegKeys(os);
    }



    /**
     * Statuses for APIProvider (JS <-> Plugin)
     *
     * @author pavel.popov
     */
    public class Status {
        public final static String NOT_INSTALLED = "not_installed";
        public final static String FULL_UPDATE_REQUIRED = "full_update_required";
        public final static String PROGRESS = "progress";
        public final static String PAUSED = "paused";
        public final static String FINISHED = "finished";
        public final static String INSTALLED = "installed";
        public final static String REPAIR = "repair";
        public final static String UNPACKING = "unpacking";
    }

}
