package ru.inn.autotests.webdriver.toolkit;

import ru.inn.autotests.webdriver.common.language.Language;

import java.io.*;
import java.util.*;

/**
 * Provides works with various texts
 *
 * @author pavel.popov
 */
public class TextProvider {

    private static String PATH_TO_PINCODES_SC = "src\\test\\resources\\pincodes\\sc_pin_dev.txt";

    // cache map
    private static Map<String, Map<String, String>> fileMaps = new HashMap<String, Map<String, String>>();

    //cache list
    private static Map<String, List<String>> fileLists = new HashMap<String, List<String>>();

    /**
     * Translates word in required language by key from test.
     *
     * @param keyWord  - the key, which must translate.
     * @param language - required language.
     */
    public static String translator(final String keyWord, final Language language) {
        if (keyWord == null || keyWord.equals("")) {
            throw new IllegalArgumentException("");
        }
        if (language == null) {
            throw new IllegalArgumentException("");
        }
        String pathFile = language.getTranslateFilePath();

        String translate = "";

        Map<String, String> mapTranslate = fileToMap(pathFile);

        boolean isKeyFound = false;

        Iterator<String> iterator = mapTranslate.keySet().iterator();
        while (iterator.hasNext()) {
            String nextKey = iterator.next();
            if (nextKey.equals(keyWord)) {
                translate = mapTranslate.get(nextKey);
                isKeyFound = true;
                break;
            }
        }

        if (isKeyFound == false) {
            throw new RuntimeException("Didn't find key word " + keyWord + " into the " + language.getNameLanguage() + ".tsl");
        }

        return translate;
    }

    public static String[] getGCBCode() {
        String[] partsCode = new String[4];
        List<String> pinCodes = fileToList(PATH_TO_PINCODES_SC);
        String pincode = pinCodes.get(0); // first pincode from list
        pinCodes.remove(0);
        listToFile(pinCodes, PATH_TO_PINCODES_SC);

        int i = 0;
        int j = 0;
        for (i = 0, j = 0; i < partsCode.length - 1; i++) {
            String partPin = pincode.substring(j, j = j + 4);
            partsCode[i] = partPin;
        }
        partsCode[partsCode.length - 1] = pincode.substring(j);
        return partsCode;
    }

    private static void listToFile(List<String> lines, String pathFile) {

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(pathFile)));
            Iterator<String> itString = lines.iterator();
            while (itString.hasNext()) {
                String line = itString.next();
                bw.append(line + "\n");
                bw.flush();
            }
            bw.close();
        } catch (FileNotFoundException e) {
            new RuntimeException("The file " + pathFile + " did not create :" + e.getCause());
        } catch (IOException ioe) {
            new RuntimeException("The file " + pathFile + " did not create :" + ioe.getCause());
        }
    }

    private static List<String> fileToList(String pathFile) {
        if (fileLists.containsKey(pathFile)) {
            return fileLists.get(pathFile);
        }

        List<String> lines = new ArrayList<String>();
        try {
            Scanner scanner = new Scanner(new FileInputStream(new File(pathFile)), "UTF-8");

            try {
                while (scanner.hasNextLine()) {
                    String nextLine = scanner.nextLine();
                    lines.add(nextLine);
                }
            } finally {
                scanner.close();
            }
        } catch (FileNotFoundException e) {
            new RuntimeException("The file " + pathFile + " is not open :" + e.getCause());
        }
        fileLists.put(pathFile, lines);
        return lines;
    }

    private static Map<String, String> fileToMap(String pathFile) {
        if (fileMaps.containsKey(pathFile)) {
            return fileMaps.get(pathFile);
        }
        Map<String, String> translate = new HashMap<String, String>();
        try {
            Scanner scanner = new Scanner(new FileInputStream(new File(pathFile)), "UTF-8");

            try {
                while (scanner.hasNextLine()) {
                    String nextLine = scanner.nextLine();
                    if (!nextLine.equals("")) {
                        String[] keyValue = nextLine.split("=");
                        translate.put(keyValue[0].trim(), keyValue[1].trim());
                    }
                }
            } finally {
                scanner.close();
            }

        } catch (FileNotFoundException e) {
            new RuntimeException("The file " + pathFile + " is not open :" + e.getCause());
        }
        fileMaps.put(pathFile, translate);
        return translate;
    }

    /**
     * Analyzes text with testcase and factory test
     *
     * @param path
     */
    @Deprecated
    public void analyzeTextFile(String path) {

        try {

            Scanner scanner = new Scanner(new FileReader(new File(path)));

            try {
                while (scanner.hasNextLine()) {
                    analyzer(scanner.nextLine());
                }
            } finally {
                scanner.close();
            }

        } catch (FileNotFoundException e) {
            new RuntimeException("The file " + path + " is not open :" + e.getCause());
        }
    }

    private void analyzer(final String line) {

        if (line.contains("TestCase")) {
            return;
        }

        String command = line.split(":")[0].split("\\.")[1].substring(1); //command without number, comma and whitespace

        System.out.println("Send command:" + command);

        String[] elementsInString = line.split(":")[1].split(" "); //split by whitespaces
        String parameters = line.split(":")[1]; // get parameters

        for (String element : elementsInString) {
            if (element.equals(";")) { // end of command
                return;
            }

            if (element.equals("is") || element.equals(" ")) {
                break;
            }

            System.out.println("Make with me:" + element);
        }

        doCommand(command, parameters);
    }

    /**
     * The part of key-driven framework
     *
     * @param command
     * @param params
     */
    @Deprecated
    private void doCommand(String command, String params) {

//		if (command.equals("open page")){
////			Game game = new Game(params);
//			if (params.equals("Lineage2DE play")){
//				session.getPage(new GamePanelPage(new Lineage2de()));
//			}
//			else if(params.equals("Lineage2EU play")){
//				session.getPage(new GamePanelPage(new Lineage2eu()));
//			}
//		}
    }

    public static void main(String[] args) {
        System.out.println(TextProvider.getGCBCode());
    }

}
