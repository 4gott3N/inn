package ru.inn.autotests.webdriver.toolkit;

import ru.inn.autotests.webdriver.common.os.OperationSystem;
import ru.inn.autotests.webdriver.composite.OperationsHelper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * @author pavel.popov
 */
public class ProcessProvider {
    private static Process process;
    private final static String CMD_START_SERVICE = "net start ";
    private final static String CMD_STOP_SERVICE = "net stop ";
    private final static String CMD_CLEAN_REGISTER = "REG DELETE ";
    private final static String CMD_SC_QUERY = "sc query ";
    private final static String CMD_EXEC_REG_32 = "regedit.exe /s lib\\setRegKeys.reg";
    private final static String CMD_EXEC_REG_64 = "regedit.exe /s lib\\setRegKeys64.reg";
    private final static int SECONDS = 40;


    private static String PATH_TO_TASKLIST = "\\system32\\tasklist.exe";

    /**
     * Default constructor
     */
    public ProcessProvider() {

    }

    /**
     * Views, is present or not process into a process list
     *
     * @param processName - the process name, ex: 4game-service.exe
     * @throws IOException
     */
    public static boolean isPresent(String processName) {
        List<String> processList = null;
        try {
            processList = getProcessList();
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < processList.size(); i++) {
            String process = processList.get(i);
            if (process.equals(processName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Start a service
     *
     * @param serviceName - the service name
     */
    public void startService(String serviceName) {
        try {
            process = Runtime.getRuntime().exec(CMD_START_SERVICE + serviceName);
        } catch (IOException e) {
            throw new RuntimeException("Didn't run " + CMD_START_SERVICE + serviceName);
        }
    }

    /**
     * Stops a service
     *
     * @param serviceName - the service name
     */
    public void stopService(String serviceName) {
        try {
            process = Runtime.getRuntime().exec(CMD_STOP_SERVICE + serviceName);
        } catch (IOException e) {
            throw new RuntimeException("Didn't run " + CMD_STOP_SERVICE + serviceName);
        }
    }

    /**
     * Cleans register after work with game and plugin
     *
     * @param os - the operation system in which must delete register folder
     */
    public void cleanRegister(final OperationSystem os) {
        try {
            process = Runtime.getRuntime().exec(CMD_CLEAN_REGISTER + os.getRegPathToPlugin() + " /f");//force
        } catch (IOException e) {
            throw new RuntimeException("Didn't clean register " + CMD_CLEAN_REGISTER + os.getRegPathToPlugin());
        }
    }

    /**
     * Sets reg keys into register
     *
     * @param os
     */
    public void setRegKeys(OperationSystem os) {
        if (os.is64bit()) {
            executeCMD(CMD_EXEC_REG_64);
        } else {
            executeCMD(CMD_EXEC_REG_32);
        }
    }


    /**
     */
    public void checkService(String serviceName) {
        try {
            process = Runtime.getRuntime().exec(CMD_SC_QUERY + serviceName);
            checkState(process);
        } catch (IOException e) {
            throw new RuntimeException("Didn't run " + CMD_STOP_SERVICE + serviceName);
        }
    }

    public static   void wait4GameProcess(){
        boolean flag;
        for (int sec = 0; sec < SECONDS; sec++, OperationsHelper.sendPause(1)) {
            if (flag = isPresent("4game-service.exe")) {
                while (!flag) {
                    flag = isPresent("4game-service.exe");
                }
                return;
            }
        }
        throw new RuntimeException("Process is not available  " + "4game-service.exe" + ". Check this process in the task list.");

    }

    /**
     * Closes task process from TaskCommander
     */
    public void killTask(final String processName) {
        if (processName == null || processName.equals("")) {
            throw new IllegalArgumentException("The process name is null: processName=" + processName);
        }
        boolean flag;
        for (int sec = 0; sec < SECONDS; sec++, OperationsHelper.sendPause(1)) {
            if (flag = isPresent(processName)) {
                int MAX_COUNT_ATTEMTS = 5;
                int countAttempts = 0;
                while (flag && countAttempts < MAX_COUNT_ATTEMTS) {
                    countAttempts++;
                    closeProcess(processName);
                    flag = isPresent(processName);
                }
                return;
            }
            else
                return;
        }
        throw new RuntimeException("Didn't close the process " + processName + ". Check this process in the task list.");
    }

    public static void close4gameProcess(){
        ProcessProvider processProvider = new ProcessProvider();
        processProvider.killTask("4game-service.exe");
    }

    /**
     * Creates text file with plugin version in the body for gets file version from plugin application. <br>Works
     * with client from @author Hayk Karapetyan.
     * @param pathToFileVersionClient
     * @param pathToApp
     * @param pathToResultFile
     */
    public void getAppVersionFile(String pathToFileVersionClient, String pathToApp, String pathToResultFile) {
        System.out.println("Executing cmd command: " + "\""+pathToFileVersionClient + "\""+" " + "\""+pathToApp+"\"" + " " + "\""+pathToResultFile+"\"");
        OperationsHelper.sendPause(2);
        executeCMD("\""+pathToFileVersionClient + "\""+" " + "\""+pathToApp+"\"" + " " + "\""+pathToResultFile+"\"");
    }

    /**
     * Executes in the command line
     *
     * @param command
     */
    private void executeCMD(String command) {
        try {
            process = Runtime.getRuntime().exec(command);//force
        } catch (IOException e) {
            throw new RuntimeException("Didn't execute the line " + command);
        }
    }

    public void activateApplicationWindow(String applicationTitle) {
//        OperationsHelper.sendPause(15);

        String path = System.getProperty("user.dir") + "\\lib\\";
        // свернем все окна
        executeCMD(path + "cmdow /MA");

        // сделаем активным окно игры
        executeCMD(path + "cmdow \""+ applicationTitle + "\" /act");
    }

    //Kills a process
    private void closeProcess(String processName) {
        String cmd = "taskkill /IM " + processName + " /F /t";
        //System.out.println("taskkill /IM " + processName + " /F /t");
        Runtime runtime = Runtime.getRuntime();
        try {
            process = runtime.exec(cmd);
        } catch (IOException e) {
            throw new RuntimeException("Didn't execute command line: " + cmd);
        }
    }

    //Checks status of service
    private void checkState(Process process) {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream(), Charset.forName("cp866")));

            String line = reader.readLine();
            while (line != null) {
                System.out.println(line);
                line = reader.readLine();
//				if(line.trim().startsWith("STATE"))
//				{
//
//					if (line.trim().substring(line.trim().indexOf(":")+1,line.trim().indexOf(":")+4).trim().equals("1"))
//						System.out.println("Stopped");
//					else
//						if (line.trim().substring(line.trim().indexOf(":")+1,line.trim().indexOf(":")+4).trim().equals("2"))
//							System.out.println("Starting....");
//						else
//							if (line.trim().substring(line.trim().indexOf(":")+1,line.trim().indexOf(":")+4).trim().equals("3"))
//								System.out.println("Stopping....");
//							else
//								if (line.trim().substring(line.trim().indexOf(":")+1,line.trim().indexOf(":")+4).trim().equals("4"))
//									System.out.println("Running");
//
//				}	
//				line=reader.readLine(); 
            }
        } catch (IOException e1) {
        }
    }

    //returns a process list
    private static List<String> getProcessList() throws IOException {
        try {
            process = Runtime.getRuntime().exec(System.getenv("windir") + PATH_TO_TASKLIST);
        } catch (IOException e) {
            throw new RuntimeException("Your tasklist is not located to path:" + PATH_TO_TASKLIST + ". Change it on the code.");
        }
        List<String> processList = new ArrayList<String>();
        String line = "";
        InputStream is = null;
        BufferedReader br = null;
        try {
            is = process.getInputStream();
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                String process = line.split(" ")[0];
                processList.add(process);
            }
        } finally {
            is.close();
            br.close();
        }
        return processList;
    }
      public static void run4GameServiceProcess(){
          ProcessProvider pp = new ProcessProvider();
          pp.startService("4game-service");
          wait4GameProcess();
      }


    /**
     * For test only
     */
    public static void main(String[] args) {
        ProcessProvider pp = new ProcessProvider();
//		try {
//			System.out.println(pp.getProcessList());
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		System.out.println(pp.isPresent("4game-service.exe"));
//		pp.cleanRegister(new WindowsSeven64());
     //   pp.checkService("4game-service");
		pp.startService("4game-service");
    }

}
