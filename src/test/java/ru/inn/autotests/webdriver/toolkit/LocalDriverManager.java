package ru.inn.autotests.webdriver.toolkit;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Sergey.Kashapov
 * Date: 18.12.13
 * Time: 15:49
 */
public class LocalDriverManager {

    private static ConcurrentHashMap<Thread, WebDriverController> webDrivers = new ConcurrentHashMap<Thread, WebDriverController>();

    public static WebDriverController getDriverController() {

        return webDrivers.get(Thread.currentThread());
    }

    public static void setWebDriverController(WebDriverController driver) {
        webDrivers.put(Thread.currentThread(), driver);
    }

    public static void cleanThreadPool() {
        for (WebDriverController controller : webDrivers.values())
            controller.shutdown();
        for (Thread cur : webDrivers.keySet())
            cur.interrupt();
        webDrivers.clear();
    }
}
