package ru.inn.autotests.webdriver.toolkit;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import ru.inn.autotests.webdriver.common.entity.Message;
import ru.inn.autotests.webdriver.common.language.English;
import ru.inn.autotests.webdriver.common.language.Russian;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URLEncoder;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class provides work with mail notification in the 4game. <br/>
 * The 4game has internal SMTP provider, which providing SMTP to HTTP request. <br/>
 */
public class MailProvider {

    public static String mailUrl;
    public static String timeout;
    public static Logger log = Logger.getLogger(MailProvider.class);
    static final String TAG_DIV = "div";
    static final String TAG_BODY = "body";
    static final String BODY_MESSAGE_ID = "maindiv";
    static final String ALL_MESSAGE_NAME = "newmessage";
    static final String ALL_MESSAGE_ID_FOR_LIVE = "body > div";
    static final String SUBJECT_MESSAGE_NAME = "subject";
    static final String FROM_MESSAGE_NAME = "from";
    static final String LANGUAGE = "lang";
    static final String FROMNAME_MESSAGE_NAME = "fromname";
    static final String TONAME_MESSAGE_NAME = "toname";
    public static final String CSS_QUERY_DIV = "div > div > div";
    public static final String CSS_QUERY_STYLE = "style";
    public static final String CSS_QUERY_IMG = "img";
    public static final String CSS_QUERY_ATTR_SRC = "src";
    public static final String CSS_QUERY_ATTR_ID = "id";
    public static final String CSS_QUERY_ATTR_NAME = "name";
    public static final String CSS_QUERY_FOOTER = "mainmail";
    public static final String CSS_QUERY_A = "a";
    public static final String CSS_QUERY_ATTR_HREF = "href";
    public static final String CSS_QUERY_ID_NAMEGAME = "#nameGame";
    public static final String CSS_QUERY_TITLE_NAMEGAME = "#gameNameTitle";
    public static final String CSS_QUERY_CONTACT = "#contact";
    public static final String CSS_QUERY_SUM = "#sum";


    static {
        mailUrl = PlatformUtils.getStageParameters("urlMail");
        timeout = YamlProvider.getParameter("Timeout", YamlProvider.appConfigs);
    }

    public MailProvider(String mailUrl, String i){
    	this.mailUrl = mailUrl;
    	this.timeout = i;
    }

    /**
     * This method returns all massages from message box.
     *
     * @param messageId - email
     */
    private static LinkedList<Message> getMessage(final String messageId) {
        LinkedList<Message> emails = new LinkedList<Message>();
        try {
            Document document = Jsoup.connect(mailUrl + messageId + ".htm").get();
            String subject = "";
            String bodyOfMessage = "";
            Elements messages = document.getElementsByAttributeValueContaining(CSS_QUERY_ATTR_NAME, ALL_MESSAGE_NAME);
            for (Element message : messages) {

                subject = message.getElementsByAttributeValue(CSS_QUERY_ATTR_NAME, SUBJECT_MESSAGE_NAME).first().text();

                bodyOfMessage = message.getElementById(BODY_MESSAGE_ID).text();

                Message email = new Message(subject, bodyOfMessage);

                String from = message.getElementsByAttributeValue(CSS_QUERY_ATTR_NAME, FROM_MESSAGE_NAME).first().text();
                email.setFrom(from);

                String lang = message.getElementsByAttributeValue(CSS_QUERY_ATTR_NAME, LANGUAGE).first().text();
                email.setLanguage(lang.equals("ru") ? new Russian() : new English());
                String fromName = message.getElementsByAttributeValue(CSS_QUERY_ATTR_NAME, FROMNAME_MESSAGE_NAME).first().text();
                email.setFromName(fromName);
                String toName = message.getElementsByAttributeValue(CSS_QUERY_ATTR_NAME, TONAME_MESSAGE_NAME).first().text();
                email.setToName(toName);
                Element maindiv = message.select(CSS_QUERY_DIV).first();
                String style = maindiv.attr(CSS_QUERY_STYLE);

                String emailBackground = style.split("background:")[1].trim().split(" ")[0];
                email.setColor(Message.Color.getColor(emailBackground));

                String linkLogo = message.select(CSS_QUERY_IMG).first().attr(CSS_QUERY_ATTR_SRC);
                email.setLogoLink(linkLogo);

                String id = maindiv.attr(CSS_QUERY_ATTR_ID);
                email.setMessageId(id);

                email.setNotificationLink(maindiv.select(CSS_QUERY_A).attr(CSS_QUERY_ATTR_HREF));

                String titleGameName = message.select(CSS_QUERY_TITLE_NAMEGAME).text();
                email.setGameName(titleGameName);
                
                String gameName = message.select(CSS_QUERY_ID_NAMEGAME).text();
                email.setGameName(gameName);

                String contact = message.select(CSS_QUERY_CONTACT).text();
                email.setContact(contact);

                String sum = message.select(CSS_QUERY_SUM).text();
                email.setSum(sum);
                emails.add(email);
            }
        } catch (IOException e) {
            throw new RuntimeException("Failed to open the connection to: " + mailUrl + messageId + ".htm");
        }
        return emails;
    }

    public static LinkedList<Message> getMessageFromLive(final String messageId) {
        LinkedList<Message> emails = new LinkedList<Message>();
        try {
            Document document = Jsoup.connect(mailUrl + messageId + ".htm").get();
            String bodyOfMessage = "";
            Elements messages = document.select(ALL_MESSAGE_ID_FOR_LIVE);
            for (Element message : messages) {

                bodyOfMessage = message.getElementById(BODY_MESSAGE_ID).text();
                Message email = new Message("4game", bodyOfMessage);
                String gameName = message.select(CSS_QUERY_ID_NAMEGAME).text();
                email.setGameName(gameName);
                Element maindiv = message.select(TAG_DIV).first();
                String style = maindiv.attr(CSS_QUERY_STYLE);

                String emailBackground = style.split("background:")[1].trim().split(" ")[0];
                email.setColor(Message.Color.getColor(emailBackground));

                String linkLogo = message.select(CSS_QUERY_IMG).first().attr(CSS_QUERY_ATTR_SRC);
                email.setLogoLink(linkLogo);

                String id = maindiv.attr(CSS_QUERY_ATTR_ID);
                email.setMessageId(id);

                String sum = message.select(CSS_QUERY_SUM).text();
                email.setSum(sum);


                String notificationLink = maindiv.select(CSS_QUERY_A).attr(CSS_QUERY_ATTR_HREF);
                email.setNotificationLink(notificationLink);
                emails.add(email);
            }
        } catch (IOException e) {
            throw new RuntimeException("Failed to open the connection to: " + mailUrl + messageId + ".htm");
        }

        return emails;
    }


    public static Message getLastMessage(String mail) {
        log.info("get last message from " + mailUrl + mail + ".htm");
        waitEmailPresent(mail);
        if (!mailUrl.contains("srv.inn.ru")) {
            return getMessage(mail).getLast();
        } else {
            return getMessageFromLive(mail).getLast();
        }
    }

    public static String getLinkFromMail(String email) {
        log.info("go to url from email " + email);
        String firstLink = MailProvider.getLastMessage(email).getNotificationLink();
        int beginPos = getMatchPosition(firstLink, "\\w\\/", 1);
        firstLink = firstLink.substring(beginPos + 1);
        log.info("url in message " + firstLink);
        return firstLink;
    }

    // The method returns the position of the occurrence of the number with the number in the text
    public static int getMatchPosition(String inputText, String match, int number) {
        int result = -1;
        Pattern pattern = Pattern.compile(match);
        Matcher matcher = pattern.matcher(inputText);
        int i = 0;
        while (matcher.find()) {
            if (++i == number) {
                result = matcher.start();
                break;
            }
        }
        return result;
    }


    public static void waitSmsPresent(String mobileNumber) {
        Document document = null;
        int timer = 0;
        while (Integer.parseInt(timeout) > timer && document==null) {
            try {
                document = Jsoup.connect(mailUrl + "notifytest." + mobileNumber + "@inn.ru.htm").get();
            } catch (IOException e) {
                sendPause(1);
                timer++;
            }

        }
    }

    public static void waitEmailPresent(String email) {
        Document document;
        Elements messages = new Elements();
        int timer = 0;
        while (messages.isEmpty() && Integer.parseInt(timeout) > timer) {
            try {
                document = Jsoup.connect(mailUrl + email + ".htm").get();
                messages = document.getElementsByTag(TAG_DIV);
                sendPause(1);
            } catch (IOException e) {
                sendPause(1);
                timer++;
            }

        }
    }


    /**
     * This method returns number of messages from message box.
     *
     * @param email - email
     */

    public static int getNumberOfMessages(final String email) {
        String url = mailUrl;
        Elements messages;
        try {
            Document document = Jsoup.connect(url + email + ".htm").get();
            messages = document.select(TAG_BODY + " > " + TAG_DIV);
        } catch (IOException e) {
            return 0;
        }
        return messages.size();
    }

    public static int getNumberOfSms(String mobileNumber) {
        Elements messages;
        if (!mailUrl.contains("srv.inn.ru")) {
            try {
                mobileNumber = URLEncoder.encode(mobileNumber, "UTF-8");
                Document document = Jsoup.connect(mailUrl + "notifytest." + mobileNumber + "@inn.ru.htm").get();
                messages = document.select(TAG_BODY + " > " + TAG_DIV);

            } catch (IOException e) {
                return 0;
            }

            return messages.size();
        } else {
            try {
                Document document = Jsoup.connect(mailUrl + "notifytest." + mobileNumber + "@inn.ru.htm").get();
                messages = document.select(TAG_BODY + " > p ");

            } catch (IOException e) {
                return 0;
            }
            return messages.size() / 3;
        }
    }


    public static Message getSms(String mobileNumber) {
        log.info("get sms code from " + mailUrl + "notifytest." + mobileNumber + "@inn.ru.htm");
        waitSmsPresent(mobileNumber);
        String code = "";
        try {
            Document document = Jsoup.connect(mailUrl + "notifytest." + mobileNumber + "@inn.ru.htm").get();
            Message mess = new Message(mobileNumber, mobileNumber);
            if (!mailUrl.contains("srv.inn.ru")) {
                Elements messages = document.select(TAG_BODY + " > " + TAG_DIV + " > " + TAG_DIV);
                Element lastSms = messages.last();
                code = lastSms.select("div[name=body]").text().split(":\\D+")[1];
            } else {
                Elements messages = document.select(TAG_BODY + " > p ");
                Element lastSms = messages.last();
                code = lastSms.text().split(":\\D+")[1];
            }

            if (code != null) {
                log.info("code from sms is " + code);
                mess.setCodeFromSMS(code);
            } else {
                throw new RuntimeException(
                        "The code was not found in the sms:" + mailUrl + "notifytest." + mobileNumber + "@inn.ru.htm");
            }
            return mess;

        } catch (IOException e) {
            throw new RuntimeException(
                    "Failed to open the connection to: " + mailUrl + "notifytest." + mobileNumber + "@inn.ru.htm");
        }
    }


    public static boolean isNewMessagePresent(String mailLogin, int numberOfMessagesThatWere) {
        log.info("Check that email has come to "+ mailUrl + mailLogin + ".htm");
        int timer = 0;
        while (getNumberOfMessages(mailLogin) < numberOfMessagesThatWere + 1) {
            if (Integer.parseInt(timeout) > timer) {
                sendPause(1);
                timer++;
            } else {
                throw new RuntimeException("The email " + mailLogin + " has not contain new message. "
                        + " Check email " + MailProvider.mailUrl + mailLogin + ".htm");
            }

        }
        return true;
    }


    /**
     * Wait for new SMS will come
     *
     * @param mobileNumber
     * @param numberOfSmsThatWere
     * @return
     */
    public static boolean isNewSmsPresent(String mobileNumber, int numberOfSmsThatWere) {
        log.info("Check that sms has come to "+ mailUrl + "notifytest." + mobileNumber + "@inn.ru.htm");
        int timer = 0;
        while (getNumberOfSms(mobileNumber) < numberOfSmsThatWere + 1) {
            if (Integer.parseInt(timeout) > timer) {
                sendPause(1);
                timer++;
            } else {
                throw new RuntimeException("The mobileNumber " + mobileNumber + " does not contain new message. "
                        + " Check number " + MailProvider.mailUrl + "notifytest." + mobileNumber + "@inn.ru.htm");
            }

            sendPause(1);
        }
        return true;
    }

    public static void sendPause(int sec) {
        try {
            Thread.sleep(sec * 1000);
        } catch (InterruptedException iex) {
            Thread.interrupted();
        }
    }
    
    //for tests only
    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException {
        // WinRegistry.readStringValues(WinRegistry.HKEY_LOCAL_MACHINE,PATH_TO_KEY,NAME_OF_KEY);
        //  System.out.println(MailProvider.getMessageFromLive("notifytest.+01111222333123@inn.ru")[36].getLinkLogo());
        //String text = "<td>some<tt>message</tt>123</td>";
        //   Message[] message = getMessage("notifytest.01366883641763@inn.ru");
        //         Language language=new Russian();
      /*  Message mess=MailProvider.getLastMessage("notifytest.01374486632023@inn.ru");
        System.out.println(mess.getGameName());    System.out.println(mess.getSum());*/
     /*   mess.setLanguage(language);
        mess.checkFromName(mess.getFromName());
        mess.checkSubjectMessage(GamePanelPage.PREMIUM_STATUS, "$Status_premium_1Day");*/
        // System.out.println( MailProvider.getSms(user.getMobileNumber()+".htm"));
        // System.out.println(MailProvider.getLastMessage("notifytest.01366883641763@inn.ru"));
        // System.out.println(getNumberOfSms("+01354603840677"));

    }


}
