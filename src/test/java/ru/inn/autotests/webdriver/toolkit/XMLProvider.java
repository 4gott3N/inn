package ru.inn.autotests.webdriver.toolkit;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import ru.inn.autotests.webdriver.common.entity.TagCase;
import ru.inn.autotests.webdriver.common.entity.TagCase.XMLParameter;
import ru.inn.autotests.webdriver.common.entity.TagCase.XMLParameter.XMLObject;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.LinkedList;
import java.util.List;

public class XMLProvider {
//	private static boolean childTag = false;

    private static int child = 0;

    /**
     * Returns List of JS parameters which parses from XML.
     *
     * @param xmlPath - the path to XML file
     */
    public static List<TagCase> getJSObjects(final String xmlPath) {

        List<TagCase> caseListParameters = new LinkedList<TagCase>();

        try {
            File fXmlFile = new File(xmlPath);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();

            NodeList caseList = doc.getElementsByTagName(Key.KEY_TAG_CASE);

            for (int cases = 0; cases < caseList.getLength(); cases++) {

                TagCase caseXMLParameter = new TagCase();

                Node caseNode = caseList.item(cases);

                Element elementNode = (Element) caseNode;

                int number = Integer.parseInt(caseNode.getAttributes().getNamedItem(Key.KEY_ATTRIBUTE_NUMBER).getNodeValue());
                caseXMLParameter.setCaseNumber(number);

                String text = caseNode.getAttributes().getNamedItem(Key.KEY_ATTRIBUTE_TEXT).getNodeValue();
                caseXMLParameter.setCaseDefinition(text);


                NodeList objectList = elementNode.getElementsByTagName(Key.KEY_TAG_OBJECT);

                XMLParameter xmlParameter = new XMLParameter();
                xmlParameter = getXMLTags(objectList);

                caseXMLParameter.setXmlParameter(xmlParameter);

                caseListParameters.add(caseXMLParameter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return caseListParameters;
    }


    /**
     * Returns XML tags from NodeList
     *
     * @param objectList
     */
    private static XMLParameter getXMLTags(NodeList objectList) {

        XMLParameter xmlParameter = new XMLParameter();

        for (int objects = 0; objects < objectList.getLength(); objects++) {

            Node objectNode = objectList.item(objects);

            if (objectNode.getNodeType() == Node.ELEMENT_NODE) {

                XMLObject xmlObject = new XMLObject();
                Element eElement = (Element) objectNode;

                xmlObject.setKey(getTagValue(Key.KEY_TAG_KEY, eElement));
                if (eElement.getElementsByTagName(Key.KEY_TAG_OBJECT).getLength() != 0) {
                    xmlObject.setXmlParameter(getXMLTags(eElement.getElementsByTagName(Key.KEY_TAG_OBJECT), child));
                    objects = objects + child; //TODO Check it
                } else {
                    if (child > 0) {
                        child++;
                    }
                    xmlObject.setValue(getTagValue(Key.KEY_TAG_VALUE, eElement));
                }

                xmlParameter.addXMLObject(xmlObject);
            }
        }
        return xmlParameter;
    }

    private static XMLParameter getXMLTags(NodeList nodeList, int counter) {
        child++;
        return getXMLTags(nodeList);
    }

    private static String getTagValue(String sTag, Element eElement) {
        NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();
        String value = nlList.item(0).getNodeValue();
        return value;
    }

    private static class Key {
        final public static String KEY_TAG_CASE = "case";
        final public static String KEY_ATTRIBUTE_NUMBER = "number";
        final public static String KEY_ATTRIBUTE_TEXT = "text";
        final public static String KEY_TAG_OBJECT = "object";
        final public static String KEY_TAG_KEY = "key";
        final public static String KEY_TAG_VALUE = "value";
    }
}
