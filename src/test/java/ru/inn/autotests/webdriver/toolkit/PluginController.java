package ru.inn.autotests.webdriver.toolkit;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import ru.inn.autotests.webdriver.common.os.OperationSystem;

import java.io.File;
import java.io.IOException;


public class PluginController {

    private final static String PROCESS_NAME_CONF = "process_name";
    private final static String SERVICE_NAME_CONF = "service_name";
    private static Logger log4j = Logger.getLogger(PluginController.class);


    /**
     * Checks that plugin is initialize.<br>
     * For do this: checks process and service, that they're created.
     */
    public boolean checkInit() {
        if (isProcessEnabled() && isServiceEnabled()) {
            return true;
        } else {
            return false;
        }
    }




    /**
     * TODO
     * Returns default path to 4game application, when it's installed
     */
    public String getPathToApp() {
        return null;
    }

    /**
     * Removes all files from the game folder, and clears register
     */
    public void tearDown(String gameFolder,OperationSystem os) {
        ProcessProvider processProvider = new ProcessProvider();
        String serviceName = YamlProvider.getParameter(SERVICE_NAME_CONF,YamlProvider.appConfigs);
        if (serviceName.indexOf(".") != -1) { //if service name has been wrote as '4game-service.exe'
            serviceName = serviceName.split("\\.")[0];
        }
       log4j.info("start stopping process");
        processProvider.stopService(serviceName);
        log4j.info("process sopped");
        processProvider.cleanRegister(os);
        log4j.info("registry has cleaned");
        clearGamesFolder(gameFolder);
        log4j.info("clear directory with games");
        processProvider.startService(serviceName);
        log4j.info("service started");
    }

    /**
     * Returns true, if current process is present into the process list.
     */
    protected boolean isProcessEnabled() {
        String processName = YamlProvider.getParameter(PROCESS_NAME_CONF,YamlProvider.appConfigs);
        ProcessProvider processProvider = new ProcessProvider();
        return processProvider.isPresent(processName);
    }

    /**
     * Returns true, if current service is present into the service list.
     */
    protected boolean isServiceEnabled() {
        String processName = YamlProvider.getParameter(SERVICE_NAME_CONF,YamlProvider.appConfigs);
        ProcessProvider processProvider = new ProcessProvider();
        return processProvider.isPresent(processName);
    }

    //TODO Check it
    private void clearGamesFolder(String gameFolderPath) {
        try {
            FileUtils.deleteDirectory(new File(gameFolderPath));
        } catch (IOException e) {

        }
    }
}
