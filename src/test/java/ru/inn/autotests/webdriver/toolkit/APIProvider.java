package ru.inn.autotests.webdriver.toolkit;

/*
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
*/

import ru.inn.autotests.webdriver.common.entity.TagCase;
import ru.inn.autotests.webdriver.common.entity.TagCase.XMLParameter;
import ru.inn.autotests.webdriver.common.entity.TagCase.XMLParameter.XMLObject;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.games.Game;

import java.io.File;
import java.util.*;

/**
 *
 * @author pavel.popov
 */
public class APIProvider {


    private static String INIT_CONSOLE_FAKE="window.Inn.BasicConsole.initFake();\n";
    private static String INIT_CONSOLE="window.Inn.BasicConsole.init();\n";
    private static String LISTEN_SENDING = "window.Inn.test_result = []; " +
            "window.Inn.test_newCallback = function(name, data, isSelfSending) { " +
            "window.Inn.test_result.push({name: name, data: data, isSelfSending:isSelfSending});" +
            "window.Inn.BasicConsole.listenSending( window.Inn.test_newCallback );";

    private static String LISTEN_GAME_STATUSES = "window.Inn.test_gameStatuses=[];" +
            "window.Inn.BasicConsole.listenReceiving( function(eventName, data) { window.Inn.test_gameStatuses.push({\"serviceId\":data.services[0].serviceId, \"status\":data.services[0].status})});" +
            "window.Inn.BasicConsole.send(\"getStatus\",{});";

    private static String GET_GAME_STATUSES = "return window.Inn.test_gameStatuses;";

    private static String RECIEVE_INSTALL_DIR = "window.Inn.BasicConsole.receive(\"setDefaultInstallDir\", {\"serviceId\":$serviceId,\"path\":\"C:/Games\"})";

    private static String RECEIVE_ERROR_720 = "window.Inn.BasicConsole.receive(\"error\",{\"code\":720})";

    private static String RECEIVE_SET_VERSION = "window.Inn.BasicConsole.receive(\"setVersions\",{\"pluginVersion\":\"2.0.0.74\",\"serviceVersion\":\"2.0.0.74\",\"trayVersion\":\"2.0.0.74\",\"need_update\":\"1\"})";

    private static String WAIT_LOADING_SCRIPTS = "return (typeof(window.Inn) !== \"undefined\" && typeof(window.Inn.BasicConsole) !== \"undefined\")";


    private static String SET_MAINTENANCE ="window.l4gModel.set(\"L4G.Maintenance/maintenance.services.%s\", true)";

    /**
     * Returns the specific element for response from API.
     *
     * @param message - the generate message for BasicConsole API
     */
    public static Object execJs(final Message message,boolean isPlugin) {
        prepareJSSending();
        String script=message.toString(isPlugin);
        if(script.contains("\n")){
            String [] scripts=script.split("\n");
            LocalDriverManager.getDriverController().executeScript(scripts[0]);
            OperationsHelper.sendPause(2);
            return  LocalDriverManager.getDriverController().executeScript(scripts[1]);
        }
        return  LocalDriverManager.getDriverController().executeScript(message.toString(isPlugin));
    }



    /**
     * Sends JS which listen requests from plugin
     */
    public static void execPrepareListenSending() {
        LocalDriverManager.getDriverController().executeScript(LISTEN_SENDING);
    }

    /**
     * Returns game statuses from API.
     */
    public static List<Map<String, String>> execGetGameStatuses() {
        LocalDriverManager.getDriverController().executeScript(LISTEN_GAME_STATUSES);
        List<Map<String, String>> statuses = (List<Map<String, String>>)
                LocalDriverManager.getDriverController().executeScript(GET_GAME_STATUSES); //re-factor it
        int numberOfGames = Integer.parseInt(YamlProvider.getParameter("gamesNumber",YamlProvider.appConfigs));
        while (statuses.size() == 0 || statuses.size() != numberOfGames) {
            statuses = (List<Map<String, String>>) LocalDriverManager.getDriverController().executeScript(GET_GAME_STATUSES);
        }
        return statuses;
    }

    /**
     * Sends JS with setInstallDir for current game
     *
     * @param game
     */
    public void execPrepareReceive(Game game,boolean isPlugin) {
        String script="";
        if(isPlugin)
            script=INIT_CONSOLE+RECIEVE_INSTALL_DIR.replace("$serviceId", game.getServiceId());
        else
            script=INIT_CONSOLE_FAKE+RECIEVE_INSTALL_DIR.replace("$serviceId", game.getServiceId());
        LocalDriverManager.getDriverController().executeScript(script);
    }

    /**
     * Sends JS with an error (720)
     */
    public void execSendError720(boolean isPlugin) {
        if(isPlugin)
        {
            LocalDriverManager.getDriverController().executeScript(INIT_CONSOLE);
            LocalDriverManager.getDriverController().executeScript(RECEIVE_ERROR_720);

        }
        else
        {
            LocalDriverManager.getDriverController().executeScript(INIT_CONSOLE_FAKE);
            LocalDriverManager.getDriverController().executeScript(RECEIVE_ERROR_720);
        }
    }

    public void execSendMaintenanceStatus(Game game, boolean isPlugin) {
        if(isPlugin)
        {
            LocalDriverManager.getDriverController().executeScript(INIT_CONSOLE);
            LocalDriverManager.getDriverController().executeScript(String.format(SET_MAINTENANCE,game.getServiceId()));

        }
        else
        {
            LocalDriverManager.getDriverController().executeScript(INIT_CONSOLE_FAKE);
            LocalDriverManager.getDriverController().executeScript(String.format(SET_MAINTENANCE,game.getServiceId()));
        }
    }

    /**
     * Sends a request with set version of 4game application
     */
    public void execSetVersion(boolean isPlugin) {
        String script="";
        if(isPlugin)
            script=INIT_CONSOLE+RECEIVE_SET_VERSION;
        else
            script=INIT_CONSOLE_FAKE+RECEIVE_SET_VERSION;
        LocalDriverManager.getDriverController().executeScript(script);
    }

    /**
     * Sends initFake method for initialization Plugin.
     */
    public void execFakeInitPlugin() {
        Message message = new Message(Method.initFake, null, null);
        APIProvider.execJs(message,false);
    }

    /**
     * Returns map with child tags
     *
     * @param xmlParameter
     */
    private static Map<String, Object> getChild(final XMLParameter xmlParameter) {
        Map<String, Object> childs = new LinkedHashMap<String, Object>();

        int sizeList = xmlParameter.size();

        for (int i = 0; i < sizeList; i++) {
            XMLObject xmlObject = xmlParameter.getXMLObject(i);
            if (xmlObject.hasChildTag()) {
                childs.put(xmlObject.getKey(), getChild(xmlObject.getXmlParameter()));
            } else {
                childs.put(xmlObject.getKey(), xmlObject.getValue());
            }
        }

        return childs;
    }

    /**
     * Waits while scripts are downloading
     */
    public void waitForScriptsToLoad(boolean isPlugin) {
        Message message = new Message(Method.isValid, null, null);

        int timeout = 0;
        while (APIProvider.execJs(message,isPlugin).toString().equals("false")) {
            if (timeout > 30) {
                throw new RuntimeException("Scripts didn't load in the block play/install! See the browser!");
            }
            pause(1);
            timeout++;
        }
        pause(1);
    }

    /**
     * xSend
     */
    public void execSendJSON(final String textFromCase, final Game game,boolean isPlugin) {
        Json json = new Json();
        Map<String, Object> services = new LinkedHashMap<String, Object>();
        List<Object> array = new LinkedList<Object>();

        String serviceId = game.getServiceId();

        List<TagCase> listXMLParameters = new LinkedList<TagCase>();
        listXMLParameters = XMLProvider.getJSObjects(new File(".").getAbsolutePath() +
                "\\src\\test\\resources\\plugin\\status.xml");

        TagCase caseParameter = null;

        for (TagCase p : listXMLParameters) {
            if (p.getCaseDefinition().equals(textFromCase)) {
                caseParameter = p;
            }
        }

        if (caseParameter == null) {
            throw (new IllegalArgumentException("Don't find " + textFromCase));
        }

        int sizeList = caseParameter.getXmlParameters().size();

        for (int i = 0; i < sizeList; i++) {
            XMLObject xmlObject = caseParameter.getXmlParameters().getXMLObject(i);
            if (xmlObject.hasChildTag()) {
                services.put(xmlObject.getKey(), getChild(xmlObject.getXmlParameter()));
            } else {
                services.put(xmlObject.getKey(), xmlObject.getValue());
            }
        }

        Iterator<String> iterator = services.keySet().iterator();
        while (iterator.hasNext()) {
            String str = iterator.next();
            if (services.get(str).toString().equals("$serviceId")) {
                services.put(str, serviceId);
            }
        }

        array.add(services);

        json.put("services", array);

        Message demoMessage = new Message(Method.receive, Command.status, json);
        APIProvider.execJs(demoMessage,isPlugin);
    }

    private static void prepareJSSending() {
        int timeout = 0;
        while (LocalDriverManager.getDriverController().executeScript(WAIT_LOADING_SCRIPTS).toString().equals("false")) {
            if (timeout > WebDriverController.TIMEOUT) {
                new RuntimeException("window.Inn isn't loading! See console in the browser.");
            }
            pause(1);
        }
    }

    private static void pause(int sec) {
        try {
            Thread.sleep(sec * 1000);
        } catch (InterruptedException e) {
            Thread.interrupted();
        }
    }

    /**
     * JSON Wrapper
     *
     * @author pavel.popov
     */
    public static class Json {

        // private static JSONObject json = new JSONObject();

        /**
         * Default constructor
         */
        public Json() {
        }

        /**
         * Puts a key/value in the JSON. If the value is null, then the key will be removed from the JSON if it is present.
         *
         * @param key   A key string.
         * @param value An object which is the value. It should be of one of these types:
         *              Boolean, Double, Integer, JSONArray, JSONObject, Long, String, or the JSONObject.NULL object.
         */
        public void put(final String key, final Object value) {
           /* if (key == null || key.isEmpty()) {
                throw (new IllegalArgumentException());
            }

            try {
                json.put(key, value);
            } catch (JSONException e) {
                new RuntimeException("JSONObject:Can't put key=" + key + " and value=" + value + ". Cause: " + e.getCause());
            }*/
        }

        /**
         * Makes a JSON text
         */
        /*
        @Override
        public String toString() {
        //   return json.toString();
        }
*/

        /**
         * Works with arrays of JSONs
         *
         * @author pavel.popov
         */
        public static class JsonArray {
         /*   private JSONArray jsonArr = new JSONArray();

            /**
             * Adds json to array of JSONs
             * @param value
             *
            public void addObject(final Map<String, Object> value) {
                jsonArr.put(value);
            }*/
        }

    }


    /**
     * Handler for sending to plugin
     *
     * @author pavel.popov
     */
    public static class CallbackElement {
        private String name;
        private Object data;
        private boolean isSelfSending;

        /**
         *
         * @param name
         * @param data
         * @param isSelfSending
         */
        protected CallbackElement(String name, Object data, boolean isSelfSending) {
            this.name = name;
            this.data = data;
            this.isSelfSending = isSelfSending;
        }

        /**
         * Returns the name of callback element
         */
        public String getName() {
            return name;
        }

        /**
         * Returns the data of callback element
         * @return
         */
        public Object getData() {
            return data;
        }

        /**
         * Returns true/false (isSelfSending)
         */
        public boolean getIsSelfSending() {
            return isSelfSending;
        }

        public static class Status {
            final public static String CANCEL = "cancel";
            final public static String UPDATE = "update";
        }
    }

    /**
     * @author pavel.popov
     */
    public static class Message {
        final String basicConsole = "window.Inn.BasicConsole";
        private Method methodName;
        private Command command;
        private Json json;


        /**
         * Creates Message in specified format. Automatically added path to BasicConsole API.
         *
         * @param methodName
         * @param json
         */
        public Message(final Method methodName, final Command command, final Json json) {
            if (methodName == null) {
                throw (new IllegalArgumentException());
            }
            this.methodName = methodName;
            this.json = json;
            this.command = command;
        }

        /**
         * Returns ready message for send to BasicConsole API.
         */

        public String toString(boolean isPlugin) {
            if(isPlugin)
            { String apiCommand =INIT_CONSOLE;
                if (command == null && json == null) {
                    apiCommand += "return " + basicConsole + "." +
                            methodName.toString() + "(" + ")";
                } else {
                    apiCommand += basicConsole + "." +
                            methodName.toString() + "(" + "\"" + command.toString() + "\"" + "," +
                            json.toString() + ")";
                }
                return apiCommand;
            } else{
                String apiCommand =INIT_CONSOLE_FAKE;
                if (command == null && json == null) {
                    apiCommand += "return " + basicConsole + "." +
                            methodName.toString() + "(" + ")";
                } else {
                    apiCommand += basicConsole + "." +
                            methodName.toString() + "(" + "\"" + command.toString() + "\"" + "," +
                            json.toString() + ")";
                }
                return apiCommand;
            }
        }
    }

    /**
     * Enumeration of methods into BasicConsole API.
     *
     * @author pavel.popov
     */
    public enum Method {
        send, receive, initFake, listenSending, isValid,
    }

    /**
     * Enumeration of commands in BasicConsole API.
     *
     * @author pavel.popov
     */
    public enum Command {
        install, update, status,
    }




}
