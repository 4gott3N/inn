package ru.inn.autotests.webdriver.toolkit;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.testng.Assert;
import ru.inn.autotests.webdriver.composite.OperationsHelper;
import ru.inn.autotests.webdriver.games.Game;

import java.util.ArrayList;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class GameData extends OperationsHelper{
    private static Matcher MATCHER;
    private static Pattern PATTERN;
    
    public static String getServiceAcc(Game game) {
        String serviceAcc="";String gameLang="";
        String gameName=Game.GameName.getGameName(game.getGameName());
        String gameData = LocalDriverManager.getDriverController().findElement(By.tagName("body")).getAttribute("data-game-data");

        if(baseUrl.contains("eu"))
        {
        	//log.info(gameName);
        	//log.info(gameData);
            gameLang=gameName.split("Lineage 2 ")[1];
            gameName=gameName.split(" "+gameLang)[0];
            String partOfGameData = gameData.substring(gameData.indexOf(gameLang));
            partOfGameData = partOfGameData.substring(0, partOfGameData.indexOf("name"));
            PATTERN = Pattern.compile(gameLang + ".+?service_account_id.+?(\\d+)");
            MATCHER = PATTERN.matcher(partOfGameData);
            MATCHER.find();
        }
        else{
        	String partOfGameData = gameData.substring(gameData.indexOf(gameName));
        	partOfGameData = partOfGameData.substring(0, partOfGameData.indexOf("name"));
	        PATTERN = Pattern.compile(gameName+".+?service_account_id.+?(\\d+)");
	        MATCHER = PATTERN.matcher(partOfGameData);
	        MATCHER.find();
        }
        try{ serviceAcc = MATCHER.group(1);
        }catch (IllegalStateException e){
            Assert.fail("Cant find service account for game " + game.getGameName());
        }
        return serviceAcc;
    }

    public static String getErrorCode(Game game) {
        String errorCode = "";String gameLang="";  String gameData="";
        try {
        String gameName=Game.GameName.getGameName(game.getGameName());
        gameData = LocalDriverManager.getDriverController().findElement(By.tagName("body")).getAttribute("data-game-data");
        String partOfGameData = "";
        if(baseUrl.contains("eu"))
        {
            gameLang=gameName.split("Lineage 2 ")[1];
            gameName=gameName.split(" "+gameLang)[0];
        }
        partOfGameData = gameData.substring(gameData.indexOf(gameName));
        partOfGameData = partOfGameData.substring(0, partOfGameData.indexOf("autopay_enabled"));
        log.info(partOfGameData);
        PATTERN = Pattern.compile(gameName+".+"+gameLang + ".+?errors.+?(\\d+)");
        MATCHER = PATTERN.matcher(partOfGameData);
        MATCHER.find();
            errorCode = MATCHER.group(1);
        } catch (IllegalStateException e) {
            //Assert.fail("There is no any errors in game data for game " + game.getGameName());
        }
        catch (StringIndexOutOfBoundsException e)
        {
            log.info(gameData);
            Assert.fail("Incorrect game data string has come");
        }
        log.info(errorCode);
        return errorCode;
    }
    
    public static String getServiceInfo(String gameId) {
        String gameData="";
        String partOfGameData = "";
        try {
        gameData = LocalDriverManager.getDriverController().findElement(By.tagName("body")).getAttribute("data-game-data");
        partOfGameData = gameData.substring(gameData.indexOf(gameId + "\":"));
        partOfGameData = partOfGameData.substring(0, partOfGameData.indexOf("autopay_enabled"));
        }
        catch (StringIndexOutOfBoundsException e)
        {
            log.info("there is no game id in the gameData " + gameId);
        }

        return partOfGameData;
    }
    
    public static String getStatusService(String gameId) {
        String statusService=getServiceInfo(gameId);
		if(!statusService.equals("")){
        	//statusService 
			int start = statusService.indexOf("status\":\"") + 9;
        	int finish = statusService.indexOf("\",\"ext_status");
        	statusService = statusService.substring(start, finish);
		}
		return statusService;
    }
    
    public static boolean isLicenceAccepted() {
    	String unacceptedLicence = LocalDriverManager.getDriverController().findElement(By.tagName("body")).getAttribute("data-unaccepted-licences");
		if(unacceptedLicence.isEmpty() || unacceptedLicence==null)
            throw new RuntimeException("There is no field data-unaccepted-licences in the GameData");
        return false;//unacceptedLicence.equals("[]");
    }

 /*   public static boolean isLicenceAccepted()  {
    	Document document;
    	String accountData = "";
		try {
			document = Jsoup.connect(baseUrl + "/launcher/accountData/").get();
		
    	accountData = document.getElementsByTag("pre").text();
    	//String unacceptedLicence = LocalDriverManager.getDriverController().findElement(By.tagName("body")).getAttribute("data-unaccepted-licences");
		if(accountData.isEmpty() || accountData==null)
            throw new RuntimeException("There is no field data-unaccepted-licences in the accountData");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return accountData.contains("[]");
    }*/
    
    public static ArrayList<String> getInstalledGames(){
       ArrayList<String> installedGames=new ArrayList<String>();
        Set<Cookie> cookies=LocalDriverManager.getDriverController().getCookies();
        for(Cookie c : cookies)
        if(c.getName().contains("inst_"))
           installedGames.add(c.getName().replace("inst_",""));
         return installedGames;
    }


}
