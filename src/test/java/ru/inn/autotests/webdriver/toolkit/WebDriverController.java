package ru.inn.autotests.webdriver.toolkit;

import net.lightbody.bmp.core.har.Har;
import net.lightbody.bmp.proxy.ProxyServer;

import org.eclipse.jetty.util.log.Log;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import ru.inn.autotests.webdriver.composite.OperationsHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Wrapper of WebDriver
 *
 * @author pavel.popov
 */
public class WebDriverController {

    private WebDriver driver;
    private static WebDriverWait waitDriver;
    public static final int PAGE_TIMEOUT = 80;
    public static final int TIMEOUT = 30;
    private static final int SCRIPT_TIMEOUT = 30;
    public static final Integer proxyPort = Integer.valueOf(
            YamlProvider.getParameter("proxyPort", YamlProvider.appConfigs));
    private final Cookie draftWSProvider = new Cookie("app-backend-first-id", "ws");
    private final String property4gameWS = "4game_ws";
    public static ProxyServer server = new ProxyServer(proxyPort);
    public static Proxy proxy = null;
    static boolean needProxy = Boolean.parseBoolean(YamlProvider.getParameter("enableProxy", YamlProvider.appConfigs));

    static {
        if (System.getenv("enableProxy") != null)
            needProxy = Boolean.parseBoolean(System.getenv("enableProxy"));
        if (needProxy) {
            try {
                server.start();
                server.setRequestTimeout(PAGE_TIMEOUT * 1000);
                proxy = server.seleniumProxy();
                server.blacklistRequests("vk.com", 200);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public WebDriverController() {
        setBrowser();
        driver.manage().timeouts().setScriptTimeout(SCRIPT_TIMEOUT, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(PAGE_TIMEOUT, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        if (System.getenv(property4gameWS) != null) {
            driver.manage().addCookie(draftWSProvider);
            System.out.println("Works through websockets");
        }
        driver.switchTo();
        Runtime.getRuntime().addShutdownHook(new ShutdownHook());
    }

    public class ShutdownHook extends Thread {
        public void run() {
            shutdown();
        }
    }

    public static void stopProxy() {
        if (needProxy) {
            try {
                server.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setBrowser() {
        String browser = System.getenv("browser");
        if (browser == null) {
            browser = YamlProvider.getParameter("browser", YamlProvider.appConfigs);
        }
        String flagFirebug = System.getenv("flagFirebug");
        if ("firefoxProf".equals(browser)) {
            try {
                //set profile
                System.setProperty("webdriver.firefox.bin", "C:\\Program Files\\ff16\\firefox.exe");
                String profileDir = YamlProvider.getParameter("profile_dir", YamlProvider.appConfigs);
                FirefoxProfile firefoxProfile = new FirefoxProfile(new File(profileDir));
                driver = new FirefoxDriver(firefoxProfile);
            } catch (Exception e) {
                driver = new FirefoxDriver();
            }
        } else if ("firefox".equals(browser)) {
            FirefoxProfile firefoxProfile = new FirefoxProfile();
            try {
                String versionFirebug = YamlProvider.getParameter("firebug-version", YamlProvider.appConfigs);
                if (YamlProvider.getParameter("firebug", YamlProvider.appConfigs).equals("true")) {
                    firefoxProfile.addExtension(
                            new File("src\\test\\resources\\extensions\\firebug-" + versionFirebug + ".xpi"));
                    firefoxProfile.setPreference("extensions.firebug.currentVersion",
                            versionFirebug); // Avoid startup screen
                }

                if (needProxy) {
                    firefoxProfile.setPreference("network.proxy.http", "localhost");
                    firefoxProfile.setPreference("network.proxy.http_port", proxyPort);
                    firefoxProfile.setPreference("network.proxy.ssl", "localhost");
                    firefoxProfile.setPreference("network.proxy.ssl_port", proxyPort);
                    firefoxProfile.setPreference("network.proxy.type", 1);
                    firefoxProfile.setPreference("network.proxy.no_proxies_on", "");
                    server.newHar(OperationsHelper.baseUrl);
                }
                //firefoxProfile.setPreference("capability.policy.trustable.sites", "https://ru.4gametest.com https://ru.4game.com");
                firefoxProfile.setPreference("plugins.click_to_play", true);
                firefoxProfile.setPreference("plugin.state.npplugin4game", 2);
                firefoxProfile.setPreference("plugin.state.flash", 0);
                firefoxProfile.setPreference("browser.download.folderList", 2);
                firefoxProfile.setPreference("browser.download.manager.showWhenStarting", false);
                firefoxProfile.setPreference("browser.download.dir",
                        YamlProvider.getParameter("path_to_download_app", YamlProvider.appConfigs));
                firefoxProfile.setPreference("intl.accept_languages", "ru");
                firefoxProfile.setPreference("general.useragent.local", "ru");
                firefoxProfile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/octet-stream");
                driver = new FirefoxDriver(firefoxProfile);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ("ie".equals(browser)) {
            DesiredCapabilities capabilitiesIe = DesiredCapabilities.internetExplorer();
            try {
                capabilitiesIe.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
                driver = new InternetExplorerDriver(capabilitiesIe);
            } catch (Exception e) {
                driver = new InternetExplorerDriver(capabilitiesIe);
            }
        } else if ("chrome".equals(browser)) {
            try {
                System.setProperty("webdriver.chrome.driver", "lib\\chromedriver.exe");
                ChromeOptions options = new ChromeOptions();
                String browserLanguage = "--lang=ru";
                options.addArguments(browserLanguage);
                driver = new ChromeDriver(options);
            } catch (Exception e) {
            }
        } else if ("opera".equals(browser)) {
            try {
//				driver = new OperaDriver();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            driver = new HtmlUnitDriver();
        }

    }


    public static void saveHarToDisk() {
        if (needProxy) {
            Har har = server.getHar();
            try {
                File file = new File(YamlProvider.getParameter("pathToTheHars", YamlProvider.appConfigs));
                if (!file.exists()) {
                    file.createNewFile();
                }
                har.writeTo(new FileOutputStream(file));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void waitForPageLoaded() {
        ExpectedCondition<Boolean> expectation = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return executeScript("return document.readyState").toString().equals("complete");
                    }
                };

        try {
            OperationsHelper.sendPause(1);
            getInstanceWaitDriver().until(expectation);
        } catch (Throwable error) {
            Assert.fail("Timeout waiting for Page Load Request to complete.");
        }
    }

    /**
     * Returns instance for current class
     */


    public void maximizeWindow() {
        driver.manage().window().maximize();
    }

    public static WebDriverWait getInstanceWaitDriver() {
//		synchronized(WebDriverController.class){
        if (waitDriver == null) {
            waitDriver = new WebDriverWait(LocalDriverManager.getDriverController().getDriver(), SCRIPT_TIMEOUT);
        }
        return waitDriver;
//		}
    }

    public String getPageAdress() {
        return driver.getCurrentUrl();
    }

    public void goToUrl(String url) {
        driver.get(url);
        waitForPageLoaded();
    }

    /**
     * Returns WebDriver
     */
    public WebDriver getDriver() {
        return driver;
    }

    public org.openqa.selenium.Cookie getCookie(String key) {
        org.openqa.selenium.Cookie cookie = driver.manage().getCookieNamed(key);
        return cookie;
    }

    public Set<org.openqa.selenium.Cookie> getCookies() {
        Set<org.openqa.selenium.Cookie> cookies = driver.manage().getCookies();
        return cookies;
    }

    /**
     * Sends into a browser
     */
    public void navigationBack() {
        driver.navigate().back();
        waitForPageLoaded();
    }

    //Delete all cookies
    public void deleteAllCookies() {
    	System.out.println("Deleting all cookies");
        driver.manage().deleteAllCookies();
    }

    public String getWindowHandle() {
        return driver.getWindowHandle();
    }

    public Set<String> getWindowHandles() {
        return driver.getWindowHandles();
    }

    // Set a cookie
    public void addCookie(String key, String value) {
        Cookie cookie = new Cookie(key, value);
        driver.manage().addCookie(cookie);
    }

    public WebElement findElement(final By by) {
        return driver.findElement(by);
    }

    public List<WebElement> findElements(final By by) {
        return driver.findElements(by);
    }


    public void get(final String url) {
        if (url.isEmpty() || url == null) {
            throw (new IllegalArgumentException());
        }
        driver.get(url);
        waitForPageLoaded();
    }


    public boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        } catch (StaleElementReferenceException se) {
            return false;
        }
    }

    public boolean isVisible(By by) {
        try {
            if (driver.findElements(by).size() > 0)
                if (driver.findElement(by).isDisplayed())
                    return true;
                else
                    return false;
        } catch (NoSuchElementException e) {
            return false;
        } catch (StaleElementReferenceException se) {
            return false;
        }
        return false;
    }

    public boolean isEnable(By by) {
        try {
            if (driver.findElements(by).size() > 0)
                if (driver.findElement(by).isEnabled())
                    return true;
                else
                    return false;
        } catch (NoSuchElementException e) {
            return false;
        } catch (StaleElementReferenceException se) {
            return false;
        }
        return false;
    }
    
    public boolean isChecked(By by) {
        try {
            if (driver.findElements(by).size() > 0)
                if (driver.findElement(by).isSelected())
                    return true;
                else
                    return false;
        } catch (NoSuchElementException e) {
            return false;
        } catch (StaleElementReferenceException se) {
            return false;
        }
        return false;
	}

    public void shutdown() {
        try {
            ProcessProvider processProvider = new ProcessProvider();
	        processProvider.killTask("plugin-container.exe");
            driver.quit();
            driver = null;
        } catch (Exception e) {
        }
    }

    /**
     * Executes JavaScript in the context of the currently selected frame or window. (See also {@link JavascriptExecutor})
     */
    public Object executeScript(String script, Object... args) {
        if (driver == null)
            throw new RuntimeException("Driver is null in method executeScript");
        return ((JavascriptExecutor) driver).executeScript(script, args);
    }

    public Object executeAsyncScript(String script, Object... args) {
        return ((JavascriptExecutor) driver).executeAsyncScript(script, args);
    }

    // Change the cookie
    public void changeCookie(String key, String value) {
        Cookie cookie = new Cookie(key, value);
        driver.manage().deleteCookieNamed(key);
        driver.manage().addCookie(cookie);
    }

    public void downloadFile(String path) {

    }

    /**
     * Returns attribute value
     *
     * @param by        -
     * @param attribute -
     */
    public String getAttributeValue(By by, String attribute) {
        return driver.findElement(by).getAttribute(attribute);
    }


    public static void pressEnter() {
        Actions action = new Actions(LocalDriverManager.getDriverController().getDriver());
        action.sendKeys(Keys.ENTER).perform();
    }

    /**
     * Refreshes current page
     */
    public void refresh() {
        driver.navigate().refresh();
        waitForPageLoaded();
    }

    /**
     * Switches to iFrame, if it needed
     *
     * @param iFrame - the name or id of iFrame
     */
    public void switchTo(String iFrame) {
        driver.switchTo().frame(iFrame);
    }

    public void switchToWindow(String iFrame) {
        driver.switchTo().window(iFrame);
    }

    public void closeWindow() {
        driver.close();
    }

    /**
     * Returns link to main content on the page
     */
    public void switchToMainContent() {
        driver.switchTo().defaultContent();
    }

    public class Cookie extends org.openqa.selenium.Cookie {

        public Cookie(String name, String value, String path, Date expiry) {
            super(name, value, path, expiry);
        }

        public Cookie(String name, String value, String domain, String path, Date expiry) {
            super(name, value, domain, path, expiry);
        }

        public Cookie(String name, String value, String domain, String path, Date expiry, boolean isSecure) {
            super(name, value, domain, path, expiry, isSecure);
        }

        public Cookie(String name, String value) {
            super(name, value);
        }

        public Cookie(String name, String value, String path) {
            super(name, value, path);
        }
    }

	


}
