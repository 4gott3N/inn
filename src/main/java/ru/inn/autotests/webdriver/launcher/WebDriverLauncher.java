package ru.inn.autotests.webdriver.launcher;

import java.nio.file.Path;
import java.nio.file.Paths;

import com.sun.jna.WString;
import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.Kernel32Util;
import com.sun.jna.platform.win32.WinBase.PROCESS_INFORMATION;
import com.sun.jna.platform.win32.WinBase.STARTUPINFO;
/**
 * @author Aleksey Niss
 */
public class WebDriverLauncher {
	
	private static String winLogin = "Administrator";
	private static String winPassword = "TestinG_123";
	private static String domain = "inn.local";
	private static String commandLineArgs = "clean test -DsuiteFile=src/test/resources/suite/test.xml";
	private static String cmdLoc = "c:\\windows\\system32\\cmd.exe";
	
	
	public static void main(String... args) {
		String currentProjectPath = getCurrentPath();
		if (args.length >0) {
			commandLineArgs = args[0];
		}
		
		WString nullW = null; //null if needed
		PROCESS_INFORMATION processInformation = new PROCESS_INFORMATION();
		STARTUPINFO startupInfo = new STARTUPINFO();
		boolean result = MoreAdvApi32.INSTANCE.CreateProcessWithLogonW(
				new WString(winLogin), //user
				new WString(domain), // domain, null if local
				new WString(winPassword), //password
				MoreAdvApi32.LOGON_WITH_PROFILE, //dwLogonFlags
				new WString(cmdLoc), //lpApplicationName
				new WString("/K " + "\"\"mvn\" \"" + commandLineArgs + "\"\""), //command line
				MoreAdvApi32.CREATE_NEW_CONSOLE, //dwCreationFlags
				null, //lpEnvironment
				new WString(currentProjectPath), //directory
				startupInfo, processInformation);

		if (!result) {
			int error = Kernel32.INSTANCE.GetLastError();
			System.out.println("OS error #" + error);
			System.out.println(Kernel32Util.formatMessageFromLastErrorCode(error));
		}
	}
	
	private static String getCurrentPath() {
		Path currentRelativePath = Paths.get("");
		String result = currentRelativePath.toAbsolutePath().toString();
		System.out.println("Current relative path is: " + result);
		return result;
	}
}